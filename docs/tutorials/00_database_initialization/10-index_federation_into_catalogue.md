# How do I index my federation verifiable credentials into catalogue?

> __Note:__  
> In the following tutorial, I suppose that credential-generator has been installed by cloning our repository. If you prefer to use docker container, you
> must adjust command.


## sync command - Usage

```bash
poetry run credential-generator federation sync --help

 Usage: credential-generator federation sync [OPTIONS]  

 The sync command is used to index generated VCs into federated catalogue.  

╭─ Options ──────────────────────────────────────────────────────────────────────────────────────────────────────╮
│ *  --name                          -n      TEXT     Federation's name.                                         │
│                                                     [default: None]                                            │
│                                                     [required]                                                 │
│    --output-root-directory         -o      TEXT     Directory where generated files can be found               │
│                                                     [default: /usr/local/lib/python3.11/site-packages/public]  │
│    --parent-domain                 -p      TEXT     Parent domain name to generate proper did web              │
│                                                     [default: dev.gaiax.ovh]                                   │
│    --port-number                           INTEGER  Port number.                                               │
│                                                     [default: 443]                                             │
│ *  --api-key                       -k      TEXT     Catalogue api key                                          │
│                                                     [default: None]                                            │
│                                                     [required]                                                 │
│    --help                                           Show this message and exit.                                │
╰────────────────────────────────────────────────────────────────────────────────────────────────────────────────╯

```

## index federation json-ld files into catalogue
Once the federation user agent has generated and stored the verifiable credentials from the generated json-ld files, you can index them in the federation's internal catalogue.

> __NOTE__:
> The federation's internal catalogue allows VCs to be indexed but does not make them available to the federated catalogue. Synchronisation must therefore be activated to find them in the federated catalogue.


example:
```bash
poetry run credential-generator federation sync \
--name aster-x \
--output-root-directory $(PWD)/public \
--parent-domain example.com \
--api-key xxx
```

---
© 2023 gxfs-fr. All rights reserved.
