# How do I bootstrap a provider?

> __Note:__  
> In the following tutorial, I suppose that credential-generator has been installed by cloning our repository. If you prefer to use docker container, you
> must adjust command.


## bootstrap command - Usage

```bash
poetry run credential-generator provider bootstrap --help

 Usage: credential-generator provider bootstrap [OPTIONS]  

 The bootstrap command reads a CSV File from provider_name directory and generates compliance-criterion  
 json-ld files and compliance-labels json-ld files in public directory.  

╭─ Options ────────────────────────────────────────────────────────────────────────────────────────────────────╮
│ *  --name                          -n      TEXT  Provider's name.                                             │
│                                                  [default: None]                                             │
│                                                  [required]                                                  │
│    --input-root-directory          -i      TEXT  Directory where csv files can be found                      │
│                                                  [default:                                                   │
│                                                  /usr/local/lib/python3.11/site-packages/public]             │
│    --output-root-directory         -o      TEXT  Directory where generated files can be found                │
│                                                  [default:                                                   │
│                                                  /usr/local/lib/python3.11/site-packages/public]             │
│    --parent-domain                 -p      TEXT  Parent domain name to generate proper did web               │
│                                                  [default: provider.dev.gaiax.ovh]                           │
|    --federation-parent-domain      -f      TEXT  Parent domain name of federation to generate proper did web |
|                                                  [default: dev.gaiax.ovh]                                    |
│    --database                      -d      TEXT  Name of database to generate                                │
│                                                  [default:                                                   │
│    --enable-gaiax-compliance                     Enable call to Gaia-X compliance                            │
│    --help                                        Show this message and exit.                                 │
╰──────────────────────────────────────────────────────────────────────────────────────────────────────────────╯
```

## bootstrapping provider files
When your database is initialized, you can now bootstrap json-ld files for your provider.

example:
```bash
poetry run credential-generator provider bootstrap \
--name ovhcloud \
--output-root-directory $(PWD)/public \
--parent-domain example.com \
--federation-parent-domain demo23.gxfs.fr \
--database aster-x.sqlite3
```

If you want to call gaia-x compliance to get Gaia-x LegalRegistrationNumber and Gaia-x Terms and Conditions Verifiable Credentials, you can
add the following flag to your bootstrap command:

```bash
poetry run credential-generator provider bootstrap \
--name ovhcloud \
--output-root-directory $(PWD)/public \
--parent-domain example.com \
--federation-parent-domain demo23.gxfs.fr \
--database aster-x.sqlite3 \
--enable-gaiax-compliance
```

Once generated, credential-generator creates a hierarchy of directories on your local system.
Your root directory for your provider follows this pattern `<auditor name>.<parent domain>`.
For example, the command above creates `ovhcloud.example.com` in your `$(PWD)/public directory`

In this directory, credential-generator stores generated json-ld files into a sub-directory corresponding to its type.

## Following step

You can now proceed to upload json-ld files into provider's user-agent : [Uploading provider json-ld files](./08-upload_provider_files.md)

---
© 2023 gxfs-fr. All rights reserved.
