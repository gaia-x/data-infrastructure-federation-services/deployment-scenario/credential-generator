
# How do I import my files into local database?

Once you have all CSV files necessary to describe your services, you can import them into a local database. This database will be used as a source of truth to generate Gaia-x compliant verifiable credentials.

> __Note:__  
> In the following tutorial, I suppose that credential-generator has been installed by cloning our repository. If you prefer to use docker container, you
> must adjust command.

## init command - Usage

```bash
poetry run credential-generator init --help

 Usage: credential-generator init [OPTIONS]  

 The init command creates a new sqlite database in your local file system and loads data from CSV files located in data-compliance directory.  

╭─ Options ─────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────╮
│ *  --name                      -n      TEXT  Federation's name.                                                                                                                                                                                                                           │
│                                              [default: None]                                                                                                                                                                                                                              │
│                                              [required]                                                                                                                                                                                                                                   │
│    --input-root-directory  -i          TEXT  Directory where csv files can be found                                                                                                                                                                                                       │
│                                              [default: /Users/daviddrugeon-hamon/Documents/code/src/gitlab.com/gaia-x/data-infrastructure-federatin-services/deployment-scenario/credential-generator/data-compliance]                                                                    │
│    --database                  -d      TEXT  Name of database to generate                                                                                                                                                                                                                 │
│                                              [default: /Users/daviddrugeon-hamon/Documents/code/src/gitlab.com/gaia-x/data-infrastructure-federatin-services/deployment-scenario/credential-generator/compliance.sqlite3]                                                                 │
│    --help                                    Show this message and exit.                                                                                                                                                                                                                  │
╰───────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────╯

```

## Initialize database  
When initializing database, you must specify:
- the name of your federation
- the directory where your CSV files are located
- the name of sqlite database

example:
```bash
poetry run credential-generator init \
--name aster-x \
--input-root-directory data-compliance \
--database aster-x.sqlite3
```

Once imported, database is located into your filesystem.

## Following step

You can now proceed to bootstrap json-ld files for your federation : [Bootstrapping a federation](./03-bootstrap_federation.md)

---
© 2023 gxfs-fr. All rights reserved.
