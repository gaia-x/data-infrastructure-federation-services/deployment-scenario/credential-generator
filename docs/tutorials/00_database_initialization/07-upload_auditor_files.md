# How do I bootstrap an auditor?

> __Note:__  
> In the following tutorial, I suppose that credential-generator has been installed by cloning our repository. If you prefer to use docker container, you
> must adjust command.


## upload command - Usage

```bash
poetry run credential-generator auditor upload --help

 Usage: credential-generator auditor upload [OPTIONS]  

 The upload command is used to upload generated VCs into auditor user agent catalogue.participant agent is not reachable, an error message is displayed.  

╭─ Options ───────────────────────────────────────────────────────────────────────────────────────────────────────╮
│ *  --name                          -n      TEXT     auditor's name.                                          │
│                                                     [default: None]                                             │
│                                                     [required]                                                  │
│    --output-root-directory         -o      TEXT     Directory where generated files can be found                │
│                                                     [default: /usr/local/lib/python3.11/site-packages/public]   │
│    --parent-domain                 -p      TEXT     Parent domain name to generate proper did web               │
│                                                     [default: auditor.dev.gaiax.ovh]                                    │
│    --port-number                           INTEGER  Port number.                                                │
│                                                     [default: 443]                                              │
│    --enable-gaiax-compliance                        Enable call to Gaia-X compliance                            │
│ *  --api-key                       -k      TEXT     User agent api key                                          │
│                                                     [default: None]                                             │
│                                                     [required]                                                  │
│    --workers                       -w      INTEGER  number of workers                                           │
│                                                     [default: 5]                                                │
│    --help                                           Show this message and exit.                                 │
╰─────────────────────────────────────────────────────────────────────────────────────────────────────────────────╯
```

## uploading auditor json-ld files
When your json-ld files for your auditor are bootstrapped, you can now upload them into auditor user-agent to generate verifiable credentials and submit them to gaia-x
decentralized clearing house to pass compliance.

> __NOTE__:
> Currently, user-agent checks an api key to protect its api. If you omit it in cli, credential-generator prompts to get it.


example:
```bash
poetry run credential-generator auditor upload \
--name aster-x \
--output-root-directory $(PWD)/public \
--parent-domain example.com \
--api-key xxx
```

If you want to call gaia-x compliance, you can add the following flag to your upload command:

```bash
poetry run credential-generator auditor upload \
--name aster-x \
--output-root-directory $(PWD)/public \
--parent-domain example.com \
--api-key xxx \
--enable-gaiax-compliance
```


> __NOTE__:
> Once uploaded, credential-generator will generate a new file called `index.json` that helps to get generated VC from a json-ld file. This file is located into sub-directory `vc`

---
© 2023 gxfs-fr. All rights reserved.
