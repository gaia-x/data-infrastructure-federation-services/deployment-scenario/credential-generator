# How do I bootstrap my federation?

> __Note:__  
> In the following tutorial, I suppose that credential-generator has been installed by cloning our repository. If you prefer to use docker container, you
> must adjust command.


## bootstrap command - Usage

```bash
poetry run credential-generator federation bootstrap --help

 Usage: credential-generator federation bootstrap [OPTIONS]  

 The bootstrap command reads a CSV File from provider_name directory and generates compliance-criterion  
 json-ld files and compliance-labels json-ld files in public directory.  

╭─ Options ────────────────────────────────────────────────────────────────────────────────────────────────╮
│ *  --name                          -n      TEXT  Federation's name.                                      │
│                                                  [default: None]                                         │
│                                                  [required]                                              │
│    --input-root-directory          -i      TEXT  Directory where csv files can be found                  │
│                                                  [default:                                               │
│                                                  /usr/local/lib/python3.11/site-packages/data-complianc… │
│    --output-root-directory  -o     TEXT  Directory where generated files can be found            │
│                                                  [default:                                               │
│                                                  /usr/local/lib/python3.11/site-packages/public]         │
│    --parent-domain                 -p      TEXT  Parent domain name to generate proper did web           │
│                                                  [default: dev.gaiax.ovh]                                │
│    --database                      -d      TEXT  Name of database to generate                            │
│                                                  [default:                                               │
│                                                   /usr/local/lib/python3.11/site-packages/compliance.sql…│
│    --enable-gaiax-compliance                     Enable call to Gaia-X compliance                        │
│    --help                                        Show this message and exit.                             │
╰──────────────────────────────────────────────────────────────────────────────────────────────────────────╯
```

## bootstrapping federation files
When your database is initialized, you can now bootstrap json-ld files for your federation.

example:
```bash
poetry run credential-generator federation bootstrap \
--name aster-x \
--output-root-directory $(PWD)/public \
--parent-domain example.com \
--database aster-x.sqlite3
```

If you want to call gaia-x compliance to get Gaia-x LegalRegistrationNumber and Gaia-x Terms and Conditions Verifiable Credentials, you can
add the following flag to your bootstrap command:

```bash
poetry run credential-generator federation bootstrap \
--name aster-x \
--output-root-directory $(PWD)/public \
--parent-domain example.com \
--database aster-x.sqlite3 \
--enable-gaiax-compliance
```

Once generated, credential-generator creates a hierarchy of directories on your local system.
Your root directory for your federation follows this pattern `<federation name>.<parent domain>`.
For example, the command above creates `aster-x.example.com` in your `$(PWD)/public directory`

In this directory, credential-generator stores generated json-ld files into a sub-directory corresponding to its type.

> __NOTE__:
> credential-generator stores Gaia-x Verifiable Credentials into sub-directory `vc/legal-participant/`

## Following step

You can now proceed to upload json-ld files into federation's user-agent : [Uploading federation json-ld files](./06-upload_federation_files.md)

---
© 2023 gxfs-fr. All rights reserved.
