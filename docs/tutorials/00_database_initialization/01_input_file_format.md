# How do I describe my participant and eventually my services?

As a gaia-x participant, you need to provide some information about your company (and eventually your service offerings if you are a service provider).
Before generating gaia-x compliant verifiable credentials, the easiest way is to describe these information in a set of CSV files.

## Input directory structure
To facilitate the integration of several types of participant, it is necessary to structure the input directory where the CSV files will be located.

> __Note:__  
> The directory where the files are located must be in the current directory. Its name doesn't matter as you can specify it on the command line using the `--input-root-directory` option.

At the root of this directory must be a directory for each participant (whatever the type). The name of the directory then corresponds to the name of the participant (whatever the case).
For example, if I want to describe the `ovhcloud` participant, I create an `ovhcloud` or `OVHCLOUD` directory in the entry directory.

Here is a complete example. The name of my directory where the various files are located is called `data-compliance`.
I'm describing various participants:
- the aenor auditor,
- the aster-x federation
- the ovhcloud provider.

Here is the expected structure:
```
data-compliance
├── AENOR
├── aster-x
└── OVHCLOUD
```

## Gaia-x participant file

At the root of the participant directory, you can describe the participant's legal information in the CSV file named `1-Participant.csv`.

> __WARNING:__  
> The name of the file is important and must correspond to `1-Participant.csv` otherwise the import into the database will not work.

The CSV file must contain these fields in the order shown:

| Field                  | Description                                                                                           |
|------------------------|-------------------------------------------------------------------------------------------------------|
| _Role_                 | Role of the participant. Value must be one of the following: **provider**, **cab** or **federation**. |
| _Id_                   | Identifiant of participant. It is mandatory for **cab** and **federation**.                           |
| _Designation_          | Legal name of the participant.                                                                        |
| _Street Address_       | Street part of the participant's address                                                              |
| _Postal Code_          | Zip code part of  the participant's address                                                           |
| _Locality_             | City part of the participant's address                                                                |
| _Country Name_         | Country code with subdivision in ISO-3166-2 format                                                    |
| _LeiCode_              | LEI Code                                                                                              |
| _VatID_                | VAT Number                                                                                            |
| _Terms And Conditions_ | URL of the participant terms and conditions page                                                      |


> __Note:__  
> Your federation must have ID **CAB-00**.
> Currently, the following IDs corresponds to these auditors (or cabs):
> - **CAB-01**: Bureau Veritas Italia
> - **CAB-02**: KIWA Cermet Italia
> - **CAB-03**: LSTI
> - **CAB-04**: Agence nationale de la sécurité des systèmes d'information
> - **CAB-05**: SGS TECNOS SA
> - **CAB-06**: Grupo S 21 SEC Gestion S.A.
> - **CAB-08**: SAP
> - **CAB-10**: Entidad National de Acreditacion
> - **CAB-16**: LNE
> - **CAB-18**: KPMG
> - **CAB-19**: Ernst and Young Certifypoint
> - **CAB-20**: Ernst and Young Switzerland
> - **CAB-21**: Ernst and Young Germany
> - **CAB-22**: BDO Auditores
> - **CAB-23**: Bureau Veritas Service France SAS
> - **CAB-24**: Perspective Risk
> - **CAB-25**: TÜV NORD GROUP
> - **CAB-26**: Uptime Institute, LLC.
> - **CAB-27**: Red ADVISOR Consultores, S.L.
> - **CAB-28**: IBERDROLA CLIENTES, S.A.U.
> - **CAB-29**: SAP SE
> - **CAB-30**: AENOR
> - **CAB-31**: TUV
> - **CAB-32**: Certificarsi EOOD
> - **CAB-33**: PWC
> - **CAB-34**: BSI Group The Netherlands BV
> - **CAB-35**: VMWare, Inc
> - **CAB-36**: Veeam Software B.V.
> - **CAB-37**: Lloyd's Register Quality Assurance Limited
> - **CAB-38**: Ernst and Young Belgium
> - **CAB-39**: Sense engineering
> - **CAB-40**: DNV GL Business Assurance B.V.


For example, to describe _aenor_ auditor, this CSV file is generated
```csv
Role;Id;Designation;Street Address;Postal Code;Locality;Country Name;LeiCode;VatID;Terms And Conditions
cab;CAB-30;AENOR;C/ Génova 6;28004;Madrid;ES-M;xxx;yyy;
```

For example, to describe _aster-x_ federation, this CSV file is generated
```csv
Role;Id;Designation;Street Address;Postal Code;Locality;Country Name;LeiCode;VatID;Terms And Conditions
federation;CAB-00;aster-x;139 Rue de Bercy;75012;Paris;FR-75C;;xxx;
```

For example, to describe _ovhcloud_ service provider, this CSV file is generated
```csv
Role;Id;Designation;Street Address;Postal Code;Locality;Country Name;LeiCode;VatID;Terms And Conditions
provider;;ovhcloud;2 Rue Kellermann;59100;Roubaix;FR-59;;xxx;
```

## Federation related files

In addition of `1-Participant.csv` file, the following CSV files must be present in federation directory:
- `ComplianceResource.csv`: this CSV file describes the list of compliance references that a federation provides
- `LabelingCriteria-Matrix.csv`: This CSV file describes a matrix between federation criteria for labelling and the compliance references

### Compliance reference list - ComplianceResource.csv

The CSV file must contain these fields in the order shown:

| Field                    | Description                                   |
|--------------------------|-----------------------------------------------|
| _ID_                     | Internal ID.                                  |
| _Champ1_                 | Optional field.                               |
| _Compliance Resource Id_ | Compliance resource ID.                       |
| _CR-ID_                  | Compliance Reference ID                       |
| _Ref Designation_        | Description of this compliance reference      |
| _Extrait_                | Date of compliance reference                  |
| _Category_               | Category of this compliance reference         |
| _Valid from_             | Date when this compliance reference is valid  |
| _Valid Until_            | Date until this compliance reference is valid |
| _Reference Doc_          | URL of reference document                     |
| _Assertion Method_       | Method to assert: Certified or Self-Declared  |
| _Assertion Date_         | Date of assertion                             |
| _Issuer Designation_     | Description done by issuer                    |

Example:
```csv
ID;Champ1;Compliance Resource Id;CR-ID;Ref Designation;Extrait;Category;Valid from;Valid Until;Reference Doc;Assertion Method;Assertion Date;Issuer Designation
1;1;CResid-XXX001;CR-01;CISPE Data Protection CoC;09/02/2021;Data Protection;2/2/22;;https://www.codeofconduct.cloud/wp-content/uploads/2022/01/CISPE-Cloud-Data-Protection-Code-of-Conduct-DIGITAL.pdf;Certified;;
2;2;CResid-XXX002;CR-02;SWIPO IaaS CoC;2020 v 3.0;Data Porting;5/10/21;;https://swipo.eu/wp-content/uploads/2020/10/SWIPO-IaaS-Code-of-Conduct-version-2020-27-May-2020-v3.0.pdf;Self-Declared;;
```
### Labelling criteria - LabellingCriteria-Matrix.csv

The CSV file must contain these fields in the order shown:

| Field                                                         | Description                                                                                      |
|---------------------------------------------------------------|--------------------------------------------------------------------------------------------------|
| _Category_                                                    | Criterion category.                                                                              |
| _criterion_                                                   | Criterion ID.                                                                                    |
| _criterion-name_                                              | Criterion name.                                                                                  |
| _description_                                                 | description of criterion                                                                         |
| _used for label level _                                       | label levels associated to this criterion separated by a comma. For example, Level 1             |
| _applicable_                                                  | if applicable, leave this field value empty                                                      |
| _self-assessed_                                               | mark this field value with `x` if this criterion is self assessed                                |
| _Verification Process _                                       | How this criterion is checked to be valid                                                        |
| _Self-declaration / self-assesment_                           | mark this field value with `x` if this criterion is self assessed                                |
| _CISPE Data Protection Code of Conduct:::CR-01:Self-declared_ | mark this field value with `x` if this criterion is self assessed and complies to this reference |
| _CISPE Data Protection Code of Conduct:::CR-01:Certified_     | mark this field value with `x` if this criterion is certified and complies to this reference     |
...

> __Note:__  
> You must add a field for each compliance reference you declared into ComplianceResource.csv file.<br/>
> the format of this field is the following:<br/>
> `Ref Designation:::Compliance Reference ID:Assertion Method`<br/><br/>
> example: <br/>
> `CISPE Data Protection Code of Conduct:::CR-01:Certified`<br/>
> `CISPE Data Protection Code of Conduct:::CR-01:Self-Declared`<br/>

Example:
```csv
Category;criterion;criterion-name;description;used for label level;applicable;self-assessed;Verification Process ;Self-declaration / self-assesment;ISO19944;CISPE Data Protection Code of Conduct:::CR-01:Self-declared;CISPE Data Protection Code of Conduct:::CR-01:Certified;C5:::CR-13:Certified:::CR-16:Certified;TISAX:::CR-24:Certified;SOC2:::CR-12:Certified;SecNumCloud:::CR-10:Certified:::CR-09:Certified;ISO 27001:::CR-04:Certified;CSA STAR Level 1;CSA STAR Level 2:::CR-21:Certified;SWIPO IaaS Code:::CR-02:Certified:::CR-02:Self-Declared;SWIPO SaaS Code:::CR-35:Certified:::CR-35:Self-Declared
Contractual governance;Criterion 1;Criterion 1.1.1; The provider shall offer the ability to establish a legally binding act. This legally binding act shall be documented;Level 1;;x;self-assessment through the trust framework ;x;;;;;;;;;;;;
```
## Service provider related files

For a service provider, the following files must be present in his directory in addition of `1-Participant.csv`:
- `3-Locations.csv`: List of known locations where services can be hosted.
- `4-Services.csv`: List of services that this provider offers.
- `5-Layers.csv`: List of known layers.
- `6-Keywords.csv`: List of known keywords
- `10-CAB2CR.csv`: List of cabs that certify that this provider has this compliance reference.
- `self-assessment.csv`: List of self assessed compliance references.

### Locations - 3-Locations.csv
The CSV file must contain these fields in the order shown:

| Field                  | Description                                |
|------------------------|--------------------------------------------|
| _Id_                   | Not used - must be present to be compliant |
| _Locationid_           | Not used - must be present to be compliant |
| _Location Id_          | ID of location.                            |
| _Provider Designation_ | Designation                                |
| _City_                 | City of this location                      |
| _State/Dpt_            | State or Department of this location       |
| _Country_              | Country of this Location                   |

Example:
```csv
;Locationid;Location Id;Provider Designation;City;State/Dpt;Country
1;Locid-001;LOC-01;IT1;Arezzo;AR;Italy
```

### Services - 4-Services.csv
The CSV file must contain these fields in the order shown:

| Field             | Description                                                                                                                                 |
|-------------------|---------------------------------------------------------------------------------------------------------------------------------------------|
| _Id_              | Not used - must be present to be compliant                                                                                                  |
| _Servid_          | Not used - must be present to be compliant                                                                                                  |
| _Service Id_      | ID of service.                                                                                                                              |
| _Designation_     | Designation                                                                                                                                 |
| _Service Version_ | Version of this service                                                                                                                     |
| _Comply With_     | List of compliance reference that this service complies to. This list must be inside " and each compliance reference id is separated by a ; |
| _Available In_    | List of location id that this service is hosted on. This list must be inside " and each location id is separated by a ;                     |
| _Level of Label_  | Label level of this service                                                                                                                 |

Example:
```csv
;Servid;Service Id;Designation;Service Version;Comply With;Available In;Level of Label
1;Servid-001;S-01;Private Cloud;;"CR-01; CR-02; CR-04; CR-05; CR-06; CR-14; CR-17; CR-25; ";"LOC-01; LOC-03; LOC-04; LOC-05; LOC-08; ";3
```

### Layers - 5-Layers.csv
The CSV file must contain these fields in the order shown:

| Field             | Description                                                                                                     |
|-------------------|-----------------------------------------------------------------------------------------------------------------|
| _Layer_           | Name of this layer                                                                                              |
| _Service ID_    | List of service id associated to this layer. This list must be inside " and each service id is separated by a ; |

Example:
```csv
Layer;Service ID
IAAS;"S-01; S-02; S-03; S-04; S-05; S-06; S-07; S-08; S-09; S-10; S-11; S-12; S-13; S-14"
PAAS;"S-15; S-16; S-18; S-19; S-20; S-21"
SAAS;S-17
```

### Keywords - 6-Keywords.csv
The CSV file must contain these fields in the order shown:

| Field         | Description                                                                                                       |
|---------------|-------------------------------------------------------------------------------------------------------------------|
| _Keyword_     | Name of keyword                                                                                                   |
| _Service ID_  | List of service id associated to this keyword. This list must be inside " and each service id is separated by a ; |

Example:
```csv
Keyword;Service IDs
Authentication;"S-11; S-20"
Backup;"S-07; S-08"
```
### CAB & Compliance references - 10-CAB2CR.csv
The CSV file must contain these fields in the order shown:

| Field                  | Description                                   |
|------------------------|-----------------------------------------------|
| _CR-ID_                | Compliance Reference ID                       |
| _Ref Designation_      | Description of this compliance reference      |
| _Version_              | Version of this compliance reference.         |
| _Valid from_           | Date when this compliance reference is valid  |
| _Valid Until_          | Date until this compliance reference is valid |
| _Reference Doc_        | URL of reference document                     |
| _Assertion Method_     | Method to assert: Certified or Self-Declared  |
| _Assertion Date_       | Date of assertion                             |
| _Issuer Designation_   | Description done by issuer                    |
| _CAB-ID_               | Identifiant of CAB                            |

Example:
```csv
CR-ID;Ref Designation; Version;Valid from ;Valid Until;Reference Doc;Assertion Method;Assertion Date;Issuer Designation;CAB-ID
CR-04;ISO 27001;2013;19/04/2022;18/04/2025;https://www.iso.org/fr/standard/54534.html;Certified;19/04/2022;TÜV NORD GROUP;CAB-25
```

### Self assessed criterions - self-assessment.csv
The CSV file must contain these fields in the order shown:

| Field                                                                   | Description                                                                                                       |
|-------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------|
| _Section_                                                               | Category of criterion                                                                                             |
| _Criterion_                                                             | Criterion id                                                                                                      |
| _Relevant for Gaia-X Labels_                                            | List of labels. Example: Level 1, 2 and 3                                                                         |
| _Service(s)  Id- for which the  self-assesment of the criterion is  OK_ | List of service id associated to this keyword. This list must be inside " and each service id is separated by a ; |
| _External available proof - Optinoal  (if any)_                         | -                                                                                                                 |

Example:
```csv
Section;Criterion;Relevant for Gaia-X Labels;Service(s)  Id- for which the  self-assesment of the criterion is  OK;External available proof - Optinoal  (if any)
Contractual governance ;Criterion 1: The provider shall offer the ability to establish a legally binding act. This legally binding act shall be documented ;Level 1, 2 and 3 ;"S-01;S-02;S-03;S-04;S-05;S-06";
```

## Following step

Thanks to these CSV files, credential-generator will create a local database with the command `init`. For more information, go to [How do I import my files into local database?](./02_import_csv_files_into_database.md)

---
© 2023 gxfs-fr. All rights reserved.
