# How do I get Aster-X compliance ?

> __Note:__  
> Prerequisite
> Have the Gaia-X compliance. Aster-X compliance is an extension of the Gaia-X compliance and the first rule to check is
> to have the Gaia-X compliance.

## Ask Aster-X compliance command - Usage

```bash
poetry run credential-generator provider aster-x-compliance --help

 Usage: credential-generator provider aster-x-compliance [OPTIONS]

 The aster-x-compliance command is used to get Aster-X compliance thanks to the Membership VC.

╭─ Options ──────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────╮
│ *  --name                   -n      TEXT     Provider name.                                                                                    │
│                                              [default: None]                                                                                   │
│                                              [required]                                                                                        │
│    --output-root-directory  -o      TEXT     Directory where generated files can be found                                                      │
│                                              [default: /Users/geromeegron/src/wescale/gaia-x/demo-22.11/credential-generator/public]           │
│    --parent-domain          -f      TEXT     Parent domain name to generate proper did web                                                     │
│                                              [default: provider.dev.gaiax.ovh]                                                                 │
│    --port-number                    INTEGER  Port number.                                                                                      │
│                                              [default: 443]                                                                                    │
│ *  --membership-vc-id               TEXT     Membership VC id                                                                                  │
│                                              [default: None]                                                                                   │
│                                              [required]                                                                                        │
│    --workers                -w      INTEGER  number of workers                                                                                 │
│                                              [default: 5]                                                                                      │
│ *  --api-key                -k      TEXT     User agent api key                                                                                │
│                                              [default: None]                                                                                   │
│                                              [required]                                                                                        │
│    --help                                    Show this message and exit.                                                                       │
╰────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────╯
```

The Aster-X compliance services are exposed by API Gateway but not yet the other service of the Federation, so we need to set the user agent API key for this command.

## Ask Aster-X compliance command - Example

We ask the Aster-X compliance for the services OVHcloud provider which passed the Gaia-X compliance.

````bash
poetry run credential-generator provider aster-x-compliance \
--parent-domain provider.demo23.gxfs.fr \
--name ovhcloud \
--api-key [API KEY] \
--membership-vc-id https://ovhcloud.provider.dev.gxdch.ovh/organization/4788298634074486817542acbbb953f0/data.json
````

The result of this command is the generation of Aster-X compliance VC which will be stored on the Aster-X Federation user-agent (look the VC ids).

If you want to index this VC inside the Federated Catalogue this will be in the following step.

## Index Aster-X compliance inside Federated Catalogue - Usage

This command will index the Aster-X compliance VC id inside the Aster-X catalogue. The ids are known thanks to the ``index.json`` file.

````bash
poetry run credential-generator provider sync_aster_x_compliance --help

 Usage: credential-generator provider sync_aster_x_compliance
            [OPTIONS]

 The sync_aster_x_compliance command is used to index Aster-X compliance VCs generated into federated catalogue.

╭─ Options ────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────╮
│ *  --name                      -n       TEXT     Provider name.                                                                                          │
│                                                  [default: None]                                                                                         │
│                                                  [required]                                                                                              │
│ *  --federation-name           -fn      TEXT     Federation name.                                                                                        │
│                                                  [default: None]                                                                                         │
│                                                  [required]                                                                                              │
│    --output-root-directory     -o       TEXT     Directory where generated files can be found                                                            │
│                                                  [default: /Users/geromeegron/src/wescale/gaia-x/demo-22.11/credential-generator/public]                 │
│    --parent-domain             -f       TEXT     Parent domain name to generate proper did web                                                           │
│                                                  [default: provider.dev.gaiax.ovh]                                                                       │
│    --federation-parent-domain  -f       TEXT     Parent domain name of federation to generate proper did web                                             │
│                                                  [default: dev.gaiax.ovh]                                                                                │
│    --database                  -d       TEXT     Name of database to generate                                                                            │
│                                                  [default: /Users/geromeegron/src/wescale/gaia-x/demo-22.11/credential-generator/compliance.sqlite3]     │
│    --port-number                        INTEGER  Port number.                                                                                            │
│                                                  [default: 443]                                                                                          │
│ *  --api-key                   -k       TEXT     Federation Catalogue api key                                                                            │
│                                                  [default: None]                                                                                         │
│                                                  [required]                                                                                              │
│    --help                                        Show this message and exit.                                                                             │
╰──────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────╯
````

## Index Aster-X compliance inside Federated Catalogue - Example

This command will index Aster-X compliance VC from OVHcloud services inside the Aster-X catalogue.

````bash
poetry run credential-generator provider sync_aster_x_compliance \
--federation-name aster-x \
--federation-parent-domain demo23.gxfs.fr \
--name ovhcloud \
--parent-domain provider.dev.gxdch.ovh \
--api-key [API KEY]
````
