# How do I index provider verifiable credentials into catalogue?

> __Note:__  
> In the following tutorial, I suppose that credential-generator has been installed by cloning our repository. If you prefer to use docker container, you
> must adjust command.


## sync command - Usage

```bash
poetry run credential-generator provider sync --help

 Usage: credential-generator provider sync [OPTIONS]  

 The sync command is used to index generated VCs into federated catalogue.  

╭─ Options ──────────────────────────────────────────────────────────────────────────────────────────────────────╮
│ *  --name                          -n      TEXT     provider's name.                                           │
│                                                     [default: None]                                            │
│                                                     [required]                                                 │
│    --output-root-directory         -o      TEXT     Directory where generated files can be found               │
│                                                     [default: /usr/local/lib/python3.11/site-packages/public]  │
│    --parent-domain                 -p      TEXT     Parent domain name to generate proper did web              │
│                                                     [default: provider.dev.gaiax.ovh]                          │
│    --port-number                           INTEGER  Port number.                                               │
│                                                     [default: 443]                                             │
│ *  --api-key                       -k      TEXT     Catalogue api key                                          │
│                                                     [default: None]                                            │
│                                                     [required]                                                 │
│    --enable-gaiax-ces                               Enable call to Gaia-X CES                                  |
|    --help                                           Show this message and exit.                                │
╰────────────────────────────────────────────────────────────────────────────────────────────────────────────────╯

```

## index provider json-ld files into catalogue
Once the provider user agent has generated and stored the verifiable credentials from the generated json-ld files, you can index them in the provider's internal catalogue.

example:
```bash
poetry run credential-generator provider sync \
--name aster-x \
--output-root-directory $(PWD)/public \
--parent-domain example.com \
--api-key xxx
```

The provider's internal catalogue allows VCs to be indexed and make them available to the federated catalogue. By default, credential-generator does not send them into Gaia-x CES.
You can add CES synchronization by adding `--enable-gaix-ces` option in `credential-generator sync` command

```bash
poetry run credential-generator provider sync \
--name aster-x \
--output-root-directory $(PWD)/public \
--parent-domain example.com \
--enable-gaiax-ces \
--api-key xxx
```
## Following step

You can now proceed to get aster-x labels from your service offering: [Getting aster-x labels](./09-get_labels)

---
© 2023 gxfs-fr. All rights reserved.
