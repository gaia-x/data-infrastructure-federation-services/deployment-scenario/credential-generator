# How do I get labels for my service offering?

> __Note:__  
> In the following tutorial, I suppose that credential-generator has been installed by cloning our repository. If you prefer to use docker container, you
> must adjust command.


## label command - Usage

```bash
poetry run credential-generator provider label --help

 Usage: credential-generator provider label [OPTIONS]

 The label command sends provider's service offering to labelling to get aster-x labels.
╭─ Options ──────────────────────────────────────────────────────────────────────────────────────────────────────╮
│ *  --name                          -n      TEXT     provider's name.                                           │
│                                                     [default: None]                                            │
│                                                     [required]                                                 │
│    --output-root-directory         -o      TEXT     Directory where generated files can be found               │
│                                                     [default: /usr/local/lib/python3.11/site-packages/public]  │
│    --parent-domain                 -p      TEXT     Parent domain name to generate proper did web              │
│                                                     [default: provider.dev.gaiax.ovh]                          │
|    --federation-parent-domain      -f      TEXT     Parent domain name of federation to generate proper did web|
|                                                     [default: dev.gaiax.ovh]                                   |
│ *  --api-key                       -k      TEXT     User agent api key                                         │
│                                                     [default: None]                                            │
│                                                     [required]                                                 │
│    --port-number                           INTEGER  Port number.                                               │
│                                                     [default: 443]                                             │
│    --database                      -d      TEXT  Name of database to generate                                  │
│                                                  [default:                                                     │
│    --help                                           Show this message and exit.                                │
╰────────────────────────────────────────────────────────────────────────────────────────────────────────────────╯
```

## get labels for my service offerings
Once the provider user agent has generated and stored the verifiable credentials from the generated json-ld files,
you can submit your Service Offerings VCs to federation labelling service.

example:
```bash
poetry run credential-generator provider label \
--name ovhcloud \
--output-root-directory $(PWD)/public \
--parent-domain example.com \
--api-key yyy
```

>__NOTE__:
>  Labelling service generates a VCs if your ServiceOffering description complies with the required criteria. These VCs are automatically stored in your user agent.

## Following step

You can now proceed to add VCs into federation catalogue: [Add provider json-ld files into catalogue](./11-index_provider_into_catalogue.md)

---
© 2023 gxfs-fr. All rights reserved.
