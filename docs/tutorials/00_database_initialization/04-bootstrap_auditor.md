# How do I bootstrap an auditor?

> __Note:__  
> In the following tutorial, I suppose that credential-generator has been installed by cloning our repository. If you prefer to use docker container, you
> must adjust command.


## bootstrap command - Usage

```bash
poetry run credential-generator auditor bootstrap --help

 Usage: credential-generator auditor bootstrap [OPTIONS]  

 The bootstrap command reads a CSV File from provider_name directory and generates compliance-criterion  
 json-ld files and compliance-labels json-ld files in public directory.  

╭─ Options ────────────────────────────────────────────────────────────────────────────────────────────────╮
│ *  --name                          -n      TEXT  Auditor's name.                                         │
│                                                  [default: None]                                         │
│                                                  [required]                                              │
│    --input-root-directory          -i      TEXT  Directory where csv files can be found                  │
│                                                  [default:                                               │
│                                                  /usr/local/lib/python3.11/site-packages/public]         │
│    --output-root-directory         -o      TEXT  Directory where generated files can be found            │
│                                                  [default:                                               │
│                                                  /usr/local/lib/python3.11/site-packages/public]         │
│    --parent-domain                 -p      TEXT  Parent domain name to generate proper did web           │
│                                                  [default: auditor.dev.gaiax.ovh]                                │
│    --database                      -d      TEXT  Name of database to generate                            │
│                                                  [default:                                               │
│                                                   /usr/local/lib/python3.11/site-packages/compliance.sql…│
│    --help                                        Show this message and exit.                             │
╰──────────────────────────────────────────────────────────────────────────────────────────────────────────╯
```

## bootstrapping auditor files
When your database is initialized, you can now bootstrap json-ld files for your auditor.

example:
```bash
poetry run credential-generator auditor bootstrap \
--name aenor \
--output-root-directory $(PWD)/public \
--parent-domain example.com \
--database aster-x.sqlite3
```

Once generated, credential-generator creates a hierarchy of directories on your local system.
Your root directory for your auditor follows this pattern `<auditor name>.<parent domain>`.
For example, the command above creates `aenor.example.com` in your `$(PWD)/public directory`

In this directory, credential-generator stores generated json-ld files into a sub-directory corresponding to its type.
## Following step

You can now proceed to upload json-ld files into auditor's user-agent : [Uploading auditor json-ld files](./07-upload_auditor_files.md)

---
© 2023 gxfs-fr. All rights reserved.
