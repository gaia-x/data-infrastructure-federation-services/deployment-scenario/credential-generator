ARG APP_NAME=credential-generator
ARG APP_PATH=/opt/$APP_NAME
ARG MODULE_NAME=credential_generator
ARG PYTHON_VERSION=3.12.1
ARG POETRY_VERSION=1.7.1

#
# Stage: staging
#
FROM python:$PYTHON_VERSION-slim as staging
ARG APP_NAME
ARG APP_PATH
ARG POETRY_VERSION

ENV \
    PYTHONDONTWRITEBYTECODE=1 \
    PYTHONUNBUFFERED=1 \
    PYTHONFAULTHANDLER=1
ENV \
    POETRY_VERSION=$POETRY_VERSION \
    POETRY_HOME="/opt/poetry" \
    POETRY_VIRTUALENVS_IN_PROJECT=true \
    POETRY_NO_INTERACTION=1

# hadolint ignore=DL3008
RUN apt-get update \
    && apt-get install --no-install-recommends -y \
        build-essential \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

RUN pip install --no-cache-dir "poetry==$POETRY_VERSION"

# Import our project files
WORKDIR $APP_PATH
COPY ./poetry.lock ./pyproject.toml ./README.md ./
COPY ./$MODULE_NAME ./src/$MODULE_NAME

#
# Stage: build
#
FROM staging as build
ARG APP_PATH

WORKDIR $APP_PATH
RUN poetry build --format wheel \
    && poetry export --format requirements.txt --output requirements.txt --without-hashes

#
# Stage: production
#
FROM python:$PYTHON_VERSION-alpine as production
ARG APP_NAME
ARG APP_PATH

ENV \
    USER_ID=65535 \
    GROUP_ID=65535 \
    USER_NAME=default-user \
    GROUP_NAME=default-user

ENV \
    PYTHONDONTWRITEBYTECODE=1 \
    PYTHONUNBUFFERED=1 \
    PYTHONFAULTHANDLER=1

ENV \
    PIP_NO_CACHE_DIR=off \
    PIP_DISABLE_PIP_VERSION_CHECK=on \
    PIP_DEFAULT_TIMEOUT=100

RUN addgroup --gid ${GROUP_ID} ${GROUP_NAME} && \
    adduser --shell /sbin/nologin --disabled-password \
    --no-create-home --uid ${USER_ID} --ingroup ${GROUP_NAME} ${USER_NAME}

# Get build artifact wheel and install it respecting dependency versions
WORKDIR $APP_PATH
COPY --from=build $APP_PATH/dist/*.whl ./
COPY --from=build $APP_PATH/requirements.txt ./
# hadolint ignore=SC2086
RUN pip install --no-cache-dir --upgrade pip==23.3.2 && \
    pip install --no-cache-dir ./${MODULE_NAME}*.whl --requirement requirements.txt

# Entrypoint script
COPY ./docker/docker-entrypoint.sh /docker-entrypoint.sh
RUN chmod +x /docker-entrypoint.sh

USER $USER_NAME
ENTRYPOINT ["/docker-entrypoint.sh"]
