# Credential Generator - JSON-LD Generator for Services Offering

[![pipeline status](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/credential-generator/badges/main/pipeline.svg)](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/credential-generator/-/commits/main)
[![coverage report](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/credential-generator/badges/main/coverage.svg)](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/credential-generator/-/commits/main)
[![Latest Release](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/credential-generator/-/badges/release.svg)](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/credential-generator/-/releases)
[![Python 3.11](https://img.shields.io/badge/python-3.11-blue.svg?logo=python)](https://www.python.org/)
[![License](https://img.shields.io/badge/license-Apache%202.0-blue.svg)](https://opensource.org/licenses/Apache-2.0)

The Credential Generator CLI is an open-source command-line interface (CLI) under the Apache v2.0 license, enabling the generation of JSON-LD files describing Gaia-X compliant objects from CSV files that detail Gaia-X participant and their services.
This CLI also allows uploading these files to a participant user agent, signing them to create Verifiable Credentials, validating compliance with Gaia-X, and pushing them to a service catalog (in case of provider participant).

## Features

- **Initialization:** Easily ingest CSV files describing participant and their service offerings into a local SQLite database.
- **JSON-LD Generation:** Easily generate JSON-LD files that comply with Gaia-X LegalParticipant and ServiceOffering objects.
- **Participant agent Upload:** Send the generated files to a web server to make them accessible online.
- **Verifiable Credentials Signing:** Sign the JSON-LD files to turn them into Verifiable Credentials, enhancing their authenticity.
- **Gaia-X Validation:** Check the files for compliance with Gaia-X standards to ensure optimal interoperability.
- **Integration with Service Catalog:** Push validated files into a federated service catalog as well as Gaia-X CES for centralized management.

## Getting Started
### Prerequisites
* Python 3.11+
* Poetry 1.6.1+

### Installation

#### Docker way

If you don't want to install Credential Generator CLI to your system, you feel free
to using our official [Docker image](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/credential-generator/container_registry) and run CLI from isolated
container:

```bash
docker run --rm -it -v ${PWD}:${PWD} -w ${PWD} registry.gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/credential-generator:1.0.0 [OPTIONS] COMMAND [ARGS]...
```
#### Python way

```bash
git clone https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/credential-generator.git
cd credential-generator
poetry install
```
## Description
```mermaid
C4Container
title CredentialGenerator Container diagram
Person(customerA, Customer, "A Gaia-X Participant", $tags="v1.0")
Container(csv, "Description Files", "CSV", "Participant description and his services.")

Container_Boundary(c1, "CredentialGenerator") {
  Container(cli, "Command Line Tool", "Python", "Helps to generate Gaia-X and Aster-X compliant objects, and publish into catalogue")
  Container(json_ld, "Description Files", "JSON-LD", "Participant description and his services.")
  ContainerDb(database, "Database", "SQLite Database", "Stores participant registration information, service descriptions, etc.")
}

Container_Boundary(c2, "Participant Web Components") {
  Container(partipant_agent, "Participant Agent", "", "Stores and publically exposes verifiable credentials.")
  Container(wallet, "Wallet", "", "Helps to sign objects with participant's cryptographically suite.")
  Container(participant_catalogue, "Catalogue", "Optional", "Catalogue of service descriptions")
}
Container_Boundary(c3, "Gaia-X Components") {
  System_Ext(gaiax_compliance, "Gaia-X Compliance", "Check if generated objects are gaia-x compliant")
  SystemQueue_Ext(gaiax_ces, "Gaia-X CES", "Publish new service description to gaia-x compatible catalogues")
}
Container_Boundary(c4, "Aster-X Components") {
  SystemQueue_Ext(asterx_pubsub, "Aster-X Catalogue Pub/sub broker", "Publish new service description to aster-x")
  System_Ext(asterx_labeling, "Aster-X Compliance", "Check if generated objects are gaia-x compliant")
}

Rel(customerA, csv, "Describes")
Rel(customerA, cli, "Uses")
Rel(cli, csv, "Import files")
Rel(cli, database, "Import files and queries")
Rel(cli, json_ld, "Generates")
Rel(cli, partipant_agent, "Stores JSON-LD files and generates VC")
Rel(cli, gaiax_compliance, "Check generated JSON-LD")
Rel(cli, participant_catalogue, "Add service description to participant catalogue")
Rel(cli, asterx_labeling, "Check criterions of service description to generate Aster-X Labels")
Rel(participant_catalogue, asterx_pubsub, "Add generated service description to Aster-X catalogue.")
Rel(participant_catalogue, gaiax_ces, "Add generated service description to Gaia-X compatible catalogues.")
Rel(partipant_agent, wallet, "Sign VC and VP")

UpdateLayoutConfig($c4ShapeInRow="2", $c4BoundaryInRow="2")
```
## Usage
```bash
$ poetry run credential-generator --help

 Usage: credential-generator [OPTIONS] COMMAND [ARGS]...  

╭─ Options ─────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────╮
│ --version             -v        Give application version and exit.                                                                                                                                                                                                                        │
│ --install-completion            Install completion for the current shell.                                                                                                                                                                                                                 │
│ --show-completion               Show completion for the current shell, to copy it or customize the installation.                                                                                                                                                                          │
│ --help                          Show this message and exit.                                                                                                                                                                                                                               │
╰───────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────╯
╭─ Commands ────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────╮
│ auditor     The auditor command helps to generate, cleanup, upload and index auditor related self-description files.                                                                                                                                                                      │
│ federation  The federation command helps to generate, cleanup, upload and index federation related self-description files.                                                                                                                                                                │
│ init        The init command creates a new sqlite database in your local file system and loads data from CSV files located in data-compliance directory.                                                                                                                                  │
│ provider    The provider command helps to generate, cleanup, upload and index provider related self-description files.                                                                                                                                                                    │
╰───────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────╯
```
## Tutorials
### Database initialization
* [How do I describe my participant and eventually my services?](docs/tutorials/00_database_initialization/01_input_file_format.md)
* [How do I import my files into local database?](./docs/tutorials/00_database_initialization/02_import_csv_files_into_database.md)

### Federation related commands
* [How do I bootstrap my federation?](docs/tutorials/00_database_initialization/03-bootstrap_federation.md)
* [How do I upload my federation json-ld files?](docs/tutorials/00_database_initialization/06-upload_federation_files.md)
* [How do I index my federation verifiable credentials into catalogue?](docs/tutorials/00_database_initialization/10-index_federation_into_catalogue)

### Auditor related commands
* [How do I bootstrap an auditor?](docs/tutorials/00_database_initialization/04-bootstrap_auditor.md)
* [How do I upload an auditor json-ld files?](docs/tutorials/00_database_initialization/07-upload_auditor_files.md)

### Provider related commands
* [How do I bootstrap a provider?](docs/tutorials/00_database_initialization/05-bootstrap_provider.md)
* [How do I upload a provider json-ld files?](docs/tutorials/00_database_initialization/08-upload_provider_files.md)
* [How do I get labels for my service offering?](docs/tutorials/00_database_initialization/09-get_labels)
* [How do I index provider's verifiable credentials into catalogue?](docs/tutorials/00_database_initialization/11-index_provider_into_catalogue)

## License

This project is licensed under the Apache v2.0 License - see the [LICENSE](LICENSE) file for details.

---

© 2023 gxfs-fr. All rights reserved.
