import asyncio
import logging
from typing import Optional

from credential_generator.infra.api.cli.common_commands import USER_AGENT_API_KEY_OPTION
from credential_generator.infra.spi.clients.asterx import FederatedCatalogueClient, UserAgentClient
from credential_generator.utilities.did_helpers import (
    get_id_from_jsonld,
    get_root_url_from_did_web,
    get_type_from_jsonld,
    get_url_from_did_web,
)
import typer

VERIFIABLE_CREDENTIAL_TYPE = "VerifiableCredential"
LEGAL_PARTICIPANT_TYPE = "gx:LegalParticipant"
PROVIDER_UNKNOWN_LIST = ["wizard.lab.gaia-x.eu"]

federated_catalogue_client = FederatedCatalogueClient(base_url="aster-x.demo23.gxfs.fr")
app = typer.Typer()
log = logging.getLogger(__name__)


def check_type(jsonld_type: list | str, required_type: str) -> bool:
    if isinstance(jsonld_type, str):
        return required_type == jsonld_type
    return required_type in jsonld_type


def get_provider_did_list():
    json_ld_list = federated_catalogue_client.get_provider_list()
    provider_list = [
        get_id_from_jsonld(json_ld["credentialSubject"])
        for json_ld in json_ld_list
        if check_type(get_type_from_jsonld(jsonld=json_ld), required_type=VERIFIABLE_CREDENTIAL_TYPE)
        and check_type(get_type_from_jsonld(jsonld=json_ld["credentialSubject"]), required_type=LEGAL_PARTICIPANT_TYPE)
    ]

    return provider_list


def extract_provider_did_root_list(provider_did_web_list: list[str]) -> list[str]:
    return [
        get_root_url_from_did_web(provider_did_web)
        for provider_did_web in provider_did_web_list
        if provider_did_web.startswith("did:web")
    ]


def extract_provider_url_from(provider_did_web_list: list[str]) -> list[str]:
    return [
        get_url_from_did_web(did_web=provider_did_web)
        for provider_did_web in provider_did_web_list
        if provider_did_web.startswith("did:web")
    ]


def remove_unknown_provider_from(provider_list: list[str]) -> list[str]:
    filtered_provider_list = []
    for unknown_provider in PROVIDER_UNKNOWN_LIST:
        filtered_provider_list.extend(list(filter(lambda x: unknown_provider not in x, provider_list)))

    return filtered_provider_list


def regenerate_did_document(provider_url: str, api_key: str, port_number: Optional[int] = 443) -> bool:
    try:
        participant_agent_client = UserAgentClient(url=provider_url, port_number=port_number, api_key=api_key)

        asyncio.run(participant_agent_client.bootstrap())
        return True
    except Exception as e:
        # Handle the exception here
        log.info(f"An error occurred: {e!s}")
        return False


@app.command()
def renew_did_documents_command(api_key: str = USER_AGENT_API_KEY_OPTION):
    provider_did_web_list = get_provider_did_list()
    provider_did_web_list = extract_provider_did_root_list(provider_did_web_list=provider_did_web_list)
    provider_list = remove_unknown_provider_from(provider_did_web_list)
    for provider in provider_list:
        log.info(f"Generating did document for provider: {provider}")
        result = regenerate_did_document(provider_url=provider, api_key=api_key)
        log.info(f"=> Result: {result}")


if __name__ == "__main__":
    app()
