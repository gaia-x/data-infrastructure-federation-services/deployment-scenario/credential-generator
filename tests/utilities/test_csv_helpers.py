# -*- coding: utf-8 -*-
import pytest

from credential_generator.utilities.csv_helpers import get_splitted_values_of
from credential_generator.utilities.csv_helpers import get_value_of
from credential_generator.utilities.csv_helpers import list_to_csv

# Test cases for the get_value_of function
# Each test case is a tuple with the following structure:
# (test_id, row, field, expected_output)

test_cases = [
    # Happy path tests with various realistic test values
    ("happy-numeric", {"age": "30"}, "age", "30"),
    ("happy-alphabetic", {"name": "Alice"}, "name", "Alice"),
    ("happy-alphanumeric", {"id": "user123"}, "id", "user123"),
    ("happy-spaces", {"address": " 123 Main St "}, "address", "123 Main St"),
    # Edge cases
    ("edge-empty-string", {"empty": ""}, "empty", ""),
    ("edge-space-only", {"space": " "}, "space", ""),
    ("edge-none-existing-field", {"name": "Alice"}, "nonexistent", None),
    # Error cases
    # Note: The function does not raise errors, it returns None for non-existing fields
]


@pytest.mark.parametrize("test_id, row, field, expected_output", test_cases)
def test_get_value_of(test_id, row, field, expected_output):
    # Act
    result = get_value_of(row, field)

    # Assert
    assert result == expected_output, f"Test {test_id} failed: expected {expected_output}, got {result}"


# Parametrized test cases for the happy path with various realistic test values
@pytest.mark.parametrize(
    "input_row, expected_output, test_id",
    [
        ("value1;value2;value3", ["value1", "value2", "value3"], "happy_path_simple"),
        ("  value1 ; value2 ; value3  ", ["value1", "value2", "value3"], "happy_path_with_spaces"),
        ("value1;value2;;value3", ["value1", "value2", "", "value3"], "happy_path_with_empty_field"),
        ("value1", ["value1"], "happy_path_single_value"),
        ("", [""], "happy_path_empty_string"),
        ("value1;value2;value3;", ["value1", "value2", "value3", ""], "happy_path_trailing_semicolon"),
    ],
)
def test_get_splitted_values_of_happy_path(input_row, expected_output, test_id):
    # Act
    result = get_splitted_values_of(input_row)

    # Assert
    assert result == expected_output, f"Failed {test_id}"


# Parametrized test cases for edge cases
@pytest.mark.parametrize(
    "input_row, expected_output, test_id",
    [
        (";", ["", ""], "edge_case_only_delimiter"),
        ("value1;value2\nvalue3", ["value1", "value2\nvalue3"], "edge_case_newline_in_value"),
        ("value1\t;value2", ["value1", "value2"], "edge_case_tab_before_delimiter"),
        ("value1;value2\t", ["value1", "value2"], "edge_case_tab_after_delimiter"),
    ],
)
def test_get_splitted_values_of_edge_cases(input_row, expected_output, test_id):
    # Act
    result = get_splitted_values_of(input_row)

    # Assert
    assert result == expected_output, f"Failed {test_id}"


# Parametrized test cases for error cases
@pytest.mark.parametrize(
    "input_row, expected_output, test_id",
    [
        (None, None, "error_case_none_input"),
    ],
)
def test_get_splitted_values_of_error_cases(input_row, expected_output, test_id):
    # Act
    result = get_splitted_values_of(input_row)

    # Assert
    assert result == expected_output, f"Failed {test_id}"


# Happy path tests with various realistic test values
@pytest.mark.parametrize(
    "value_list, delimiter, expected_output, test_id",
    [
        (["a", "b", "c"], ";", "a;b;c", "id=happy_path_semicolon"),
        (["1", "2", "3"], ",", "1,2,3", "id=happy_path_comma"),
        (["name", "age", "location"], "|", "name|age|location", "id=happy_path_pipe"),
        (["apple", "banana", "cherry"], " ", "apple banana cherry", "id=happy_path_space"),
        (["John Doe", "Jane Smith"], ";", "John Doe;Jane Smith", "id=happy_path_names"),
        ([], ";", "", "id=happy_path_empty_list"),
        (["single"], ";", "single", "id=happy_path_single_element"),
    ],
)
def test_list_to_csv_happy_path(value_list, delimiter, expected_output, test_id):
    # Act
    result = list_to_csv(value_list, delimiter)

    # Assert
    assert result == expected_output, f"Failed {test_id}"


# Edge cases
@pytest.mark.parametrize(
    "value_list, delimiter, expected_output, test_id",
    [
        (["a;b", "c;d"], ";", "a;b;c;d", "id=edge_case_semicolon_in_values"),
        (["a,b", "c,d"], ",", "a,b,c,d", "id=edge_case_comma_in_values"),
        (["a|b", "c|d"], "|", "a|b|c|d", "id=edge_case_pipe_in_values"),
        (["a b", "c d"], " ", "a b c d", "id=edge_case_space_in_values"),
        (["", "b", "c"], ";", ";b;c", "id=edge_case_empty_string_in_values"),
        (["a", "", "c"], ";", "a;;c", "id=edge_case_empty_string_in_middle"),
        (["a", "b", ""], ";", "a;b;", "id=edge_case_empty_string_at_end"),
    ],
)
def test_list_to_csv_edge_cases(value_list, delimiter, expected_output, test_id):
    # Act
    result = list_to_csv(value_list, delimiter)

    # Assert
    assert result == expected_output, f"Failed {test_id}"


# Error cases
@pytest.mark.parametrize(
    "value_list, delimiter, expected_exception, test_id",
    [
        (None, ";", TypeError, "id=error_case_none_value_list"),
        (123, ";", TypeError, "id=error_case_non_iterable_value_list"),
        (["a", "b", "c"], None, TypeError, "id=error_case_none_delimiter"),
        (["a", "b", "c"], 5, TypeError, "id=error_case_non_string_delimiter"),
    ],
)
def test_list_to_csv_error_cases(value_list, delimiter, expected_exception, test_id):
    # Act and Assert
    with pytest.raises(expected_exception):
        list_to_csv(value_list, delimiter)
        assert False, f"Failed {test_id}"
