# -*- coding: utf-8 -*-
from pathlib import Path

import httpx
import pytest
from unittest.mock import patch
from credential_generator.utilities import (
    compute_sha256_document_from_url,
    ADMINISTRATIVE_LOCATIONS,
    find_administrative_location,
)
from credential_generator.utilities.file_helpers import get_all_files_from

# Test data for parametrized tests
test_data_happy_path = [
    (
        "http://example.com/file1.txt",
        200,
        "93cbdf55c2d4f53b387bd60c4fd204a6e91d70bf3bec62fe508891582f908b8d",
        "200_ok_file1",
    ),
    (
        "https://example.org/document.pdf",
        200,
        "8ef0400ecca89e4ed8514f85305589afddbed93e1995f77c8fc24d488ecd9d22",
        "200_ok_document",
    ),
    (
        "https://example.org/file2.txt",
        404,
        "",
        "404_ok_document",
    ),
]

test_data_edge_cases = [
    ("", "", "empty_url"),
    (None, "", "none_url"),
]

test_data_error_cases = [
    ("http://example.com/timeout", "timeout_error"),
    ("http://example.com/connecterror", "connect_error"),
    # Add more error cases if necessary
]


# Mock response content for hashing
def mock_response_content(url):
    match url:
        case "http://example.com/file1.txt":
            return b"Content of file 1"
        case "https://example.org/document.pdf":
            return b"Content of document"
        case _:
            return b""


# Happy path tests
@pytest.mark.parametrize("url, status_code, expected_hash, test_id", test_data_happy_path)
def test_compute_sha256_document_from_url_happy_path(url, status_code, expected_hash, test_id):
    with patch("httpx.Client.get") as mock_get:
        # Arrange
        mock_get.return_value.status_code = status_code
        mock_get.return_value.content = mock_response_content(url)

        # Act
        actual_hash = compute_sha256_document_from_url(url)

        # Assert
        assert actual_hash == expected_hash, f"Test failed for {test_id}"


# Edge cases tests
@pytest.mark.parametrize("url, expected_hash, test_id", test_data_edge_cases)
def test_compute_sha256_document_from_url_edge_cases(url, expected_hash, test_id):
    # Act
    actual_hash = compute_sha256_document_from_url(url)

    # Assert
    assert actual_hash == expected_hash, f"Test failed for {test_id}"


# Error cases tests
@pytest.mark.parametrize("url, test_id", test_data_error_cases)
def test_compute_sha256_document_from_url_error_cases(url, test_id):
    with patch("httpx.Client.get") as mock_get:
        # Arrange
        if test_id == "timeout_error":
            mock_get.side_effect = httpx.TimeoutException("Timeout occurred")
        elif test_id == "connect_error":
            mock_get.side_effect = httpx.ConnectError("Connection error occurred")

        # Act
        actual_hash = compute_sha256_document_from_url(url)

        # Assert
        assert actual_hash == "", f"Test failed for {test_id}"


@pytest.mark.parametrize(
    "test_id, country_name, department, expected, expected_exception",
    [
        # Happy path tests
        ("HP_01", "France", "Paris", "FR-75", None),
        ("HP_02", "Belgium", "West-Vlaanderen", "BE-VWV", None),
        # Edge cases
        ("EC_01", "Belgium", "Nonexistent", "BE", None),
        ("EC_01", "Belgium", "", "BE", None),
        # Error cases
        ("ER_01", None, "Paris", None, ValueError),
        ("ER_02", "France", None, None, ValueError),
        ("ER_02", "Nonexistent", "Paris", None, None),
    ],
)
def test_find_administrative_location(test_id, country_name, department, expected, expected_exception):
    # Act

    # Assert
    if expected_exception:
        with pytest.raises(expected_exception):
            result = find_administrative_location(country_name, department)
    else:
        result = find_administrative_location(country_name, department)
        assert result == expected
