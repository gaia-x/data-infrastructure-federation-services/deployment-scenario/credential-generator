# -*- coding: utf-8 -*-
import pytest
from pathlib import Path

from credential_generator.utilities.file_helpers import (
    get_all_files_from,
    get_all_files_of_type,
    get_all_files_from_name,
)


class TestGetAllFilesFrom:
    #  Returns a list of Path objects when given a valid source directory.
    def test_valid_source_directory(self, create_directory_hierarchy):
        # Given
        txt_dir, _ = create_directory_hierarchy
        source_directory = Path(txt_dir)

        # When
        result = get_all_files_from(source_directory)

        # Then
        assert isinstance(result, list)
        assert all(isinstance(file, Path) for file in result)

    #  Returns an empty list when given an empty source directory.
    def test_empty_source_directory(self, tmp_path_factory):
        # Given
        source_directory = Path(tmp_path_factory.mktemp("tmp", numbered=True))

        # When
        result = get_all_files_from(source_directory)

        # Then
        assert isinstance(result, list)
        assert len(result) == 0

    #  Raises a TypeError when given a source_directory that is not a Path object.
    def test_invalid_source_directory_type(self):
        # Given
        source_directory = "path/to/source/directory"

        # When/Then
        with pytest.raises(TypeError):
            get_all_files_from(source_directory)

    #  Returns an empty list when given a source directory that does not exist.
    def test_nonexistent_source_directory(self):
        # Given
        source_directory = Path("path/to/nonexistent/directory")

        # When
        result = get_all_files_from(source_directory)

        # Then
        assert isinstance(result, list)
        assert len(result) == 0

    #  Returns an empty list when given a source directory that is a file.
    def test_source_directory_is_file(self):
        # Given
        source_directory = Path("path/to/file")

        # When
        result = get_all_files_from(source_directory)

        # Then
        assert isinstance(result, list)
        assert len(result) == 0


class TestGetAllFilesOfType:
    #  Should return an empty list when no files match the specified types
    def test_empty_list_when_no_files_match_types(self, tmp_path_factory):
        # Given
        source_directory = Path(tmp_path_factory.mktemp("tmp", numbered=True))
        types = [".txt", ".csv"]

        # When
        files = get_all_files_of_type(source_directory, types)

        # Then
        assert files == []

    #  Should return a list of Path objects representing the paths of files that match the specified types in the source directory and its subdirectories
    def test_list_of_matching_files(self, create_directory_hierarchy):
        # Given
        txt_dir, _ = create_directory_hierarchy

        source_directory = Path(txt_dir).parent
        types = [".txt", ".csv"]

        # When
        files = get_all_files_of_type(source_directory, types)

        # Then
        assert isinstance(files, list)
        assert all(isinstance(file, Path) for file in files)

    #  Should exclude files with the name "index.json" from the returned list
    def test_exclude_index_json_files(self):
        # Given
        source_directory = Path("/path/to/directory")
        types = [".txt", ".csv"]

        # When
        files = get_all_files_of_type(source_directory, types)

        # Then
        assert all(file.name != "index.json" for file in files)

    #  Should correctly match file types specified with or without leading dot (e.g. ".txt" or "txt")
    #  Should raise a TypeError if source_directory is not a Path object
    def test_raise_type_error_if_source_directory_not_path_object(self):
        # Given
        source_directory = "/path/to/directory"
        types = [".txt", ".csv"]

        # When, Then
        with pytest.raises(TypeError):
            get_all_files_of_type(source_directory, types)

    #  Should raise a TypeError if types is not a list of strings
    def test_raise_type_error_if_types_not_list_of_strings(self, tmp_path_factory):
        # Given
        source_directory = Path(tmp_path_factory.mktemp("tmp", numbered=True))
        types = [".txt", 123]

        # When, Then
        with pytest.raises(TypeError):
            get_all_files_of_type(source_directory, types)

    #  Should return an empty list when source_directory does not exist
    def test_empty_list_when_source_directory_does_not_exist(self):
        # Given
        source_directory = Path("/path/to/nonexistent/directory")
        types = [".txt", ".csv"]

        # When
        files = get_all_files_of_type(source_directory, types)

        # Then
        assert files == []

    #  Should return an empty list when source_directory is a file and not a directory
    def test_empty_list_when_source_directory_is_file_not_directory(self):
        # Given
        source_directory = Path("/path/to/file.txt")
        types = [".txt", ".csv"]

        # When
        files = get_all_files_of_type(source_directory, types)

        # Then
        assert files == []

    #  Should return an empty list when types is an empty list
    def test_empty_list_when_types_empty(self):
        # Given
        source_directory = Path("/path/to/directory")
        types = []

        # When
        files = get_all_files_of_type(source_directory, types)

        # Then
        assert files == []


class TestGetAllFilesFromName:
    #  Returns an empty list when no files match the specified filename in the source directory and its subdirectories.
    def test_no_matching_files(self, create_files):
        # Given
        source_directory = Path(create_files)
        filename = "test_file.txt"

        # When
        result = get_all_files_from_name(source_directory, filename)

        # Then
        assert result == []

    #  Returns a list of Path objects representing the paths of files that match the specified filename in the source directory and its subdirectories.
    def test_matching_files_in_source_and_subdirectories(self, create_files):
        # Given
        source_directory = Path(create_files)
        filename = "foo.txt"

        # When
        result = get_all_files_from_name(source_directory, filename)

        # Then
        assert isinstance(result, list)
        assert all(isinstance(file, Path) for file in result)
        assert len(result) == 1

    #  Raises a TypeError when the source_directory parameter is not a Path object.
    def test_source_directory_not_path_object(self):
        # Given
        source_directory = "path/to/source/directory"
        filename = "test_file.txt"

        # When/Then
        with pytest.raises(TypeError):
            get_all_files_from_name(source_directory, filename)

    #  Raises a ValueError when the filename parameter is not a string.
    def test_filename_not_string(self, create_files):
        # Given
        source_directory = Path(create_files)
        filename = 123

        # When/Then
        with pytest.raises(TypeError):
            get_all_files_from_name(source_directory, filename)

    #  Returns an empty list when the source directory does not exist.
    def test_source_directory_not_exist(self):
        # Given
        source_directory = Path("nonexistent/directory")
        filename = "test_file.txt"

        # When
        result = get_all_files_from_name(source_directory, filename)

        # Then
        assert result == []

    #  Returns an empty list when the source directory is empty.
    def test_source_directory_empty(self, tmp_path_factory):
        # Given
        source_directory = Path(tmp_path_factory.mktemp("tmp", numbered=True))
        filename = "test_file.txt"

        # When
        result = get_all_files_from_name(source_directory, filename)

        # Then
        assert result == []

    #  Returns an empty list when the filename parameter is an empty string.
    def test_filename_empty_string(self, create_files):
        # Given
        source_directory = Path(create_files)
        filename = ""

        # When
        result = get_all_files_from_name(source_directory, filename)

        # Then
        assert result == []
