# -*- coding: utf-8 -*-
import hashlib

import pytest

from credential_generator.utilities.did_helpers import (
    calculate_provider_hash_value,
    extract_directory_from_object_id,
    extract_file_path_from_object_id_without_participant_part,
    get_well_known_root_object_id,
    get_root_url_from_object_id,
)
from credential_generator.utilities.did_helpers import (
    compute_hash_value,
    HashType,
    calculate_federator_hash_value,
    calculate_auditor_hash_value,
    calculate_service_offering_hash_value,
    calculate_location_hash_value,
    calculate_compliance_criterion_hash_value,
    calculate_compliance_label_hash_value,
    calculate_located_service_hash_value,
    calculate_compliance_reference_hash_value,
    calculate_compliance_certification_scheme_hash_value,
    calculate_compliance_certificate_claim_hash_value,
    calculate_third_party_compliance_certificate_claim_hash_value,
    calculate_self_assessed_certificate_claim_hash_value,
    extract_file_path_from_object_id,
    get_participant_folder,
)


# Enum for HashType is not provided in the snippet, assuming it's defined elsewhere
# If not, it should be defined as follows:
# from enum import Enum
# class HashType(Enum):
#     SHA256 = 1
#     SHA512 = 2


@pytest.mark.parametrize(
    "test_id, input_bytes, hash_type, expected_output",
    [
        # Happy path tests
        ("HP-1", b"test", HashType.SHA256, hashlib.sha256(b"test").hexdigest()),
        ("HP-2", b"another_test", HashType.SHA512, hashlib.sha512(b"another_test").hexdigest()),
    ],
)
def test_compute_hash_value_happy_path(test_id, input_bytes, hash_type, expected_output):
    # Act
    result = compute_hash_value(input_bytes, hash_type)

    # Assert
    assert result == expected_output, f"Test Failed: {test_id}"


@pytest.mark.parametrize(
    "test_id, input_bytes, hash_type, expected_exception",
    [
        # Edge cases
        ("EC-1", b"", HashType.SHA256, TypeError),  # Empty byte string
        # Error cases
        ("ER-1", None, HashType.SHA256, TypeError),  # None as input
        ("ER-2", "not bytes", HashType.SHA256, TypeError),  # Non-bytes input
        ("ER-3", b"valid", "invalid hash type", TypeError),  # Invalid hash type
        ("ER-4", 12, HashType.SHA256, TypeError),  # Invalid hash type
        ("ER-5", True, HashType.SHA256, TypeError),  # Invalid hash type
        ("ER-6", [], HashType.SHA256, TypeError),  # Invalid hash type
        ("ER-7", {}, HashType.SHA256, TypeError),  # Invalid hash type
        ("ER-8", set(), HashType.SHA256, TypeError),  # Invalid hash type
        # Add more error cases for different invalid inputs
    ],
)
def test_compute_hash_value_error_cases(test_id, input_bytes, hash_type, expected_exception):
    # Act / Assert
    with pytest.raises(expected_exception):
        compute_hash_value(input_bytes, hash_type)


# Test cases for the happy path with various realistic test values
@pytest.mark.parametrize(
    "name, expected_hash, test_id",
    [
        ("A", "559aead08264d5795d3909718cdd05abd49572e84fe55590eef31a88a08fdffd", "HP1"),
        ("B", "df7e70e5021544f4834bbee64a9e3789febc4be81470df629cad6ddb03320a5c", "HP2"),
        ("GaiaX-Provider", "968760e5195080dd898b6f0517384d7a3fff1269fad75b1a0c3b531fecfc8039", "HP3"),
    ],
    ids=lambda test_id: test_id,
)
def test_calculate_provider_hash_value_happy_path(name, expected_hash, test_id):
    # Act
    result = calculate_provider_hash_value(name)

    # Assert
    assert result == expected_hash, f"Test Failed: {test_id}"


# Test cases for edge cases
@pytest.mark.parametrize(
    "test_id, name, expected_exception",
    [
        # Edge cases
        ("EC-1", b"", TypeError),  # Empty byte string
        # Error cases
        ("ER-1", None, TypeError),  # None as input
        ("ER-2", 12, TypeError),  # Invalid hash type
        ("ER-3", True, TypeError),  # Invalid hash type
        ("ER-4", [], TypeError),  # Invalid hash type
        ("ER-5", {}, TypeError),  # Invalid hash type
        ("ER-6", set(), TypeError),  # Invalid hash type
    ],
)
def test_calculate_provider_hash_value_error_cases(test_id, name, expected_exception):
    # Act / Assert
    with pytest.raises(expected_exception):
        result = calculate_provider_hash_value(name)


@pytest.mark.parametrize(
    "name, expected_hash, test_id",
    [
        ("A", "559aead08264d5795d3909718cdd05abd49572e84fe55590eef31a88a08fdffd", "HP1"),
        ("B", "df7e70e5021544f4834bbee64a9e3789febc4be81470df629cad6ddb03320a5c", "HP2"),
        ("GaiaX-Federation", "e99381d9d0750ace7811c8d4018490dbe6eac50cd8dfeddc54c661e36a773d80", "HP3"),
    ],
    ids=lambda test_id: test_id,
)
def test_calculate_federator_hash_value_happy_path(name, expected_hash, test_id):
    # Act
    result = calculate_federator_hash_value(name)

    # Assert
    assert result == expected_hash, f"Test Failed: {test_id}"


# Test cases for edge cases
@pytest.mark.parametrize(
    "test_id, name, expected_exception",
    [
        # Edge cases
        ("EC-1", b"", TypeError),  # Empty byte string
        # Error cases
        ("ER-1", None, TypeError),  # None as input
        ("ER-2", 12, TypeError),  # Invalid hash type
        ("ER-3", True, TypeError),  # Invalid hash type
        ("ER-4", [], TypeError),  # Invalid hash type
        ("ER-5", {}, TypeError),  # Invalid hash type
        ("ER-6", set(), TypeError),  # Invalid hash type
    ],
)
def test_calculate_federator_hash_value_error_cases(test_id, name, expected_exception):
    # Act / Assert
    with pytest.raises(expected_exception):
        result = calculate_federator_hash_value(name)


@pytest.mark.parametrize(
    "name, expected_hash, test_id",
    [
        ("A", "559aead08264d5795d3909718cdd05abd49572e84fe55590eef31a88a08fdffd", "HP1"),
        ("B", "df7e70e5021544f4834bbee64a9e3789febc4be81470df629cad6ddb03320a5c", "HP2"),
        ("GaiaX-Auditor", "8409ff3d2182ff9097181b7e0f6e5f874188e491a5ec64bb3334954315b83e4d", "HP3"),
    ],
    ids=lambda test_id: test_id,
)
def test_calculate_auditor_hash_value_happy_path(name, expected_hash, test_id):
    # Act
    result = calculate_auditor_hash_value(name)

    # Assert
    assert result == expected_hash, f"Test Failed: {test_id}"


# Test cases for edge cases
@pytest.mark.parametrize(
    "test_id, name, expected_exception",
    [
        # Edge cases
        ("EC-1", b"", TypeError),  # Empty byte string
        # Error cases
        ("ER-1", None, TypeError),  # None as input
        ("ER-2", 12, TypeError),  # Invalid hash type
        ("ER-3", True, TypeError),  # Invalid hash type
        ("ER-4", [], TypeError),  # Invalid hash type
        ("ER-5", {}, TypeError),  # Invalid hash type
        ("ER-6", set(), TypeError),  # Invalid hash type
    ],
)
def test_calculate_auditor_hash_value_error_cases(test_id, name, expected_exception):
    # Act / Assert
    with pytest.raises(expected_exception):
        result = calculate_auditor_hash_value(name)


@pytest.mark.parametrize(
    "name, expected_hash, test_id",
    [
        ("A", "559aead08264d5795d3909718cdd05abd49572e84fe55590eef31a88a08fdffd", "HP1"),
        ("B", "df7e70e5021544f4834bbee64a9e3789febc4be81470df629cad6ddb03320a5c", "HP2"),
        ("GaiaX-ServiceOffering", "dd2c9e1a3147dcc4410be9f77bbfe8a7ebd63939e8fc48b7cb3e596ce7a7bcd6", "HP3"),
    ],
    ids=lambda test_id: test_id,
)
def test_calculate_service_offering_hash_value_happy_path(name, expected_hash, test_id):
    # Act
    result = calculate_service_offering_hash_value(name)

    # Assert
    assert result == expected_hash, f"Test Failed: {test_id}"


# Test cases for edge cases
@pytest.mark.parametrize(
    "test_id, name, expected_exception",
    [
        # Edge cases
        ("EC-1", b"", TypeError),  # Empty byte string
        # Error cases
        ("ER-1", None, TypeError),  # None as input
        ("ER-2", 12, TypeError),  # Invalid hash type
        ("ER-3", True, TypeError),  # Invalid hash type
        ("ER-4", [], TypeError),  # Invalid hash type
        ("ER-5", {}, TypeError),  # Invalid hash type
        ("ER-6", set(), TypeError),  # Invalid hash type
    ],
)
def test_calculate_service_offering_hash_value_error_cases(test_id, name, expected_exception):
    # Act / Assert
    with pytest.raises(expected_exception):
        result = calculate_service_offering_hash_value(name)


@pytest.mark.parametrize(
    "name, expected_hash, test_id",
    [
        ("A", "559aead08264d5795d3909718cdd05abd49572e84fe55590eef31a88a08fdffd", "HP1"),
        ("B", "df7e70e5021544f4834bbee64a9e3789febc4be81470df629cad6ddb03320a5c", "HP2"),
        ("GaiaX-Location", "e1cfd9f0ec99776e056aa109447659c4ef6c246945565ae77cd4963653ea6d83", "HP3"),
    ],
    ids=lambda test_id: test_id,
)
def test_calculate_location_hash_value_happy_path(name, expected_hash, test_id):
    # Act
    result = calculate_location_hash_value(name)

    # Assert
    assert result == expected_hash, f"Test Failed: {test_id}"


# Test cases for edge cases
@pytest.mark.parametrize(
    "test_id, name, expected_exception",
    [
        # Edge cases
        ("EC-1", b"", TypeError),  # Empty byte string
        # Error cases
        ("ER-1", None, TypeError),  # None as input
        ("ER-2", 12, TypeError),  # Invalid hash type
        ("ER-3", True, TypeError),  # Invalid hash type
        ("ER-4", [], TypeError),  # Invalid hash type
        ("ER-5", {}, TypeError),  # Invalid hash type
        ("ER-6", set(), TypeError),  # Invalid hash type
    ],
)
def test_calculate_location_hash_value_error_cases(test_id, name, expected_exception):
    # Act / Assert
    with pytest.raises(expected_exception):
        result = calculate_location_hash_value(name)


@pytest.mark.parametrize(
    "criterion, level, expected_hash, test_id",
    [
        ("A", "1", "5fcf266633d890a92c0abea11eb8b7d5707520996570308cc5bcf1343d967cfd", "HP1"),
        ("B", "1", "73371dad9160577fe8ba54b03c2389b0dec7a1bbe74d820543923bda84dc213a", "HP2"),
        ("GaiaX-Criterion", "1", "76a39358b844e9c127c4e15cca2e5301589cd89e5ea6c0d839acd614859d1081", "HP3"),
        ("A", "2", "9f3f7174161a4321ebf7db9f9c7717e61d6265257aee8a2e03679d2e22e709b4", "HP1"),
        ("B", "2", "99dd8626af1fa2320641cc8cf02ec279f668e6928be22b171a5608e4949822dc", "HP2"),
        ("GaiaX-Criterion", "2", "683ea6af3b1dfec35cdd9ed6a8391f507eceaef0b75488ca017f45f967fd69b8", "HP3"),
        ("A", "3", "8319844bfa346f137216b4a64e0e590df5c771522f7d1589cf5c2b803e1c568c", "HP1"),
        ("B", "3", "01e259671b0f18602009d9a1471d49fd7f40dd915a59fa12f84e89d9147eb53a", "HP2"),
        ("GaiaX-Criterion", "3", "ea21c11743dcf309c01a565975962e241021caf43a3ad25f1bf43980d8fe209a", "HP3"),
    ],
    ids=lambda test_id: test_id,
)
def test_calculate_compliance_criterion_hash_value_happy_path(criterion, level, expected_hash, test_id):
    # Act
    result = calculate_compliance_criterion_hash_value(criterion, level)

    # Assert
    assert result == expected_hash, f"Test Failed: {test_id}"


# Test cases for edge cases
@pytest.mark.parametrize(
    "test_id, criterion, level, expected_exception",
    [
        # Edge cases
        ("EC-1", b"", "1", TypeError),  # Empty byte string
        ("EC-2", "Criterion", b"", TypeError),  # Empty byte string
        # Error cases
        ("ER-1", None, "1", TypeError),  # None as input
        ("ER-2", 12, "1", TypeError),  # Invalid hash type
        ("ER-3", True, "1", TypeError),  # Invalid hash type
        ("ER-4", [], "1", TypeError),  # Invalid hash type
        ("ER-5", {}, "1", TypeError),  # Invalid hash type
        ("ER-6", set(), "1", TypeError),  # Invalid hash type
        ("ER-7", None, "1", TypeError),  # None as input
        ("ER-8", 12, "1", TypeError),  # Invalid hash type
        ("ER-9", True, "1", TypeError),  # Invalid hash type
        ("ER-10", [], "1", TypeError),  # Invalid hash type
        ("ER-11", {}, "1", TypeError),  # Invalid hash type
        ("ER-12", set(), "1", TypeError),  # Invalid hash type
    ],
)
def test_calculate_compliance_criterion_hash_value_error_cases(test_id, criterion, level, expected_exception):
    # Act / Assert
    with pytest.raises(expected_exception):
        result = calculate_compliance_criterion_hash_value(criterion, level)


@pytest.mark.parametrize(
    "name, expected_hash, test_id",
    [
        ("A", "559aead08264d5795d3909718cdd05abd49572e84fe55590eef31a88a08fdffd", "HP1"),
        ("B", "df7e70e5021544f4834bbee64a9e3789febc4be81470df629cad6ddb03320a5c", "HP2"),
        ("GaiaX-Level", "137fa3410e05f4af981361b3fc888a8f4a6a152e3dba73d9d8166b08490bad3a", "HP3"),
    ],
    ids=lambda test_id: test_id,
)
def test_calculate_label_hash_value_happy_path(name, expected_hash, test_id):
    # Act
    result = calculate_compliance_label_hash_value(name)

    # Assert
    assert result == expected_hash, f"Test Failed: {test_id}"


# Test cases for edge cases
@pytest.mark.parametrize(
    "test_id, name, expected_exception",
    [
        # Edge cases
        ("EC-1", b"", TypeError),  # Empty byte string
        # Error cases
        ("ER-1", None, TypeError),  # None as input
        ("ER-2", 12, TypeError),  # Invalid hash type
        ("ER-3", True, TypeError),  # Invalid hash type
        ("ER-4", [], TypeError),  # Invalid hash type
        ("ER-5", {}, TypeError),  # Invalid hash type
        ("ER-6", set(), TypeError),  # Invalid hash type
    ],
)
def test_calculate_label_hash_value_error_cases(test_id, name, expected_exception):
    # Act / Assert
    with pytest.raises(expected_exception):
        result = calculate_compliance_label_hash_value(name)


@pytest.mark.parametrize(
    "service, location, expected_hash, test_id",
    [
        ("A", "1", "77ac3499c4b9ace3a7e85223ac926f2070c008cfa9fb1950d20c5a0b2a18e1a4", "HP1"),
        ("B", "1", "f0b14740a3c8640a3b271657d25184e8f4477620a4d65df9bdcd442fa440527b", "HP2"),
        ("GaiaX-Criterion", "1", "5d034158067bb7fc2a618be47cd9e00790efecbb4530c5f90954184a767d595e", "HP3"),
        ("A", "2", "1da9f3f16811b2bac8e9f9a8f9c1f3bf241c600eeb31f01f244cbf5cf90906fa", "HP1"),
        ("B", "2", "bc97bddf99eb060224e178cff6480d20522b69dbd96e75106aa03546b234a698", "HP2"),
        ("GaiaX-Criterion", "2", "89f0b3b4dc43a9d34f652b6d7be2499bd42f3ffe3bf84110ee81b23d71e59f23", "HP3"),
        ("A", "3", "f16260f78f59ee4f09b7f0ad821c471509e21d8c112015b999891d26fd28d1e8", "HP1"),
        ("B", "3", "6a779c742828027f7a821042e4a03d0b8769f0dc1de2d8058a1d64e510af375b", "HP2"),
        ("GaiaX-Criterion", "3", "d718e8da5bc71bf55cec23109ec7130813f7b7bb588598cde2e2892cae4eea23", "HP3"),
    ],
    ids=lambda test_id: test_id,
)
def test_calculate_service_location_hash_value_happy_path(service, location, expected_hash, test_id):
    # Act
    result = calculate_located_service_hash_value(service, location)

    # Assert
    assert result == expected_hash, f"Test Failed: {test_id}"


# Test cases for edge cases
@pytest.mark.parametrize(
    "test_id, service, location, expected_exception",
    [
        # Edge cases
        ("EC-1", b"", "1", TypeError),  # Empty byte string
        ("EC-2", "Criterion", b"", TypeError),  # Empty byte string
        # Error cases
        ("ER-1", None, "1", TypeError),  # None as input
        ("ER-2", 12, "1", TypeError),  # Invalid hash type
        ("ER-3", True, "1", TypeError),  # Invalid hash type
        ("ER-4", [], "1", TypeError),  # Invalid hash type
        ("ER-5", {}, "1", TypeError),  # Invalid hash type
        ("ER-6", set(), "1", TypeError),  # Invalid hash type
        ("ER-7", None, "1", TypeError),  # None as input
        ("ER-8", 12, "1", TypeError),  # Invalid hash type
        ("ER-9", True, "1", TypeError),  # Invalid hash type
        ("ER-10", [], "1", TypeError),  # Invalid hash type
        ("ER-11", {}, "1", TypeError),  # Invalid hash type
        ("ER-12", set(), "1", TypeError),  # Invalid hash type
    ],
)
def test_calculate_service_location_hash_value_error_cases(test_id, service, location, expected_exception):
    # Act / Assert
    with pytest.raises(expected_exception):
        result = calculate_located_service_hash_value(service, location)


@pytest.mark.parametrize(
    "title, version, expected_hash, test_id",
    [
        ("A", "1", "16a36e86f6fed5d465ff332511a0ce1a863b55d364b25a7cdaa25db19abf9648", "HP1"),
        ("B", "1", "5b950e77941d01cdf246d00b1ece546bc95234b77d98b44c9187e2733afa696a", "HP2"),
        ("GaiaX-Criterion", "1", "432f7903e17598454085280c6348606f194966a8372b1e65f451bcced60b36d5", "HP3"),
        ("A", "2", "c8361f9b468e68c86da024270e0949ce139cb704b8d7cce586681b99f3a7ea56", "HP1"),
        ("B", "2", "abdbc2b5cc2c7a519b72bf7a164c58ebf892ab0c2df6468213705cc2f0da8561", "HP2"),
        ("GaiaX-Criterion", "2", "c71fd1f1dd35979aa0d957bf8a4c1ed1fe678ee40c5171a2caedfa9e2fde2fb3", "HP3"),
        ("A", "3", "1398b376fdcce25c5a5399367e76891e85121c010ec919cc243b1a519d95bbc6", "HP1"),
        ("B", "3", "0cd20d37dbaa799d1d2f6f04adbab0b9e958b083f38e06512cdefadd20863f98", "HP2"),
        ("GaiaX-Criterion", "3", "31106491c7b8d8939fdbe9d78f9bc63db122ea82c98d0ba0361c1508c6c9c1e3", "HP3"),
    ],
    ids=lambda test_id: test_id,
)
def test_calculate_compliance_reference_hash_value_happy_path(title, version, expected_hash, test_id):
    # Act
    result = calculate_compliance_reference_hash_value(title, version)

    # Assert
    assert result == expected_hash, f"Test Failed: {test_id}"


# Test cases for edge cases
@pytest.mark.parametrize(
    "test_id, title, version, expected_exception",
    [
        # Edge cases
        ("EC-1", b"", "1", TypeError),  # Empty byte string
        ("EC-2", "Criterion", b"", TypeError),  # Empty byte string
        # Error cases
        ("ER-1", None, "1", TypeError),  # None as input
        ("ER-2", 12, "1", TypeError),  # Invalid hash type
        ("ER-3", True, "1", TypeError),  # Invalid hash type
        ("ER-4", [], "1", TypeError),  # Invalid hash type
        ("ER-5", {}, "1", TypeError),  # Invalid hash type
        ("ER-6", set(), "1", TypeError),  # Invalid hash type
        ("ER-7", None, "1", TypeError),  # None as input
        ("ER-8", 12, "1", TypeError),  # Invalid hash type
        ("ER-9", True, "1", TypeError),  # Invalid hash type
        ("ER-10", [], "1", TypeError),  # Invalid hash type
        ("ER-11", {}, "1", TypeError),  # Invalid hash type
        ("ER-12", set(), "1", TypeError),  # Invalid hash type
    ],
)
def test_calculate_compliance_reference_hash_value_error_cases(test_id, title, version, expected_exception):
    # Act / Assert
    with pytest.raises(expected_exception):
        result = calculate_compliance_reference_hash_value(title, version)


@pytest.mark.parametrize(
    "name, expected_hash, test_id",
    [
        ("A", "559aead08264d5795d3909718cdd05abd49572e84fe55590eef31a88a08fdffd", "HP1"),
        ("B", "df7e70e5021544f4834bbee64a9e3789febc4be81470df629cad6ddb03320a5c", "HP2"),
        ("GaiaX-Scheme", "1a1a1230b302c27a31a3828a5f2ccb0d787284a41795f56f9d251cd9a0427e9e", "HP3"),
    ],
    ids=lambda test_id: test_id,
)
def test_calculate_compliance_certification_scheme_hash_value_happy_path(name, expected_hash, test_id):
    # Act
    result = calculate_compliance_certification_scheme_hash_value(name)

    # Assert
    assert result == expected_hash, f"Test Failed: {test_id}"


# Test cases for edge cases
@pytest.mark.parametrize(
    "test_id, name, expected_exception",
    [
        # Edge cases
        ("EC-1", b"", TypeError),  # Empty byte string
        # Error cases
        ("ER-1", None, TypeError),  # None as input
        ("ER-2", 12, TypeError),  # Invalid hash type
        ("ER-3", True, TypeError),  # Invalid hash type
        ("ER-4", [], TypeError),  # Invalid hash type
        ("ER-5", {}, TypeError),  # Invalid hash type
        ("ER-6", set(), TypeError),  # Invalid hash type
    ],
)
def test_calculate_compliance_certification_scheme_hash_value_error_cases(test_id, name, expected_exception):
    # Act / Assert
    with pytest.raises(expected_exception):
        result = calculate_compliance_certification_scheme_hash_value(name)


@pytest.mark.parametrize(
    "located_service_object_id, scheme_object_id, expected_hash, test_id",
    [
        ("A", "1", "16a36e86f6fed5d465ff332511a0ce1a863b55d364b25a7cdaa25db19abf9648", "HP1"),
        ("B", "1", "5b950e77941d01cdf246d00b1ece546bc95234b77d98b44c9187e2733afa696a", "HP2"),
        ("GaiaX-Criterion", "1", "432f7903e17598454085280c6348606f194966a8372b1e65f451bcced60b36d5", "HP3"),
        ("A", "2", "c8361f9b468e68c86da024270e0949ce139cb704b8d7cce586681b99f3a7ea56", "HP1"),
        ("B", "2", "abdbc2b5cc2c7a519b72bf7a164c58ebf892ab0c2df6468213705cc2f0da8561", "HP2"),
        ("GaiaX-Criterion", "2", "c71fd1f1dd35979aa0d957bf8a4c1ed1fe678ee40c5171a2caedfa9e2fde2fb3", "HP3"),
        ("A", "3", "1398b376fdcce25c5a5399367e76891e85121c010ec919cc243b1a519d95bbc6", "HP1"),
        ("B", "3", "0cd20d37dbaa799d1d2f6f04adbab0b9e958b083f38e06512cdefadd20863f98", "HP2"),
        ("GaiaX-Criterion", "3", "31106491c7b8d8939fdbe9d78f9bc63db122ea82c98d0ba0361c1508c6c9c1e3", "HP3"),
    ],
    ids=lambda test_id: test_id,
)
def test_calculate_compliance_certification_claim_hash_value_happy_path(
    located_service_object_id, scheme_object_id, expected_hash, test_id
):
    # Act
    result = calculate_compliance_certificate_claim_hash_value(located_service_object_id, scheme_object_id)

    # Assert
    assert result == expected_hash, f"Test Failed: {test_id}"


# Test cases for edge cases
@pytest.mark.parametrize(
    "test_id, located_service_object_id, scheme_object_id, expected_exception",
    [
        # Edge cases
        ("EC-1", b"", "1", TypeError),  # Empty byte string
        ("EC-2", "Criterion", b"", TypeError),  # Empty byte string
        # Error cases
        ("ER-1", None, "1", TypeError),  # None as input
        ("ER-2", 12, "1", TypeError),  # Invalid hash type
        ("ER-3", True, "1", TypeError),  # Invalid hash type
        ("ER-4", [], "1", TypeError),  # Invalid hash type
        ("ER-5", {}, "1", TypeError),  # Invalid hash type
        ("ER-6", set(), "1", TypeError),  # Invalid hash type
        ("ER-7", None, "1", TypeError),  # None as input
        ("ER-8", 12, "1", TypeError),  # Invalid hash type
        ("ER-9", True, "1", TypeError),  # Invalid hash type
        ("ER-10", [], "1", TypeError),  # Invalid hash type
        ("ER-11", {}, "1", TypeError),  # Invalid hash type
        ("ER-12", set(), "1", TypeError),  # Invalid hash type
    ],
)
def test_calculate_compliance_certification_claim_hash_value_error_cases(
    test_id, located_service_object_id, scheme_object_id, expected_exception
):
    # Act / Assert
    with pytest.raises(expected_exception):
        result = calculate_compliance_certificate_claim_hash_value(located_service_object_id, scheme_object_id)


@pytest.mark.parametrize(
    "located_service_object_id, scheme_object_id, expected_hash, test_id",
    [
        ("A", "1", "16a36e86f6fed5d465ff332511a0ce1a863b55d364b25a7cdaa25db19abf9648", "HP1"),
        ("B", "1", "5b950e77941d01cdf246d00b1ece546bc95234b77d98b44c9187e2733afa696a", "HP2"),
        ("GaiaX-Criterion", "1", "432f7903e17598454085280c6348606f194966a8372b1e65f451bcced60b36d5", "HP3"),
        ("A", "2", "c8361f9b468e68c86da024270e0949ce139cb704b8d7cce586681b99f3a7ea56", "HP1"),
        ("B", "2", "abdbc2b5cc2c7a519b72bf7a164c58ebf892ab0c2df6468213705cc2f0da8561", "HP2"),
        ("GaiaX-Criterion", "2", "c71fd1f1dd35979aa0d957bf8a4c1ed1fe678ee40c5171a2caedfa9e2fde2fb3", "HP3"),
        ("A", "3", "1398b376fdcce25c5a5399367e76891e85121c010ec919cc243b1a519d95bbc6", "HP1"),
        ("B", "3", "0cd20d37dbaa799d1d2f6f04adbab0b9e958b083f38e06512cdefadd20863f98", "HP2"),
        ("GaiaX-Criterion", "3", "31106491c7b8d8939fdbe9d78f9bc63db122ea82c98d0ba0361c1508c6c9c1e3", "HP3"),
    ],
    ids=lambda test_id: test_id,
)
def test_calculate_third_party_compliance_certification_claim_hash_value_happy_path(
    located_service_object_id, scheme_object_id, expected_hash, test_id
):
    # Act
    result = calculate_third_party_compliance_certificate_claim_hash_value(located_service_object_id, scheme_object_id)

    # Assert
    assert result == expected_hash, f"Test Failed: {test_id}"


# Test cases for edge cases
@pytest.mark.parametrize(
    "test_id, located_service_object_id, scheme_object_id, expected_exception",
    [
        # Edge cases
        ("EC-1", b"", "1", TypeError),  # Empty byte string
        ("EC-2", "Criterion", b"", TypeError),  # Empty byte string
        # Error cases
        ("ER-1", None, "1", TypeError),  # None as input
        ("ER-2", 12, "1", TypeError),  # Invalid hash type
        ("ER-3", True, "1", TypeError),  # Invalid hash type
        ("ER-4", [], "1", TypeError),  # Invalid hash type
        ("ER-5", {}, "1", TypeError),  # Invalid hash type
        ("ER-6", set(), "1", TypeError),  # Invalid hash type
        ("ER-7", None, "1", TypeError),  # None as input
        ("ER-8", 12, "1", TypeError),  # Invalid hash type
        ("ER-9", True, "1", TypeError),  # Invalid hash type
        ("ER-10", [], "1", TypeError),  # Invalid hash type
        ("ER-11", {}, "1", TypeError),  # Invalid hash type
        ("ER-12", set(), "1", TypeError),  # Invalid hash type
    ],
)
def test_calculate_third_party_compliance_certification_claim_hash_value_error_cases(
    test_id, located_service_object_id, scheme_object_id, expected_exception
):
    # Act / Assert
    with pytest.raises(expected_exception):
        result = calculate_third_party_compliance_certificate_claim_hash_value(
            located_service_object_id, scheme_object_id
        )


@pytest.mark.parametrize(
    "name, expected_hash, test_id",
    [
        (["A"], "7651b17ff0fbdd2848ec9526ab8c632e2f570ce5616ee1651adbea5683ec2f77", "HP1"),
        (["B"], "84c7bd663db2dcba6f04ca4e660efe2441b73bfd315bb96a7c7f2dd8feb36677", "HP2"),
        (["GaiaX-Scheme"], "5d624a0870bd9b551e4c9ee7e719163ee5bb13021a18d5c17c349b066d0a9f8a", "HP3"),
        (["A", "B"], "03d856e68a2482e34227a824aab23fe3801efae02ea3e8feedde2f1e08c09904", "HP4"),
        (["B", "A"], "b963ecafb2b33d69782fe1ff79be1e9d44326d51412762d31cf4ac518f90128d", "HP5"),
        (["GaiaX-Scheme", "A"], "3f076a81c807ebef01eb52e6425bfe0981b04258c46210ed0dd000d705056659", "HP6"),
    ],
    ids=lambda test_id: test_id,
)
def test_calculate_self_assessed_certificate_claim_hash_value_happy_path(name, expected_hash, test_id):
    # Act
    result = calculate_self_assessed_certificate_claim_hash_value(name)

    # Assert
    assert result == expected_hash, f"Test Failed: {test_id}"


# Test cases for edge cases
@pytest.mark.parametrize(
    "test_id, name, expected_exception",
    [
        # Edge cases
        ("EC-1", [], TypeError),  # Empty byte string
        # Error cases
        ("ER-1", None, TypeError),  # None as input
        ("ER-2", 12, TypeError),  # Invalid hash type
        ("ER-3", True, TypeError),  # Invalid hash type
        ("ER-4", {}, TypeError),  # Invalid hash type
        ("ER-5", set(), TypeError),  # Invalid hash type
        ("ER-6", [None], TypeError),  # Invalid hash type
        ("ER-7", [12], TypeError),  # Invalid hash type
        ("ER-8", [True], TypeError),  # Invalid hash type
        ("ER-9", [{}], TypeError),  # Invalid hash type
        ("ER-10", [[]], TypeError),  # Invalid hash type
        ("ER-11", [None, "A"], TypeError),  # Invalid hash type
        ("ER-12", [12, "A"], TypeError),  # Invalid hash type
        ("ER-13", [True, "A"], TypeError),  # Invalid hash type
        ("ER-14", [{}, "A"], TypeError),  # Invalid hash type
        ("ER-15", [[], "A"], TypeError),  # Invalid hash type
    ],
)
def test_calculate_self_assessed_certificate_claim_hash_value_error_cases(test_id, name, expected_exception):
    # Act / Assert
    with pytest.raises(expected_exception):
        result = calculate_self_assessed_certificate_claim_hash_value(name)


# Happy path tests with various realistic test values
@pytest.mark.parametrize(
    "object_id, expected, test_id",
    [
        ("https://example.com/data.json", "example.com/", "HP_1"),
        ("https://example.com/subdir/data.json", "example.com/subdir/", "HP_2"),
        ("https://example.com/subdir/subsubdir/data.json", "example.com/subdir/subsubdir/", "HP_3"),
    ],
)
def test_extract_directory_from_object_id_happy_path(object_id, expected, test_id):
    # Act
    result = extract_directory_from_object_id(object_id)

    # Assert
    assert result == expected, f"Test Failed: {test_id}"


# Edge cases
@pytest.mark.parametrize(
    "object_id, expected, test_id",
    [
        ("https://example.com/data.json/data.json", "example.com/data.json/", "EC_1"),
        ("https://example.com//data.json", "example.com//", "EC_2"),
    ],
)
def test_extract_directory_from_object_id_edge_cases(object_id, expected, test_id):
    # Act
    result = extract_directory_from_object_id(object_id)

    # Assert
    assert result == expected


# Error cases
@pytest.mark.parametrize(
    "object_id, error_message, test_id",
    [
        (None, "object_id must be not None", "EC_1"),
        (12, "object_id must be a string starting with https://", "EC_2"),
        (True, "object_id must be a string starting with https://", "EC_3"),
        ([], "object_id must be a string starting with https://", "EC_4"),
        ({}, "object_id must be a string starting with https://", "EC_5"),
        (set(), "object_id must be a string starting with https://", "EC_6"),
        ("example.com/data.json", "example.com/data.json is not a valid did web. It must start with https://", "EC_7"),
    ],
)
def test_extract_directory_from_object_id_error_cases(object_id, error_message, test_id):
    # Act & Assert
    with pytest.raises(ValueError) as exc_info:
        extract_directory_from_object_id(object_id)

    # Assert
    assert str(exc_info.value) == error_message


@pytest.mark.parametrize(
    "object_id, expected, test_id",
    [
        ("https://example.com/data.json", "example.com/data.json", "HP_1"),
        ("https://example.com/subdir:data.json", "example.com/subdir/data.json", "HP_2"),
        ("https://example.com/subdir/subsubdir:data.json", "example.com/subdir/subsubdir/data.json", "HP_3"),
    ],
)
def test_extract_file_path_from_object_id_happy_path(object_id, expected, test_id):
    # Act
    result = extract_file_path_from_object_id(object_id)

    # Assert
    assert result == expected, f"Test Failed: {test_id}"


# Edge cases
@pytest.mark.parametrize(
    "object_id, expected, test_id",
    [
        ("https://example.com/data.json/data.json", "example.com/data.json/data.json", "EC_1"),
        ("https://example.com//data.json", "example.com//data.json", "EC_2"),
    ],
)
def test_extract_file_path_from_object_id_edge_cases(object_id, expected, test_id):
    # Act
    result = extract_file_path_from_object_id(object_id)

    # Assert
    assert result == expected


# Error cases
@pytest.mark.parametrize(
    "object_id, error_message, test_id",
    [
        (None, "object_id must be not None", "EC_1"),
        (12, "object_id must be a string starting with https://", "EC_2"),
        (True, "object_id must be a string starting with https://", "EC_3"),
        ([], "object_id must be a string starting with https://", "EC_4"),
        ({}, "object_id must be a string starting with https://", "EC_5"),
        (set(), "object_id must be a string starting with https://", "EC_6"),
        ("example.com/data.json", "example.com/data.json is not a valid did web. It must start with https://", "EC_7"),
    ],
)
def test_extract_file_path_from_object_id_error_cases(object_id, error_message, test_id):
    # Act & Assert
    with pytest.raises(ValueError) as exc_info:
        extract_file_path_from_object_id(object_id)

    # Assert
    assert str(exc_info.value) == error_message


@pytest.mark.parametrize(
    "object_id, expected, test_id",
    [
        ("https://example.com/vc/data.json", "vc/data.json", "HP_1"),
        ("https://example.com/vc/subdir/data.json", "vc/subdir/data.json", "HP_2"),
        ("https://example.com/vc/subdir/subsubdir/data.json", "vc/subdir/subsubdir/data.json", "HP_3"),
    ],
)
def test_file_path_from_object_id_without_participant_happy_path(object_id, expected, test_id):
    # Act
    result = extract_file_path_from_object_id_without_participant_part(object_id, "vc")

    # Assert
    assert result == expected, f"Test Failed: {test_id}"


# Edge cases
@pytest.mark.parametrize(
    "object_id, expected, test_id",
    [
        ("https://example.com/data.json", "example.com/data.json", "EC_1"),
        ("https://example.com/vc/subdir:data.json", "vc/subdir/data.json", "EC_2"),
        ("https://example.com/vc/subdir/subsubdir/data.json", "vc/subdir/subsubdir/data.json", "EC_3"),
    ],
)
def test_extract_directory_from_object_id_edge_cases(object_id, expected, test_id):
    # Act
    result = extract_file_path_from_object_id_without_participant_part(object_id, "vc")

    # Assert
    assert result == expected


# Error cases
@pytest.mark.parametrize(
    "object_id, error_message, test_id",
    [
        (None, "object_id must be not None", "EC_1"),
        (12, "object_id must be a string starting with https://", "EC_2"),
        (True, "object_id must be a string starting with https://", "EC_3"),
        ([], "object_id must be a string starting with https://", "EC_4"),
        ({}, "object_id must be a string starting with https://", "EC_5"),
        (set(), "object_id must be a string starting with https://", "EC_6"),
        ("example.com/data.json", "example.com/data.json is not a valid did web. It must start with https://", "EC_7"),
    ],
)
def test_file_path_from_object_id_without_participant_error_cases(object_id, error_message, test_id):
    # Act & Assert
    with pytest.raises(ValueError) as exc_info:
        extract_file_path_from_object_id_without_participant_part(object_id, "vc")

    # Assert
    assert str(exc_info.value) == error_message


@pytest.mark.parametrize(
    "object_id, expected, test_id",
    [
        ("https://example.com/vc/data.json", "https://example.com/vc/data.json/.well-known", "HP_1"),
        ("https://example.com/vc/subdir/data.json", "https://example.com/vc/subdir/data.json/.well-known", "HP_2"),
        (
            "https://example.com/vc/subdir/subsubdir/data.json",
            "https://example.com/vc/subdir/subsubdir/data.json/.well-known",
            "HP_3",
        ),
    ],
)
def test_get_well_known_root_object_id_happy_path(object_id, expected, test_id):
    # Act
    result = get_well_known_root_object_id(object_id)

    # Assert
    assert result == expected, f"Test Failed: {test_id}"


# Edge cases
@pytest.mark.parametrize(
    "object_id, expected, test_id",
    [
        ("https://example.com/data.json", "https://example.com/data.json/.well-known", "EC_1"),
        ("https://example.com/vc/subdir:data.json", "https://example.com/vc/subdir:data.json/.well-known", "EC_2"),
        (
            "https://example.com/vc/subdir/subsubdir:data.json",
            "https://example.com/vc/subdir/subsubdir:data.json/.well-known",
            "EC_3",
        ),
    ],
)
def test_get_well_known_root_object_id_edge_cases(object_id, expected, test_id):
    # Act
    result = get_well_known_root_object_id(object_id)

    # Assert
    assert result == expected


# Error cases
@pytest.mark.parametrize(
    "object_id, error_message, test_id",
    [
        (None, "object_id must be not None", "EC_1"),
        (12, "object_id must be a string starting with https://", "EC_2"),
        (True, "object_id must be a string starting with https://", "EC_3"),
        ([], "object_id must be a string starting with https://", "EC_4"),
        ({}, "object_id must be a string starting with https://", "EC_5"),
        (set(), "object_id must be a string starting with https://", "EC_6"),
        ("example.com/data.json", "example.com/data.json is not a valid did web. It must start with https://", "EC_7"),
    ],
)
def test_get_well_known_root_object_id_error_cases(object_id, error_message, test_id):
    # Act & Assert
    with pytest.raises(ValueError) as exc_info:
        get_well_known_root_object_id(object_id)

    # Assert
    assert str(exc_info.value) == error_message


@pytest.mark.parametrize(
    "participant_name, parent_domain_name, expected_value, test_id",
    [
        ("A", "1", "A.1", "HP1"),
        ("B", "1", "B.1", "HP2"),
    ],
    ids=lambda test_id: test_id,
)
def test_get_participant_folder_happy_path(participant_name, parent_domain_name, expected_value, test_id):
    # Act
    result = get_participant_folder(participant_name, parent_domain_name)

    # Assert
    assert result == expected_value, f"Test Failed: {test_id}"


# Test cases for edge cases
@pytest.mark.parametrize(
    "test_id, participant_name, parent_domain_name, expected_exception",
    [
        # Edge cases
        ("EC-1", b"", "1", TypeError),  # Empty byte string
        ("EC-2", "Criterion", b"", TypeError),  # Empty byte string
        # Error cases
        ("ER-1", None, "1", TypeError),  # None as input
        ("ER-2", 12, "1", TypeError),  # Invalid hash type
        ("ER-3", True, "1", TypeError),  # Invalid hash type
        ("ER-4", [], "1", TypeError),  # Invalid hash type
        ("ER-5", {}, "1", TypeError),  # Invalid hash type
        ("ER-6", set(), "1", TypeError),  # Invalid hash type
        ("ER-7", None, "1", TypeError),  # None as input
        ("ER-8", 12, "1", TypeError),  # Invalid hash type
        ("ER-9", True, "1", TypeError),  # Invalid hash type
        ("ER-10", [], "1", TypeError),  # Invalid hash type
        ("ER-11", {}, "1", TypeError),  # Invalid hash type
        ("ER-12", set(), "1", TypeError),  # Invalid hash type
    ],
)
def test_get_participant_folder_error_cases(test_id, participant_name, parent_domain_name, expected_exception):
    # Act / Assert
    with pytest.raises(expected_exception):
        result = get_participant_folder(participant_name, parent_domain_name)


@pytest.mark.parametrize(
    "object_id, expected, test_id",
    [
        ("https://example.com/data.json", "example.com", "HP_1"),
        ("https://example.com/subdir/data.json", "example.com", "HP_2"),
    ],
)
def test_get_root_url_from_object_id_happy_path(object_id, expected, test_id):
    # Act
    result = get_root_url_from_object_id(object_id)

    # Assert
    assert result == expected, f"Test Failed: {test_id}"


# Edge cases
@pytest.mark.parametrize(
    "object_id, expected, test_id",
    [
        ("https://example.com/vc/subdir/data.json", "example.com", "EC_1"),
        (
            "https://example.com/vc/subdir/subsubdir:data.json",
            "example.com",
            "EC_2",
        ),
    ],
)
def test_get_root_url_from_object_id_edge_cases(object_id, expected, test_id):
    # Act
    result = get_root_url_from_object_id(object_id)

    # Assert
    assert result == expected


# Error cases
@pytest.mark.parametrize(
    "object_id, error_message, test_id",
    [
        (None, "object_id must be not None", "EC_1"),
        (12, "object_id must be a string starting with https://", "EC_2"),
        (True, "object_id must be a string starting with https://", "EC_3"),
        ([], "object_id must be a string starting with https://", "EC_4"),
        ({}, "object_id must be a string starting with https://", "EC_5"),
        (set(), "object_id must be a string starting with https://", "EC_6"),
        ("example.com/data.json", "example.com/data.json is not a valid did web. It must start with https://", "EC_7"),
    ],
)
def test_get_root_url_from_object_id_error_cases(object_id, error_message, test_id):
    # Act & Assert
    with pytest.raises(ValueError) as exc_info:
        get_root_url_from_object_id(object_id)

    # Assert
    assert str(exc_info.value) == error_message
