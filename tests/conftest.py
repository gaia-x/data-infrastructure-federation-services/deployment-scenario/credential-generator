# -*- coding: utf-8 -*-
# Standard Library
import asyncio
import json
import os
import shutil
from pathlib import Path
from typing import List

import pytest

from credential_generator.infra.spi.database.sqlite.repository import SQLiteRepository


@pytest.fixture(scope="session")
def event_loop():
    loop = asyncio.get_event_loop()
    yield loop
    loop.close()


@pytest.fixture(scope="session")
def anyio_backend():
    return "asyncio"


@pytest.fixture(scope="session")
def create_directory(tmp_path_factory) -> Path:
    """
    Fixture that creates directories in the tmp_path/tmp directory,
    and returns the directory.
    """
    tmpdir = tmp_path_factory.mktemp("tmp", numbered=True)

    yield tmpdir

    shutil.rmtree(str(tmpdir))


@pytest.fixture(scope="function", autouse=True)
def create_files(create_directory) -> Path:
    """Fixture that creates files in the tmp_path/tmp directory,
    writes something stuff, then and returns the directory."""

    parent = Path(create_directory, "txt")
    parent.mkdir(parents=True, exist_ok=True)

    for filename in ("foo.txt", "bar.txt", "baz.txt"):
        file = Path(create_directory, "txt", filename)
        file.write_text(filename)

    yield parent


@pytest.fixture(scope="function", autouse=True)
def create_directory_hierarchy(create_directory) -> List[str]:
    """Fixture that creates files in the tmp_path/tmp directory,
    writes something stuff, then and returns the directory."""

    txt_parent = Path(create_directory, "txt")
    txt_parent.mkdir(parents=True, exist_ok=True)

    json_parent = Path(create_directory, "json")
    json_parent.mkdir(parents=True, exist_ok=True)

    for filename in ("foo.txt", "bar.txt", "baz.txt"):
        file = Path(txt_parent, filename)
        file.write_text(filename)

    for filename in ("foo.json", "bar.json", "baz.json"):
        file = Path(json_parent, filename)
        file.write_text(json.dumps({"name": filename}))

    yield [str(txt_parent.absolute()), str(json_parent.absolute())]


@pytest.fixture(scope="function")
def participant_name_environment_value(request, monkeypatch):
    """Fixture that set environment variable PARTICIPANT_NAME"""
    envs = {"PARTICIPANT_NAME": request.param}
    monkeypatch.setattr(os, "environ", envs)

    print(request.param)
    yield (request.param, "ovhcloud" if request.param is None else request.param)


@pytest.fixture(scope="function")
def sqlite_repo(tmp_path):
    # Arrange
    db_path = tmp_path / "test_database.db"
    repo = SQLiteRepository(database_path=db_path)
    repo.create_database()
    yield repo

    shutil.rmtree(db_path)
