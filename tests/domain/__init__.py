# -*- coding: utf-8 -*-
# Standard Library

import pytest

# data-initialiser Library
from credential_generator.domain import (
    ComplianceLevel,
    CriterionCategory,
    UnknownComplianceLevelError,
    UnknownCriterionCategoryError,
)


@pytest.mark.parametrize(
    "value, expected_value",
    [
        ("Level 1", ComplianceLevel.LEVEL_1),
        ("level 1", ComplianceLevel.LEVEL_1),
        ("LEVEL 1", ComplianceLevel.LEVEL_1),
        ("LEVel 1", ComplianceLevel.LEVEL_1),
        ("Level 2", ComplianceLevel.LEVEL_2),
        ("level 2", ComplianceLevel.LEVEL_2),
        ("LEVEL 2", ComplianceLevel.LEVEL_2),
        ("LEVel 2", ComplianceLevel.LEVEL_2),
        ("Level 3", ComplianceLevel.LEVEL_3),
        ("level 3", ComplianceLevel.LEVEL_3),
        ("LEVEL 3", ComplianceLevel.LEVEL_3),
        ("LEVel 3", ComplianceLevel.LEVEL_3),
    ],
)
def test_compliance_level_from_str_should_create_known_level(value, expected_value):
    # Given
    # Then
    level = ComplianceLevel.from_str(value)

    # Assert
    assert expected_value == level


@pytest.mark.parametrize(
    "value",
    [
        "Level 1 ",
        " level 1",
        "2",
        "",
        None,
    ],
)
def test_compliance_level_from_str_should_create_raise_error_when_unknown_level(value):
    # Given
    # Then
    with pytest.raises(UnknownComplianceLevelError):
        ComplianceLevel.from_str(value)


@pytest.mark.parametrize(
    "value, expected_value",
    [
        ("Contractual governance", CriterionCategory.CONTRACTUAL_GOVERNANCE),
        ("contractual governance", CriterionCategory.CONTRACTUAL_GOVERNANCE),
        ("CONTRACTUAL GOVERNANCE", CriterionCategory.CONTRACTUAL_GOVERNANCE),
        ("Transparency", CriterionCategory.TRANSPARENCY),
        ("transparency", CriterionCategory.TRANSPARENCY),
        ("TRANSPARENCY", CriterionCategory.TRANSPARENCY),
        ("Data Protection", CriterionCategory.DATA_PROTECTION),
        ("Data protection", CriterionCategory.DATA_PROTECTION),
        ("DATA PROTECTION", CriterionCategory.DATA_PROTECTION),
        ("Security", CriterionCategory.SECURITY),
        ("security", CriterionCategory.SECURITY),
        ("SECURITY", CriterionCategory.SECURITY),
        ("Portability", CriterionCategory.PORTABILITY),
        ("portability", CriterionCategory.PORTABILITY),
        ("PORTABILITY", CriterionCategory.PORTABILITY),
        ("European Control", CriterionCategory.EUROPEAN_CONTROL),
        ("european Control", CriterionCategory.EUROPEAN_CONTROL),
        ("EUROPEAN CONTROL", CriterionCategory.EUROPEAN_CONTROL),
    ],
)
def test_criterion_category_from_str_should_create_known_category(value, expected_value):
    # Given
    # Then
    level = CriterionCategory.from_str(value)

    # Assert
    assert expected_value == level


@pytest.mark.parametrize(
    "value",
    [
        "Level 1 ",
        " level 1",
        "2",
        "",
        None,
    ],
)
def test_criterion_category_from_str_should_create_raise_error_when_unknown_category(value):
    # Given
    # Then
    with pytest.raises(UnknownCriterionCategoryError):
        CriterionCategory.from_str(value)
