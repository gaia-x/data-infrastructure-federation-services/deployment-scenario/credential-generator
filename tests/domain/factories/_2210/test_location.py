# -*- coding: utf-8 -*-
import pytest
from credential_generator.domain._2210 import GaiaxObjectWithDidWeb
from credential_generator.domain._2210.gxfs.gx_participant import Location
from credential_generator.domain.factories._2210.gxfs.participant import create_location


from credential_generator.domain.factories._2210.gxfs.__init__ import get_location_did, get_location_vc_did


class TestGetLocationDid:
    #  Returns a valid location DID when given valid input parameters.
    def test_valid_location_did(self):
        # Given
        parent_domain_name = "example.com"
        provider_name = "provider1"
        location_id = "12345"

        # When
        location_did = get_location_did(parent_domain_name, provider_name, location_id)

        # Then
        assert (
            location_did
            == "https://provider1.provider.example.com/location-json/5994471abb01112afcc18159f6cc74b4f511b99806da59b3caf5a9c173cacfc5/data.json"
        )

    #  Returns a location DID with the correct provider name and parent domain name.
    def test_correct_provider_and_parent_domain_name(self):
        # Given
        parent_domain_name = "example.com"
        provider_name = "provider1"
        location_id = "12345"

        # When
        location_did = get_location_did(parent_domain_name, provider_name, location_id)

        # Then
        assert "provider1.provider.example.com" in location_did

    #  Returns a location DID with the correct ontology type.
    def test_correct_ontology_type(self):
        # Given
        parent_domain_name = "example.com"
        provider_name = "provider1"
        location_id = "12345"

        # When
        location_did = get_location_did(parent_domain_name, provider_name, location_id)

        # Then
        assert "location" in location_did

    #  Returns a location DID with the correct data.json file path.
    def test_correct_data_json_file_path(self):
        # Given
        parent_domain_name = "example.com"
        provider_name = "provider1"
        location_id = "12345"

        # When
        location_did = get_location_did(parent_domain_name, provider_name, location_id)

        # Then
        assert "/data.json" in location_did

    #  Returns a location DID with the correct location hash value.
    def test_correct_location_hash_value(self):
        # Given
        parent_domain_name = "example.com"
        provider_name = "provider1"
        location_id = "12345"

        # When
        location_did = get_location_did(parent_domain_name, provider_name, location_id)

        # Then
        assert "5994471abb01112afcc18159f6cc74b4f511b99806da59b3caf5a9c173cacfc5" in location_did

    #  Raises a TypeError if parent_domain_name is None.
    def test_parent_domain_name_is_none(self):
        # Given
        parent_domain_name = None
        provider_name = "provider1"
        location_id = "12345"

        # When/Then
        with pytest.raises(TypeError):
            get_location_did(parent_domain_name, provider_name, location_id)

    #  Raises a TypeError if provider_name is None.
    def test_provider_name_is_none(self):
        # Given
        parent_domain_name = "example.com"
        provider_name = None
        location_id = "12345"

        # When/Then
        with pytest.raises(TypeError):
            get_location_did(parent_domain_name, provider_name, location_id)

    #  Raises a TypeError if location_id is None.
    def test_location_id_is_none(self):
        # Given
        parent_domain_name = "example.com"
        provider_name = "provider1"
        location_id = None

        # When/Then
        with pytest.raises(TypeError):
            get_location_did(parent_domain_name, provider_name, location_id)

    #  Raises a TypeError if parent_domain_name is an empty string.
    def test_parent_domain_name_is_empty_string(self):
        # Given
        parent_domain_name = ""
        provider_name = "provider1"
        location_id = "12345"

        # When/Then
        with pytest.raises(ValueError):
            get_location_did(parent_domain_name, provider_name, location_id)

    #  Raises a TypeError if provider_name is an empty string.
    def test_provider_name_is_empty_string(self):
        # Given
        parent_domain_name = "example.com"
        provider_name = ""
        location_id = "12345"

        # When/Then
        with pytest.raises(ValueError):
            get_location_did(parent_domain_name, provider_name, location_id)

    #  Raises a TypeError if location_id is an empty string.
    def test_raises_type_error_if_location_id_is_empty_string(self):
        # Given
        parent_domain_name = "example.com"
        provider_name = "provider1"
        location_id = ""

        # When, Then
        with pytest.raises(ValueError):
            get_location_did(parent_domain_name, provider_name, location_id)


class TestGetLocationVCId:
    #  Returns a valid location DID when given valid input parameters.
    def test_valid_location_did(self):
        # Given
        parent_domain_name = "example.com"
        provider_name = "provider1"
        location_id = "12345"

        # When
        location_did = get_location_vc_did(parent_domain_name, provider_name, location_id)

        # Then
        assert (
            location_did
            == "https://provider1.provider.example.com/location/5994471abb01112afcc18159f6cc74b4f511b99806da59b3caf5a9c173cacfc5/data.json"
        )

    #  Returns a location DID with the correct provider name and parent domain name.
    def test_correct_provider_and_parent_domain_name(self):
        # Given
        parent_domain_name = "example.com"
        provider_name = "provider1"
        location_id = "12345"

        # When
        location_did = get_location_vc_did(parent_domain_name, provider_name, location_id)

        # Then
        assert "provider1.provider.example.com" in location_did

    #  Returns a location DID with the correct ontology type.
    def test_correct_ontology_type(self):
        # Given
        parent_domain_name = "example.com"
        provider_name = "provider1"
        location_id = "12345"

        # When
        location_did = get_location_vc_did(parent_domain_name, provider_name, location_id)

        # Then
        assert "location" in location_did

    #  Returns a location DID with the correct data.json file path.
    def test_correct_data_json_file_path(self):
        # Given
        parent_domain_name = "example.com"
        provider_name = "provider1"
        location_id = "12345"

        # When
        location_did = get_location_vc_did(parent_domain_name, provider_name, location_id)

        # Then
        assert "/data.json" in location_did

    #  Returns a location DID with the correct location hash value.
    def test_correct_location_hash_value(self):
        # Given
        parent_domain_name = "example.com"
        provider_name = "provider1"
        location_id = "12345"

        # When
        location_did = get_location_vc_did(parent_domain_name, provider_name, location_id)

        # Then
        assert "5994471abb01112afcc18159f6cc74b4f511b99806da59b3caf5a9c173cacfc5" in location_did

    #  Raises a TypeError if parent_domain_name is None.
    def test_parent_domain_name_is_none(self):
        # Given
        parent_domain_name = None
        provider_name = "provider1"
        location_id = "12345"

        # When/Then
        with pytest.raises(TypeError):
            get_location_vc_did(parent_domain_name, provider_name, location_id)

    #  Raises a TypeError if provider_name is None.
    def test_provider_name_is_none(self):
        # Given
        parent_domain_name = "example.com"
        provider_name = None
        location_id = "12345"

        # When/Then
        with pytest.raises(TypeError):
            get_location_vc_did(parent_domain_name, provider_name, location_id)

    #  Raises a TypeError if location_id is None.
    def test_location_id_is_none(self):
        # Given
        parent_domain_name = "example.com"
        provider_name = "provider1"
        location_id = None

        # When/Then
        with pytest.raises(TypeError):
            get_location_vc_did(parent_domain_name, provider_name, location_id)

    #  Raises a TypeError if parent_domain_name is an empty string.
    def test_parent_domain_name_is_empty_string(self):
        # Given
        parent_domain_name = ""
        provider_name = "provider1"
        location_id = "12345"

        # When/Then
        with pytest.raises(ValueError):
            get_location_vc_did(parent_domain_name, provider_name, location_id)

    #  Raises a TypeError if provider_name is an empty string.
    def test_provider_name_is_empty_string(self):
        # Given
        parent_domain_name = "example.com"
        provider_name = ""
        location_id = "12345"

        # When/Then
        with pytest.raises(ValueError):
            get_location_vc_did(parent_domain_name, provider_name, location_id)

    #  Raises a TypeError if location_id is an empty string.
    def test_raises_type_error_if_location_id_is_empty_string(self):
        # Given
        parent_domain_name = "example.com"
        provider_name = "provider1"
        location_id = ""

        # When, Then
        with pytest.raises(ValueError):
            get_location_vc_did(parent_domain_name, provider_name, location_id)


class TestCreateLocation:
    #  Should create a Location object with all required attributes and valid values.
    def test_create_location_with_required_attributes_and_valid_values(self):
        # Given
        parent_domain_name = "example.com"
        provider_name = "ABC"
        location_id = "12345"
        country = "France"
        state = "Nord"
        urban_area = "Roubaix"
        provider_designation = "XYZ"
        service_ids = ["SER-01", "SER-02"]

        # When
        location = create_location(
            parent_domain_name=parent_domain_name,
            provider_name=provider_name,
            location_id=location_id,
            country=country,
            state=state,
            urban_area=urban_area,
            provider_designation=provider_designation,
            service_ids=service_ids,
        )

        # Then
        assert isinstance(location, Location)
        assert (
            location.did_web
            == "https://ABC.provider.example.com/location-json/5994471abb01112afcc18159f6cc74b4f511b99806da59b3caf5a9c173cacfc5/data.json"
        )
        assert isinstance(location.hasProvider, GaiaxObjectWithDidWeb)
        assert (
            location.hasProvider.did_web
            == "https://ABC.provider.example.com/legal-participant-json/b5d4045c3f466fa91fe2cc6abe79232a1a57cdf104f7a26e716e0a1e2789df78/data.json"
        )
        assert location.country == "France"
        assert location.state == "Nord"
        assert location.urbanArea == "Roubaix"
        assert location.providerDesignation == "XYZ"
        assert location.hasAdministrativeLocation is not None
        assert all(isinstance(service, GaiaxObjectWithDidWeb) for service in location.canHostServiceOffering)
        assert all(isinstance(service, GaiaxObjectWithDidWeb) for service in location.hasLocatedServiceOffering)

    #  Should create a Location object with all optional attributes set to None.
    def test_create_location_with_optional_attributes_set_to_none(self):
        # Given
        parent_domain_name = "example.com"
        provider_name = "ABC"
        location_id = "12345"
        country = "France"
        state = "Nord"
        urban_area = None
        provider_designation = None
        service_ids = []

        # When
        location = create_location(
            parent_domain_name=parent_domain_name,
            provider_name=provider_name,
            location_id=location_id,
            country=country,
            state=state,
            urban_area=urban_area,
            provider_designation=provider_designation,
            service_ids=service_ids,
        )

        # Then
        assert isinstance(location, Location)
        assert (
            location.did_web
            == "https://ABC.provider.example.com/location-json/5994471abb01112afcc18159f6cc74b4f511b99806da59b3caf5a9c173cacfc5/data.json"
        )
        assert isinstance(location.hasProvider, GaiaxObjectWithDidWeb)
        assert (
            location.hasProvider.did_web
            == "https://ABC.provider.example.com/legal-participant-json/b5d4045c3f466fa91fe2cc6abe79232a1a57cdf104f7a26e716e0a1e2789df78/data.json"
        )
        assert location.country == "France"
        assert location.state == "Nord"
        assert location.urbanArea is None
        assert location.providerDesignation is None
        assert location.hasAdministrativeLocation is not None
        assert all(isinstance(service, GaiaxObjectWithDidWeb) for service in location.canHostServiceOffering)
        assert all(isinstance(service, GaiaxObjectWithDidWeb) for service in location.hasLocatedServiceOffering)
