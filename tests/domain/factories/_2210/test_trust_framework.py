# -*- coding: utf-8 -*-
import pytest

# data-initialiser Library
from credential_generator.domain._2210.trust_framework.participant import LegalPerson
from credential_generator.domain._2210.trust_framework.terms_and_conditions import GaiaXTermsAndConditions
from credential_generator.domain.factories._2210.trust_framework import (
    InvalidParticipantNameException,
    create_provider,
    get_gaiax_terms_and_conditions_id,
    get_gaiax_terms_and_conditions_vc_id,
    create_gaiax_terms_and_conditions,
)

PARENT_DOMAIN_NAME = "test.com"
LEGAL_NAME = "Aruba"
LEI_CODE = "81560069E326FF3BE121"
COUNTRY_CODE = "IT-BG"
REGISTRATION_NUMBER = ""


def check_returned_object(result: object):
    assert isinstance(result, LegalPerson)
    assert result.legalRegistrationNumber.leiCode == LEI_CODE
    assert result.legalAddress.country_subdivision_code == COUNTRY_CODE
    assert result.headquarterAddress.country_subdivision_code == COUNTRY_CODE


def test_valid_provider_name():
    legal_person = create_provider(
        parent_domain_name=PARENT_DOMAIN_NAME,
        provider_name="aruba",
        legal_name=LEGAL_NAME,
        country_name=COUNTRY_CODE,
        lei_code=LEI_CODE,
        registration_number=REGISTRATION_NUMBER,
    )
    check_returned_object(legal_person)


@pytest.mark.parametrize("provider_name", ["aruba ", " aruba", " aruba "])
def test_provider_name_with_spaces(provider_name):
    legal_person = create_provider(
        parent_domain_name=PARENT_DOMAIN_NAME,
        provider_name=provider_name,
        legal_name=LEGAL_NAME,
        country_name=COUNTRY_CODE,
        lei_code=LEI_CODE,
        registration_number=REGISTRATION_NUMBER,
    )
    check_returned_object(legal_person)


@pytest.mark.parametrize("provider_name", ["", " ", "  "])
def test_empty_provider_name(provider_name):
    with pytest.raises(InvalidParticipantNameException) as exc_info:
        create_provider(
            parent_domain_name=PARENT_DOMAIN_NAME,
            provider_name=provider_name,
            legal_name=LEGAL_NAME,
            country_name=COUNTRY_CODE,
            lei_code=LEI_CODE,
            registration_number=REGISTRATION_NUMBER,
        )
    assert str(exc_info.value) == "Participant name is empty, cannot create a LegalPerson object"


class TestGetGaiaxTermsAndConditionsId:
    #  Should return a valid URL when given valid parent domain name and provider name.
    def test_valid_url(self):
        # Given
        parent_domain_name = "example.com"
        provider_name = "MyProvider"

        # When
        result = get_gaiax_terms_and_conditions_id(parent_domain_name, provider_name)

        # Then
        assert result.startswith("https://")
        assert result.endswith("/data.json")
        assert "MyProvider" in result
        assert "gaiax-terms-and-conditions" in result

    #  Should return a URL that starts with 'https://' when given valid parent domain name and provider name.
    def test_url_starts_with_https(self):
        # Given
        parent_domain_name = "example.com"
        provider_name = "MyProvider"

        # When
        result = get_gaiax_terms_and_conditions_id(parent_domain_name, provider_name)

        # Then
        assert result.startswith("https://")

    #  Should return a URL that ends with '/data.json' when given valid parent domain name and provider name.
    def test_url_ends_with_data_json(self):
        # Given
        parent_domain_name = "example.com"
        provider_name = "MyProvider"

        # When
        result = get_gaiax_terms_and_conditions_id(parent_domain_name, provider_name)

        # Then
        assert result.endswith("/data.json")

    #  Should return a URL that contains the provider hash value when given valid parent domain name and provider name.
    def test_url_contains_provider_hash_value(self):
        # Given
        parent_domain_name = "example.com"
        provider_name = "MyProvider"

        # When
        result = get_gaiax_terms_and_conditions_id(parent_domain_name, provider_name)

        # Then
        assert (
            result
            == "https://MyProvider.example.com/gaiax-terms-and-conditions-json/6ee8ee948acd5a1a63c53ea9dc7b02f10de149f58866c111bac5c7f251f9a4cc/data.json"
        )

    #  Should return a URL that contains the directory name for GaiaX Terms and Conditions when given valid parent domain name and provider name.
    def test_url_contains_directory_name(self):
        # Given
        parent_domain_name = "example.com"
        provider_name = "MyProvider"

        # When
        result = get_gaiax_terms_and_conditions_id(parent_domain_name, provider_name)

        # Then
        assert "gaiax-terms-and-conditions" in result

    #  Should raise an exception when given an empty parent domain name.
    def test_empty_parent_domain_name(self):
        # Given
        parent_domain_name = ""
        provider_name = "MyProvider"

        # When/Then
        with pytest.raises(Exception):
            get_gaiax_terms_and_conditions_id(parent_domain_name, provider_name)

    #  Should raise an exception when given an empty provider name.
    def test_empty_provider_name(self):
        # Given
        parent_domain_name = "example.com"
        provider_name = ""

        # When/Then
        with pytest.raises(Exception):
            get_gaiax_terms_and_conditions_id(parent_domain_name, provider_name)


class TestGetGaiaxTermsAndConditionsVcId:
    #  Returns a string with a valid ID when valid inputs are provided.
    def test_valid_inputs_provided(self):
        # Given
        parent_domain_name = "example.com"
        provider_name = "Provider"

        # When
        result = get_gaiax_terms_and_conditions_vc_id(parent_domain_name, provider_name)

        # Then
        assert isinstance(result, str)
        assert (
            result
            == "https://Provider.example.com/gaiax-terms-and-conditions/472590ae974d4c1f44b3780df0b152d9119f076c61bfb3e8cb6affd7889ac0a8/data.json"
        )

    #  Returns a string with a valid ID when the provider name contains special characters.
    def test_provider_name_special_characters(self):
        # Given
        parent_domain_name = "example.com"
        provider_name = "Provider!@#$%^&*()"

        # When
        result = get_gaiax_terms_and_conditions_vc_id(parent_domain_name, provider_name)

        # Then
        assert isinstance(result, str)
        assert result.startswith("https://Provider!@#$%^&*().example.com")

    #  Returns a string with a valid ID when the provider name is an empty string.
    def test_provider_name_empty(self):
        # Given
        parent_domain_name = "example.com"
        provider_name = ""

        # When
        with pytest.raises(ValueError):
            get_gaiax_terms_and_conditions_vc_id(parent_domain_name, provider_name)

    #  Returns a string with a valid ID when the parent domain name is an empty string.
    def test_parent_domain_name_empty(self):
        # Given
        parent_domain_name = ""
        provider_name = "Provider"

        # When
        with pytest.raises(ValueError):
            get_gaiax_terms_and_conditions_vc_id(parent_domain_name, provider_name)

    #  Returns a string with a valid ID when the parent domain name contains special characters.
    def test_parent_domain_name_special_characters(self):
        # Given
        parent_domain_name = "example!@#$%^&*().com"
        provider_name = "Provider"

        # When
        result = get_gaiax_terms_and_conditions_vc_id(parent_domain_name, provider_name)

        # Then
        assert isinstance(result, str)
        assert result.startswith("https://Provider.example!@#$%^&*().com")

    #  Raises an exception when the parent domain name is None.
    def test_parent_domain_name_none(self):
        # Given
        parent_domain_name = None
        provider_name = "Provider"

        # When/Then
        with pytest.raises(Exception):
            get_gaiax_terms_and_conditions_vc_id(parent_domain_name, provider_name)

    #  Raises an exception when the provider name is None.
    def test_provider_name_none(self):
        # Given
        parent_domain_name = "example.com"
        provider_name = None

        # When/Then
        with pytest.raises(Exception):
            get_gaiax_terms_and_conditions_vc_id(parent_domain_name, provider_name)

    #  Raises an exception when the parent domain name is not a string.
    def test_parent_domain_name_not_string(self):
        # Given
        parent_domain_name = 123
        provider_name = "Provider"

        # When/Then
        with pytest.raises(TypeError):
            get_gaiax_terms_and_conditions_vc_id(parent_domain_name, provider_name)

    #  Raises an exception when the provider name is not a string.
    def test_provider_name_not_string(self):
        # Given
        parent_domain_name = "example.com"
        provider_name = 123

        # When/Then
        with pytest.raises(TypeError):
            get_gaiax_terms_and_conditions_vc_id(parent_domain_name, provider_name)


class TestCreateGaiaxTermsAndConditions:
    #  Creates a GaiaXTermsAndConditions instance with valid parameters.
    def test_creates_gaiax_terms_and_conditions_with_valid_parameters(self):
        # Given
        parent_domain_name = "example.com"
        provider_name = "provider"
        text = "Sample terms and conditions"

        # When
        result = create_gaiax_terms_and_conditions(parent_domain_name, provider_name, text)

        # Then
        assert isinstance(result, GaiaXTermsAndConditions)
        assert (
            result.did_web
            == "https://provider.example.com/gaiax-terms-and-conditions-json/5c4c1964340aca5b65393bbe9d3249cdd71be26665b3320ad694f034f2743283/data.json"
        )
        assert result.termAndConditions == text

    #  Raises InvalidProviderNameException if provider name is empty.
    def test_raises_invalid_provider_name_exception_if_provider_name_is_empty(self):
        # Given
        parent_domain_name = "example.com"
        provider_name = ""
        text = "Sample terms and conditions"

        # When/Then
        with pytest.raises(ValueError):
            create_gaiax_terms_and_conditions(parent_domain_name, provider_name, text)

    #  Raises InvalidServiceIDException if service id is empty.
    def test_raises_invalid_service_id_exception_if_service_id_is_empty(self):
        # Given
        parent_domain_name = ""
        provider_name = "provider"
        text = "Sample terms and conditions"

        # When/Then
        with pytest.raises(ValueError):
            create_gaiax_terms_and_conditions(parent_domain_name, provider_name, text)

    #  Raises an exception if text is empty.
    def test_raises_exception_if_text_is_empty(self):
        # Given
        parent_domain_name = "example.com"
        provider_name = "provider"
        text = ""

        # When/Then
        with pytest.raises(ValueError):
            create_gaiax_terms_and_conditions(parent_domain_name, provider_name, text)

    #  Raises an exception if parent domain name is not a string.
    def test_raises_exception_if_parent_domain_name_is_not_a_string(self):
        # Given
        parent_domain_name = 123
        provider_name = "provider"
        text = "Sample terms and conditions"

        # When/Then
        with pytest.raises(TypeError):
            create_gaiax_terms_and_conditions(parent_domain_name, provider_name, text)
