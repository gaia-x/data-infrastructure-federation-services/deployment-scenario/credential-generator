# -*- coding: utf-8 -*-

from pydantic import ValidationError

from credential_generator.domain import ComplianceLevel, CriterionCategory, OntologyType
from credential_generator.domain._2210 import GaiaxObjectWithDidWeb
from credential_generator.domain._2210.gxfs.gx_compliance import (
    ComplianceCriterion,
    ComplianceReference,
    ComplianceCertificationScheme,
    SelfAssessedComplianceCriteriaClaim,
    ComplianceReferenceManager,
)
from credential_generator.domain._2210.trust_framework.participant import LegalRegistrationNumber, Address
from credential_generator.domain.factories._2210.gxfs import get_compliance_criterion_id
from credential_generator.domain.factories._2210.gxfs.compliance import (
    create_compliance_criterion,
    create_compliance_certification_scheme,
    create_self_assessed_compliance_criteria_claim,
    create_compliance_reference_manager,
)
from credential_generator.utilities.did_helpers import calculate_auditor_hash_value


class TestCreateComplianceCriterion:
    #  Creates a ComplianceCriterion object with valid inputs
    def test_valid_inputs(self):
        parent_domain_name = "example.com"
        provider_name = "acme"
        criterion_name = "security"
        criterion_level = ComplianceLevel.LEVEL_1
        criterion_category = CriterionCategory.SECURITY
        description = "This criterion ensures the security of the system."
        self_assessed = True

        criterion = create_compliance_criterion(
            parent_domain_name=parent_domain_name,
            provider_name=provider_name,
            name=criterion_name,
            level=criterion_level,
            category=criterion_category,
            description=description,
            self_assessed=self_assessed,
        )

        assert isinstance(criterion, ComplianceCriterion)
        assert criterion.name == criterion_name
        assert criterion.level == criterion_level.value
        assert criterion.reference_type == criterion_category.value
        assert criterion.description == description
        assert criterion.self_assessed == self_assessed

    #  Generates a DID for the ComplianceCriterion object
    def test_generate_did(self):
        parent_domain_name = "example.com"
        provider_name = "acme"
        criterion_name = "security"
        criterion_level = ComplianceLevel.LEVEL_1
        criterion_category = CriterionCategory.SECURITY
        description = "This criterion ensures the security of the system."
        self_assessed = True

        criterion = create_compliance_criterion(
            parent_domain_name=parent_domain_name,
            provider_name=provider_name,
            name=criterion_name,
            level=criterion_level,
            category=criterion_category,
            description=description,
            self_assessed=self_assessed,
        )

        assert criterion.did_web.startswith("https://acme.example.com")
        assert criterion.did_web.endswith("/data.json")

    #  Uses the generated DID to construct the ComplianceCriterion object
    def test_construct_with_did(self):
        parent_domain_name = "example.com"
        provider_name = "acme"
        criterion_name = "security"
        criterion_level = ComplianceLevel.LEVEL_1
        criterion_category = CriterionCategory.SECURITY
        description = "This criterion ensures the security of the system."
        self_assessed = True

        did = get_compliance_criterion_id(
            parent_domain_name=parent_domain_name,
            federator_name=provider_name,
            criterion_name=criterion_name,
            criterion_level=criterion_level.value,
        )

        criterion = create_compliance_criterion(
            parent_domain_name=parent_domain_name,
            provider_name=provider_name,
            name=criterion_name,
            level=criterion_level,
            category=criterion_category,
            description=description,
            self_assessed=self_assessed,
        )

        assert criterion.did_web == did

    #  Raises an error if parent_domain_name is None
    def test_none_parent_domain_name(self):
        parent_domain_name = None
        provider_name = "acme"
        criterion_name = "security"
        criterion_level = ComplianceLevel.LEVEL_1
        criterion_category = CriterionCategory.SECURITY
        description = "This criterion ensures the security of the system."
        self_assessed = True

        with pytest.raises(TypeError):
            create_compliance_criterion(
                parent_domain_name=parent_domain_name,
                provider_name=provider_name,
                name=criterion_name,
                level=criterion_level,
                category=criterion_category,
                description=description,
                self_assessed=self_assessed,
            )

    #  Raises an error if provider_name is None
    def test_none_provider_name(self):
        parent_domain_name = "example.com"
        provider_name = None
        criterion_name = "security"
        criterion_level = ComplianceLevel.LEVEL_1
        criterion_category = CriterionCategory.SECURITY
        description = "This criterion ensures the security of the system."
        self_assessed = True

        with pytest.raises(TypeError):
            create_compliance_criterion(
                parent_domain_name=parent_domain_name,
                provider_name=provider_name,
                name=criterion_name,
                level=criterion_level,
                category=criterion_category,
                description=description,
                self_assessed=self_assessed,
            )

    #  Raises an error if name is None
    def test_none_name(self):
        parent_domain_name = "example.com"
        provider_name = "acme"
        criterion_name = None
        criterion_level = ComplianceLevel.LEVEL_1
        criterion_category = CriterionCategory.SECURITY
        description = "This criterion ensures the security of the system."
        self_assessed = True

        with pytest.raises(TypeError):
            create_compliance_criterion(
                parent_domain_name=parent_domain_name,
                provider_name=provider_name,
                name=criterion_name,
                level=criterion_level,
                category=criterion_category,
                description=description,
                self_assessed=self_assessed,
            )


class TestCreateComplianceCertificationScheme:
    #  Generates a ComplianceCertificationScheme object with a unique DID based on the provided inputs.
    def test_unique_did_generation(self):
        parent_domain_name = "example.com"
        federator_name = "federator1"
        compliance_reference = ComplianceReference(
            did_web="did:web:1",
            compliance_reference_id="12345",
            reference_url="http://test.com",
            reference_sha256="12",
            title="A",
            compliance_reference_manager="d",
        )
        compliance_criterions = [
            ComplianceCriterion(
                did_web="did:web:1",
                name="criterion1",
                level=ComplianceLevel.LEVEL_1,
                reference_type=CriterionCategory.SECURITY,
                description="description1",
                self_assessed=True,
            )
        ]

        scheme = create_compliance_certification_scheme(
            parent_domain_name, federator_name, compliance_reference, compliance_criterions
        )

        assert isinstance(scheme, ComplianceCertificationScheme)
        assert scheme.did_web != ""
        assert scheme.did_web is not None

        # @todo: change assertion when refactoring generation
        assert scheme.did_web.startswith(
            f"https://federator1.example.com/{OntologyType.get_directory_name(OntologyType.COMPLIANCE_CERT_SCHEME, is_vc=True)}"
        )
        assert scheme.did_web.endswith("/data.json")
        assert scheme.compliance_reference == compliance_reference
        assert scheme.criteria_list == compliance_criterions

    #  Returns a ComplianceCertificationScheme object with an empty criteria list if no compliance criterions are provided.
    def test_returns_object_with_empty_criteria_list(self):
        parent_domain_name = "example.com"
        federator_name = "federator1"
        compliance_reference = ComplianceReference(
            did_web="did:web:1",
            compliance_reference_id="12345",
            reference_url="http://test.com",
            reference_sha256="12",
            title="A",
            compliance_reference_manager="d",
        )

        scheme = create_compliance_certification_scheme(parent_domain_name, federator_name, compliance_reference, [])

        assert isinstance(scheme, ComplianceCertificationScheme)
        assert scheme.criteria_list == []

    #  Raises an exception if the parent_domain_name input is None.
    def test_raises_exception_if_parent_domain_name_is_none(self):
        parent_domain_name = None
        federator_name = "federator1"
        compliance_reference = ComplianceReference(
            did_web="did:web:1",
            compliance_reference_id="12345",
            reference_url="http://test.com",
            reference_sha256="12",
            title="A",
            compliance_reference_manager="d",
        )
        compliance_criterions = [
            ComplianceCriterion(
                did_web="did:web:1",
                name="criterion1",
                level=ComplianceLevel.LEVEL_1,
                reference_type=CriterionCategory.SECURITY,
                description="description1",
                self_assessed=True,
            )
        ]

        with pytest.raises(Exception):
            create_compliance_certification_scheme(
                parent_domain_name, federator_name, compliance_reference, compliance_criterions
            )

    #  Raises an exception if the federator_name input is None.
    def test_raises_exception_if_federator_name_is_none(self):
        parent_domain_name = "example.com"
        federator_name = None
        compliance_reference = ComplianceReference(
            did_web="did:web:1",
            compliance_reference_id="12345",
            reference_url="http://test.com",
            reference_sha256="12",
            title="A",
            compliance_reference_manager="d",
        )
        compliance_criterions = [
            ComplianceCriterion(
                did_web="did:web:1",
                name="criterion1",
                level=ComplianceLevel.LEVEL_1,
                reference_type=CriterionCategory.SECURITY,
                description="description1",
                self_assessed=True,
            )
        ]

        with pytest.raises(Exception):
            create_compliance_certification_scheme(
                parent_domain_name, federator_name, compliance_reference, compliance_criterions
            )


class TestCreateSelfAssessedComplianceCriteriaClaim:
    #  Should return a SelfAssessedComplianceCriteriaClaim object with the correct did_web, located_service_offering, and criteria fields when given valid inputs.
    def test_valid_inputs(self):
        parent_domain_name = "example.com"
        provider_name = "provider"
        federator_name = "federation"
        located_service_offerings = [
            GaiaxObjectWithDidWeb(did_web="did_web_1"),
            GaiaxObjectWithDidWeb(did_web="did_web_2"),
        ]
        criteria = [("criterion1", "1"), ("criterion2", "2")]

        result = create_self_assessed_compliance_criteria_claim(
            parent_domain_name=parent_domain_name,
            federator_name=federator_name,
            federator_domain_name=parent_domain_name,
            provider_name=provider_name,
            located_service_offerings=located_service_offerings,
            criteria=criteria,
        )

        assert isinstance(result, SelfAssessedComplianceCriteriaClaim)
        assert result.did_web.startswith(
            f"https://provider.provider.example.com/{OntologyType.get_directory_name(OntologyType.SELF_ASSESSED_COMPLIANCE_CRITERIA_CLAIM)}"
        )
        assert result.located_service_offering == located_service_offerings
        # assert result.criteria == [
        #     GaiaxObjectWithDidWeb(
        #         did_web="did:web:federation.example.com:participant:8ce333c5811acbdc46a0ba06bc27621a23b2550b1469b3b679a9f4a3c7470772/compliance-criterion/1540a35d74012eef3afa8bc09d0d39efe2db50286d2598f63a964b114ede8351/data.json"
        #     ),
        #     GaiaxObjectWithDidWeb(
        #         did_web="did:web:federation.example.com:participant:8ce333c5811acbdc46a0ba06bc27621a23b2550b1469b3b679a9f4a3c7470772/compliance-criterion/fbcc541c3ec773bc96444ee721b272294415e83d633ded17c8fa3bf0bfa41b3e/data.json"
        #     ),
        # ]

    #  Should be able to handle an empty list of criteria and return a SelfAssessedComplianceCriteriaClaim object with an empty criteria field.
    def test_empty_criteria(self):
        parent_domain_name = "example.com"
        provider_name = "provider"
        federator_name = "federation"
        located_service_offerings = [
            GaiaxObjectWithDidWeb(did_web="did_web_1"),
            GaiaxObjectWithDidWeb(did_web="did_web_2"),
        ]
        criteria = []

        with pytest.raises(TypeError):
            create_self_assessed_compliance_criteria_claim(
                parent_domain_name, federator_name, provider_name, located_service_offerings, criteria
            )

    #  Should be able to handle an empty list of located_service_offerings and return a SelfAssessedComplianceCriteriaClaim object with an empty located_service_offering field.
    def test_empty_located_service_offerings(self):
        parent_domain_name = "example.com"
        provider_name = "provider"
        federator_name = "federation"
        located_service_offerings = []
        criteria = [("criterion1", "1"), ("criterion2", "2")]

        result = create_self_assessed_compliance_criteria_claim(
            parent_domain_name=parent_domain_name,
            federator_name=federator_name,
            federator_domain_name=parent_domain_name,
            provider_name=provider_name,
            located_service_offerings=located_service_offerings,
            criteria=criteria,
        )
        assert isinstance(result, SelfAssessedComplianceCriteriaClaim)
        assert result.did_web.startswith(
            "https://provider.provider.example.com/self-assessed-compliance-criteria-claim"
        )
        assert result.located_service_offering == []

    #  Should raise an error if parent_domain_name is not provided.
    def test_missing_parent_domain_name(self):
        parent_domain_name = None
        federator_name = "federation"
        provider_name = "provider"
        located_service_offerings = [
            GaiaxObjectWithDidWeb(did_web="did_web_1"),
            GaiaxObjectWithDidWeb(did_web="did_web_2"),
        ]
        criteria = [("criterion1", "1"), ("criterion2", "2")]

        with pytest.raises(TypeError):
            create_self_assessed_compliance_criteria_claim(
                parent_domain_name, federator_name, provider_name, located_service_offerings, criteria
            )

    #  Should raise an error if provider_name is not provided.
    def test_missing_provider_name(self):
        parent_domain_name = "example.com"
        federator_name = "federation"
        provider_name = None
        located_service_offerings = [
            GaiaxObjectWithDidWeb(did_web="did_web_1"),
            GaiaxObjectWithDidWeb(did_web="did_web_2"),
        ]
        criteria = [("criterion1", "1"), ("criterion2", "2")]

        with pytest.raises(TypeError):
            create_self_assessed_compliance_criteria_claim(
                parent_domain_name, federator_name, provider_name, located_service_offerings, criteria
            )

    #  Should raise an error if located_service_offerings is not provided.
    def test_missing_located_service_offerings(self):
        parent_domain_name = "example.com"
        federator_name = "federation"
        provider_name = "provider"
        located_service_offerings = None
        criteria = [("criterion1", "1"), ("criterion2", "2")]

        with pytest.raises(ValidationError):
            create_self_assessed_compliance_criteria_claim(
                parent_domain_name=parent_domain_name,
                federator_name=federator_name,
                federator_domain_name=parent_domain_name,
                provider_name=provider_name,
                located_service_offerings=located_service_offerings,
                criteria=criteria,
            )


# Generated by CodiumAI

from credential_generator.domain.factories._2210.gxfs.compliance import create_compliance_assessment_body

import pytest


class TestCreateComplianceAssessmentBody:
    #  Creates a compliance assessment body with all required fields when both lei_code and registration_number are provided.
    def test_creates_compliance_assessment_body_with_lei_code_and_registration_number(self):
        # Given
        parent_domain_name = "example.com"
        auditor_name = "Auditor"
        legal_name = "Legal Entity"
        country_name = "BE-BRU"
        lei_code = "LEI123"
        registration_number = "REG123"

        # When
        result = create_compliance_assessment_body(
            parent_domain_name=parent_domain_name,
            auditor_name=auditor_name,
            legal_name=legal_name,
            country_name=country_name,
            lei_code=lei_code,
            registration_number=registration_number,
        )

        # Then
        assert (
            result.did_web
            == "https://Auditor.auditor.example.com/legal-participant-json/465b5967e292896bee2bbe8a67298bf1eec40ae84cf2ea96990cedcf87630442/data.json"
        )
        assert result.legalName == legal_name
        assert result.legalAddress.country_subdivision_code == country_name
        assert result.headquarterAddress.country_subdivision_code == country_name
        assert isinstance(result.legalRegistrationNumber, LegalRegistrationNumber)
        assert (
            result.legalRegistrationNumber.did_web
            == "https://Auditor.example.com/gaiax-legal-registration-number/465b5967e292896bee2bbe8a67298bf1eec40ae84cf2ea96990cedcf87630442/data.json"
        )
        assert result.legalRegistrationNumber.leiCode == lei_code
        assert result.legalRegistrationNumber.vatID is None

    #  Creates a compliance assessment body with all required fields when only lei_code is provided.
    def test_creates_compliance_assessment_body_with_lei_code(self):
        # Given
        parent_domain_name = "example.com"
        auditor_name = "Auditor"
        legal_name = "Legal Entity"
        country_name = "BE-BRU"
        lei_code = "LEI123"
        registration_number = None

        # When
        result = create_compliance_assessment_body(
            parent_domain_name=parent_domain_name,
            auditor_name=auditor_name,
            legal_name=legal_name,
            country_name=country_name,
            lei_code=lei_code,
            registration_number=registration_number,
        )

        # Then
        assert (
            result.did_web
            == "https://Auditor.auditor.example.com/legal-participant-json/465b5967e292896bee2bbe8a67298bf1eec40ae84cf2ea96990cedcf87630442/data.json"
        )
        assert result.legalName == legal_name
        assert result.legalAddress.country_subdivision_code == country_name
        assert result.headquarterAddress.country_subdivision_code == country_name
        assert isinstance(result.legalRegistrationNumber, LegalRegistrationNumber)
        assert (
            result.legalRegistrationNumber.did_web
            == "https://Auditor.example.com/gaiax-legal-registration-number/465b5967e292896bee2bbe8a67298bf1eec40ae84cf2ea96990cedcf87630442/data.json"
        )
        assert result.legalRegistrationNumber.leiCode == lei_code
        assert result.legalRegistrationNumber.vatID is None

    #  Creates a compliance assessment body with all required fields when only registration_number is provided.
    def test_creates_compliance_assessment_body_with_registration_number(self):
        # Given
        parent_domain_name = "example.com"
        auditor_name = "Auditor"
        legal_name = "Legal Entity"
        country_name = "BE-BRU"
        lei_code = None
        registration_number = "REG123"

        # When
        result = create_compliance_assessment_body(
            parent_domain_name=parent_domain_name,
            auditor_name=auditor_name,
            legal_name=legal_name,
            country_name=country_name,
            lei_code=lei_code,
            registration_number=registration_number,
        )

        # Then
        assert (
            result.did_web
            == "https://Auditor.auditor.example.com/legal-participant-json/465b5967e292896bee2bbe8a67298bf1eec40ae84cf2ea96990cedcf87630442/data.json"
        )
        assert result.legalName == legal_name
        assert result.legalAddress.country_subdivision_code == country_name
        assert result.headquarterAddress.country_subdivision_code == country_name
        assert isinstance(result.legalRegistrationNumber, LegalRegistrationNumber)
        assert (
            result.legalRegistrationNumber.did_web
            == "https://Auditor.example.com/gaiax-legal-registration-number/465b5967e292896bee2bbe8a67298bf1eec40ae84cf2ea96990cedcf87630442/data.json"
        )
        assert result.legalRegistrationNumber.leiCode is None
        assert result.legalRegistrationNumber.vatID == registration_number

    #  Uses the provided parent_domain_name, auditor_name, legal_name, country_name, lei_code, and registration_number to create the compliance assessment body.
    def test_uses_provided_parameters_to_create_compliance_assessment_body(self):
        # Given
        parent_domain_name = "example.com"
        auditor_name = "Auditor"
        legal_name = "Legal Entity"
        country_name = "BE-BRU"
        lei_code = "LEI123"
        registration_number = "REG123"

        # When
        result = create_compliance_assessment_body(
            parent_domain_name=parent_domain_name,
            auditor_name=auditor_name,
            legal_name=legal_name,
            country_name=country_name,
            lei_code=lei_code,
            registration_number=registration_number,
        )

        # Then
        assert (
            result.did_web
            == "https://Auditor.auditor.example.com/legal-participant-json/465b5967e292896bee2bbe8a67298bf1eec40ae84cf2ea96990cedcf87630442/data.json"
        )
        assert result.legalName == legal_name
        assert result.legalAddress.country_subdivision_code == country_name
        assert result.headquarterAddress.country_subdivision_code == country_name
        assert isinstance(result.legalRegistrationNumber, LegalRegistrationNumber)
        assert (
            result.legalRegistrationNumber.did_web
            == "https://Auditor.example.com/gaiax-legal-registration-number/465b5967e292896bee2bbe8a67298bf1eec40ae84cf2ea96990cedcf87630442/data.json"
        )
        assert result.legalRegistrationNumber.leiCode == lei_code
        assert result.legalRegistrationNumber.vatID is None

    #  Computes the auditor hash value using the provided auditor_name.
    def test_computes_auditor_hash_value(self):
        # Given
        auditor_name = "Auditor"

        # When
        result = calculate_auditor_hash_value(auditor_name=auditor_name)

        # Then
        assert result == "465b5967e292896bee2bbe8a67298bf1eec40ae84cf2ea96990cedcf87630442"

    #  Raises a ValueError when neither lei_code nor registration_number is provided.
    def test_raises_value_error_when_neither_lei_code_nor_registration_number_is_provided(self):
        # Given
        parent_domain_name = "example.com"
        auditor_name = "Auditor"
        legal_name = "Legal Entity"
        country_name = "BE-BRU"
        lei_code = None
        registration_number = None

        # When/Then
        with pytest.raises(ValueError):
            create_compliance_assessment_body(
                parent_domain_name=parent_domain_name,
                auditor_name=auditor_name,
                legal_name=legal_name,
                country_name=country_name,
                lei_code=lei_code,
                registration_number=registration_number,
            )


class TestCreateComplianceReferenceManager:
    #  Should create a ComplianceReferenceManager object with the provided parameters
    def test_create_compliance_reference_manager_with_parameters(self):
        # Given
        parent_domain_name = "example.com"
        federation_name = "Federation"
        legal_name = "Entity1"
        country_name = "BE-BRU"
        lei_code = "LEI123"
        registration_number = "REG123"
        has_compliance_references = ["ref1", "ref2"]

        # When
        result = create_compliance_reference_manager(
            parent_domain_name,
            federation_name,
            legal_name,
            country_name,
            lei_code,
            registration_number,
            has_compliance_references,
        )

        # Then
        assert isinstance(result, ComplianceReferenceManager)
        assert (
            result.did_web
            == "https://Federation.example.com/legal-participant-json/f8c6e8016cfb9bead843882a23372b1af43ea313cda1fb84a325ed5521805767/data.json"
        )
        assert result.legalName == legal_name
        assert result.legalAddress.country_subdivision_code == country_name
        assert result.headquarterAddress.country_subdivision_code == country_name
        assert isinstance(result.legalRegistrationNumber, LegalRegistrationNumber)
        assert (
            result.legalRegistrationNumber.did_web
            == "https://Federation.example.com/gaiax-legal-registration-number/f8c6e8016cfb9bead843882a23372b1af43ea313cda1fb84a325ed5521805767/data.json"
        )
        assert result.legalRegistrationNumber.leiCode == lei_code
        assert result.legalRegistrationNumber.vatID is None
        assert result.has_compliance_references == has_compliance_references

    #  Should set has_compliance_references to an empty list if not provided
    def test_create_compliance_reference_manager_without_compliance_references(self):
        # Given
        parent_domain_name = "example.com"
        federation_name = "Federation"
        legal_name = "Entity1"
        country_name = "BE-BRU"
        lei_code = "LEI123"
        registration_number = "REG123"
        has_compliance_references = None

        # When
        result = create_compliance_reference_manager(
            parent_domain_name,
            federation_name,
            legal_name,
            country_name,
            lei_code,
            registration_number,
            has_compliance_references,
        )

        # Then
        assert isinstance(result, ComplianceReferenceManager)
        assert result.has_compliance_references == []

    #  Should set legalAddress and headquarterAddress to an Address object with the provided country_name
    def test_create_compliance_reference_manager_with_country_name(self):
        # Given
        parent_domain_name = "example.com"
        federation_name = "Auditor1"
        legal_name = "Entity1"
        country_name = "BE-BRU"
        lei_code = "LEI123"
        registration_number = "REG123"
        has_compliance_references = ["ref1", "ref2"]

        # When
        result = create_compliance_reference_manager(
            parent_domain_name,
            federation_name,
            legal_name,
            country_name,
            lei_code,
            registration_number,
            has_compliance_references,
        )

        # Then
        assert isinstance(result, ComplianceReferenceManager)
        assert isinstance(result.legalAddress, Address)
        assert result.legalAddress.country_subdivision_code == country_name
        assert isinstance(result.headquarterAddress, Address)
        assert result.headquarterAddress.country_subdivision_code == country_name

    #  Should create a LegalRegistrationNumber object with the provided lei_code and registration_number
    def test_create_compliance_reference_manager_with_lei_code_and_registration_number(self):
        # Given
        parent_domain_name = "example.com"
        federation_name = "Auditor1"
        legal_name = "Entity1"
        country_name = "BE-BRU"
        lei_code = "LEI123"
        registration_number = "REG123"
        has_compliance_references = ["ref1", "ref2"]

        # When
        result = create_compliance_reference_manager(
            parent_domain_name,
            federation_name,
            legal_name,
            country_name,
            lei_code,
            registration_number,
            has_compliance_references,
        )

        # Then
        assert isinstance(result, ComplianceReferenceManager)
        assert isinstance(result.legalRegistrationNumber, LegalRegistrationNumber)
        assert result.legalRegistrationNumber.leiCode == lei_code
        assert result.legalRegistrationNumber.vatID is None

    #  Should create a LegalRegistrationNumber object with only registration_number if lei_code is not provided
    def test_create_compliance_reference_manager_with_registration_number_only(self):
        # Given
        parent_domain_name = "example.com"
        federation_name = "Auditor1"
        legal_name = "Entity1"
        country_name = "BE-BRU"
        registration_number = "REG123"
        has_compliance_references = ["ref1", "ref2"]

        # When
        result = create_compliance_reference_manager(
            parent_domain_name=parent_domain_name,
            federation_name=federation_name,
            legal_name=legal_name,
            country_name=country_name,
            lei_code=None,
            registration_number=registration_number,
            has_compliance_references=has_compliance_references,
        )

        # Then
        assert isinstance(result, ComplianceReferenceManager)
        assert isinstance(result.legalRegistrationNumber, LegalRegistrationNumber)
        assert result.legalRegistrationNumber.leiCode is None
        assert result.legalRegistrationNumber.vatID == registration_number

    #  Should raise an exception if neither lei_code nor registration_number is provided
    def test_create_compliance_reference_manager_without_lei_code_and_registration_number(self):
        # Given
        parent_domain_name = "example.com"
        federation_name = "Auditor1"
        legal_name = "Entity1"
        country_name = "BE-BRU"

        # When/Then
        with pytest.raises(ValueError):
            result = create_compliance_reference_manager(
                parent_domain_name=parent_domain_name,
                federation_name=federation_name,
                legal_name=legal_name,
                country_name=country_name,
                lei_code=None,
                registration_number=None,
                has_compliance_references=[],
            )
