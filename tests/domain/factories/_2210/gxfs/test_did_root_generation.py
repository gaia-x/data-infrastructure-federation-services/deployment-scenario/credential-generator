# -*- coding: utf-8 -*-
import pytest
from credential_generator.domain.factories._2210 import (
    get_federator_legal_participant_document_id,
    get_federator_id_root,
    get_auditor_id_root,
    get_provider_legal_participant_document_id,
    get_auditor_legal_participant_document_id,
)


class TestGetFederatorDidRoot:
    #  Should return a valid did web for a federation s with a valid parent domain name and federator name.
    def test_valid_did_web_provider_s(self):
        # Given
        parent_domain_name = "example.com"
        federator_name = "federation"

        # When
        result = get_federator_id_root(parent_domain_name, federator_name)

        # Then
        assert isinstance(result, str)
        assert result == "https://federation.example.com"

    #  Should return a valid did web for a federation with a valid parent domain containing auditor keyword name and federator name.
    def test_valid_did_web_auditor(self):
        # Given
        parent_domain_name = "auditor.example.com"
        federator_name = "federation"

        # When
        result = get_federator_id_root(parent_domain_name, federator_name)

        # Then
        assert isinstance(result, str)
        assert result == "https://federation.example.com"

    #  Should return a valid did web for a federation s with a valid parent domain containing provider keyword name  and federator name that contains numbers.
    def test_valid_did_web_provider_s_with_numbers(self):
        # Given
        parent_domain_name = "provider.example.com"
        federator_name = "federation"

        # When
        result = get_federator_id_root(parent_domain_name, federator_name)

        # Then
        assert isinstance(result, str)
        assert result == "https://federation.example.com"

    #  Should return a valid did web for a federation with a valid parent domain name and federator name that contains numbers.
    def test_valid_did_web_auditor_with_numbers(self):
        # Given
        parent_domain_name = "example.com"
        federator_name = "federation123"

        # When
        result = get_federator_id_root(parent_domain_name, federator_name)

        # Then
        assert isinstance(result, str)
        assert result == "https://federation123.example.com"

    #  Should return a valid did web for a provider s with a valid parent domain name and federator name that contains special characters.
    def test_valid_did_web_provider_s_with_special_characters(self):
        # Given
        parent_domain_name = "example.com"
        federator_name = "federation!@#"

        # When
        result = get_federator_id_root(parent_domain_name, federator_name)

        # Then
        assert isinstance(result, str)
        assert result == "https://federation!@#.example.com"

    #  Should raise a TypeError if parent_domain_name is not a string.
    def test_raise_type_error_parent_domain_name_not_string(self):
        # Given
        parent_domain_name = 123
        federator_name = "federation"

        # When, Then
        with pytest.raises(TypeError):
            get_federator_id_root(parent_domain_name, federator_name)

    #  Should raise a TypeError if federator_name is not a string.
    def test_raise_type_error_federator_name_not_string(self):
        # Given
        parent_domain_name = "example.com"
        federator_name = 123

        # When, Then
        with pytest.raises(TypeError):
            get_federator_id_root(parent_domain_name, federator_name)

    #  Should raise a ValueError if parent_domain_name is an empty string.
    def test_raise_value_error_parent_domain_name_empty_string(self):
        # Given
        parent_domain_name = ""
        federator_name = "federation"

        # When, Then
        with pytest.raises(ValueError):
            get_federator_id_root(parent_domain_name, federator_name)

    #  Should raise a ValueError if federator_name is an empty string.
    def test_raise_value_error_federator_name_empty_string(self):
        # Given
        parent_domain_name = "example.com"
        federator_name = ""

        # When, Then
        with pytest.raises(ValueError):
            get_federator_id_root(parent_domain_name, federator_name)

    #  Should raise a ValueError if federator_name contains only whitespace.
    def test_raise_value_error_federator_name_whitespace(self):
        # Given
        parent_domain_name = "example.com"
        federator_name = "   "

        # When, Then
        with pytest.raises(ValueError):
            get_federator_id_root(parent_domain_name, federator_name)


class TestGetAuditorDidRoot:
    #  Should return a string representing a did web for a provider s when given a valid parent domain name and auditor name.
    def test_valid_parent_domain_name_and_auditor_name(self):
        # Given
        parent_domain_name = "example.com"
        auditor_name = "auditor1"

        # When
        result = get_auditor_id_root(parent_domain_name, auditor_name)

        # Then
        assert isinstance(result, str)
        assert result == "https://auditor1.auditor.example.com"

    #  Should replace "provider" with "auditor" in the parent domain name when "provider" is present.
    def test_replace_provider_with_auditor(self):
        # Given
        parent_domain_name = "provider.example.com"
        auditor_name = "auditor1"

        # When
        result = get_auditor_id_root(parent_domain_name, auditor_name)

        # Then
        assert isinstance(result, str)
        assert result == "https://auditor1.auditor.example.com"

    #  Should raise a TypeError when either parent_domain_name or auditor_name is not a string.
    def test_raise_type_error(self):
        # Given
        parent_domain_name = 123
        auditor_name = "auditor1"

        # When, Then
        with pytest.raises(TypeError):
            get_auditor_id_root(parent_domain_name, auditor_name)

        # Given
        parent_domain_name = "example.com"
        auditor_name = 123

        # When, Then
        with pytest.raises(TypeError):
            get_auditor_id_root(parent_domain_name, auditor_name)

    #  Should raise a ValueError when auditor_name is an empty string or contains only whitespace.
    def test_raise_value_error_empty_auditor_name(self):
        # Given
        parent_domain_name = "example.com"
        auditor_name = ""

        # When, Then
        with pytest.raises(ValueError):
            get_auditor_id_root(parent_domain_name, auditor_name)

        # Given
        parent_domain_name = "example.com"
        auditor_name = "   "

        # When, Then
        with pytest.raises(ValueError):
            get_auditor_id_root(parent_domain_name, auditor_name)

    #  Should raise a ValueError when parent_domain_name is an empty string or contains only whitespace.
    def test_raise_value_error_empty_parent_domain_name(self):
        # Given
        parent_domain_name = ""
        auditor_name = "auditor1"

        # When, Then
        with pytest.raises(ValueError):
            get_auditor_id_root(parent_domain_name, auditor_name)

        # Given
        parent_domain_name = "   "
        auditor_name = "auditor1"

        # When, Then
        with pytest.raises(ValueError):
            get_auditor_id_root(parent_domain_name, auditor_name)

    #  Should handle parent_domain_name with "auditor" in it.
    def test_handle_parent_domain_name_with_auditor(self):
        # Given
        parent_domain_name = "auditor.example.com"
        auditor_name = "auditor1"

        # When
        result = get_auditor_id_root(parent_domain_name, auditor_name)

        # Then
        assert isinstance(result, str)
        assert result == "https://auditor1.auditor.example.com"


class TestGetProviderLegalParticipantDocumentID:
    #  Should return a valid did web for a legal participant.
    def test_should_return_valid_did_web_for_legal_participant(self):
        # Given
        parent_domain_name = "example.com"
        provider_name = "provider1"
        uuid_param = "ec9c6cb5-48a7-4b0e-8b62-76a52e9528dd"

        # When
        result = get_provider_legal_participant_document_id(parent_domain_name, provider_name, uuid_param)

        # Then
        assert (
            result
            == "https://provider1.provider.example.com/legal-participant-json/ec9c6cb5-48a7-4b0e-8b62-76a52e9528dd/data.json"
        )

    #  Should return a valid did web for a legal participant with a UUID parameter.
    def test_should_return_valid_did_web_for_legal_participant_with_uuid_parameter(self):
        # Given
        parent_domain_name = "example.com"
        provider_name = "provider1"
        uuid_param = "ec9c6cb5-48a7-4b0e-8b62-76a52e9528dd"

        # When
        result = get_provider_legal_participant_document_id(parent_domain_name, provider_name, uuid_param)

        # Then
        assert (
            result
            == "https://provider1.provider.example.com/legal-participant-json/ec9c6cb5-48a7-4b0e-8b62-76a52e9528dd/data.json"
        )

    #  Should return a valid did web for a legal participant with a provider name containing numbers.
    def test_should_return_valid_did_web_for_legal_participant_with_provider_name_containing_numbers(self):
        # Given
        parent_domain_name = "example.com"
        provider_name = "provider123"
        uuid_param = "ec9c6cb5-48a7-4b0e-8b62-76a52e9528dd"

        # When
        result = get_provider_legal_participant_document_id(parent_domain_name, provider_name, uuid_param)

        # Then
        assert (
            result
            == "https://provider123.provider.example.com/legal-participant-json/ec9c6cb5-48a7-4b0e-8b62-76a52e9528dd/data.json"
        )

    #  Should return a valid did web for a legal participant with a parent domain name containing numbers.
    def test_should_return_valid_did_web_for_legal_participant_with_parent_domain_name_containing_numbers(self):
        # Given
        parent_domain_name = "example123.com"
        provider_name = "provider1"
        uuid_param = "ec9c6cb5-48a7-4b0e-8b62-76a52e9528dd"

        # When
        result = get_provider_legal_participant_document_id(parent_domain_name, provider_name, uuid_param)

        # Then
        assert (
            result
            == "https://provider1.provider.example123.com/legal-participant-json/ec9c6cb5-48a7-4b0e-8b62-76a52e9528dd/data.json"
        )

    #  Should return a valid did web for a legal participant with a provider name containing special characters.
    def test_should_return_valid_did_web_for_legal_participant_with_provider_name_containing_special_characters(self):
        # Given
        parent_domain_name = "example.com"
        provider_name = "provider!@#$%^&*()"
        uuid_param = "ec9c6cb5-48a7-4b0e-8b62-76a52e9528dd"

        # When
        result = get_provider_legal_participant_document_id(parent_domain_name, provider_name, uuid_param)

        # Then
        assert (
            result
            == "https://provider!@#$%^&*().provider.example.com/legal-participant-json/ec9c6cb5-48a7-4b0e-8b62-76a52e9528dd/data.json"
        )

    #  Should raise a TypeError if parent_domain_name is not a string.
    def test_should_raise_type_error_if_parent_domain_name_is_not_string(self):
        # Given
        parent_domain_name = 12345
        provider_name = "provider1"
        uuid_param = "ec9c6cb5-48a7-4b0e-8b62-76a52e9528dd"

        # When/Then
        with pytest.raises(TypeError):
            get_provider_legal_participant_document_id(parent_domain_name, provider_name, uuid_param)

    #  Should raise a TypeError if provider_name is not a string.
    def test_should_raise_type_error_if_provider_name_is_not_string(self):
        # Given
        parent_domain_name = "example.com"
        provider_name = 12345
        uuid_param = "ec9c6cb5-48a7-4b0e-8b62-76a52e9528dd"

        # When/Then
        with pytest.raises(TypeError):
            get_provider_legal_participant_document_id(parent_domain_name, provider_name, uuid_param)

    #  Should raise a TypeError if uuid_param is not a string or None.
    def test_should_raise_type_error_if_uuid_param_is_not_string_or_none(self):
        # Given
        parent_domain_name = "example.com"
        provider_name = "provider1"
        uuid_param = 12345

        # When/Then
        with pytest.raises(TypeError):
            get_provider_legal_participant_document_id(parent_domain_name, provider_name, uuid_param)

    #  Should raise a ValueError if parent_domain_name is an empty string.
    def test_should_raise_value_error_if_parent_domain_name_is_empty_string(self):
        # Given
        parent_domain_name = ""
        provider_name = "provider1"
        uuid_param = "12345"

        # When/Then
        with pytest.raises(ValueError):
            get_provider_legal_participant_document_id(parent_domain_name, provider_name, uuid_param)


class TestGetFederationLegalParticipantDocumentID:
    #  Should return a valid did web for a legal participant.
    def test_should_return_valid_did_web_for_legal_participant(self):
        # Given
        parent_domain_name = "example.com"
        federation_name = "federation"
        uuid_param = "ec9c6cb5-48a7-4b0e-8b62-76a52e9528dd"

        # When
        result = get_federator_legal_participant_document_id(parent_domain_name, federation_name, uuid_param)

        # Then
        assert (
            result
            == "https://federation.example.com/legal-participant-json/ec9c6cb5-48a7-4b0e-8b62-76a52e9528dd/data.json"
        )

    #  Should return a valid did web for a legal participant with a provider name containing numbers.
    def test_should_return_valid_did_web_for_legal_participant_with_provider_name_containing_numbers(self):
        # Given
        parent_domain_name = "example.com"
        federation_name = "federation123"
        uuid_param = "ec9c6cb5-48a7-4b0e-8b62-76a52e9528dd"

        # When
        result = get_federator_legal_participant_document_id(parent_domain_name, federation_name, uuid_param)

        # Then
        assert (
            result
            == "https://federation123.example.com/legal-participant-json/ec9c6cb5-48a7-4b0e-8b62-76a52e9528dd/data.json"
        )

    #  Should return a valid did web for a legal participant with a parent domain name containing numbers.
    def test_should_return_valid_did_web_for_legal_participant_with_parent_domain_name_containing_numbers(self):
        # Given
        parent_domain_name = "example123.com"
        federation_name = "federation"
        uuid_param = "ec9c6cb5-48a7-4b0e-8b62-76a52e9528dd"

        # When
        result = get_federator_legal_participant_document_id(parent_domain_name, federation_name, uuid_param)

        # Then
        assert (
            result
            == "https://federation.example123.com/legal-participant-json/ec9c6cb5-48a7-4b0e-8b62-76a52e9528dd/data.json"
        )

    #  Should return a valid did web for a legal participant with a provider name containing special characters.
    def test_should_return_valid_did_web_for_legal_participant_with_provider_name_containing_special_characters(self):
        # Given
        parent_domain_name = "example.com"
        federation_name = "federation!@#$%^&*()"
        uuid_param = "ec9c6cb5-48a7-4b0e-8b62-76a52e9528dd"

        # When
        result = get_federator_legal_participant_document_id(parent_domain_name, federation_name, uuid_param)

        # Then
        assert (
            result
            == "https://federation!@#$%^&*().example.com/legal-participant-json/ec9c6cb5-48a7-4b0e-8b62-76a52e9528dd/data.json"
        )

    #  Should raise a TypeError if parent_domain_name is not a string.
    def test_should_raise_type_error_if_parent_domain_name_is_not_string(self):
        # Given
        parent_domain_name = 12345
        federation_name = "federation"
        uuid_param = "ec9c6cb5-48a7-4b0e-8b62-76a52e9528dd"

        # When/Then
        with pytest.raises(TypeError):
            result = get_federator_legal_participant_document_id(parent_domain_name, federation_name, uuid_param)

    #  Should raise a TypeError if provider_name is not a string.
    def test_should_raise_type_error_if_provider_name_is_not_string(self):
        # Given
        parent_domain_name = "example.com"
        federation_name = 12345
        uuid_param = "ec9c6cb5-48a7-4b0e-8b62-76a52e9528dd"

        # When/Then
        with pytest.raises(TypeError):
            result = get_federator_legal_participant_document_id(parent_domain_name, federation_name, uuid_param)

    #  Should raise a TypeError if uuid_param is not a string or None.
    def test_should_raise_type_error_if_uuid_param_is_not_string_or_none(self):
        # Given
        parent_domain_name = "example.com"
        federation_name = "federation"
        uuid_param = 12345

        # When/Then
        with pytest.raises(TypeError):
            result = get_federator_legal_participant_document_id(parent_domain_name, federation_name, uuid_param)

    #  Should raise a ValueError if parent_domain_name is an empty string.
    def test_should_raise_value_error_if_parent_domain_name_is_empty_string(self):
        # Given
        parent_domain_name = ""
        federation_name = "federation"
        uuid_param = "12345"

        # When/Then
        with pytest.raises(ValueError):
            result = get_federator_legal_participant_document_id(parent_domain_name, federation_name, uuid_param)


class TestGetAuditorLegalParticipantDocumentID:
    #  Should return a valid did web for a legal participant.
    def test_should_return_valid_did_web_for_legal_participant(self):
        # Given
        parent_domain_name = "example.com"
        auditor_name = "auditor1"
        uuid_param = "ec9c6cb5-48a7-4b0e-8b62-76a52e9528dd"

        # When
        result = get_auditor_legal_participant_document_id(parent_domain_name, auditor_name, uuid_param)

        # Then
        assert (
            result
            == "https://auditor1.auditor.example.com/legal-participant-json/ec9c6cb5-48a7-4b0e-8b62-76a52e9528dd/data.json"
        )

    #  Should return a valid did web for a legal participant with a provider name containing numbers.
    def test_should_return_valid_did_web_for_legal_participant_with_provider_name_containing_numbers(self):
        # Given
        parent_domain_name = "example.com"
        auditor_name = "auditor123"
        uuid_param = "ec9c6cb5-48a7-4b0e-8b62-76a52e9528dd"

        # When
        result = get_auditor_legal_participant_document_id(parent_domain_name, auditor_name, uuid_param)

        # Then
        assert (
            result
            == "https://auditor123.auditor.example.com/legal-participant-json/ec9c6cb5-48a7-4b0e-8b62-76a52e9528dd/data.json"
        )

    #  Should return a valid did web for a legal participant with a parent domain name containing numbers.
    def test_should_return_valid_did_web_for_legal_participant_with_parent_domain_name_containing_numbers(self):
        # Given
        parent_domain_name = "example123.com"
        auditor_name = "auditor"
        uuid_param = "ec9c6cb5-48a7-4b0e-8b62-76a52e9528dd"

        # When
        result = get_auditor_legal_participant_document_id(parent_domain_name, auditor_name, uuid_param)

        # Then
        assert (
            result
            == "https://auditor.auditor.example123.com/legal-participant-json/ec9c6cb5-48a7-4b0e-8b62-76a52e9528dd/data.json"
        )

    #  Should return a valid did web for a legal participant with a provider name containing special characters.
    def test_should_return_valid_did_web_for_legal_participant_with_provider_name_containing_special_characters(self):
        # Given
        parent_domain_name = "example.com"
        auditor_name = "auditor!@#$%^&*()"
        uuid_param = "ec9c6cb5-48a7-4b0e-8b62-76a52e9528dd"

        # When
        result = get_auditor_legal_participant_document_id(parent_domain_name, auditor_name, uuid_param)

        # Then
        assert (
            result
            == "https://auditor!@#$%^&*().auditor.example.com/legal-participant-json/ec9c6cb5-48a7-4b0e-8b62-76a52e9528dd/data.json"
        )

    #  Should raise a TypeError if parent_domain_name is not a string.
    def test_should_raise_type_error_if_parent_domain_name_is_not_string(self):
        # Given
        parent_domain_name = 12345
        auditor_name = "auditor"
        uuid_param = "ec9c6cb5-48a7-4b0e-8b62-76a52e9528dd"

        # When/Then
        with pytest.raises(TypeError):
            result = get_auditor_legal_participant_document_id(parent_domain_name, auditor_name, uuid_param)

    #  Should raise a TypeError if provider_name is not a string.
    def test_should_raise_type_error_if_provider_name_is_not_string(self):
        # Given
        parent_domain_name = "example.com"
        auditor_name = 12345
        uuid_param = "ec9c6cb5-48a7-4b0e-8b62-76a52e9528dd"

        # When/Then
        with pytest.raises(TypeError):
            result = get_auditor_legal_participant_document_id(parent_domain_name, auditor_name, uuid_param)

    #  Should raise a TypeError if uuid_param is not a string or None.
    def test_should_raise_type_error_if_uuid_param_is_not_string_or_none(self):
        # Given
        parent_domain_name = "example.com"
        auditor_name = "auditor"
        uuid_param = 12345

        # When/Then
        with pytest.raises(TypeError):
            result = get_auditor_legal_participant_document_id(parent_domain_name, auditor_name, uuid_param)

    #  Should raise a ValueError if parent_domain_name is an empty string.
    def test_should_raise_value_error_if_parent_domain_name_is_empty_string(self):
        # Given
        parent_domain_name = ""
        auditor_name = "auditor"
        uuid_param = "12345"

        # When/Then
        with pytest.raises(ValueError):
            result = get_auditor_legal_participant_document_id(parent_domain_name, auditor_name, uuid_param)
