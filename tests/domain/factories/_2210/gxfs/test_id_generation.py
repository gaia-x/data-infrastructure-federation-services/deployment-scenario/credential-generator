# -*- coding: utf-8 -*-
from credential_generator.domain.factories._2210.gxfs.__init__ import get_compliance_label_id
from credential_generator.domain.factories._2210.gxfs.__init__ import get_compliance_reference_id

import pytest


class TestGetComplianceLabelId:
    #  Returns a compliance label ID when given valid input parameters.
    def test_returns_compliance_label_id_with_valid_input_parameters(self):
        # Given
        parent_domain_name = "provider.example.com"
        federator_name = "federator1"
        label_name = "label1"
        label_level = "level1"

        # When
        compliance_label_id = get_compliance_label_id(parent_domain_name, federator_name, label_name, label_level)

        # Then
        assert (
            compliance_label_id
            == "https://federator1.example.com/compliance-label-json/5bce0fe268fa6b4b801fb1082f185b90000ab1258202c653f64b4f037d172336/data.json"
        )

    #  Handles parent domain names with 'provider.' or 'auditor.' prefixes.
    def test_handles_parent_domain_names_with_prefixes(self):
        # Given
        parent_domain_name_provider = "provider.example.com"
        parent_domain_name_auditor = "auditor.example.com"
        federator_name = "federator1"
        label_name = "label1"
        label_level = "level1"

        # When
        compliance_label_id_provider = get_compliance_label_id(
            parent_domain_name_provider, federator_name, label_name, label_level
        )
        compliance_label_id_auditor = get_compliance_label_id(
            parent_domain_name_auditor, federator_name, label_name, label_level
        )

        # Then
        assert (
            compliance_label_id_provider
            == "https://federator1.example.com/compliance-label-json/5bce0fe268fa6b4b801fb1082f185b90000ab1258202c653f64b4f037d172336/data.json"
        )
        assert (
            compliance_label_id_auditor
            == "https://federator1.example.com/compliance-label-json/5bce0fe268fa6b4b801fb1082f185b90000ab1258202c653f64b4f037d172336/data.json"
        )

    #  Handles compliance label levels with special characters and spaces.
    def test_handles_compliance_label_levels_with_special_characters_and_spaces(self):
        # Given
        parent_domain_name = "provider.example.com"
        federator_name = "federator1"
        label_name = "label1"
        label_level = "level!@#$%^&*()_+-={}[]|\\:;\"'<>,.?/~`"

        # When
        compliance_label_id = get_compliance_label_id(parent_domain_name, federator_name, label_name, label_level)

        # Then
        assert (
            compliance_label_id
            == "https://federator1.example.com/compliance-label-json/ab0a6ef74d56ca820bc4d86019aeab99c19016b47eb25727536942f64ef17446/data.json"
        )

    #  Raises a TypeError when any required parameter is None.
    def test_raises_type_error_when_any_required_parameter_is_none(self):
        # Given
        parent_domain_name = "provider.example.com"
        federator_name = "federator1"
        label_name = "label1"
        label_level = None

        # When/Then
        with pytest.raises(TypeError):
            get_compliance_label_id(parent_domain_name, federator_name, label_name, label_level)


class TestGetComplianceReferenceDid:
    #  Returns a valid compliance reference DID for valid input values.
    def test_valid_input_values(self):
        # Given
        parent_domain_name = "example.com"
        federator_name = "federator"
        compliance_reference_title = "Compliance Reference"
        compliance_reference_version = "1.0"

        expected_did = "https://federator.example.com/compliance-reference-json/451761778a8ffe7dca44eba84da5a7ca64effc49a36aa207363aed698af1485f/data.json"

        # When
        result = get_compliance_reference_id(
            parent_domain_name, federator_name, compliance_reference_title, compliance_reference_version
        )

        # Then
        assert result.startswith(expected_did)

    def test_raises_type_error_when_parent_domain_name_is_none(self):
        # Given
        parent_domain_name = None
        federator_name = "federator"
        compliance_reference_title = "Compliance Reference"
        compliance_reference_version = "1.0"

        # When/Then
        with pytest.raises(TypeError):
            get_compliance_reference_id(
                parent_domain_name, federator_name, compliance_reference_title, compliance_reference_version
            )

    def test_raises_type_error_when_federator_name_is_none(self):
        # Given
        parent_domain_name = "example.com"
        federator_name = None
        compliance_reference_title = "Compliance Reference"
        compliance_reference_version = "1.0"

        # When/Then
        with pytest.raises(TypeError):
            get_compliance_reference_id(
                parent_domain_name, federator_name, compliance_reference_title, compliance_reference_version
            )

    def test_raises_type_error_when_compliance_reference_title_is_none(self):
        # Given
        parent_domain_name = "example.com"
        federator_name = "federator"
        compliance_reference_title = None
        compliance_reference_version = "1.0"

        # When/Then
        with pytest.raises(TypeError):
            get_compliance_reference_id(
                parent_domain_name, federator_name, compliance_reference_title, compliance_reference_version
            )

    def test_raises_type_error_when_compliance_reference_version_is_none(self):
        # Given
        parent_domain_name = "example.com"
        federator_name = "federator"
        compliance_reference_title = "Compliance Reference"
        compliance_reference_version = None

        # When/Then
        with pytest.raises(TypeError):
            get_compliance_reference_id(
                parent_domain_name, federator_name, compliance_reference_title, compliance_reference_version
            )

    def test_raises_type_error_when_parent_domain_name_is_empty_string(self):
        # Given
        parent_domain_name = ""
        federator_name = "federator"
        compliance_reference_title = "Compliance Reference"
        compliance_reference_version = "1.0"

        # When/Then
        with pytest.raises(ValueError):
            get_compliance_reference_id(
                parent_domain_name, federator_name, compliance_reference_title, compliance_reference_version
            )

    def test_raises_type_error_when_federator_name_is_empty_string(self):
        # Given
        parent_domain_name = "example.com"
        federator_name = ""
        compliance_reference_title = "Compliance Reference"
        compliance_reference_version = "1.0"

        # When/Then
        with pytest.raises(ValueError):
            get_compliance_reference_id(
                parent_domain_name, federator_name, compliance_reference_title, compliance_reference_version
            )

    def test_raises_type_error_when_compliance_reference_title_is_empty_string(self):
        # Given
        parent_domain_name = "example.com"
        federator_name = "federator"
        compliance_reference_title = ""
        compliance_reference_version = "1.0"

        # When/Then
        with pytest.raises(ValueError):
            get_compliance_reference_id(
                parent_domain_name, federator_name, compliance_reference_title, compliance_reference_version
            )
