# -*- coding: utf-8 -*-
import pytest

from credential_generator.domain.factories._2210 import (
    get_compliance_reference_manager_legal_participant_document_id,
)
from credential_generator.domain.factories._2210.gxfs import (
    get_compliance_reference_id,
    get_compliance_certification_scheme_id,
    get_third_party_compliance_certification_scheme_id,
    get_self_assessed_certificate_claim_did,
)
from credential_generator.utilities.did_helpers import (
    calculate_federator_hash_value,
    calculate_self_assessed_certificate_claim_hash_value,
)


class TestGetComplianceReferenceDid:
    #  Tests that the function returns the correct compliance reference did for valid input values.
    def test_valid_input_values(self):
        parent_domain_name = "example.com"
        federator_name = "federator"
        compliance_reference_title = "Compliance Reference"
        compliance_reference_version = "1.0"

        expected_did = "https://federator.example.com/compliance-reference"

        assert get_compliance_reference_id(
            parent_domain_name, federator_name, compliance_reference_title, compliance_reference_version
        ).startswith(expected_did)

    #  Tests that the function returns the correct compliance reference did for minimum length values of compliance_reference_title and compliance_reference_version.
    def test_minimum_length_values(self):
        parent_domain_name = "example.com"
        federator_name = "federator"
        compliance_reference_title = "A"
        compliance_reference_version = "0"

        expected_did = "https://federator.example.com/compliance-reference"

        assert get_compliance_reference_id(
            parent_domain_name, federator_name, compliance_reference_title, compliance_reference_version
        ).startswith(expected_did)

    #  Tests that the function returns the correct compliance reference did for maximum length values of compliance_reference_title and compliance_reference_version.
    def test_maximum_length_values(self):
        parent_domain_name = "example.com"
        federator_name = "federator"
        compliance_reference_title = "A" * 100
        compliance_reference_version = "0" * 100

        expected_did = "https://federator.example.com/compliance-reference"

        assert get_compliance_reference_id(
            parent_domain_name, federator_name, compliance_reference_title, compliance_reference_version
        ).startswith(expected_did)

    #  Tests that the function returns the correct compliance reference did for alphanumeric values of compliance_reference_title and compliance_reference_version.
    def test_alphanumeric_values(self):
        parent_domain_name = "example.com"
        federator_name = "federator"
        compliance_reference_title = "Compliance123"
        compliance_reference_version = "1.0alpha"

        expected_did = "https://federator.example.com/compliance-reference"

        assert get_compliance_reference_id(
            parent_domain_name, federator_name, compliance_reference_title, compliance_reference_version
        ).startswith(expected_did)

    #  Tests that the function returns the correct compliance reference did for special character values of compliance_reference_title and compliance_reference_version.
    def test_special_character_values(self):
        parent_domain_name = "example.com"
        federator_name = "federator"
        compliance_reference_title = "!@#$%^&*()"
        compliance_reference_version = "1.0!@#$%^&*()"

        expected_did = "https://federator.example.com/compliance-reference"

        assert get_compliance_reference_id(
            parent_domain_name, federator_name, compliance_reference_title, compliance_reference_version
        ).startswith(expected_did)

    #  Tests that the function gracefully handles errors and exceptions.
    def test_error_handling(self):
        parent_domain_name = "example.com"
        federator_name = "federator"
        compliance_reference_title = None
        compliance_reference_version = "1.0"

        with pytest.raises(TypeError):
            get_compliance_reference_id(
                parent_domain_name, federator_name, compliance_reference_title, compliance_reference_version
            )


class TestGetComplianceCertificationSchemeDid:
    #  Tests that the function returns the correct compliance certification scheme DID for a valid input.
    def test_happy_path(self):
        parent_domain_name = "example.com"
        federator_name = "federator"
        compliance_reference_id = "12345"
        expected_did = "https://federator.example.com/compliance-certification-scheme"

        assert get_compliance_certification_scheme_id(
            parent_domain_name, federator_name, compliance_reference_id
        ).startswith(expected_did)

    #  Tests that the function returns the correct compliance certification scheme DID when the input strings are empty.
    def test_empty_input(self):
        parent_domain_name = ""
        federator_name = ""
        compliance_reference_id = ""
        expected_did = "did:web./:participant:/compliance-certification-scheme//data.json"

        with pytest.raises(ValueError):
            assert (
                get_compliance_certification_scheme_id(parent_domain_name, federator_name, compliance_reference_id)
                == expected_did
            )

    #  Tests that the function returns the correct compliance certification scheme DID when the input strings contain special characters.
    def test_special_characters(self):
        parent_domain_name = "example.com"
        federator_name = "federator!@#$%^&*()"
        compliance_reference_id = "12345!@#$%^&*()"
        expected_did = "https://federator!@#$%^&*().example.com/compliance-certification-scheme"

        assert get_compliance_certification_scheme_id(
            parent_domain_name, federator_name, compliance_reference_id
        ).startswith(expected_did)

    #  Tests that the function returns the correct compliance certification scheme DID when the input strings are very long.
    def test_long_input(self):
        parent_domain_name = "a" * 10
        federator_name = "b" * 10
        compliance_reference_id = "c" * 10
        expected_did = "https://bbbbbbbbbb.aaaaaaaaaa/compliance-certification-scheme"

        assert get_compliance_certification_scheme_id(
            parent_domain_name, federator_name, compliance_reference_id
        ).startswith(expected_did)

    #  Tests that the function returns the correct compliance certification scheme DID when the input strings contain non-ASCII characters.
    def test_non_ascii_characters(self):
        parent_domain_name = "example.com"
        federator_name = "federator_日本語"
        compliance_reference_id = "12345_한글"
        expected_did = "https://federator_日本語.example.com/compliance-certification-scheme-json/"

        assert get_compliance_certification_scheme_id(
            parent_domain_name, federator_name, compliance_reference_id
        ).startswith(expected_did)

    #  Tests that the function returns the correct compliance certification scheme DID for an edge case scenario.
    def test_edge_case(self):
        parent_domain_name = "example.com"
        federator_name = "federator"
        compliance_reference_id = "1"
        expected_did = "https://federator.example.com/compliance-certification-scheme"

        assert get_compliance_certification_scheme_id(
            parent_domain_name, federator_name, compliance_reference_id
        ).startswith(expected_did)


class TestGetThirdPartyComplianceCertificationSchemeDid:
    #  Tests that the function returns the correct URL path for a valid input.
    def test_happy_path(self):
        # Arrange
        parent_domain_name = "example.com"
        federator_name = "federator"
        compliance_reference_id = "12345"

        # Act
        result = get_third_party_compliance_certification_scheme_id(
            parent_domain_name, federator_name, compliance_reference_id
        )

        # Assert
        assert result.startswith("https://federator.example.com/third-party-compliance-certification-scheme")

    #  Tests that the function returns the correct URL path when compliance_reference_id is empty.
    def test_empty_compliance_reference_id(self):
        # Arrange
        parent_domain_name = "example.com"
        federator_name = "federator"
        compliance_reference_id = ""

        # Act
        with pytest.raises(ValueError):
            get_third_party_compliance_certification_scheme_id(
                parent_domain_name, federator_name, compliance_reference_id
            )

    #  Tests that the function returns the correct URL path when parent_domain_name is empty.
    def test_empty_parent_domain_name(self):
        # Arrange
        parent_domain_name = ""
        federator_name = "federator"
        compliance_reference_id = "12345"

        # Act
        with pytest.raises(ValueError):
            get_third_party_compliance_certification_scheme_id(
                parent_domain_name, federator_name, compliance_reference_id
            )

    #  Tests that the function returns the correct URL path when federator_name is empty.
    def test_empty_federator_name(self):
        # Arrange
        parent_domain_name = "example.com"
        federator_name = ""
        compliance_reference_id = "12345"

        # Act
        with pytest.raises(ValueError):
            get_third_party_compliance_certification_scheme_id(
                parent_domain_name, federator_name, compliance_reference_id
            )

    #  Tests that the function returns the correct URL path when compliance_reference_id is None.
    def test_none_compliance_reference_id(self):
        # Arrange
        parent_domain_name = "example.com"
        federator_name = "federator"
        compliance_reference_id = None

        # Act
        with pytest.raises(TypeError):
            get_third_party_compliance_certification_scheme_id(
                parent_domain_name, federator_name, compliance_reference_id
            )

    #  Tests that the function returns the correct URL path when parent_domain_name is None.
    def test_none_parent_domain_name(self):
        # Arrange
        parent_domain_name = None
        federator_name = "federator"
        compliance_reference_id = "12345"

        # Act
        with pytest.raises(TypeError):
            get_third_party_compliance_certification_scheme_id(
                parent_domain_name, federator_name, compliance_reference_id
            )


class TestGetComplianceReferenceManagerDid:
    #  Tests that the function returns the correct DID when given a valid parent domain name and manager name
    def test_valid_input(self):
        parent_domain_name = "example.com"
        manager_name = "manager1"
        expected_did = f"https://{manager_name}.{parent_domain_name}"

        assert get_compliance_reference_manager_legal_participant_document_id(
            parent_domain_name, manager_name
        ).startswith(expected_did)

    #  Tests that the function handles parent domain names and manager names containing special characters
    def test_special_characters(self):
        parent_domain_name = "example.com"
        manager_name = "manager@#$%^&*"
        expected_did = "https://manager@#$%^&*.example.com/legal-participant-json"

        assert get_compliance_reference_manager_legal_participant_document_id(
            parent_domain_name, manager_name
        ).startswith(expected_did)

    #  Tests that the function handles parent domain names and manager names containing numbers
    def test_numbers(self):
        parent_domain_name = "example.com"
        manager_name = "manager123"
        expected_did = "https://manager123.example.com/legal-participant-json"

        assert get_compliance_reference_manager_legal_participant_document_id(
            parent_domain_name, manager_name
        ).startswith(expected_did)

    #  Tests that the function returns an empty string when given an empty parent domain name
    def test_empty_parent_domain_name(self):
        parent_domain_name = ""
        manager_name = "manager1"

        with pytest.raises(ValueError) as err:
            get_compliance_reference_manager_legal_participant_document_id(parent_domain_name, manager_name)

    #  Tests that the function returns the correct DID when given an empty manager name
    def test_empty_manager_name(self):
        parent_domain_name = "example.com"
        manager_name = ""

        with pytest.raises(ValueError) as err:
            get_compliance_reference_manager_legal_participant_document_id(parent_domain_name, manager_name)

    #  Tests that the function handles very long parent domain names and manager names
    def test_long_input(self):
        parent_domain_name = "a" * 1000
        manager_name = "b" * 1000
        expected_did = f"https://{'b'* 1000 }.{'a' * 1000}/legal-participant-json"
        assert get_compliance_reference_manager_legal_participant_document_id(
            parent_domain_name, manager_name
        ).startswith(expected_did)


class TestGetSelfAssessedCertificateClaimDid:
    #  Should return a string with length greater than 0
    def test_return_string_length_greater_than_zero(self):
        parent_domain_name = "example.com"
        provider_name = "provider"
        criteria = ["criterion1", "criterion2"]
        result = get_self_assessed_certificate_claim_did(parent_domain_name, provider_name, criteria)
        assert isinstance(result, str)
        assert len(result) > 0

    #  Should return a string containing the provider name, parent domain name, and the hash value of the criteria
    def test_return_string_contains_provider_name_parent_domain_name_and_criteria_hash_value(self):
        parent_domain_name = "example.com"
        provider_name = "provider"
        criteria = ["criterion1", "criterion2"]
        result = get_self_assessed_certificate_claim_did(parent_domain_name, provider_name, criteria)
        assert provider_name in result
        assert parent_domain_name in result
        assert calculate_self_assessed_certificate_claim_hash_value(criteria) in result

    #  Should return a string ending with 'data.json'
    def test_return_string_ending_with_data_json(self):
        parent_domain_name = "example.com"
        provider_name = "provider"
        criteria = ["criterion1", "criterion2"]
        result = get_self_assessed_certificate_claim_did(parent_domain_name, provider_name, criteria)
        assert result.endswith("data.json")

    #  Should raise an error if parent_domain_name is None
    def test_raise_error_if_parent_domain_name_is_none(self):
        parent_domain_name = None
        provider_name = "provider"
        criteria = ["criterion1", "criterion2"]
        with pytest.raises(TypeError):
            get_self_assessed_certificate_claim_did(parent_domain_name, provider_name, criteria)

    #  Should raise an error if provider_name is None
    def test_raise_error_if_provider_name_is_none(self):
        parent_domain_name = "example.com"
        provider_name = None
        criteria = ["criterion1", "criterion2"]
        with pytest.raises(TypeError):
            get_self_assessed_certificate_claim_did(parent_domain_name, provider_name, criteria)

    #  Should raise an error if criteria is None
    def test_raise_error_if_criteria_is_none(self):
        parent_domain_name = "example.com"
        provider_name = "provider"
        criteria = None
        with pytest.raises(TypeError):
            get_self_assessed_certificate_claim_did(parent_domain_name, provider_name, criteria)
