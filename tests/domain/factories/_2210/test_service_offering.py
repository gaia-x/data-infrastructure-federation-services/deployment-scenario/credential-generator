# -*- coding: utf-8 -*-
import pytest

from credential_generator.domain._2210.trust_framework.service_offering import RequestType, AccessTypeMeans
from credential_generator.domain.factories._2210.gxfs.service import create_gxfs_service_offering
from credential_generator.domain.factories._2210.trust_framework import (
    get_service_offering_id,
    get_service_offering_vc_id,
)


class TestGetServiceOfferingId:
    #  Returns a string with the correct format when valid input is provided.
    def test_returns_string_with_correct_format_when_valid_input_provided(self):
        # Given
        parent_domain_name = "example.com"
        provider_name = "p"
        service_id = "12345"

        # When
        result = get_service_offering_id(parent_domain_name, provider_name, service_id)

        # Then
        assert isinstance(result, str)
        assert result.startswith("https://p.provider.example.com/service-offering-json/")
        assert result.endswith("/data.json")

    #  Works correctly when the provider name is empty.
    def test_error_when_provider_name_is_empty(self):
        # Given
        parent_domain_name = "example.com"
        provider_name = ""
        service_id = "12345"

        # When
        with pytest.raises(ValueError):
            get_service_offering_id(parent_domain_name, provider_name, service_id)

        # Then

    #  Works correctly when the service id is empty.
    def test_error_when_service_id_is_empty(self):
        # Given
        parent_domain_name = "example.com"
        provider_name = "provider"
        service_id = ""

        # When
        with pytest.raises(ValueError):
            get_service_offering_id(parent_domain_name, provider_name, service_id)

    #  Works correctly when the parent domain name is empty.
    def test_error_when_parent_domain_name_is_empty(self):
        # Given
        parent_domain_name = ""
        provider_name = "provider"
        service_id = "12345"

        # When
        with pytest.raises(ValueError):
            get_service_offering_id(parent_domain_name, provider_name, service_id)

    #  Works correctly when the provider name contains special characters.
    def test_works_correctly_when_provider_name_contains_special_characters(self):
        # Given
        parent_domain_name = "example.com"
        provider_name = "provider@123"
        service_id = "12345"

        # When
        result = get_service_offering_id(parent_domain_name, provider_name, service_id)

        # Then
        assert isinstance(result, str)
        assert result.startswith("https://provider@123.provider.example.com/service-offering-json/")
        assert result.endswith("/data.json")

    #  Raises an exception when the parent domain name is None.
    def test_raises_exception_when_parent_domain_name_is_none(self):
        # Given
        parent_domain_name = None
        provider_name = "provider"
        service_id = "12345"

        # When/Then
        with pytest.raises(Exception):
            get_service_offering_id(parent_domain_name, provider_name, service_id)

    #  Raises an exception when the provider name is None.
    def test_raises_exception_when_provider_name_is_none(self):
        # Given
        parent_domain_name = "example.com"
        provider_name = None
        service_id = "12345"

        # When/Then
        with pytest.raises(TypeError):
            get_service_offering_id(parent_domain_name, provider_name, service_id)

    #  Raises an exception when the service id is None.
    def test_raises_exception_when_service_id_is_none(self):
        # Given
        parent_domain_name = "example.com"
        provider_name = "provider"
        service_id = None

        # When/Then
        with pytest.raises(TypeError):
            get_service_offering_id(parent_domain_name, provider_name, service_id)


class TestGetServiceOfferingVCId:
    #  Returns a string with the correct format when valid input is provided.
    def test_returns_string_with_correct_format_when_valid_input_provided(self):
        # Given
        parent_domain_name = "example.com"
        provider_name = "p"
        service_id = "12345"

        # When
        result = get_service_offering_vc_id(parent_domain_name, provider_name, service_id)

        # Then
        assert isinstance(result, str)
        assert result.startswith("https://p.provider.example.com/service-offering/")
        assert result.endswith("/data.json")

    #  Works correctly when the provider name is empty.
    def test_error_when_provider_name_is_empty(self):
        # Given
        parent_domain_name = "example.com"
        provider_name = ""
        service_id = "12345"

        # When
        with pytest.raises(ValueError):
            get_service_offering_vc_id(parent_domain_name, provider_name, service_id)

        # Then

    #  Works correctly when the service id is empty.
    def test_error_when_service_id_is_empty(self):
        # Given
        parent_domain_name = "example.com"
        provider_name = "provider"
        service_id = ""

        # When
        with pytest.raises(ValueError):
            result = get_service_offering_vc_id(parent_domain_name, provider_name, service_id)

    #  Works correctly when the parent domain name is empty.
    def test_error_when_parent_domain_name_is_empty(self):
        # Given
        parent_domain_name = ""
        provider_name = "provider"
        service_id = "12345"

        # When
        with pytest.raises(ValueError):
            get_service_offering_vc_id(parent_domain_name, provider_name, service_id)

    #  Works correctly when the provider name contains special characters.
    def test_works_correctly_when_provider_name_contains_special_characters(self):
        # Given
        parent_domain_name = "example.com"
        provider_name = "provider@123"
        service_id = "12345"

        # When
        result = get_service_offering_vc_id(parent_domain_name, provider_name, service_id)

        # Then
        assert isinstance(result, str)
        assert result.startswith("https://provider@123.provider.example.com/service-offering/")
        assert result.endswith("/data.json")

    #  Raises an exception when the parent domain name is None.
    def test_raises_exception_when_parent_domain_name_is_none(self):
        # Given
        parent_domain_name = None
        provider_name = "provider"
        service_id = "12345"

        # When/Then
        with pytest.raises(Exception):
            get_service_offering_vc_id(parent_domain_name, provider_name, service_id)

    #  Raises an exception when the provider name is None.
    def test_raises_exception_when_provider_name_is_none(self):
        # Given
        parent_domain_name = "example.com"
        provider_name = None
        service_id = "12345"

        # When/Then
        with pytest.raises(TypeError):
            get_service_offering_vc_id(parent_domain_name, provider_name, service_id)

    #  Raises an exception when the service id is None.
    def test_raises_exception_when_service_id_is_none(self):
        # Given
        parent_domain_name = "example.com"
        provider_name = "provider"
        service_id = None

        # When/Then
        with pytest.raises(TypeError):
            get_service_offering_vc_id(parent_domain_name, provider_name, service_id)


class TestCreateGxfsServiceOffering:
    #  Should create a GxfsServiceOffering object with the correct did_web attribute
    def test_correct_did_web_attribute(self):
        # Given
        parent_domain_name = "example.com"
        provider_name = "Provider"
        service_id = "12345"
        service_name = "Example Service"
        description = "This is an example service"
        keywords = ["example", "service"]
        layers = ["layer1", "layer2"]
        terms_and_conditions = "https://example.com/terms"
        format_type = "application/json"
        policy = "default: allow"
        request_type = RequestType.API
        access_type = AccessTypeMeans.DIGITAL

        # When
        service_offering = create_gxfs_service_offering(
            parent_domain_name=parent_domain_name,
            provider_name=provider_name,
            service_id=service_id,
            service_name=service_name,
            description=description,
            keywords=keywords,
            layers=layers,
            terms_and_conditions=terms_and_conditions,
            format_type=format_type,
            policy=policy,
            request_type=request_type,
            access_type=access_type,
        )

        # Then
        assert (
            service_offering.did_web
            == "https://Provider.provider.example.com/service-offering-json/5994471abb01112afcc18159f6cc74b4f511b99806da59b3caf5a9c173cacfc5/data.json"
        )

    #  Should create a GxfsServiceOffering object with the correct name attribute
    def test_correct_name_attribute(self):
        # Given
        parent_domain_name = "example.com"
        provider_name = "Provider"
        service_id = "12345"
        service_name = "Example Service"
        description = "This is an example service"
        keywords = ["example", "service"]
        layers = ["layer1", "layer2"]
        terms_and_conditions = "https://example.com/terms"
        format_type = "application/json"
        policy = "default: allow"
        request_type = RequestType.API
        access_type = AccessTypeMeans.DIGITAL

        # When
        service_offering = create_gxfs_service_offering(
            parent_domain_name=parent_domain_name,
            provider_name=provider_name,
            service_id=service_id,
            service_name=service_name,
            description=description,
            keywords=keywords,
            layers=layers,
            terms_and_conditions=terms_and_conditions,
            format_type=format_type,
            policy=policy,
            request_type=request_type,
            access_type=access_type,
        )

        # Then
        assert service_offering.name == service_name

    #  Should create a GxfsServiceOffering object with the correct providedBy attribute
    def test_correct_providedBy_attribute(self):
        # Given
        parent_domain_name = "example.com"
        provider_name = "Provider"
        service_id = "12345"
        service_name = "Example Service"
        description = "This is an example service"
        keywords = ["example", "service"]
        layers = ["layer1", "layer2"]
        terms_and_conditions = "https://example.com/terms"
        format_type = "application/json"
        policy = "default: allow"
        request_type = RequestType.API
        access_type = AccessTypeMeans.DIGITAL

        # When
        service_offering = create_gxfs_service_offering(
            parent_domain_name=parent_domain_name,
            provider_name=provider_name,
            service_id=service_id,
            service_name=service_name,
            description=description,
            keywords=keywords,
            layers=layers,
            terms_and_conditions=terms_and_conditions,
            format_type=format_type,
            policy=policy,
            request_type=request_type,
            access_type=access_type,
        )

        # Then
        assert (
            service_offering.providedBy.did_web
            == "https://Provider.provider.example.com/legal-participant-json/472590ae974d4c1f44b3780df0b152d9119f076c61bfb3e8cb6affd7889ac0a8/data.json"
        )

    #  Should create a GxfsServiceOffering object with the correct dataAccountExport attribute
    def test_correct_dataAccountExport_attribute(self):
        # Given
        parent_domain_name = "example.com"
        provider_name = "GaiaX Provider"
        service_id = "12345"
        service_name = "Example Service"
        description = "This is an example service"
        keywords = ["example", "service"]
        layers = ["layer1", "layer2"]
        terms_and_conditions = "https://example.com/terms"
        format_type = "application/json"
        policy = "default: allow"
        request_type = RequestType.API
        access_type = AccessTypeMeans.DIGITAL

        # When
        service_offering = create_gxfs_service_offering(
            parent_domain_name=parent_domain_name,
            provider_name=provider_name,
            service_id=service_id,
            service_name=service_name,
            description=description,
            keywords=keywords,
            layers=layers,
            terms_and_conditions=terms_and_conditions,
            format_type=format_type,
            policy=policy,
            request_type=request_type,
            access_type=access_type,
        )

        # Then
        assert len(service_offering.dataAccountExport) == 1
        assert service_offering.dataAccountExport[0].requestType == request_type
        assert service_offering.dataAccountExport[0].accessType == access_type
        assert service_offering.dataAccountExport[0].formatType == format_type
