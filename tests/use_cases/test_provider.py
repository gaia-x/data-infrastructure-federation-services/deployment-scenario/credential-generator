# -*- coding: utf-8 -*-
import json
import unittest
from pathlib import Path
from unittest.mock import patch

import pytest

from credential_generator.domain._2210.trust_framework.participant import LegalPerson
from credential_generator.use_cases.federation import FederationUseCase
from credential_generator.use_cases.gaiax_participant import LegalParticipantUseCase
from credential_generator.use_cases.provider import ProviderUseCase


# Define a fixture for the GaiaXParticipant instance
@pytest.fixture
def participant_instance(create_directory):
    yield ProviderUseCase(
        csv_root_directory=None,
        jsonld_root_directory=create_directory,
        parent_domain_name="test_domain",
        name="test_name",
    )


# Parametrized test cases
@pytest.mark.parametrize(
    "test_id, participant_folder, json_content, expected_result, expected_exception",
    [
        # Happy path tests with various realistic test values
        ("happy_path_valid_json", "participant_folder", {"key": "value"}, {"key": "value"}, None),
        ("happy_path_empty_json", "participant_folder", {}, {}, None),
        # Edge cases
        ("edge_case_empty_file", "participant_folder", "", "", None),
        ("edge_case_file_not_found", "non_existent_folder", None, {}, "FileNotFoundError"),
        # Error cases
    ],
)
def test_get_json_index(
    test_id, participant_folder, json_content, expected_result, participant_instance, expected_exception, mocker
):
    # Arrange
    mocker.patch("credential_generator.utilities.did_helpers.get_participant_folder", return_value=participant_folder)

    if expected_exception == "FileNotFoundError":
        mocker.patch("builtins.open", side_effect=FileNotFoundError)
        result = participant_instance._get_json_index()
        assert result == expected_result
    else:
        mocker.patch("builtins.open", mocker.mock_open(read_data=json.dumps(json_content)))
        result = participant_instance._get_json_index()
        assert result == expected_result


@pytest.mark.parametrize(
    "test_id, parent_domain_name, participant_name, jsonld_root_directory, json_index, expected_filepath",
    [
        # Happy path tests with various realistic test values
        (
            "HP-1",
            "provider.example.com",
            "participant1",
            "/data/jsonld",
            {"key": "value"},
            "/data/jsonld/participant1.provider.example.com/vc/index.json",
        ),
        (
            "HP-2",
            "test.org",
            "participant2",
            "/var/jsonld",
            {"data": 123},
            "/var/jsonld/participant2.provider.test.org/vc/index.json",
        ),
        # Edge cases
        (
            "EC-1",
            "auditor.example.com",
            "participant1",
            "/data/jsonld",
            {},
            "/data/jsonld/participant1.provider.example.com/vc/index.json",
        ),
        # Empty json_index
        # Error cases are not explicitly defined here as the function does not handle errors itself.
        # Errors would be raised by the called functions and should be tested within their own test suites.
    ],
)
def test_store_json_index(
    test_id, parent_domain_name, participant_name, jsonld_root_directory, json_index, expected_filepath
):
    # Arrange
    participant = ProviderUseCase(
        parent_domain_name=parent_domain_name,
        name=participant_name,
        jsonld_root_directory=jsonld_root_directory,
        csv_root_directory=None,
    )
    with patch("credential_generator.use_cases.gaiax_participant.save_json_on_disk_by_filepath") as mock_save_json:
        # Act
        filepath = Path(expected_filepath)
        participant._store_json_index(index_file=filepath, json_index=json_index)

        # Assert
        mock_save_json.assert_called_once_with(filepath=filepath, data=json_index)


@pytest.mark.parametrize(
    "test_input, expected_output, expected_exception, test_id",
    [
        # Happy path tests with various realistic test values
        (
            [
                {"@id": "123", "credentialSubject": {"@id": "abc", "type": ["Person", "Student"]}},
                {"id": "456", "credentialSubject": {"id": "def", "type": "Person"}},
            ],
            {"Student": {"abc": "123"}, "Person": {"def": "456"}},
            None,
            "HP-1",
        ),
        # Edge case: Empty list
        ([], {}, None, "EC-1_EMPTY_LIST_ID"),
        # Edge case: List with None values
        (
            [None, {"id": "789", "credentialSubject": {"id": "ghi", "type": "Person"}}],
            {"Person": {"ghi": "789"}},
            None,
            "EC-2__NONE_VALUES_ID",
        ),
        # Error case: Invalid type in the credentialSubject
        (
            [{"id": "101112", "credentialSubject": {"id": "jkl", "type": 123}}],
            None,
            TypeError,
            "ER-1_INVALID_TYPE_ID",
        ),
        # Error case: Missing 'id' or '@id' in the result
        ([{"credentialSubject": {"id": "mno", "type": "Person"}}], None, KeyError, "ER-2__MISSING_ID_ID"),
    ],
)
def test_generate_vc_index(test_input, expected_output, expected_exception, test_id):
    if expected_exception is not None:
        with pytest.raises(expected_exception):
            LegalParticipantUseCase._LegalParticipantUseCase__generate_vc_index(test_input)
    else:
        actual_output = LegalParticipantUseCase._LegalParticipantUseCase__generate_vc_index(test_input)

        assert actual_output == expected_output


# Mocks for the dependencies
@pytest.fixture
def mock_log():
    with patch("credential_generator.use_cases.gaiax_participant.log") as mock:
        yield mock


@pytest.fixture
def mock_create_gaiax_terms_and_condition_object():
    with patch(
        "credential_generator.use_cases.gaiax_participant.self.__create_gaiax_terms_and_condition_object"
    ) as mock:
        yield mock


@pytest.fixture
def mock_request_registration_number_vc():
    with patch("credential_generator.use_cases.gaiax_participant.self.__request_registration_number_vc") as mock:
        yield mock


@pytest.fixture
def mock_dump_gaiax_terms_and_conditions_to_json_ld():
    with patch("credential_generator.use_cases.gaiax_participant.dump_gaiax_terms_and_conditions_to_json_ld") as mock:
        yield mock


@pytest.fixture
def mock_dump_legal_registration_number_vc():
    with patch("credential_generator.use_cases.gaiax_participant.dump_legal_registration_number_vc") as mock:
        yield mock


@pytest.fixture
def mock_extract_file_path_from_did_web():
    with patch("credential_generator.use_cases.gaiax_participant.extract_file_path_from_object_id") as mock:
        yield mock
