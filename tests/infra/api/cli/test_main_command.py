# -*- coding: utf-8 -*-
from typer.testing import CliRunner

from credential_generator import __app_name__, __version__
from credential_generator.infra.api.cli import app, CSV_ROOT_DIRECTORY_OPTION, FEDERATION_NAME_OPTION
from credential_generator.infra.api.cli.auditor_commands import AUDITOR_NAME_OPTION
from credential_generator.infra.api.cli.common_commands import (
    JSONLD_ROOT_DIRECTORY_OPTION,
    PARENT_DOMAIN_OPTION,
    DATABASE_FILENAME_OPTION,
    PORT_NUMBER_OPTION,
    CATALOGUE_API_KEY_OPTION,
    FEDERATION_PARENT_DOMAIN_OPTION,
)
from credential_generator.infra.api.cli.provider_commands import PROVIDER_NAME_OPTION

runner = CliRunner()


class TestVersionCommand:
    def test_version_command(self):
        result = runner.invoke(app=app, args=["--version"])
        assert result.exit_code == 0
        assert f"🇪🇺 Gaia-X {__app_name__}" in result.stdout
        assert f"Version v{__version__}" in result.stdout

    def test_help_command(self):
        result = runner.invoke(app=app, args=["--help"])
        assert result.exit_code == 0
        assert "Options" in result.stdout
        assert "--version" in result.stdout
        assert "--install-completion" in result.stdout
        assert "--show-completion" in result.stdout
        assert "--help" in result.stdout
        assert "Commands" in result.stdout
        assert "auditor" in result.stdout
        assert "federation" in result.stdout
        assert "init" in result.stdout
        assert "provider" in result.stdout


class TestInitializeDatabaseCommand:
    def test_help_command(self):
        result = runner.invoke(app=app, args=["init", "--help"])
        assert result.exit_code == 0
        assert all(name in result.stdout for name in FEDERATION_NAME_OPTION.param_decls)
        assert all(name in result.stdout for name in CSV_ROOT_DIRECTORY_OPTION.param_decls)
        assert all(name in result.stdout for name in DATABASE_FILENAME_OPTION.param_decls)
        assert "--help" in result.stdout


class TestAuditorDatabaseCommand:
    def test_help_command(self):
        result = runner.invoke(app=app, args=["auditor", "--help"])
        assert result.exit_code == 0
        assert "--help" in result.stdout
        assert "bootstrap" in result.stdout
        assert "cleanup" in result.stdout
        assert "upload" in result.stdout

    def test_bootstrap_help_command(self):
        result = runner.invoke(app=app, args=["auditor", "bootstrap", "--help"])
        assert result.exit_code == 0
        assert all(name in result.stdout for name in AUDITOR_NAME_OPTION.param_decls)
        assert all(name in result.stdout for name in CSV_ROOT_DIRECTORY_OPTION.param_decls)
        assert all(name in result.stdout for name in JSONLD_ROOT_DIRECTORY_OPTION.param_decls)
        assert all(name in result.stdout for name in PARENT_DOMAIN_OPTION.param_decls)
        assert all(name in result.stdout for name in DATABASE_FILENAME_OPTION.param_decls)
        assert "--help" in result.stdout

    def test_cleanup_help_command(self):
        result = runner.invoke(app=app, args=["auditor", "cleanup", "--help"])
        assert result.exit_code == 0
        assert all(name in result.stdout for name in AUDITOR_NAME_OPTION.param_decls)
        assert all(name in result.stdout for name in JSONLD_ROOT_DIRECTORY_OPTION.param_decls)
        assert all(name in result.stdout for name in PARENT_DOMAIN_OPTION.param_decls)
        assert "--help" in result.stdout

    def test_upload_help_command(self):
        result = runner.invoke(app=app, args=["auditor", "upload", "--help"])
        assert result.exit_code == 0
        assert all(name in result.stdout for name in AUDITOR_NAME_OPTION.param_decls)
        assert all(name in result.stdout for name in JSONLD_ROOT_DIRECTORY_OPTION.param_decls)
        assert all(name in result.stdout for name in PARENT_DOMAIN_OPTION.param_decls)
        assert "--help" in result.stdout


class TestFederationDatabaseCommand:
    def test_help_command(self):
        result = runner.invoke(app=app, args=["federation", "--help"])
        assert result.exit_code == 0
        assert "--help" in result.stdout
        assert "bootstrap" in result.stdout
        assert "cleanup" in result.stdout
        assert "upload" in result.stdout

    def test_bootstrap_help_command(self):
        result = runner.invoke(app=app, args=["federation", "bootstrap", "--help"])
        assert result.exit_code == 0
        assert all(name in result.stdout for name in FEDERATION_NAME_OPTION.param_decls)
        assert all(name in result.stdout for name in CSV_ROOT_DIRECTORY_OPTION.param_decls)
        assert all(name in result.stdout for name in JSONLD_ROOT_DIRECTORY_OPTION.param_decls)
        assert all(name in result.stdout for name in PARENT_DOMAIN_OPTION.param_decls)
        assert all(name in result.stdout for name in DATABASE_FILENAME_OPTION.param_decls)
        assert "--help" in result.stdout

    def test_cleanup_help_command(self):
        result = runner.invoke(app=app, args=["federation", "cleanup", "--help"])
        assert result.exit_code == 0
        assert all(name in result.stdout for name in FEDERATION_NAME_OPTION.param_decls)
        assert all(name in result.stdout for name in JSONLD_ROOT_DIRECTORY_OPTION.param_decls)
        assert all(name in result.stdout for name in PARENT_DOMAIN_OPTION.param_decls)
        assert "--help" in result.stdout

    def test_upload_help_command(self):
        result = runner.invoke(app=app, args=["federation", "upload", "--help"])
        assert result.exit_code == 0
        assert all(name in result.stdout for name in FEDERATION_NAME_OPTION.param_decls)
        assert all(name in result.stdout for name in JSONLD_ROOT_DIRECTORY_OPTION.param_decls)
        assert all(name in result.stdout for name in PARENT_DOMAIN_OPTION.param_decls)
        assert "--help" in result.stdout

    def test_sync_help_command(self):
        result = runner.invoke(app=app, args=["federation", "sync", "--help"])
        assert result.exit_code == 0
        assert all(name in result.stdout for name in PROVIDER_NAME_OPTION.param_decls)
        assert all(name in result.stdout for name in JSONLD_ROOT_DIRECTORY_OPTION.param_decls)
        assert all(name in result.stdout for name in PARENT_DOMAIN_OPTION.param_decls)
        assert all(name in result.stdout for name in PORT_NUMBER_OPTION.param_decls)
        assert all(name in result.stdout for name in CATALOGUE_API_KEY_OPTION.param_decls)
        assert "--help" in result.stdout


class TestProviderDatabaseCommand:
    def test_help_command(self):
        result = runner.invoke(app=app, args=["provider", "--help"])
        assert result.exit_code == 0
        assert "--help" in result.stdout
        assert "bootstrap" in result.stdout
        assert "cleanup" in result.stdout
        assert "upload" in result.stdout

    def test_bootstrap_help_command(self):
        result = runner.invoke(app=app, args=["provider", "bootstrap", "--help"])
        assert result.exit_code == 0
        assert all(name in result.stdout for name in PROVIDER_NAME_OPTION.param_decls)
        assert all(name in result.stdout for name in CSV_ROOT_DIRECTORY_OPTION.param_decls)
        assert all(name in result.stdout for name in JSONLD_ROOT_DIRECTORY_OPTION.param_decls)
        assert all(name in result.stdout for name in PARENT_DOMAIN_OPTION.param_decls)
        assert all(name in result.stdout for name in FEDERATION_PARENT_DOMAIN_OPTION.param_decls)
        assert all(name in result.stdout for name in DATABASE_FILENAME_OPTION.param_decls)
        assert "--help" in result.stdout

    def test_cleanup_help_command(self):
        result = runner.invoke(app=app, args=["provider", "cleanup", "--help"])
        assert result.exit_code == 0
        assert all(name in result.stdout for name in PROVIDER_NAME_OPTION.param_decls)
        assert all(name in result.stdout for name in JSONLD_ROOT_DIRECTORY_OPTION.param_decls)
        assert all(name in result.stdout for name in PARENT_DOMAIN_OPTION.param_decls)
        assert "--help" in result.stdout

    def test_upload_help_command(self):
        result = runner.invoke(app=app, args=["provider", "upload", "--help"])
        assert result.exit_code == 0
        assert all(name in result.stdout for name in PROVIDER_NAME_OPTION.param_decls)
        assert all(name in result.stdout for name in JSONLD_ROOT_DIRECTORY_OPTION.param_decls)
        assert all(name in result.stdout for name in PARENT_DOMAIN_OPTION.param_decls)
        assert "--help" in result.stdout

    def test_sync_help_command(self):
        result = runner.invoke(app=app, args=["provider", "sync", "--help"])
        assert result.exit_code == 0
        assert all(name in result.stdout for name in PROVIDER_NAME_OPTION.param_decls)
        assert all(name in result.stdout for name in JSONLD_ROOT_DIRECTORY_OPTION.param_decls)
        assert all(name in result.stdout for name in PARENT_DOMAIN_OPTION.param_decls)
        assert all(name in result.stdout for name in PORT_NUMBER_OPTION.param_decls)
        assert all(name in result.stdout for name in CATALOGUE_API_KEY_OPTION.param_decls)
        assert "--help" in result.stdout
