# -*- coding: utf-8 -*-
import csv
from pathlib import Path

from credential_generator.infra.spi.filesystem.reader.compliance_criterion_csv_reader import (
    ComplianceCriterionCSVReader,
)
from credential_generator.infra.spi.filesystem.reader.compliance_reference_csv_reader import (
    ComplianceReferenceCSVReader,
)


class TestComplianceReferenceCSVReader:
    def test_read_with_valid_csv_file_return_expected_data_structure(self, create_directory):
        # Create a temporary CSV file with valid data
        input_file = Path(create_directory, "temp.csv")
        with open(file=input_file, mode="w", newline="", encoding="utf-8-sig") as csvfile:
            writer = csv.writer(csvfile, delimiter=";")
            writer.writerow(
                [
                    "ID",
                    "Champ1",
                    "Compliance Resource Id",
                    "CR-ID",
                    "Ref Designation",
                    "Extrait",
                    "Category",
                    "Valid from",
                    "Valid Until",
                    "Reference Doc",
                    "Assertion Method",
                    "Assertion Date",
                    "Issuer Designation",
                ]
            )
            writer.writerow(
                [
                    "1",
                    "1",
                    "XXX001",
                    "CR-01",
                    "Criterion 2",
                    "9/9/2009",
                    "Data Protection",
                    "2/2/2022",
                    "",
                    "https://example.com/1",
                    "Certified",
                    "",
                    "",
                ]
            )
            writer.writerow(
                [
                    "2",
                    "2",
                    "XXX002",
                    "CR-02",
                    "Criterion 2",
                    "20/02/2002",
                    "Data Porting",
                    "1/1/21",
                    "",
                    "https://example.com/2",
                    "Self-Declared",
                    "",
                    "",
                ]
            )

        # Create an instance of ComplianceCriterionCSVReader
        reader = ComplianceReferenceCSVReader(
            datasource_root_directory=create_directory,
            filename="temp.csv",
        )

        # Call the method under test
        references = reader.read()

        # Assert the result is a dictionary with the expected keys and values
        assert isinstance(references, list)
        assert len(references) == 2

        assert all(
            reference in references
            for reference in [
                {
                    "title": "Criterion 2",
                    "version": "9/9/2009",
                    "criterion_reference_id": "CR-01",
                    "category": "Data Protection",
                    "valid_from": None,
                    "valid_until": "",
                    "reference": "https://example.com/1",
                    "assertion_method": "Certified",
                    "assertion_date": "",
                    "issuer_designation": "",
                },
                {
                    "title": "Criterion 2",
                    "version": "20/02/2002",
                    "criterion_reference_id": "CR-02",
                    "category": "Data Porting",
                    "valid_from": None,
                    "valid_until": "",
                    "reference": "https://example.com/2",
                    "assertion_method": "Self-Declared",
                    "assertion_date": "",
                    "issuer_designation": "",
                },
            ]
        )

        # Remove the temporary CSV file
        input_file.unlink()

    def test_read_with_empty_csv_file_return_empty_data_structure(self, create_directory):
        # Create a temporary CSV file with valid data
        input_file = Path(create_directory, "temp.csv")
        with open(file=input_file, mode="w", newline="", encoding="utf-8-sig") as csvfile:
            writer = csv.writer(csvfile, delimiter=";")

        # Create an instance of ComplianceCriterionCSVReader
        reader = ComplianceReferenceCSVReader(
            datasource_root_directory=create_directory,
            filename="temp.csv",
        )

        # Call the method under test
        references = reader.read()

        # Assert the result is a dictionary with the expected keys and values
        assert isinstance(references, list)
        assert len(references) == 0

        # Remove the temporary CSV file
        input_file.unlink()

    def test_read_with_csv_file_with_only_header_return_empty_data_structure(self, create_directory):
        # Create a temporary CSV file with valid data
        input_file = Path(create_directory, "temp.csv")
        with open(file=input_file, mode="w", newline="", encoding="utf-8-sig") as csvfile:
            writer = csv.writer(csvfile, delimiter=";")
            writer.writerow(
                [
                    "ID",
                    "Champ1",
                    "Compliance Resource Id",
                    "CR-ID",
                    "Ref Designation",
                    "Extrait",
                    "Category",
                    "Valid from",
                    "Valid Until",
                    "Reference Doc",
                    "Assertion Method",
                    "Assertion Date",
                    "Issuer Designation",
                ]
            )

        # Create an instance of ComplianceCriterionCSVReader
        reader = ComplianceReferenceCSVReader(
            datasource_root_directory=create_directory,
            filename="temp.csv",
        )

        # Call the method under test
        references = reader.read()

        # Assert the result is a dictionary with the expected keys and values
        assert isinstance(references, list)
        assert len(references) == 0

        # Remove the temporary CSV file
        input_file.unlink()
