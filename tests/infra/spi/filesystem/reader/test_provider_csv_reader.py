# -*- coding: utf-8 -*-
import csv
from pathlib import Path

from credential_generator.infra.spi.filesystem.reader.provider_csv_reader import (
    read_location,
    read_layers,
    read_keywords,
    read_services,
    read_self_assessments,
    read_provider_compliance_schemes,
)


class TestProviderCSVReader:
    def test_read_location_with_csv_file_with_only_header_return_empty_data_structure(self, create_directory):
        # Create a temporary CSV file with valid data
        input_file = Path(create_directory, "3-Locations.csv")
        with open(file=input_file, mode="w", newline="", encoding="utf-8-sig") as csvfile:
            writer = csv.writer(csvfile, delimiter=";")
            writer.writerow(
                [
                    "Location Id",
                    "Provider Designation",
                    "City;State/Dpt",
                    "Country",
                ]
            )

        # Create an instance of ComplianceCriterionCSVReader
        # Call the method under test
        references = read_location(input_file, "test")

        # Assert the result is a dictionary with the expected keys and values
        assert isinstance(references, list)
        assert len(references) == 0

        # Remove the temporary CSV file
        input_file.unlink()

    def test_read_location_with_unknown_csv_file_return_empty_data_structure(self, create_directory):
        # Create a temporary CSV file with valid data
        input_file = Path(create_directory, "3-Locations.csv")

        # Create an instance of ComplianceCriterionCSVReader
        # Call the method under test
        references = read_location(input_file, "test")

        # Assert the result is a dictionary with the expected keys and values
        assert isinstance(references, list)
        assert len(references) == 0

    def test_read_location_with_directory_return_empty_data_structure(self, create_directory):
        # Create a temporary CSV file with valid data
        input_file = Path(create_directory)

        # Create an instance of ComplianceCriterionCSVReader
        # Call the method under test
        references = read_location(input_file, "test")

        # Assert the result is a dictionary with the expected keys and values
        assert isinstance(references, list)
        assert len(references) == 0

    def test_read_location_with_empty_csv_file__return_empty_data_structure(self, create_directory):
        # Create a temporary CSV file with valid data
        input_file = Path(create_directory, "3-Locations.csv")
        with open(file=input_file, mode="w", newline="", encoding="utf-8-sig") as csvfile:
            writer = csv.writer(csvfile, delimiter=";")

        # Call the method under test
        references = read_location(input_file, "test")

        # Assert the result is a dictionary with the expected keys and values
        assert isinstance(references, list)
        assert len(references) == 0

        # Remove the temporary CSV file
        input_file.unlink()

    def test_read_location_with_csv_file_return_expected_data_structure(self, create_directory):
        # Create a temporary CSV file with valid data
        input_file = Path(create_directory, "3-Locations.csv")
        with open(file=input_file, mode="w", newline="", encoding="utf-8-sig") as csvfile:
            writer = csv.writer(csvfile, delimiter=";")
            writer.writerow(
                [
                    "Location Id",
                    "Provider Designation",
                    "City",
                    "State/Dpt",
                    "Country",
                ]
            )
            writer.writerow(["LOC-01", "PAR1", "Paris", "IDF", "France"])
            writer.writerow(["LOC-02", "PAR2", "Paris", "IDF", "France"])

        # Call the method under test
        references = read_location(input_file, "test")

        # Assert the result is a dictionary with the expected keys and values
        assert isinstance(references, list)
        assert len(references) == 2

        assert all(
            reference in references
            for reference in [
                {
                    "city": "Paris",
                    "country": "France",
                    "designation": "PAR1",
                    "location_id": "LOC-01",
                    "provider_name": "test",
                    "state": "IDF",
                },
                {
                    "city": "Paris",
                    "country": "France",
                    "designation": "PAR2",
                    "location_id": "LOC-02",
                    "provider_name": "test",
                    "state": "IDF",
                },
            ]
        )

        # Remove the temporary CSV file
        input_file.unlink()

    def test_read_layers_with_csv_file_with_only_header_return_empty_data_structure(self, create_directory):
        # Create a temporary CSV file with valid data
        input_file = Path(create_directory, "5-Layers.csv")
        with open(file=input_file, mode="w", newline="", encoding="utf-8-sig") as csvfile:
            writer = csv.writer(csvfile, delimiter=";")
            writer.writerow(
                [
                    "Layer",
                    "Service ID",
                ]
            )

        # Create an instance of ComplianceCriterionCSVReader
        references = read_layers(input_file, "test")

        # Assert the result is a dictionary with the expected keys and values
        assert isinstance(references, list)
        assert len(references) == 0

        # Remove the temporary CSV file
        input_file.unlink()

    def test_read_layers_with_empty_csv_file__return_empty_data_structure(self, create_directory):
        # Create a temporary CSV file with valid data
        input_file = Path(create_directory, "5-Layers.csv")
        with open(file=input_file, mode="w", newline="", encoding="utf-8-sig") as csvfile:
            writer = csv.writer(csvfile, delimiter=";")

        # Call the method under test
        references = read_layers(input_file, "test")

        # Assert the result is a dictionary with the expected keys and values
        assert isinstance(references, list)
        assert len(references) == 0

        # Remove the temporary CSV file
        input_file.unlink()

    def test_read_layers_with_csv_file_return_expected_data_structure(self, create_directory):
        # Create a temporary CSV file with valid data
        # Create a temporary CSV file with valid data
        input_file = Path(create_directory, "5-Layers.csv")
        with open(file=input_file, mode="w", newline="", encoding="utf-8-sig") as csvfile:
            writer = csv.writer(csvfile, delimiter=";")
            writer.writerow(
                [
                    "Layer",
                    "Service ID",
                ]
            )
            writer.writerow(
                [
                    "IAAS",
                    "S-01;S-02",
                ]
            )
            writer.writerow(
                [
                    "PAAS",
                ]
            )

        # Call the method under test
        references = read_layers(input_file, "test")

        # Assert the result is a dictionary with the expected keys and values
        assert isinstance(references, list)
        assert len(references) == 2

        assert all(
            reference in references
            for reference in [
                {"layer": "IAAS", "provider_name": "test", "service_ids": ["S-01", "S-02"]},
                {"layer": "PAAS", "provider_name": "test", "service_ids": None},
            ]
        )

        # Remove the temporary CSV file
        input_file.unlink()

    def test_read_keywords_with_csv_file_with_only_header_return_empty_data_structure(self, create_directory):
        # Create a temporary CSV file with valid data
        input_file = Path(create_directory, "6-Keywords.csv")
        with open(file=input_file, mode="w", newline="", encoding="utf-8-sig") as csvfile:
            writer = csv.writer(csvfile, delimiter=";")
            writer.writerow(
                [
                    "Keyword",
                    "Service IDs",
                ]
            )

        # Call the method under test
        references = read_keywords(input_file, "test")

        # Assert the result is a dictionary with the expected keys and values
        assert isinstance(references, list)
        assert len(references) == 0

        # Remove the temporary CSV file
        input_file.unlink()

    def test_read_keywords_with_empty_csv_file__return_empty_data_structure(self, create_directory):
        # Create a temporary CSV file with valid data
        input_file = Path(create_directory, "6-Keywords.csv")
        with open(file=input_file, mode="w", newline="", encoding="utf-8-sig") as csvfile:
            writer = csv.writer(csvfile, delimiter=";")

        # Call the method under test
        references = read_keywords(input_file, "test")

        # Assert the result is a dictionary with the expected keys and values
        assert isinstance(references, list)
        assert len(references) == 0

        # Remove the temporary CSV file
        input_file.unlink()

    def test_read_keywords_with_csv_file_return_expected_data_structure(self, create_directory):
        # Create a temporary CSV file with valid data
        # Create a temporary CSV file with valid data
        input_file = Path(create_directory, "6-Keywords.csv")
        with open(file=input_file, mode="w", newline="", encoding="utf-8-sig") as csvfile:
            writer = csv.writer(csvfile, delimiter=";")
            writer.writerow(
                [
                    "Keyword",
                    "Service IDs",
                ]
            )
            writer.writerow(
                [
                    "Authentication",
                ]
            )
            writer.writerow(["Compute", "S-01;S-02"])

        # Call the method under test
        references = read_keywords(input_file, "test")

        # Assert the result is a dictionary with the expected keys and values
        assert isinstance(references, list)
        assert len(references) == 2

        assert all(
            reference in references
            for reference in [
                {"keyword": "Authentication", "provider_name": "test", "service_ids": None},
                {"keyword": "Compute", "provider_name": "test", "service_ids": ["S-01", "S-02"]},
            ]
        )

        # Remove the temporary CSV file
        input_file.unlink()

    def test_read_services_with_csv_file_with_only_header_return_empty_data_structure(self, create_directory):
        # Create a temporary CSV file with valid data
        input_file = Path(create_directory, "4-Services.csv")
        with open(file=input_file, mode="w", newline="", encoding="utf-8-sig") as csvfile:
            writer = csv.writer(csvfile, delimiter=";")
            writer.writerow(
                [
                    "Service Id",
                    "Designation",
                    "Service Version",
                    "Comply With",
                    "Available In",
                    "Level of Label",
                ]
            )

        # Call the method under test
        references = read_keywords(input_file, "test")

        # Assert the result is a dictionary with the expected keys and values
        assert isinstance(references, list)
        assert len(references) == 0

        # Remove the temporary CSV file
        input_file.unlink()

    def test_read_services_with_empty_csv_file__return_empty_data_structure(self, create_directory):
        # Create a temporary CSV file with valid data
        input_file = Path(create_directory, "4-Services.csv")
        with open(file=input_file, mode="w", newline="", encoding="utf-8-sig") as csvfile:
            writer = csv.writer(csvfile, delimiter=";")

        # Call the method under test
        references = read_services(input_file, "test")

        # Assert the result is a dictionary with the expected keys and values
        assert isinstance(references, list)
        assert len(references) == 0

        # Remove the temporary CSV file
        input_file.unlink()

    def test_read_services_with_csv_file_return_expected_data_structure(self, create_directory):
        # Create a temporary CSV file with valid data
        # Create a temporary CSV file with valid data
        input_file = Path(create_directory, "4-Services.csv")
        with open(file=input_file, mode="w", newline="", encoding="utf-8-sig") as csvfile:
            writer = csv.writer(csvfile, delimiter=";")
            writer.writerow(
                [
                    "Service Id",
                    "Designation",
                    "Service Version",
                    "Comply With",
                    "Available In",
                    "Level of Label",
                ]
            )
            writer.writerow(
                [
                    "S-01",
                    "IaaS public",
                    "",
                    "CR-01; CR-02",
                    "LOC-01",
                    "2",
                ]
            )

        # Call the method under test
        references = read_services(input_file, "test")

        # Assert the result is a dictionary with the expected keys and values
        assert isinstance(references, list)
        assert len(references) == 1

        assert all(
            reference in references
            for reference in [
                {
                    "compliance_reference_ids": ["CR-01", "CR-02"],
                    "description": None,
                    "designation": "IaaS public",
                    "level": "2",
                    "location_ids": ["LOC-01"],
                    "provider_name": "test",
                    "service_id": "S-01",
                    "service_version": "",
                }
            ]
        )

        # Remove the temporary CSV file
        input_file.unlink()

    def test_read_self_assessments_with_csv_file_with_only_header_return_empty_data_structure(self, create_directory):
        # Create a temporary CSV file with valid data
        input_file = Path(create_directory, "self-assessment.csv")
        with open(file=input_file, mode="w", newline="", encoding="utf-8-sig") as csvfile:
            writer = csv.writer(csvfile, delimiter=";")
            writer.writerow(
                [
                    "Section",
                    "Criterion",
                    "Relevant for Gaia-X Labels",
                    "Service(s)  Id- for which the  self-assesment of the criterion is  OK",
                    "External available proof - Optinoal  (if any)",
                ]
            )

        # Call the method under test
        references = read_self_assessments(input_file, "test")

        # Assert the result is a dictionary with the expected keys and values
        assert isinstance(references, list)
        assert len(references) == 0

        # Remove the temporary CSV file
        input_file.unlink()

    def test_read_self_assessments_with_empty_csv_file__return_empty_data_structure(self, create_directory):
        # Create a temporary CSV file with valid data
        input_file = Path(create_directory, "self-assessment.csv")
        with open(file=input_file, mode="w", newline="", encoding="utf-8-sig") as csvfile:
            writer = csv.writer(csvfile, delimiter=";")

        # Call the method under test
        references = read_self_assessments(input_file, "test")

        # Assert the result is a dictionary with the expected keys and values
        assert isinstance(references, list)
        assert len(references) == 0

        # Remove the temporary CSV file
        input_file.unlink()

    def test_read_self_assessments_with_csv_file_return_expected_data_structure(self, create_directory):
        # Create a temporary CSV file with valid data
        # Create a temporary CSV file with valid data
        input_file = Path(create_directory, "self-assessment.csv")
        with open(file=input_file, mode="w", newline="", encoding="utf-8-sig") as csvfile:
            writer = csv.writer(csvfile, delimiter=";")
            writer.writerow(
                [
                    "Section",
                    "Criterion",
                    "Relevant for Gaia-X Labels",
                    "Service(s)  Id- for which the  self-assesment of the criterion is  OK",
                    "External available proof - Optinoal  (if any)",
                ]
            )
            writer.writerow(["Section01", "Criterion 1", "Level 1 and 2", "S-01; S-02", "CR-01"])

        # Call the method under test
        references = read_self_assessments(input_file, "test")

        # Assert the result is a dictionary with the expected keys and values
        assert isinstance(references, list)
        assert len(references) == 1

        assert all(
            reference in references
            for reference in [
                {
                    "compliance_reference_ids": ["CR-01"],
                    "criterion": "Criterion 1",
                    "provider_name": "test",
                    "section": "Section01",
                    "service_ids": ["S-01", "S-02"],
                }
            ]
        )

        # Remove the temporary CSV file
        input_file.unlink()

    def test_read_provider_compliance_schemes_with_csv_file_with_only_header_return_empty_data_structure(
        self, create_directory
    ):
        # Create a temporary CSV file with valid data
        input_file = Path(create_directory, "10-CAB2CR.csv")
        with open(file=input_file, mode="w", newline="", encoding="utf-8-sig") as csvfile:
            writer = csv.writer(csvfile, delimiter=";")
            writer.writerow(
                [
                    "CR-ID",
                    "Ref Designation",
                    "Version",
                    "Valid from",
                    "Valid Until",
                    "Reference Doc",
                    "Assertion Method",
                    "Assertion Date",
                    "Issuer Designation",
                    "CAB-ID",
                ]
            )

        # Call the method under test
        references = read_provider_compliance_schemes(input_file, "test")

        # Assert the result is a dictionary with the expected keys and values
        assert isinstance(references, list)
        assert len(references) == 0

        # Remove the temporary CSV file
        input_file.unlink()

    def test_read_provider_compliance_schemes_with_empty_csv_file__return_empty_data_structure(self, create_directory):
        # Create a temporary CSV file with valid data
        input_file = Path(create_directory, "10-CAB2CR.csv")
        with open(file=input_file, mode="w", newline="", encoding="utf-8-sig") as csvfile:
            writer = csv.writer(csvfile, delimiter=";")

        # Call the method under test
        references = read_provider_compliance_schemes(input_file, "test")

        # Assert the result is a dictionary with the expected keys and values
        assert isinstance(references, list)
        assert len(references) == 0

        # Remove the temporary CSV file
        input_file.unlink()

    def test_read_provider_compliance_schemes_with_csv_file_return_expected_data_structure(self, create_directory):
        # Create a temporary CSV file with valid data
        # Create a temporary CSV file with valid data
        input_file = Path(create_directory, "10-CAB2CR.csv")
        with open(file=input_file, mode="w", newline="", encoding="utf-8-sig") as csvfile:
            writer = csv.writer(csvfile, delimiter=";")
            writer.writerow(
                [
                    "CR-ID",
                    "Ref Designation",
                    "Version",
                    "Valid from",
                    "Valid Until",
                    "Reference Doc",
                    "Assertion Method",
                    "Assertion Date",
                    "Issuer Designation",
                    "CAB-ID",
                ]
            )
            writer.writerow(
                [
                    "CR-01",
                    "test",
                    "v01",
                    "01/01/2001",
                    "01/12/2030",
                    "https://example.com/1",
                    "Certified",
                    "02/05/2015",
                    "MLA",
                    "CAB-10",
                ]
            )
            writer.writerow(
                [
                    "CR-02",
                    "test",
                    "v02",
                    "02/10/2022",
                    "01/12/2030",
                    "https://example.com/2",
                    "Self-Declared",
                    "03/05/2020",
                    "",
                    "",
                ]
            )

        # Call the method under test
        references = read_provider_compliance_schemes(input_file, "test")

        # Assert the result is a dictionary with the expected keys and values
        assert isinstance(references, list)
        assert len(references) == 2

        assert all(
            reference in references
            for reference in [
                {
                    "provider_name": "test",
                    "cab_id": "CAB-10",
                    "issuer_designation": "MLA",
                    "assertion_date": "02/05/2015",
                    "assertion_method": "Certified",
                    "reference_doc": "https://example.com/1",
                    "valid_until": "01/12/2030",
                    "valid_from": "01/01/2001",
                    "version": "v01",
                    "compliance_reference_designation": "test",
                    "compliance_reference_id": "CR-01",
                },
                {
                    "provider_name": "test",
                    "cab_id": "",
                    "issuer_designation": "",
                    "assertion_date": "03/05/2020",
                    "assertion_method": "Self-Declared",
                    "reference_doc": "https://example.com/2",
                    "valid_until": "01/12/2030",
                    "valid_from": "02/10/2022",
                    "version": "v02",
                    "compliance_reference_designation": "test",
                    "compliance_reference_id": "CR-02",
                },
            ]
        )

        # Remove the temporary CSV file
        input_file.unlink()
