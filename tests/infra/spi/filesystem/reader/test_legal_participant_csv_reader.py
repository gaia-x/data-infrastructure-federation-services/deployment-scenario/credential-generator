# -*- coding: utf-8 -*-
import csv
from pathlib import Path

from credential_generator.infra.spi.filesystem.reader.legal_participant_csv_reader import read_legal_participant


class TestLegalParticipantCSVReader:
    def test_read_legal_participant_with_empty_csv_file__return_empty_data_structure(self, create_directory):
        # Create a temporary CSV file with valid data
        input_file = Path(create_directory, "1-Participant.csv")
        with open(file=input_file, mode="w", newline="", encoding="utf-8-sig") as csvfile:
            writer = csv.writer(csvfile, delimiter=";")

        # Create an instance of ComplianceCriterionCSVReader
        # Call the method under test
        references = read_legal_participant(input_file, "test")

        # Assert the result is a dictionary with the expected keys and values
        assert isinstance(references, dict)
        assert len(references) == 0

        # Remove the temporary CSV file
        input_file.unlink()

    def test_read_legal_participant_with_csv_file_with_only_header_return_empty_data_structure(self, create_directory):
        # Create a temporary CSV file with valid data
        input_file = Path(create_directory, "1-Participant.csv")
        with open(file=input_file, mode="w", newline="", encoding="utf-8-sig") as csvfile:
            writer = csv.writer(csvfile, delimiter=";")
            writer.writerow(
                [
                    "Role",
                    "Id",
                    "Designation",
                    "VatID",
                    "Country Name",
                    "Street Address",
                    "Locality",
                    "Postal Code",
                    "LeiCode",
                    "Terms And Conditions",
                ]
            )

        # Call the method under test
        references = read_legal_participant(input_file, "test")

        # Assert the result is a dictionary with the expected keys and values
        assert isinstance(references, dict)
        assert len(references) == 0

        # Remove the temporary CSV file
        input_file.unlink()

    def test_read_legal_participant_with_csv_file_return_expected_data_structure(self, create_directory):
        # Create a temporary CSV file with valid data
        input_file = Path(create_directory, "1-Participant.csv")
        with open(file=input_file, mode="w", newline="", encoding="utf-8-sig") as csvfile:
            writer = csv.writer(csvfile, delimiter=";")
            writer.writerow(
                [
                    "Role",
                    "Id",
                    "Designation",
                    "Street Address",
                    "Postal Code",
                    "Locality",
                    "Country Name",
                    "LeiCode",
                    "VatID",
                    "Terms And Conditions",
                ]
            )
            writer.writerow(
                [
                    "federation",
                    "CAB-00",
                    "test",
                    "55 rue du Faubourg Saint-Honoré",
                    "75008",
                    "Paris",
                    "FR-IDF",
                    "",
                    "FR0123456789",
                    "",
                ]
            )
            writer.writerow(
                [
                    "federation",
                    "CAB-01",
                    "test2",
                    "",
                    "75001",
                    "Paris",
                    "FR-IDF",
                    "",
                    "FR1234567890",
                    "",
                ]
            )

        # Call the method under test
        references = read_legal_participant(input_file, "test")

        # Assert the result is a dictionary with the expected keys and values
        assert isinstance(references, dict)
        assert references in [
            {
                "city": "Paris",
                "country": "FR-IDF",
                "designation": "test",
                "id": "CAB-00",
                "lei_code": "",
                "postal_code": "75008",
                "role": "federation",
                "street_address": "55 rue du Faubourg Saint-Honoré",
                "terms_and_conditions": "",
                "vat_id": "FR0123456789",
                "name": "test",
            }
        ]

        # Remove the temporary CSV file
        input_file.unlink()

    def test_read_legal_participant_with_unknown_csv_file_return_empty_data_structure(self, create_directory):
        # Create a temporary CSV file with valid data
        input_file = Path(create_directory, "1-Participant.csv")

        # Call the method under test
        references = read_legal_participant(input_file, "test")

        # Assert the result is a dictionary with the expected keys and values
        assert isinstance(references, dict)
        assert len(references) == 0

    def test_read_legal_participant_with_directory_return_empty_data_structure(self, create_directory):
        # Create a temporary CSV file with valid data
        input_file = Path(create_directory)

        # Call the method under test
        references = read_legal_participant(input_file, "test")

        # Assert the result is a dictionary with the expected keys and values
        assert isinstance(references, dict)
        assert len(references) == 0
