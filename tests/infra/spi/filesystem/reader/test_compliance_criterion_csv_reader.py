# -*- coding: utf-8 -*-
import csv
from pathlib import Path

from credential_generator.infra.spi.filesystem.reader.compliance_criterion_csv_reader import (
    ComplianceCriterionCSVReader,
)


class TestComplianceCriterionCSVReader:
    def test_read_criterions_and_labels_with_valid_csv_file_return_expected_data_structure(self, create_directory):
        # Create a temporary CSV file with valid data
        input_file = Path(create_directory, "temp.csv")
        with open(file=input_file, mode="w", newline="", encoding="utf-8-sig") as csvfile:
            writer = csv.writer(csvfile, delimiter=";")
            writer.writerow(
                [
                    "Category",
                    "criterion",
                    "criterion-name",
                    "description",
                    "used for label level",
                    "applicable",
                    "self-assessed",
                    "Verification Process ",
                    "Self-declaration / self-assesment",
                    "Scheme 1:::CR-1:Self-Declared",
                    "Scheme 1:::CR-2:Certified",
                ]
            )
            writer.writerow(
                [
                    "Contractual governance",
                    "Criterion 1",
                    "Criterion 1.1.1",
                    "",
                    "Level 1",
                    "",
                    "x",
                    "self-assessment through the trust framework",
                    "x",
                    "x",
                    "",
                ]
            )
            writer.writerow(
                [
                    "Contractual governance",
                    "Criterion 1",
                    "Criterion 1.1.1",
                    "",
                    "Level 2",
                    "",
                    "x",
                    "self-assessment through the trust framework",
                    "x",
                    "x",
                    "",
                ]
            )
            writer.writerow(
                [
                    "Contractual governance",
                    "Criterion 2",
                    "Criterion 1.1.2",
                    "",
                    "Level 2",
                    "",
                    "x",
                    "self-assessment through the trust framework",
                    "x",
                    "",
                    "x",
                ]
            )

        # Create an instance of ComplianceCriterionCSVReader
        reader = ComplianceCriterionCSVReader(datasource_root_directory=create_directory, filename="temp.csv")

        # Call the method under test
        criterions, levels = reader.read_criterions_and_labels()

        # Assert the result is a dictionary with the expected keys and values
        assert isinstance(criterions, list)
        assert isinstance(levels, list)
        assert len(criterions) == 2
        assert len(levels) == 2

        assert all(
            criterion in criterions
            for criterion in [
                {
                    "id": "Criterion 1",
                    "name": "Criterion 1.1.1",
                    "category": "Contractual governance",
                    "description": "",
                    "levels": {"Level 1", "Level 2"},
                    "self_assessed": True,
                },
                {
                    "id": "Criterion 2",
                    "name": "Criterion 1.1.2",
                    "category": "Contractual governance",
                    "description": "",
                    "levels": {"Level 2"},
                    "self_assessed": True,
                },
            ]
        )
        assert all(level in levels for level in ["Level 1", "Level 2"])

        # Remove the temporary CSV file
        input_file.unlink()

    def test_read_criterions_and_labels_with_with_only_header_in_csv_file_return_empty_dict(self, create_directory):
        # Create a temporary CSV file with valid data
        input_file = Path(create_directory, "temp.csv")
        with open(file=input_file, mode="w", newline="", encoding="utf-8-sig") as csvfile:
            writer = csv.writer(csvfile, delimiter=";")
            writer.writerow(
                [
                    "Category",
                    "criterion",
                    "criterion-name",
                    "description",
                    "used for label level",
                    "applicable",
                    "self-assessed",
                    "Verification Process ",
                    "Self-declaration / self-assesment",
                    "Scheme 1:::CR-1:Self-Declared",
                    "Scheme 1:::CR-2:Certified",
                ]
            )

        # Create an instance of ComplianceCriterionCSVReader
        reader = ComplianceCriterionCSVReader(datasource_root_directory=create_directory, filename="temp.csv")

        # Call the method under test
        criterions, levels = reader.read_criterions_and_labels()

        # Assert the result is a dictionary with the expected keys and values
        assert isinstance(criterions, list)
        assert isinstance(levels, list)
        assert len(criterions) == 0
        assert len(levels) == 0

        # Remove the temporary CSV file
        input_file.unlink()

    def test_read_criterions_and_labels_with_empty_csv_file_return_empty_dict(self, create_directory):
        # Create a temporary empty CSV file
        input_file = Path(create_directory, "temp.csv")
        with open(file=input_file, mode="w", newline="", encoding="utf-8-sig") as csvfile:
            pass

        # Create an instance of ComplianceCriterionCSVReader
        reader = ComplianceCriterionCSVReader(datasource_root_directory=create_directory, filename="temp.csv")

        # Call the method under test
        criterions, levels = reader.read_criterions_and_labels()

        # Assert the result is a dictionary with the expected keys and values
        assert isinstance(criterions, list)
        assert isinstance(levels, list)
        assert len(criterions) == 0
        assert len(levels) == 0

        # Remove the temporary CSV file
        input_file.unlink()

    def test_read_compliance_certification_schemes_with_valid_csv_file_return_expected_data_structure(
        self, create_directory
    ):
        # Create a temporary CSV file with valid data
        input_file = Path(create_directory, "temp.csv")
        with open(file=input_file, mode="w", newline="", encoding="utf-8-sig") as csvfile:
            writer = csv.writer(csvfile, delimiter=";")
            writer.writerow(
                ["applicable", "criterion", "used for label level", "CR-1: Self-Declared", "CR-2: Certified"]
            )
            writer.writerow(["", "Criterion 1", "Level 1", "x", ""])
            writer.writerow(["", "Criterion 2", "Level 2", "", "x"])

        # Create an instance of ComplianceCriterionCSVReader
        reader = ComplianceCriterionCSVReader(datasource_root_directory=create_directory, filename="temp.csv")

        # Call the method under test
        schemes, third_party_schemes = reader.read_compliance_certification_schemes()

        # Assert the result is a dictionary with the expected keys and values
        assert isinstance(schemes, dict)
        assert isinstance(third_party_schemes, dict)
        assert "CR-1" in schemes
        assert "CR-2" in third_party_schemes
        assert isinstance(schemes["CR-1"], list)
        assert isinstance(third_party_schemes["CR-2"], list)
        assert schemes["CR-1"] == ["Criterion 1-Level 1"]
        assert third_party_schemes["CR-2"] == ["Criterion 2-Level 2"]

        # Remove the temporary CSV file
        input_file.unlink()

    def test_read_compliance_certification_schemes_with_empty_csv_file_return_empty_dict(self, create_directory):
        # Create a temporary empty CSV file
        input_file = Path(create_directory, "temp.csv")
        with open(file=input_file, mode="w", newline="", encoding="utf-8-sig") as csvfile:
            pass

        # Create an instance of ComplianceCriterionCSVReader
        reader = ComplianceCriterionCSVReader(datasource_root_directory=create_directory, filename="temp.csv")

        # Call the method under test
        schemes, third_party_schemes = reader.read_compliance_certification_schemes()

        # Assert the result is an empty dictionary
        assert isinstance(schemes, dict)
        assert isinstance(third_party_schemes, dict)
        assert schemes == {}
        assert third_party_schemes == {}

        # Remove the temporary CSV file
        input_file.unlink()

    def test_read_compliance_certification_schemes_with_only_header_in_csv_file_return_empty_dict(
        self, create_directory
    ):
        # Create a temporary CSV file with only a header
        input_file = Path(create_directory, "temp.csv")
        with open(file=input_file, mode="w", newline="", encoding="utf-8-sig") as csvfile:
            writer = csv.writer(csvfile, delimiter=";")
            writer.writerow(["applicable", "criterion", "CR-1: Self-Declared", "CR-2: Certified"])

        # Create an instance of ComplianceCriterionCSVReader
        reader = ComplianceCriterionCSVReader(datasource_root_directory=create_directory, filename="temp.csv")

        # Call the method under test
        schemes, third_party_schemes = reader.read_compliance_certification_schemes()

        # Assert the result is an empty dictionary
        assert isinstance(schemes, dict)
        assert isinstance(third_party_schemes, dict)
        assert schemes == {}
        assert third_party_schemes == {}

        # Remove the temporary CSV file
        input_file.unlink()

    def test_read_compliance_certification_schemes_with_missing_fields_in_csv_file_return_empty_dict(
        self, create_directory
    ):
        # Create a temporary CSV file with missing fields
        input_file = Path(create_directory, "temp.csv")
        with open(file=input_file, mode="w", newline="", encoding="utf-8-sig") as csvfile:
            writer = csv.writer(csvfile, delimiter=";")
            writer.writerow(["applicable", "criterion", "CR-1: Self-Declared", "CR-2: Certified"])
            writer.writerow(["", "Criterion 1"])

        # Create an instance of ComplianceCriterionCSVReader
        reader = ComplianceCriterionCSVReader(datasource_root_directory=create_directory, filename="temp.csv")

        # Call the method under test and assert that it raises an exception
        schemes, third_party_schemes = reader.read_compliance_certification_schemes()
        assert isinstance(schemes, dict)
        assert isinstance(third_party_schemes, dict)
        assert schemes == {}
        assert third_party_schemes == {}

        # Remove the temporary CSV file
        input_file.unlink()
