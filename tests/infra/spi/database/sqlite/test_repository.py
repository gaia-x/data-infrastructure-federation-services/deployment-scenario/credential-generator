# -*- coding: utf-8 -*-
import pytest
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, joinedload
from credential_generator.infra.spi.database.sqlite.entities import (
    Base,
    ComplianceReferenceManager,
    ComplianceAssessmentsBody,
    Provider,
    ComplianceCertificationScheme,
    ComplianceCriterionComplianceCertificationSchemeLink,
    ComplianceCriterion,
    ComplianceReference,
    Keyword,
    Layer,
    Level,
    Location,
    ThirdPartyComplianceCertificationSchemeCABProviderLink,
    ComplianceCertificationSchemeProviderLink,
    Service,
)
from credential_generator.infra.spi.database.sqlite.repository import SQLiteRepository

# Setup for the database connection and session
TEST_DATABASE_URL = "sqlite:///:memory:"  # In-memory database for testing


# Fixture for the repository with the in-memory database
@pytest.fixture
def repository():
    repository = SQLiteRepository(database_url=TEST_DATABASE_URL)
    repository.create_database()
    yield repository


# Parametrized test cases for the happy path
@pytest.mark.parametrize(
    "participant, expected_exception, test_id",
    [
        # Test ID: #HP-1
        ({"name": "Compliance Manager A"}, None, "HP-1"),
        # Test ID: #HP-2
        ({"name": "Compliance Manager B", "designation": "Manager B Description"}, None, "HP-2"),
        ({"designation": "test"}, ValueError, "EC-1"),
    ],
)
def test_add_compliance_reference_manager(repository, participant, expected_exception, test_id):
    # Arrange
    # (No arrange step needed as input values are provided via test parameters)

    # Act

    if expected_exception:
        with pytest.raises(expected_exception):
            repository.add_compliance_reference_manager(participant)
    else:
        repository.add_compliance_reference_manager(participant)

        # Assert
        Session = sessionmaker(bind=repository._SQLiteRepository__engine)
        with Session() as s:
            result = s.query(ComplianceReferenceManager).filter_by(name=participant["name"]).one_or_none()
            assert result is not None, test_id
            assert result.name == participant["name"]
            assert result.street == participant.get("street", None)
            assert result.locality == participant.get("locality", None)
            assert result.postal_code == participant.get("postal_code", None)
            assert result.country == participant.get("country", None)
            assert result.registration_number == participant.get("registration_number", None)
            assert result.lei_code == participant.get("lei_code", None)
            assert result.designation == participant.get("designation", None)
            assert result.terms_and_conditions == participant.get("terms_and_conditions", None)


@pytest.mark.parametrize(
    "participant, expected_exception, test_id",
    [
        # Test ID: #HP-1
        ({"id": "CAB-1", "name": "Cab 1"}, None, "HP-1"),
        # Test ID: #HP-2
        ({"id": "CAB-1", "name": "CAB 2", "designation": "Manager B Description"}, None, "HP-2"),
        ({"designation": "test"}, ValueError, "EC-1"),
    ],
)
def test_add_cab(repository, participant, expected_exception, test_id):
    # Arrange
    # (No arrange step needed as input values are provided via test parameters)

    # Act

    if expected_exception:
        with pytest.raises(expected_exception):
            repository.add_cab(participant)
    else:
        repository.add_cab(participant)

        # Assert
        Session = sessionmaker(bind=repository._SQLiteRepository__engine)
        with Session() as s:
            result = s.query(ComplianceAssessmentsBody).filter_by(name=participant["name"]).one_or_none()
            assert result is not None, test_id
            assert result.name == participant["name"]
            assert result.street == participant.get("street", None)
            assert result.locality == participant.get("locality", None)
            assert result.postal_code == participant.get("postal_code", None)
            assert result.country == participant.get("country", None)
            assert result.registration_number == participant.get("registration_number", None)
            assert result.lei_code == participant.get("lei_code", None)
            assert result.designation == participant.get("designation", None)
            assert result.terms_and_conditions == participant.get("terms_and_conditions", None)


@pytest.mark.parametrize(
    "participant, expected_exception, test_id",
    [
        # Test ID: #HP-1
        ({"name": "Provider A"}, None, "HP-1"),
        # Test ID: #HP-2
        ({"name": "Provider B", "designation": "Manager B Description"}, None, "HP-2"),
        ({"designation": "test"}, ValueError, "EC-1"),
    ],
)
def test_add_provider(repository, participant, expected_exception, test_id):
    # Arrange
    # (No arrange step needed as input values are provided via test parameters)

    # Act

    if expected_exception:
        with pytest.raises(expected_exception):
            repository.add_provider(participant)
    else:
        repository.add_provider(participant)

        # Assert
        Session = sessionmaker(bind=repository._SQLiteRepository__engine)
        with Session() as s:
            result = s.query(Provider).filter_by(name=participant["name"]).one_or_none()
            assert result is not None, test_id
            assert result.name == participant["name"]
            assert result.street == participant.get("street", None)
            assert result.locality == participant.get("locality", None)
            assert result.postal_code == participant.get("postal_code", None)
            assert result.country == participant.get("country", None)
            assert result.registration_number == participant.get("registration_number", None)
            assert result.lei_code == participant.get("lei_code", None)
            assert result.designation == participant.get("designation", None)
            assert result.terms_and_conditions == participant.get("terms_and_conditions", None)


@pytest.mark.parametrize(
    "scheme_id, scheme, expected_exception, test_id",
    [
        # Test ID: #HP-1
        ("cr_id1", {"cr_id1": []}, None, "HP-1"),
        ("cr_id2", {"cr_id2": ["CRIT1-Level 1"]}, None, "HP-2"),
        # Error paths:
        ("cr_id1", "", TypeError, "EP-1"),
        ("cr_id1", [], TypeError, "EP-1"),
        ("cr_id1", 12, TypeError, "EP-1"),
        ("cr_id1", True, TypeError, "EP-1"),
    ],
)
def test_add_compliance_certification_scheme(repository, scheme_id, scheme, expected_exception, test_id):
    # Arrange
    # (No arrange step needed as input values are provided via test parameters)

    # Act

    if expected_exception:
        with pytest.raises(expected_exception):
            repository.add_compliance_certification_scheme(scheme)
    else:
        repository.add_compliance_certification_scheme(scheme)

        # Assert
        Session = sessionmaker(bind=repository._SQLiteRepository__engine)
        with Session() as s:
            result = s.query(ComplianceCertificationScheme).filter_by(compliance_reference_id=scheme_id).one_or_none()
            assert result is not None, test_id
            assert result.compliance_reference_id == scheme_id

            result = (
                s.query(ComplianceCriterionComplianceCertificationSchemeLink)
                .filter_by(compliance_reference_id=scheme_id)
                .one_or_none()
            )
            assert result is not None if len(scheme[scheme_id]) > 0 else result is None, test_id
            if result is not None:
                assert result.compliance_reference_id == scheme_id


@pytest.mark.parametrize(
    "criterions, expected_exception, test_id",
    [
        # Happy Paths
        (
            [
                {
                    "id": "crit_id1",
                    "name": "crit1",
                    "description": "criterion 1",
                    "category": "security",
                    "self_assessed": True,
                    "levels": [],
                }
            ],
            None,
            "HP-1",
        ),
        (
            [
                {
                    "id": "crit_id1",
                    "name": "crit1",
                    "description": "criterion 1",
                    "category": "security",
                    "self_assessed": True,
                    "levels": ["Level 1"],
                }
            ],
            None,
            "HP-2",
        ),
        # Edge cases
        (
            [
                {
                    "id": "crit_id1",
                    "name": "crit1",
                    "description": "criterion 1",
                    "category": "security",
                    "self_assessed": True,
                    "levels": ["1"],
                }
            ],
            None,
            "HP-2",
        ),
        # Error Paths
        (
            {
                "id": "crit_id1",
                "name": "crit1",
                "description": "criterion 1",
                "category": "security",
                "self_assessed": True,
                "levels": ["Level 1"],
            },
            TypeError,
            "EP-1",
        ),
        (
            [
                [
                    "crit_id1",
                    "crit1",
                    "criterion 1",
                    "security",
                    True,
                    ["Level 1"],
                ],
            ],
            TypeError,
            "EP-2",
        ),
        (
            [
                [
                    "crit_id1",
                    "crit1",
                    "criterion 1",
                    "security",
                    True,
                    ["Level 1"],
                ],
                {
                    "id": "crit_id2",
                    "name": "crit2",
                    "description": "criterion 2",
                    "category": "security",
                    "self_assessed": False,
                    "levels": ["Level 2"],
                },
            ],
            TypeError,
            "EP-3",
        ),
    ],
)
def test_add_compliance_criterions(repository, criterions, expected_exception, test_id):
    # Arrange
    # (No arrange step needed as input values are provided via test parameters)

    # Act

    if expected_exception:
        with pytest.raises(expected_exception):
            repository.add_compliance_criterions(criterions)
    else:
        repository.add_compliance_criterions(criterions)

        # Assert
        Session = sessionmaker(bind=repository._SQLiteRepository__engine)
        with Session() as s:
            result = s.query(ComplianceCriterion).options(joinedload(ComplianceCriterion.levels)).all()
            assert result is not None, test_id
            for current in result:
                assert current.criterion_id in [criterion.get("id") for criterion in criterions]
                assert current.name in [criterion.get("name") for criterion in criterions]
                assert current.description in [criterion.get("description") for criterion in criterions]
                assert current.category in [criterion.get("category") for criterion in criterions]
                assert current.self_assessed in [criterion.get("self_assessed") for criterion in criterions]
                assert all(
                    level in [criterion.get("self_assessed") for criterion in criterions] for level in current.levels
                )


@pytest.mark.parametrize(
    "references, expected_exception, test_id",
    [
        # Happy Paths
        (
            [
                {
                    "criterion_reference_id": "id1",
                    "compliance_reference_manager_name": "ref",
                    "title": "reference 1",
                    "version": "1",
                    "category": "security",
                }
            ],
            None,
            "HP-1",
        ),
        (
            [
                {
                    "criterion_reference_id": "id1",
                    "compliance_reference_manager_name": "ref",
                    "title": "reference 1",
                    "version": "1",
                    "category": "security",
                    "valid_from": "2023-01-01",
                    "valid_until": "2023-12-31",
                    "reference": "ref",
                    "document_sha256": "123456",
                    "assertion_method": "2023-12-31",
                    "assertion_date": "2023-01-02",
                    "issuer_designation": "2023-12-31",
                }
            ],
            None,
            "HP-1",
        ),
        # Edge cases
        # Error Paths
        (
            {
                "criterion_reference_id": "id1",
                "compliance_reference_manager_name": "ref",
                "title": "reference 1",
                "version": "1",
                "category": "security",
            },
            TypeError,
            "EP-1",
        ),
        (
            [
                [
                    "id1",
                    "ref",
                    "reference 1",
                    "1",
                    "security",
                ],
            ],
            TypeError,
            "EP-2",
        ),
        (
            [
                [
                    "id1",
                    "ref",
                    "reference 1",
                    "1",
                    "security",
                ],
                {
                    "criterion_reference_id": "id1",
                    "compliance_reference_manager_name": "ref",
                    "title": "reference 1",
                    "version": "1",
                    "category": "security",
                },
            ],
            TypeError,
            "EP-3",
        ),
    ],
)
def test_add_compliance_references(repository, references, expected_exception, test_id):
    # Arrange
    # (No arrange step needed as input values are provided via test parameters)

    # Act

    if expected_exception:
        with pytest.raises(expected_exception):
            repository.add_compliance_references(references)
    else:
        repository.add_compliance_references(references)

        # Assert
        Session = sessionmaker(bind=repository._SQLiteRepository__engine)
        with Session() as s:
            result = s.query(ComplianceReference).all()
            assert result is not None, test_id
            for current in result:
                assert current.compliance_reference_id in [
                    reference.get("criterion_reference_id") for reference in references
                ]
                assert current.compliance_reference_manager_name in [
                    reference.get("compliance_reference_manager_name") for reference in references
                ]
                assert current.designation in [reference.get("title") for reference in references]
                assert current.version in [reference.get("version") for reference in references]
                assert current.category in [reference.get("category") for reference in references]
                assert current.valid_from in [reference.get("valid_from") for reference in references]
                assert current.valid_until in [reference.get("valid_until") for reference in references]
                assert current.reference in [reference.get("reference") for reference in references]
                assert current.assertion_method in [reference.get("assertion_method") for reference in references]
                assert current.valid_date in [reference.get("assertion_date") for reference in references]
                assert current.issuer_designation in [reference.get("issuer_designation") for reference in references]
                assert current.reference_hash in [reference.get("document_sha256") for reference in references]


@pytest.mark.parametrize(
    "keywords, expected_exception, test_id",
    [
        # Happy Paths
        (
            [
                "keyword1",
                "keyword2",
            ],
            None,
            "HP-1",
        ),
        (
            [],
            None,
            "HP-1",
        ),
        # Edge cases
        # Error Paths
        (
            {},
            TypeError,
            "EP-1",
        ),
        (
            [
                [
                    "keyword1",
                ],
            ],
            TypeError,
            "EP-2",
        ),
        (
            [
                "keyword1",
                {},
            ],
            TypeError,
            "EP-3",
        ),
        (
            12,
            TypeError,
            "EP-4",
        ),
        (
            True,
            TypeError,
            "EP-5",
        ),
    ],
)
def test_add_keywords(repository, keywords, expected_exception, test_id):
    # Arrange
    # (No arrange step needed as input values are provided via test parameters)

    # Act

    if expected_exception:
        with pytest.raises(expected_exception):
            repository.add_keywords(keywords)
    else:
        repository.add_keywords(keywords)

        # Assert
        Session = sessionmaker(bind=repository._SQLiteRepository__engine)
        with Session() as s:
            result = s.query(Keyword).all()
            assert result is not None, test_id
            for current in result:
                assert current.id in keywords


@pytest.mark.parametrize(
    "layers, expected_exception, test_id",
    [
        # Happy Paths
        (
            [
                "layer1",
                "layer2",
            ],
            None,
            "HP-1",
        ),
        (
            [],
            None,
            "HP-1",
        ),
        # Edge cases
        # Error Paths
        (
            {},
            TypeError,
            "EP-1",
        ),
        (
            [
                [
                    "layer1",
                ],
            ],
            TypeError,
            "EP-2",
        ),
        (
            [
                "layer1",
                {},
            ],
            TypeError,
            "EP-3",
        ),
        (
            12,
            TypeError,
            "EP-4",
        ),
        (
            True,
            TypeError,
            "EP-5",
        ),
    ],
)
def test_add_layers(repository, layers, expected_exception, test_id):
    # Arrange
    # (No arrange step needed as input values are provided via test parameters)

    # Act

    if expected_exception:
        with pytest.raises(expected_exception):
            repository.add_layers(layers)
    else:
        repository.add_layers(layers)

        # Assert
        Session = sessionmaker(bind=repository._SQLiteRepository__engine)
        with Session() as s:
            result = s.query(Layer).all()
            assert result is not None, test_id
            for current in result:
                assert current.id in layers


@pytest.mark.parametrize(
    "levels, expected_exception, test_id",
    [
        # Happy Paths
        (
            [
                "level1",
                "level2",
            ],
            None,
            "HP-1",
        ),
        (
            [],
            None,
            "HP-1",
        ),
        # Edge cases
        # Error Paths
        (
            {},
            TypeError,
            "EP-1",
        ),
        (
            [
                [
                    "level1",
                ],
            ],
            TypeError,
            "EP-2",
        ),
        (
            [
                "level1",
                {},
            ],
            TypeError,
            "EP-3",
        ),
        (
            12,
            TypeError,
            "EP-4",
        ),
        (
            True,
            TypeError,
            "EP-5",
        ),
    ],
)
def test_add_levels(repository, levels, expected_exception, test_id):
    # Arrange
    # (No arrange step needed as input values are provided via test parameters)

    # Act

    if expected_exception:
        with pytest.raises(expected_exception):
            repository.add_levels(levels)
    else:
        repository.add_levels(levels)

        # Assert
        Session = sessionmaker(bind=repository._SQLiteRepository__engine)
        with Session() as s:
            result = s.query(Level).all()
            assert result is not None, test_id
            for current in result:
                assert current.level_id in levels


@pytest.mark.parametrize(
    "locations, expected_exception, test_id",
    [
        # Happy Paths
        (
            [
                {
                    "location_id": "location1",
                    "provider_name": "test",
                    "designation": "a location for test purpose",
                    "city": "St Malo",
                    "state": "Bretagne",
                    "country": "France",
                }
            ],
            None,
            "HP-2",
        ),
        # Edge cases
        # Error Paths
        (
            [
                {
                    "location_id": "location1",
                    "provider_name": "test",
                }
            ],
            ValueError,
            "EP-0",
        ),
        (
            {
                "location_id": "location1",
                "provider_name": "test",
                "designation": "a location for test purpose",
                "city": "St Malo",
                "state": "Bretagne",
                "country": "France",
            },
            TypeError,
            "EP-1",
        ),
        (
            [
                [
                    "location1",
                    "test",
                ],
            ],
            TypeError,
            "EP-2",
        ),
        (
            [
                [
                    "location1",
                    "test",
                ],
                {
                    "location_id": "location1",
                    "provider_name": "test",
                    "designation": "a location for test purpose",
                    "city": "St Malo",
                    "state": "Bretagne",
                    "country": "France",
                },
            ],
            TypeError,
            "EP-3",
        ),
    ],
)
def test_add_locations(repository, locations, expected_exception, test_id):
    # Arrange
    # (No arrange step needed as input values are provided via test parameters)

    # Act

    if expected_exception:
        with pytest.raises(expected_exception):
            repository.add_locations(locations)
    else:
        repository.add_locations(locations)

        # Assert
        Session = sessionmaker(bind=repository._SQLiteRepository__engine)
        with Session() as s:
            result = s.query(Location).all()
            assert result is not None, test_id
            for current in result:
                assert current.provider_name in [location.get("provider_name") for location in locations]
                assert current.location_external_id in [location.get("location_id") for location in locations]
                assert current.designation in [location.get("designation") for location in locations]
                assert current.city in [location.get("city") for location in locations]
                assert current.state in [location.get("state") for location in locations]
                assert current.country in [location.get("country") for location in locations]


@pytest.mark.parametrize(
    "schemes, expected_exception, test_id",
    [
        # Test ID: #HP-1
        (
            [
                {
                    "cab_id": "CAB-01",
                    "provider_name": "Provider A",
                    "compliance_reference_id": "Ref-01",
                    "assertion_method": "Certified",
                }
            ],
            None,
            "HP-1",
        ),
        (
            [
                {
                    "provider_name": "Provider A",
                    "compliance_reference_id": "Ref-01",
                    "assertion_method": "Not Certified",
                }
            ],
            None,
            "HP-2",
        ),
        (
            [
                {
                    "cab_id": "CAB-01",
                    "provider_name": "Provider A",
                    "compliance_reference_id": "Ref-01",
                    "assertion_method": "Certified",
                },
                {
                    "provider_name": "Provider A",
                    "compliance_reference_id": "Ref-01",
                    "assertion_method": "Not Certified",
                },
            ],
            None,
            "HP-3",
        ),
        (
            [],
            None,
            "HP-4",
        ),
        # Error paths:
        ("", TypeError, "EP-1"),
        ({}, TypeError, "EP-2"),
        (12, TypeError, "EP-3"),
        (True, TypeError, "EP-4"),
    ],
)
def test_add_third_party_compliance_certification_scheme(repository, schemes, expected_exception, test_id):
    # Arrange
    # (No arrange step needed as input values are provided via test parameters)

    # Act

    if expected_exception:
        with pytest.raises(expected_exception):
            repository.add_provider_compliances_schemes(schemes)
    else:
        repository.add_provider_compliances_schemes(schemes)

        # Assert
        Session = sessionmaker(bind=repository._SQLiteRepository__engine)
        with Session() as s:
            result = s.query(ComplianceCertificationScheme).all()
            assert result is not None, test_id
            for current in result:
                assert current.compliance_reference_id in [
                    scheme.get("compliance_reference_id")
                    for scheme in schemes
                    if scheme.get("assertion_method") == "Certified"
                ]

            result = s.query(ComplianceCertificationSchemeProviderLink).all()
            assert result is not None, test_id
            for current in result:
                assert current.compliance_reference_id in [
                    scheme.get("compliance_reference_id")
                    for scheme in schemes
                    if scheme.get("assertion_method") == "Not Certified"
                ]


@pytest.mark.parametrize(
    "services, expected_exception, test_id",
    [
        # Test ID: #HP-1
        (
            [],
            None,
            "HP-1",
        ),
        (
            [
                {
                    "provider_name": "Provider A",
                    "service_id": "service 1",
                    "designation": "designation 1",
                    "service_version": "service_version 1",
                    "description": "description for service 1",
                }
            ],
            None,
            "HP-2",
        ),
        (
            [
                {
                    "provider_name": "Provider A",
                    "service_id": "service 1",
                    "designation": "designation 1",
                    "service_version": "service_version 1",
                    "description": "description for service 1",
                    "level": "Level 1",
                }
            ],
            None,
            "HP-3",
        ),
        (
            [
                {
                    "provider_name": "Provider A",
                    "service_id": "service 1",
                    "designation": "designation 1",
                    "service_version": "service_version 1",
                    "description": "description for service 1",
                    "level": "Level 1",
                    "layers": ["IAAS"],
                },
                {
                    "provider_name": "Provider A",
                    "service_id": "service 2",
                    "designation": "designation 2",
                    "service_version": "service_version 2",
                    "description": "description for service 2",
                    "level": "Level 1",
                    "layers": ["IAAS"],
                    "keywords": ["security"],
                },
                {
                    "provider_name": "Provider A",
                    "service_id": "service 3",
                    "designation": "designation 3",
                    "service_version": "service_version 3",
                    "description": "description for service 3",
                    "level": "Level 1",
                    "keywords": ["security"],
                    "location_ids": ["loc-1"],
                },
                {
                    "provider_name": "Provider A",
                    "service_id": "service 4",
                    "designation": "designation 4",
                    "service_version": "service_version 4",
                    "description": "description for service 3",
                    "level": "Level 1",
                    "keywords": ["security"],
                    "location_ids": ["loc-1"],
                },
                {
                    "provider_name": "Provider A",
                    "service_id": "service 5",
                    "designation": "designation 5",
                    "service_version": "service_version 5",
                    "description": "description for service 5",
                    "level": "Level 1",
                    "keywords": ["security"],
                    "location_ids": ["loc-1"],
                    "compliance_reference_ids": ["reference 1"],
                },
                {
                    "provider_name": "Provider A",
                    "service_id": "service 6",
                    "designation": "designation 6",
                    "service_version": "service_version 6",
                    "description": "description for service 6",
                    "level": "Level 1",
                    "keywords": ["security"],
                    "location_ids": ["loc-1"],
                    "self_assessments": ["criterion 1"],
                },
            ],
            None,
            "HP-4",
        ),
        # Error paths:
        ("", TypeError, "EP-1"),
        ({}, TypeError, "EP-2"),
        (12, TypeError, "EP-3"),
        (True, TypeError, "EP-4"),
    ],
)
def test_add_services(repository, services, expected_exception, test_id):
    # Arrange
    # (No arrange step needed as input values are provided via test parameters)

    # Act

    if expected_exception:
        with pytest.raises(expected_exception):
            repository.add_services(services)
    else:
        repository.add_services(services)

        # Assert
        Session = sessionmaker(bind=repository._SQLiteRepository__engine)
        with Session() as s:
            result = (
                s.query(Service)
                .join(Service.keywords)
                .options(joinedload(Service.keywords))
                .join(Service.layers)
                .options(joinedload(Service.layers))
                .join(Service.locations)
                .options(joinedload(Service.locations))
                .join(Service.locations)
                .options(joinedload(Service.compliance_references))
                .join(Service.self_assessed_criterions)
                .options(joinedload(Service.self_assessed_criterions))
                .all()
            )
            assert result is not None, test_id
            for current in result:
                assert current.provider_name in [service.get("provider_name") for service in services]
                assert current.service_external_id in [service.get("service_id") for service in services]
                assert current.designation in [service.get("designation") for service in services]
                assert current.service_version in [service.get("service_version") for service in services]
                assert current.description in [service.get("description") for service in services]
                assert current.level_id in [service.get("level") for service in services]
                for keyword in current.keywords:
                    assert keyword in [service.get("keywords") for service in services]
                for layer in current.layers:
                    assert layer in [service.get("layers") for service in services]
                for location in current.locations:
                    assert location in [service.get("location_ids") for service in services]
                for compliance_references in current.compliance_references:
                    assert compliance_references in [service.get("compliance_reference_ids") for service in services]
                for self_assessed_criterions in current.self_assessed_criterions:
                    assert self_assessed_criterions in [service.get("self_assessments") for service in services]


@pytest.mark.parametrize(
    "test_id, input_name, expected_result",
    [
        # Happy path tests with various realistic test values
        ("HP-1", "CAB Name 1", ComplianceAssessmentsBody(cab_id="CAB-00", name="CAB Name 1")),
        ("HP-2", "CAB Name 2", ComplianceAssessmentsBody(cab_id="CAB-01", name="CAB Name 2")),
        ("HP-2", "CAB Name 3", None),
        ("HP-3", None, None),
        ("HP-4", "", None),
    ],
)
def test_get_compliance_assessments_body_from_name(test_id, repository, input_name, expected_result):
    session_class = sessionmaker(bind=repository._SQLiteRepository__engine)
    with session_class() as session:
        session.add_all(
            [
                ComplianceAssessmentsBody(cab_id="CAB-00", name="CAB Name 1"),
                ComplianceAssessmentsBody(cab_id="CAB-01", name="CAB Name 2"),
            ]
        )
        session.commit()

    result = repository.get_compliance_assessments_body_from_name(input_name)
    if expected_result is None:
        assert result == expected_result
    else:
        assert result is not None
        assert isinstance(result, ComplianceAssessmentsBody)
        assert result.cab_id == expected_result.cab_id
        assert result.name == expected_result.name


@pytest.mark.parametrize(
    "test_id, input_name, expected_result",
    [
        # Happy path tests with various realistic test values
        ("HP-1", "Name 1", Provider(name="Name 1")),
        ("HP-2", "Name 2", Provider(name="Name 2")),
        ("HP-2", "Name 3", None),
        ("HP-3", None, None),
        ("HP-4", "", None),
    ],
)
def test_get_provider_from_name(test_id, repository, input_name, expected_result):
    session_class = sessionmaker(bind=repository._SQLiteRepository__engine)
    with session_class() as session:
        session.add_all(
            [
                Provider(name="Name 1"),
                Provider(name="Name 2"),
            ]
        )
        session.commit()

    result = repository.get_provider_from_name(input_name)
    if expected_result is None:
        assert result == expected_result
    else:
        assert result is not None
        assert isinstance(result, Provider)
        assert result.name == expected_result.name


@pytest.mark.parametrize(
    "test_id, input_name, expected_result",
    [
        # Happy path tests with various realistic test values
        ("HP-1", "Name 1", ComplianceReferenceManager(name="Name 1")),
        ("HP-2", "Name 2", ComplianceReferenceManager(name="Name 2")),
        ("HP-2", "Name 3", None),
        ("HP-3", None, None),
        ("HP-4", "", None),
    ],
)
def test_get_compliance_reference_manager_from_name(test_id, repository, input_name, expected_result):
    session_class = sessionmaker(bind=repository._SQLiteRepository__engine)
    with session_class() as session:
        session.add_all(
            [
                ComplianceReferenceManager(name="Name 1"),
                ComplianceReferenceManager(name="Name 2"),
            ]
        )
        session.commit()

    result = repository.get_compliance_reference_manager_from_name(input_name)
    if expected_result is None:
        assert result == expected_result
    else:
        assert result is not None
        assert isinstance(result, ComplianceReferenceManager)
        assert result.name == expected_result.name


@pytest.mark.parametrize(
    "test_id, input_names",
    [
        # Happy path tests with various realistic test values
        ("HP-1", []),
        ("HP-1", ["Name 1"]),
        ("HP-1", ["Name 1", "Name 2"]),
        ("HP-3", None),
    ],
)
def get_compliance_reference_manager_names(test_id, repository, input_names):
    session_class = sessionmaker(bind=repository._SQLiteRepository__engine)
    if input_names is not None:
        with session_class() as session:
            session.add_all(input_names)
            session.commit()

    result = repository.get_compliance_reference_manager_names(input_names)
    if input_names is None or len(input_names) == 0:
        assert result == []
    else:
        assert result is not None
        assert isinstance(result, list)
        assert len(result) == len(input_names)
        assert all(current.name in input_names for current in result)


@pytest.mark.parametrize(
    "test_id, input, expected_result",
    [
        # Happy path tests with various realistic test values
        ("HP-1", "Level 1", Level(level_id="Level 1")),
        ("HP-2", "Level 2", Level(level_id="Level 2")),
        ("HP-2", "Level 3", None),
        ("HP-3", None, None),
        ("HP-4", "", None),
    ],
)
def test_level(test_id, repository, input, expected_result):
    session_class = sessionmaker(bind=repository._SQLiteRepository__engine)
    with session_class() as session:
        session.add_all(
            [
                Level(level_id="Level 1"),
                Level(level_id="Level 2"),
            ]
        )
        session.commit()

    result = repository.get_level(input)
    if expected_result is None:
        assert result == expected_result
    else:
        assert result is not None
        assert isinstance(result, Level)
        assert result.level_id == expected_result.level_id


@pytest.mark.parametrize(
    "test_id, input, expected_result",
    [
        # Happy path tests with various realistic test values
        ("HP-1", "Level 1", Level(level_id="Level 1")),
        ("HP-2", "Level 2", Level(level_id="Level 2")),
        ("HP-2", "Level 3", None),
        ("HP-3", None, None),
        ("HP-4", "", None),
    ],
)
def test_get_level(test_id, repository, input, expected_result):
    session_class = sessionmaker(bind=repository._SQLiteRepository__engine)
    with session_class() as session:
        session.add_all(
            [
                Level(level_id="Level 1"),
                Level(level_id="Level 2"),
            ]
        )
        session.commit()

    result = repository.get_level(input)
    if expected_result is None:
        assert result == expected_result
    else:
        assert result is not None
        assert isinstance(result, Level)
        assert result.level_id == expected_result.level_id

#@todo Add more tests to check complex queries
