# -*- coding: utf-8 -*-
import pytest

from credential_generator.infra.spi.database.sqlite.entities import (
    ComplianceReferenceManager,
    ComplianceAssessmentsBody,
    Provider,
    ComplianceCertificationScheme,
    ComplianceCertificationSchemeProviderLink,
    ComplianceCriterion,
    ComplianceReference,
    Keyword,
    Layer,
    Level,
    Location,
    Service,
)
from credential_generator.infra.spi.database.sqlite.entities.factory import (
    to_compliance_reference_manager,
    to_compliance_certification_scheme,
    to_cab,
    to_provider,
    to_compliance_certification_scheme_provider_link,
    to_compliance_criterion,
    to_compliance_reference_entity,
    to_keyword_entity,
    to_layer_entity,
    to_level_entity,
    to_location_entity,
    to_service_entity,
)

# Assuming the __check_required_properties function raises a KeyError if a required property is missing
# and PARTICIPANT_MUST_BE_NOT_NONE_ERROR_MESSAGE is a constant string defined elsewhere in the module.

# Test IDs for parametrization
HAPPY_PATH_ID = "happy"
EDGE_CASE_ID = "edge"
ERROR_CASE_ID = "error"


# Parametrized test for happy path, edge cases, and error cases
@pytest.mark.parametrize(
    "test_id, participant, expected_exception, expected_result",
    [
        # Happy path tests with various realistic test values
        (
            HAPPY_PATH_ID,
            {
                "name": "ABC-FEDERATION",
                "designation": "abc-federation",
                "vat_id": "1234567890",
                "lei_code": "abc-federation",
                "terms_and_conditions": "https://abc-federation.gaia-x.community/terms",
                "street_address": "Avenue des Arts 6-9",
                "city": "Bruxelles",
                "postal_code": "1210",
                "country": "BE",
            },
            None,
            ComplianceReferenceManager(
                name="ABC-FEDERATION",
                designation="abc-federation",
                registration_number="1234567890",
                lei_code="abc-federation",
                terms_and_conditions="https://abc-federation.gaia-x.community/terms",
                street="Avenue des Arts 6-9",
                locality="Bruxelles",
                postal_code="1210",
                country="BE",
            ),
        ),
        # Edge cases
        (
            EDGE_CASE_ID,
            {
                "name": "",
                "designation": None,
                "vat_id": None,
                "lei_code": None,
                "terms_and_conditions": None,
                "street_address": None,
                "city": None,
                "postal_code": None,
                "country": None,
            },
            None,
            ComplianceReferenceManager(
                name="",
                designation=None,
                registration_number=None,
                lei_code=None,
                terms_and_conditions=None,
                street=None,
                locality=None,
                postal_code=None,
                country=None,
            ),
        ),
        # Error cases
        (ERROR_CASE_ID, None, ValueError, None),  # participant is None
        (ERROR_CASE_ID, {}, ValueError, None),  # Missing required 'name' key
    ],
)
def test_to_compliance_reference_manager(test_id, participant, expected_exception, expected_result):
    # Arrange
    # (Omitted if all input values are provided via test parameters)

    # Act
    if expected_exception:
        with pytest.raises(expected_exception):
            to_compliance_reference_manager(participant)
    else:
        result = to_compliance_reference_manager(participant)

        assert result.name == expected_result.name
        assert result.designation == expected_result.designation
        assert result.street == expected_result.street
        assert result.locality == expected_result.locality
        assert result.postal_code == expected_result.postal_code
        assert result.country == expected_result.country
        assert result.registration_number == expected_result.registration_number
        assert result.terms_and_conditions == expected_result.terms_and_conditions


@pytest.mark.parametrize(
    "test_id, participant, expected_exception, expected_result",
    [
        # Happy path tests with various realistic test values
        (
            HAPPY_PATH_ID,
            {
                "id": "cab-00",
                "name": "ABC-FEDERATION",
                "designation": "abc-federation",
                "vat_id": "1234567890",
                "lei_code": "abc-federation",
                "terms_and_conditions": "https://abc-federation.gaia-x.community/terms",
                "street_address": "Avenue des Arts 6-9",
                "city": "Bruxelles",
                "postal_code": "1210",
                "country": "BE",
            },
            None,
            ComplianceAssessmentsBody(
                cab_id="cab-00",
                name="ABC-FEDERATION",
                designation="abc-federation",
                registration_number="1234567890",
                lei_code="abc-federation",
                terms_and_conditions="https://abc-federation.gaia-x.community/terms",
                street="Avenue des Arts 6-9",
                locality="Bruxelles",
                postal_code="1210",
                country="BE",
            ),
        ),
        # Edge cases
        (
            EDGE_CASE_ID,
            {
                "id": "cab-00",
                "name": "",
                "designation": None,
                "vat_id": None,
                "lei_code": None,
                "terms_and_conditions": None,
                "street_address": None,
                "city": None,
                "postal_code": None,
                "country": None,
            },
            None,
            ComplianceAssessmentsBody(
                cab_id="cab-00",
                name="",
                designation=None,
                registration_number=None,
                lei_code=None,
                terms_and_conditions=None,
                street=None,
                locality=None,
                postal_code=None,
                country=None,
            ),
        ),
        # Error cases
        (ERROR_CASE_ID, None, ValueError, None),  # participant is None
        (ERROR_CASE_ID, {}, ValueError, None),  # Missing required 'name' key
        (ERROR_CASE_ID, {"name": "cab"}, ValueError, None),  # Missing required 'id' key
    ],
)
def test_to_cab(test_id, participant, expected_exception, expected_result):
    # Arrange
    # (Omitted if all input values are provided via test parameters)

    # Act
    if expected_exception:
        with pytest.raises(expected_exception):
            to_cab(participant)
    else:
        result = to_cab(participant)

        assert result.cab_id == expected_result.cab_id, test_id
        assert result.name == expected_result.name, test_id
        assert result.designation == expected_result.designation, test_id
        assert result.street == expected_result.street, test_id
        assert result.locality == expected_result.locality, test_id
        assert result.postal_code == expected_result.postal_code, test_id
        assert result.country == expected_result.country, test_id
        assert result.registration_number == expected_result.registration_number, test_id
        assert result.terms_and_conditions == expected_result.terms_and_conditions, test_id


@pytest.mark.parametrize(
    "test_id, participant, expected_exception, expected_result",
    [
        # Happy path tests with various realistic test values
        (
            HAPPY_PATH_ID,
            {
                "name": "ABC-FEDERATION",
                "designation": "abc-federation",
                "vat_id": "1234567890",
                "lei_code": "abc-federation",
                "terms_and_conditions": "https://abc-federation.gaia-x.community/terms",
                "street_address": "Avenue des Arts 6-9",
                "city": "Bruxelles",
                "postal_code": "1210",
                "country": "BE",
            },
            None,
            Provider(
                name="ABC-FEDERATION",
                designation="abc-federation",
                registration_number="1234567890",
                lei_code="abc-federation",
                terms_and_conditions="https://abc-federation.gaia-x.community/terms",
                street="Avenue des Arts 6-9",
                locality="Bruxelles",
                postal_code="1210",
                country="BE",
            ),
        ),
        # Edge cases
        (
            EDGE_CASE_ID,
            {
                "name": "",
                "designation": None,
                "vat_id": None,
                "lei_code": None,
                "terms_and_conditions": None,
                "street_address": None,
                "city": None,
                "postal_code": None,
                "country": None,
            },
            None,
            Provider(
                name="",
                designation=None,
                registration_number=None,
                lei_code=None,
                terms_and_conditions=None,
                street=None,
                locality=None,
                postal_code=None,
                country=None,
            ),
        ),
        # Error cases
        (ERROR_CASE_ID, None, ValueError, None),  # participant is None
        (ERROR_CASE_ID, {}, ValueError, None),  # Missing required 'name' key
    ],
)
def test_to_provider(test_id, participant, expected_exception, expected_result):
    # Arrange
    # (Omitted if all input values are provided via test parameters)

    # Act
    if expected_exception:
        with pytest.raises(expected_exception):
            to_provider(participant)
    else:
        result = to_provider(participant)

        assert result.name == expected_result.name, test_id
        assert result.designation == expected_result.designation, test_id
        assert result.street == expected_result.street, test_id
        assert result.locality == expected_result.locality, test_id
        assert result.postal_code == expected_result.postal_code, test_id
        assert result.country == expected_result.country, test_id
        assert result.registration_number == expected_result.registration_number, test_id
        assert result.terms_and_conditions == expected_result.terms_and_conditions, test_id


# Happy path tests
@pytest.mark.parametrize(
    "test_id, input_data, expected_exception",
    [
        (f"{HAPPY_PATH_ID}_1", {"cr_id1": {}}, None),
        (
            f"{HAPPY_PATH_ID}_2",
            {"cr_id2": {}},
            None,
        ),
        (
            f"{EDGE_CASE_ID}_1",
            {},
            None,
        ),
        (
            f"{ERROR_CASE_ID}_1",
            None,
            ValueError,
        ),
        # (ERROR_CASE_ID),
    ],
)
def test_to_compliance_certification_scheme_happy_path(test_id, input_data, expected_exception):
    # Act
    if expected_exception:
        with pytest.raises(expected_exception):
            to_compliance_certification_scheme(input_data)
    else:
        result = to_compliance_certification_scheme(input_data)
        # Assert
        assert isinstance(result, list), f"Test ID: {test_id} - The result should be a list."
        assert all(
            isinstance(item, ComplianceCertificationScheme) for item in result
        ), f"Test ID: {test_id} - All items in the result should be ComplianceCertificationSchemeEntity instances."
        assert len(result) == len(
            input_data
        ), f"Test ID: {test_id} - The result list should have the same number of items as the input dictionary."
        for entity in result:
            assert (
                entity.compliance_reference_id in input_data
            ), f"Test ID: {test_id} - The entity's compliance_reference_id should be a key in the input dictionary."


# Happy path tests with various realistic test values
@pytest.mark.parametrize(
    "test_id, provider_compliances_schemes, expected_output",
    [
        # Test ID: 1 - Single entry, non-certified
        (
            1,
            [
                {
                    "provider_name": "Provider A",
                    "compliance_reference_id": "123",
                    "cab_id": "456",
                    "assertion_method": "Not Certified",
                }
            ],
            [ComplianceCertificationSchemeProviderLink(provider_name="Provider A", compliance_reference_id="123")],
        ),
        # Test ID: 2 - Multiple entries, mixed certification status
        (
            2,
            [
                {
                    "provider_name": "Provider A",
                    "compliance_reference_id": "123",
                    "cab_id": "456",
                    "assertion_method": "Not Certified",
                },
                {
                    "provider_name": "Provider B",
                    "compliance_reference_id": "789",
                    "cab_id": "101112",
                    "assertion_method": "Certified",
                },
            ],
            [ComplianceCertificationSchemeProviderLink(provider_name="Provider A", compliance_reference_id="123")],
        ),
    ],
)
def test_happy_path(test_id, provider_compliances_schemes, expected_output):
    # Act
    result = to_compliance_certification_scheme_provider_link(provider_compliances_schemes)

    # Assert
    assert isinstance(result, list), f"Test ID: {test_id} - The result should be a list."
    assert all(
        isinstance(item, ComplianceCertificationSchemeProviderLink) for item in result
    ), f"Test ID: {test_id} - All items in the result should be ComplianceCertificationSchemeEntity instances."
    assert len(result) == len(
        expected_output
    ), f"Test ID: {test_id} - The result list should have the same number of items as the input dictionary."


# Edge cases
@pytest.mark.parametrize(
    "test_id, provider_compliances_schemes, expected_output",
    [
        # Test ID: 3 - Empty list
        (3, [], []),
        # Test ID: 4 - Missing 'cab_id' should be filtered out
        (
            4,
            [{"provider_name": "Provider C", "compliance_reference_id": "321", "assertion_method": "Not Certified"}],
            [],
        ),
    ],
)
def test_edge_cases(test_id, provider_compliances_schemes, expected_output):
    # Act
    result = to_compliance_certification_scheme_provider_link(provider_compliances_schemes)

    # Assert
    assert result == expected_output, f"Failed test ID: {test_id}"


# Error cases
@pytest.mark.parametrize(
    "test_id, provider_compliances_schemes, expected_exception",
    [
        # Test ID: 5 - Input is None
        (5, None, ValueError),
    ],
)
def test_error_cases(test_id, provider_compliances_schemes, expected_exception):
    # Act / Assert
    with pytest.raises(expected_exception):
        to_compliance_certification_scheme_provider_link(provider_compliances_schemes)


class LevelEnum:
    LEVEL_1 = "Level 1"
    LEVEL_2 = "Level 2"


class CategoryEnum:
    SECURITY = "Security"
    PRIVACY = "Privacy"


# Mocking the ComplianceCriterion class to include enum fields for the purpose of this test.
# In actual tests, use the real ComplianceCriterion class.
ComplianceCriterion.Level = LevelEnum
ComplianceCriterion.Category = CategoryEnum


# The test function is parametrized with various cases for happy path, edge cases, and error cases.
@pytest.mark.parametrize(
    "test_id, input_dict, expected_output, expected_exception",
    [
        # Happy path tests
        (
            "HP-1",
            {
                "id": "1",
                "name": "Criterion 1",
                "category": CategoryEnum.SECURITY,
                "description": "Description 1",
                "self_assessed": True,
            },
            ComplianceCriterion(
                criterion_id="1",
                name="Criterion 1",
                category=CategoryEnum.SECURITY,
                description="Description 1",
                self_assessed=True,
            ),
            None,
        ),
        (
            "HP-2",
            {
                "id": "2",
                "name": "Criterion 2",
                "category": CategoryEnum.PRIVACY,
                "description": "Description 2",
                "self_assessed": False,
            },
            ComplianceCriterion(
                criterion_id="2",
                name="Criterion 2",
                category=CategoryEnum.PRIVACY,
                description="Description 2",
                self_assessed=False,
            ),
            None,
        ),
        # Edge cases
        (
            "EC-1",
            {"id": "0", "name": "", "category": CategoryEnum.SECURITY, "description": "", "self_assessed": True},
            ComplianceCriterion(
                criterion_id="0",
                name="",
                category=CategoryEnum.SECURITY,
                description="",
                self_assessed=True,
            ),
            None,
        ),
        # Error cases
        (
            "ER-1",
            {
                "name": "Missing ID",
                "category": CategoryEnum.SECURITY,
                "description": "Description",
                "self_assessed": True,
            },
            None,
            ValueError,
        ),
        (
            "ER-2",
            {"id": 3, "category": CategoryEnum.SECURITY, "description": "Description", "self_assessed": True},
            None,
            ValueError,
        ),
        (
            "ER-3",
            {"id": 4, "name": "Criterion 4", "description": "Description", "self_assessed": True},
            None,
            ValueError,
        ),
        (
            "ER-4",
            {"id": 5, "name": "Criterion 5", "category": CategoryEnum.SECURITY, "self_assessed": True},
            None,
            ValueError,
        ),
    ],
)
def test_to_compliance_criterion(test_id, input_dict, expected_output, expected_exception):
    # Arrange
    # (Omitted since all input values are provided via test parameters)

    # Act
    if expected_exception:
        with pytest.raises(expected_exception):
            result = to_compliance_criterion(input_dict)
    else:
        result = to_compliance_criterion(input_dict)

    # Assert
    if not expected_exception:
        assert result.name == expected_output.name, f"Test {test_id} failed: Output not as expected."
        assert result.description == expected_output.description, f"Test {test_id} failed: Output not as expected."
        assert result.criterion_id == expected_output.criterion_id, f"Test {test_id} failed: Output not as expected."
        assert result.levels == expected_output.levels, f"Test {test_id} failed: Output not as expected."
        assert result.self_assessed == expected_output.self_assessed, f"Test {test_id} failed: Output not as expected."
        assert result.category == expected_output.category, f"Test {test_id} failed: Output not as expected."


@pytest.mark.parametrize(
    "test_id, input_data, expected_exception, expected_output",
    [
        # Happy path tests with various realistic test values
        (
            "HP-1",
            {
                "criterion_reference_id": "reference1",
                "compliance_reference_manager_name": "manager1",
                "title": "designation1",
                "version": "version1",
                "category": "category1",
                "valid_from": "2022-01-01",
                "valid_until": "2022-12-31",
                "reference": "https://example.com",
                "assertion_method": "method1",
                "assertion_date": "2022-06-30",
                "issuer_designation": "issuer1",
                "document_sha256": "hash1",
            },
            None,
            ComplianceReference(
                compliance_reference_id="reference1",
                compliance_reference_manager_name="manager1",
                designation="designation1",
                version="version1",
                category="category1",
                valid_from="2022-01-01",
                valid_until="2022-12-31",
                reference="https://example.com",
                assertion_method="method1",
                valid_date="2022-06-30",
                issuer_designation="issuer1",
                reference_hash="hash1",
            ),
        ),
        # Edge cases
        (
            "EC-1",
            {
                "criterion_reference_id": "",
                "compliance_reference_manager_name": "manager1",
                "title": "designation1",
                "version": "version1",
                "category": "category1",
                "valid_from": "2022-01-01",
                "valid_until": "2022-12-31",
                "reference": "https://example.com",
                "assertion_method": "method1",
                "assertion_date": "2022-06-30",
                "issuer_designation": "issuer1",
                "document_sha256": "hash1",
            },
            None,
            ComplianceReference(
                compliance_reference_id="",
                compliance_reference_manager_name="manager1",
                designation="designation1",
                version="version1",
                category="category1",
                valid_from="2022-01-01",
                valid_until="2022-12-31",
                reference="https://example.com",
                assertion_method="method1",
                valid_date="2022-06-30",
                issuer_designation="issuer1",
                reference_hash="hash1",
            ),
        ),
        # Error cases
        (
            "ERR-1",
            {
                "compliance_reference_manager_name": "manager1",
                "title": "designation1",
                "version": "version1",
                "category": "category1",
                "valid_from": "2022-01-01",
                "valid_until": "2022-12-31",
                "reference": "https://example.com",
                "assertion_method": "method1",
                "assertion_date": "2022-06-30",
                "issuer_designation": "issuer1",
                "document_sha256": "hash1",
            },
            ValueError,
            None,
        ),  # Missing 'criterion_reference_id'
    ],
)
def test_to_compliance_reference_entity(test_id, input_data, expected_exception, expected_output):
    # Act
    if expected_exception:
        with pytest.raises(expected_exception):
            to_compliance_reference_entity(input_data)
    else:
        result = to_compliance_reference_entity(input_data)

        # Assert
        assert result.compliance_reference_id == expected_output.compliance_reference_id
        assert result.compliance_reference_manager_name == expected_output.compliance_reference_manager_name
        assert result.designation == expected_output.designation
        assert result.version == expected_output.version
        assert result.category == expected_output.category
        assert result.valid_from == expected_output.valid_from
        assert result.valid_until == expected_output.valid_until
        assert result.reference == expected_output.reference
        assert result.assertion_method == expected_output.assertion_method
        assert result.valid_date == expected_output.valid_date
        assert result.issuer_designation == expected_output.issuer_designation
        assert result.reference_hash == expected_output.reference_hash


@pytest.mark.parametrize(
    "test_id, input_data, expected_exception, expected_output",
    [
        # Happy path tests with various realistic test values
        (
            "HP-1",
            "keyword1",
            None,
            Keyword(
                id="keyword1",
            ),
        ),
        # Error cases
        (
            "ERR-1",
            "",
            ValueError,
            Keyword(
                id="",
            ),
        ),
        (
            "ERR-2",
            None,
            TypeError,
            None,
        ),  # Missing 'criterion_reference_id'
    ],
)
def test_to_keywordy(test_id, input_data, expected_exception, expected_output):
    # Act
    if expected_exception:
        with pytest.raises(expected_exception):
            to_keyword_entity(input_data)
    else:
        result = to_keyword_entity(input_data)

        # Assert
        assert result.id == expected_output.id, test_id


@pytest.mark.parametrize(
    "test_id, input_data, expected_exception, expected_output",
    [
        # Happy path tests with various realistic test values
        (
            "HP-1",
            "layer1",
            None,
            Layer(
                id="layer1",
            ),
        ),
        # Error cases
        (
            "ERR-1",
            "",
            ValueError,
            Layer(
                id="",
            ),
        ),
        (
            "ERR-2",
            None,
            TypeError,
            None,
        ),  # Missing 'criterion_reference_id'
    ],
)
def test_to_layer(test_id, input_data, expected_exception, expected_output):
    # Act
    if expected_exception:
        with pytest.raises(expected_exception):
            to_layer_entity(input_data)
    else:
        result = to_layer_entity(input_data)

        # Assert
        assert result.id == expected_output.id


@pytest.mark.parametrize(
    "test_id, input_data, expected_exception, expected_output",
    [
        # Happy path tests with various realistic test values
        (
            "HP-1",
            "Level 1",
            None,
            Level(
                level_id="1",
            ),
        ),
        (
            "HP-2",
            "level1",
            None,
            Level(
                level_id="level1",
            ),
        ),
        # Error cases
        (
            "ERR-1",
            "",
            ValueError,
            Level(
                level_id="",
            ),
        ),
        (
            "ERR-2",
            None,
            TypeError,
            None,
        ),  # Missing 'criterion_reference_id'
    ],
)
def test_to_level(test_id, input_data, expected_exception, expected_output):
    # Act
    if expected_exception:
        with pytest.raises(expected_exception):
            to_level_entity(input_data)
    else:
        result = to_level_entity(input_data)

        # Assert
        assert result.level_id == expected_output.level_id


@pytest.mark.parametrize(
    "test_id, input_data, expected_exception, expected_output",
    [
        # Happy path tests with various realistic test values
        (
            "HP-1",
            {
                "provider_name": "test",
                "location_id": "LOC1",
            },
            None,
            Location(id="test__loc1", provider_name="test"),
        ),
        (
            "HP-2",
            {
                "provider_name": "test",
                "location_id": "LOC2",
                "designation": "location 2",
            },
            None,
            Location(id="test__loc2", provider_name="test", designation="location 2"),
        ),
        (
            "HP-2",
            {
                "provider_name": "test",
                "location_id": "LOC3",
                "designation": "location 3",
                "city": "test",
            },
            None,
            Location(id="test__loc3", provider_name="test", designation="location 3", city="test"),
        ),
        # Error cases
        ("ERR-1", "", ValueError, None),
        (
            "ERR-2",
            None,
            ValueError,
            None,
        ),  # Missing 'criterion_reference_id'
        (
            "ERR-3",
            {"provider_name": "test"},
            ValueError,
            None,
        ),  # Missing 'criterion_reference_id'
        (
            "ERR-3",
            {"location_id": "test"},
            ValueError,
            None,
        ),  # Missing 'criterion_reference_id'
    ],
)
def test_to_location(test_id, input_data, expected_exception, expected_output):
    # Act
    if expected_exception:
        with pytest.raises(expected_exception):
            to_location_entity(input_data)
    else:
        result = to_location_entity(input_data)

        # Assert
        assert result.id == expected_output.id
        assert result.provider_name == expected_output.provider_name
        assert result.designation == expected_output.designation
        assert result.city == expected_output.city
        assert result.state == expected_output.state
        assert result.country == expected_output.country


@pytest.mark.parametrize(
    "test_id, input_data, expected_exception, expected_output",
    [
        # Happy path tests with various realistic test values
        (
            "HP-1",
            {
                "provider_name": "test",
                "service_id": "svc",
                "designation": "",
                "service_version": "1",
                "description": "svc1",
            },
            None,
            Service(id="test__svc", provider_name="test", designation="", service_version="1", description="svc1"),
        ),
        # Error cases
        ("ERR-1", "", ValueError, None),
        (
            "ERR-2",
            None,
            ValueError,
            None,
        ),  # Missing 'criterion_reference_id'
        (
            "ERR-3",
            {"provider_name": "test"},
            ValueError,
            None,
        ),  # Missing 'criterion_reference_id'
        (
            "ERR-3",
            {"service_id": "test"},
            ValueError,
            None,
        ),  # Missing 'criterion_reference_id'
    ],
)
def test_to_location(test_id, input_data, expected_exception, expected_output):
    # Act
    if expected_exception:
        with pytest.raises(expected_exception):
            to_service_entity(input_data)
    else:
        result = to_service_entity(input_data)

        # Assert
        assert result.id == expected_output.id
        assert result.provider_name == expected_output.provider_name
        assert result.designation == expected_output.designation
