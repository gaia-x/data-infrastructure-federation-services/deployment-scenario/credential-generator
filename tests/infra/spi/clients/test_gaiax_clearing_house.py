# -*- coding: utf-8 -*-
from typing import Final

import httpx
import pytest
from httpx import Response

from credential_generator.config import (
    GX_TRUST_FRAMEWORK_CONTEXT,
    GAIA_X_CLEARING_HOUSE_REGISTRY_BASE_URL,
    GAIA_X_CLEARING_HOUSE_NOTARY_BASE_URL,
)
from credential_generator.domain._2210.trust_framework.participant import LegalRegistrationNumber

# Assuming the existence of the following classes and exceptions based on the provided context
from credential_generator.infra.spi.clients.gaiax_clearing_house import (
    GXCHRegistryClient,
    GaiaXClearingHouseRegistryException,
    GaiaXClearingHouseApiVersion,
    GXCHNotaryClient,
    GaiaXClearingHouseNotaryException,
)
from credential_generator.infra.spi.clients.gaiax_clearing_house import (
    to_registration_number_request,
    TermsAndConditions,
)

DEFAULT_CONTEXT: Final = {"@context": GX_TRUST_FRAMEWORK_CONTEXT}


# Happy path tests with various realistic test values
@pytest.mark.parametrize(
    "test_id, input_value, expected",
    [
        (
            "HP-1",
            LegalRegistrationNumber(did_web="did:web:example.com:1", vatID="123456789"),
            {"gx:vatID": "123456789", "id": "did:web:example.com:1", "type": "gx:legalRegistrationNumber"}
            | DEFAULT_CONTEXT,
        ),
        (
            "HP-2",
            LegalRegistrationNumber(did_web="did:web:example.com:1", leiCode="987654321"),
            {"gx:leiCode": "987654321", "id": "did:web:example.com:1", "type": "gx:legalRegistrationNumber"}
            | DEFAULT_CONTEXT,
        ),
        (
            "HP-3",
            LegalRegistrationNumber(did_web="did:web:example.com:1", local="987654321"),
            {"gx:local": "987654321", "id": "did:web:example.com:1", "type": "gx:legalRegistrationNumber"}
            | DEFAULT_CONTEXT,
        ),
        (
            "HP-4",
            LegalRegistrationNumber(did_web="did:web:example.com:1", taxID="987654321"),
            {"gx:taxID": "987654321", "id": "did:web:example.com:1", "type": "gx:legalRegistrationNumber"}
            | DEFAULT_CONTEXT,
        ),
        (
            "HP-5",
            LegalRegistrationNumber(did_web="did:web:example.com:1", euid="987654321"),
            {"gx:EUID": "987654321", "id": "did:web:example.com:1", "type": "gx:legalRegistrationNumber"}
            | DEFAULT_CONTEXT,
        ),
        (
            "HP-6",
            LegalRegistrationNumber(did_web="did:web:example.com:1", eori="987654321"),
            {"gx:EORI": "987654321", "id": "did:web:example.com:1", "type": "gx:legalRegistrationNumber"}
            | DEFAULT_CONTEXT,
        ),
        (
            "HP-7",
            LegalRegistrationNumber(did_web="did:web:example.com:1", leiCode="987654321", vatID="123456789"),
            {
                "gx:vatID": "123456789",
                "gx:leiCode": "987654321",
                "id": "did:web:example.com:1",
                "type": "gx:legalRegistrationNumber",
            }
            | DEFAULT_CONTEXT,
        ),
    ],
)
def test_to_registration_number_request_happy_path(test_id, input_value, expected):
    # Act
    result = to_registration_number_request(input_value)

    # Assert
    assert result == expected, f"Failed test ID: {test_id}"


@pytest.mark.parametrize(
    "status_code, response_content, version, expected_result, test_id",
    [
        # Happy path tests with various realistic test values
        (
            200,
            {"text": "Terms text", "version": "22.10", "hash": "hashvalue"},
            "22.10",
            TermsAndConditions(text="Terms text", version="22.10", hash="hashvalue"),
            "happy-path-default-version",
        ),
        (
            200,
            {"text": "Terms text", "version": "22.11", "hash": "hashvalue"},
            "22.11",
            TermsAndConditions(text="Terms text", version="22.11", hash="hashvalue"),
            "happy-path-specific-version",
        ),
        # Edge case: Missing hash in response, should compute hash
        (
            200,
            {"text": "Terms text", "version": "22.10"},
            "22.10",
            TermsAndConditions(
                text="Terms text",
                version="22.10",
                hash="170715a0f7b49c5e1f6478f4f4956fb4eb78c499f6fd0d7da322b26782a1c312",
            ),
            "edge-case-compute-hash",
        ),
        # Error cases
        (400, {}, "22.10", GaiaXClearingHouseRegistryException(), "error-case-400"),
        (400, {}, "22.10", GaiaXClearingHouseRegistryException(), "error-case-409"),
        (500, {}, "22.10", GaiaXClearingHouseRegistryException(), "error-case-500"),
    ],
)
def test_get_gaix_terms_and_conditions(respx_mock, status_code, response_content, version, expected_result, test_id):
    # Arrange
    respx_mock.get(
        f"{GAIA_X_CLEARING_HOUSE_REGISTRY_BASE_URL}/{GaiaXClearingHouseApiVersion.V1_STAGING.value}/api/termsAndConditions?version={version}"
    ).mock(return_value=httpx.Response(status_code=status_code, json=response_content))

    client = GXCHRegistryClient()

    # Act and Assert
    if isinstance(expected_result, GaiaXClearingHouseRegistryException):
        with pytest.raises(GaiaXClearingHouseRegistryException):
            client.get_gaix_terms_and_conditions(version=version)
    else:
        terms_and_conditions = client.get_gaix_terms_and_conditions(version=version)
        assert terms_and_conditions == expected_result, test_id


@pytest.mark.parametrize(
    "vc_id, legal_registration_number, status_code, response_content, expected_exception, test_id",
    [
        # Happy path tests with various realistic test values
        (
            "did:web:test.com:vc/1",
            LegalRegistrationNumber(did_web="did:web:test.com:1", vatID="123456789"),
            200,
            {
                "@context": ["https://www.w3.org/2018/credentials/v1", "https://w3id.org/security/suites/jws-2020/v1"],
                "type": ["VerifiableCredential"],
                "id": "did:web:aster-x.demo23.gxfs.fr:participant:d30a153a1080f608ca5ab38877d3f8076c6d4d465dd6cf9110accd6c6dfcd36d/vc/legal-participant/gaiax-legal-registration-number.json",
                "issuer": "did:web:registration.lab.gaia-x.eu:development",
                "issuanceDate": "2023-11-24T14:55:04.556Z",
                "credentialSubject": {
                    "@context": "https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#",
                    "type": "gx:legalRegistrationNumber",
                    "id": "did:web:aster-x.demo23.gxfs.fr:participant:d30a153a1080f608ca5ab38877d3f8076c6d4d465dd6cf9110accd6c6dfcd36d/vc/legal-participant/gaiax-legal-registration-number.json",
                    "gx:vatID": "123456789",
                    "gx:vatID-countryCode": "FR",
                },
                "evidence": [
                    {
                        "gx:evidenceURL": "http://ec.europa.eu/taxation_customs/vies/services/checkVatService",
                        "gx:executionDate": "2023-11-24T14:55:04.556Z",
                        "gx:evidenceOf": "gx:vatID",
                    }
                ],
                "proof": {
                    "type": "JsonWebSignature2020",
                    "created": "2023-11-24T14:55:04.568Z",
                    "proofPurpose": "assertionMethod",
                    "verificationMethod": "did:web:registration.lab.gaia-x.eu:v1#X509-JWK2020",
                    "jws": "eyJhbGciOiJQUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19..Lm1xTnABpqcNSDxbu1ZwVwNF9CaW0ZkDBiIV5ShplSjuGKvbk_3Qx821Ib-7N2wqpFC4YyeZ_wdpeYxI8VX_pz3ok226imrilorRA7iDlCm8zds1A4By3V09KX90wqH31DyY6sBQue4DWH2uDiXlrXR55fCDZ2o29lSaUOC-E1LvjbBR29RVgF8KJMbh0UoRIcZe5P7sGqYfnklwJWAvtTcjK_oISwA3zz4Gv3yTip0Gbxj-O0bRZbyupgJg6v3c_Ai-JTL7PQI80_xq1qda4DoeOonfs64RuFx9eoXcK49--t4U0s6HAZt0iJN8l2JYbQWB9D5G62BXLInoPkbLKQ",
                },
            },
            None,
            "happy-path-default-version",
        ),
        # Error cases
        (
            "did:web:test.com:vc/1",
            LegalRegistrationNumber(did_web="did:web:test.com:1", vatID="123456789"),
            400,
            {},
            GaiaXClearingHouseNotaryException(),
            "error-case-400",
        ),
        (
            "did:web:test.com:vc/1",
            LegalRegistrationNumber(did_web="did:web:test.com:1", vatID="123456789"),
            404,
            {},
            GaiaXClearingHouseNotaryException(),
            "error-case-404",
        ),
        (
            "did:web:test.com:vc/1",
            LegalRegistrationNumber(did_web="did:web:test.com:1", vatID="123456789"),
            409,
            {},
            GaiaXClearingHouseNotaryException(),
            "error-case-409",
        ),
        (
            "did:web:test.com:vc/1",
            LegalRegistrationNumber(did_web="did:web:test.com:1", vatID="123456789"),
            500,
            {},
            GaiaXClearingHouseNotaryException(),
            "error-case-500",
        ),
    ],
)
def test_get_registration_number_vc(
    respx_mock, vc_id, legal_registration_number, status_code, response_content, expected_exception, test_id
):
    # Arrange
    respx_mock.post(
        f"{GAIA_X_CLEARING_HOUSE_NOTARY_BASE_URL}/{GaiaXClearingHouseApiVersion.V1_STAGING.value}/registrationNumberVC?vcid={vc_id}"
    ).mock(return_value=httpx.Response(status_code=status_code, json=response_content))

    client = GXCHNotaryClient()

    # Act and Assert
    if isinstance(expected_exception, GaiaXClearingHouseNotaryException):
        with pytest.raises(GaiaXClearingHouseNotaryException):
            client.get_registration_number_vc(did_web=vc_id, legal_registration_number=legal_registration_number)
    else:
        terms_and_conditions = client.get_registration_number_vc(
            did_web=vc_id, legal_registration_number=legal_registration_number
        )
        assert terms_and_conditions == response_content, test_id
