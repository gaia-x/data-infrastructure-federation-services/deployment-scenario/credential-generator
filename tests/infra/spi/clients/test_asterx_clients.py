# -*- coding: utf-8 -*-
import json
from http import HTTPStatus
from pathlib import Path
from ssl import SSLCertVerificationError

import httpx
import pytest

from credential_generator.config import DEFAULT_ASTER_CONTEXT
from credential_generator.domain import OntologyType
from credential_generator.infra.spi.clients import BadRequestException, HostNotReachable, NotAuthorizedException
from credential_generator.infra.spi.clients.asterx import UserAgentClient, ProviderCatalogueClient, LabellingAgentClient


@pytest.mark.anyio
@pytest.mark.parametrize(
    "status_code, response_content, thrown_exception, expected_exception, test_id",
    [
        # Happy path tests with various realistic test values
        (
            HTTPStatus.OK,
            "User Agent for ovhcloud OK",
            None,
            None,
            "happy-path-default-version",
        ),
        # Error cases
        (
            -1,
            {},
            SSLCertVerificationError,
            HostNotReachable(""),
            "error-case-ssl",
        ),
        (HTTPStatus.BAD_REQUEST, {}, None, BadRequestException, "error-case-400"),
        (
            HTTPStatus.IM_A_TEAPOT,
            {},
            None,
            HostNotReachable(""),
            "error-case-418",
        ),
        (
            HTTPStatus.INTERNAL_SERVER_ERROR,
            {},
            None,
            HostNotReachable(""),
            "error-case-500",
        ),
    ],
)
async def test_user_agent_client_ping(
    respx_mock, status_code, response_content, expected_exception, thrown_exception, test_id
):
    # Arrange
    if thrown_exception is not None:
        respx_mock.get("http://example.com/healthcheck").mock(side_effect=thrown_exception)
    else:
        respx_mock.get("http://example.com/healthcheck").mock(
            return_value=httpx.Response(status_code=status_code, json=response_content)
        )

    client = UserAgentClient(url="example.com", port_number=80, api_key="123")

    # Act and Assert
    if expected_exception is not None:
        with pytest.raises(Exception):
            await client.ping()
    else:
        result = await client.ping()
        assert result is True, test_id


@pytest.mark.anyio
@pytest.mark.parametrize(
    "status_code, response_content, thrown_exception, expected_exception, test_id",
    [
        # Happy path tests with various realistic test values
        (
            HTTPStatus.OK,
            '"OK"',
            None,
            None,
            "happy-path-default-version",
        ),
        # Error cases
        (
            -1,
            {},
            SSLCertVerificationError,
            HostNotReachable(""),
            "error-case-ssl",
        ),
        (HTTPStatus.BAD_REQUEST, {}, None, BadRequestException(""), "error-case-400"),
        (HTTPStatus.FORBIDDEN, {}, None, NotAuthorizedException(""), "error-case-403"),
    ],
)
async def test_user_agent_client_bootstrap_provider(
    respx_mock, status_code, response_content, thrown_exception, expected_exception, test_id
):
    # Arrange
    if thrown_exception is not None:
        respx_mock.post("http://example.com/api/bootstrap-provider").mock(side_effect=thrown_exception)
    else:
        respx_mock.post("http://example.com/api/bootstrap-provider").mock(
            return_value=httpx.Response(status_code=status_code, json=response_content)
        )

    client = UserAgentClient(url="example.com", port_number=80, api_key="123")

    # Act and Assert
    if expected_exception is not None:
        with pytest.raises(Exception):
            await client.bootstrap()
    else:
        await client.bootstrap()


@pytest.mark.anyio
@pytest.mark.parametrize(
    "status_code, response_content, thrown_exception, expected_exception, object_type, ontology_type, test_id",
    [
        # Happy path tests with various realistic test values
        (
            HTTPStatus.OK,
            {"msg": "OK"},
            None,
            None,
            "test:ServiceOffering",
            OntologyType.SERVICE_OFFERING.value,
            "happy-path-default-version",
        ),
        # Error cases
        (
            HTTPStatus.OK,
            {"msg": "OK"},
            None,
            FileNotFoundError,
            None,
            OntologyType.SERVICE_OFFERING.value,
            "error-case-filenotfound",
        ),
        (
            HTTPStatus.OK,
            {"msg": "OK"},
            None,
            FileNotFoundError,
            None,
            OntologyType.SERVICE_OFFERING.value,
            "error-case-filenotfound",
        ),
        (
            -1,
            {},
            SSLCertVerificationError,
            HostNotReachable,
            "test:ServiceOffering",
            OntologyType.SERVICE_OFFERING.value,
            "error-case-ssl",
        ),
        (
            HTTPStatus.BAD_REQUEST,
            {},
            None,
            BadRequestException,
            "test:ServiceOffering",
            OntologyType.SERVICE_OFFERING.value,
            "error-case-400",
        ),
        (
            HTTPStatus.FORBIDDEN,
            {},
            None,
            NotAuthorizedException,
            "test:ServiceOffering",
            OntologyType.SERVICE_OFFERING.value,
            "error-case-403",
        ),
    ],
)
async def test_user_agent_client_store_object(
    respx_mock,
    create_directory,
    status_code,
    response_content,
    thrown_exception,
    expected_exception,
    object_type,
    ontology_type,
    test_id,
):
    filename = Path(create_directory, "test_store_object.json")
    if object_type is not None:
        with open(file=filename, mode="w", newline="", encoding="utf-8") as input_file:
            json.dump(fp=input_file, obj={"@type": object_type})

    # Arrange
    if thrown_exception is not None:
        respx_mock.post(f"http://example.com/api/store_object/{ontology_type}").mock(side_effect=thrown_exception)
    else:
        respx_mock.post(f"http://example.com/api/store_object/{ontology_type}").mock(
            return_value=httpx.Response(status_code=status_code, json=response_content)
        )

    client = UserAgentClient(url="example.com", port_number=80, api_key="123")

    # Act and Assert
    if expected_exception is not None:
        with pytest.raises(Exception):
            await client.store_object(filename)
    else:
        result = await client.store_object(filename)
        assert result == {"msg": "OK"}, test_id

    if filename.exists():
        filename.unlink()


@pytest.mark.anyio
@pytest.mark.parametrize(
    "status_code, response_content, thrown_exception, input, expected_exception, test_id",
    [
        # Happy path tests with various realistic test values
        (
            HTTPStatus.OK,
            "OK",
            None,
            {"@context": DEFAULT_ASTER_CONTEXT, "@type": "gx:ServiceOffering"},
            None,
            "happy-path-default-version",
        ),
        # Error cases
        (
            -1,
            {},
            SSLCertVerificationError,
            {"@context": DEFAULT_ASTER_CONTEXT, "@type": "gx:ServiceOffering"},
            HostNotReachable(""),
            "error-case-ssl",
        ),
        (
            HTTPStatus.BAD_REQUEST,
            {},
            None,
            {"@context": DEFAULT_ASTER_CONTEXT, "@type": "gx:ServiceOffering"},
            BadRequestException(""),
            "error-case-400",
        ),
        (
            HTTPStatus.FORBIDDEN,
            {},
            None,
            {"@context": DEFAULT_ASTER_CONTEXT, "@type": "gx:ServiceOffering"},
            NotAuthorizedException(""),
            "error-case-403",
        ),
    ],
)
async def test_sign_vc_with_content(
    respx_mock, status_code, response_content, thrown_exception, input, expected_exception, test_id
):
    # Arrange
    if thrown_exception is not None:
        respx_mock.put("http://example.com/api/vc-request").mock(side_effect=thrown_exception)
    else:
        respx_mock.put("http://example.com/api/vc-request").mock(
            return_value=httpx.Response(status_code=status_code, json=response_content)
        )

    client = UserAgentClient(url="example.com", port_number=80, api_key="123")

    # Act and Assert
    if expected_exception is not None:
        with pytest.raises(Exception):
            await client.sign_vc_with_content(input)
    else:
        result = await client.sign_vc_with_content(input)
        assert result == "OK", test_id


@pytest.mark.anyio
@pytest.mark.parametrize(
    "status_code, response_content, thrown_exception, expected_exception, object_type, ontology_type, test_id",
    [
        # Happy path tests with various realistic test values
        (
            HTTPStatus.OK,
            {"msg": "OK"},
            None,
            None,
            "test:ServiceOffering",
            OntologyType.SERVICE_OFFERING.value,
            "happy-path-default-version",
        ),
        # Error cases
        (
            HTTPStatus.OK,
            {"msg": "OK"},
            None,
            FileNotFoundError(),
            None,
            OntologyType.SERVICE_OFFERING.value,
            "error-case-filenotfound",
        ),
        (
            HTTPStatus.OK,
            {"msg": "OK"},
            None,
            FileNotFoundError(),
            None,
            OntologyType.SERVICE_OFFERING.value,
            "error-case-filenotfound",
        ),
        (
            -1,
            {},
            SSLCertVerificationError,
            HostNotReachable(""),
            "test:ServiceOffering",
            OntologyType.SERVICE_OFFERING.value,
            "error-case-ssl",
        ),
        (
            HTTPStatus.BAD_REQUEST,
            {},
            None,
            BadRequestException(""),
            "test:ServiceOffering",
            OntologyType.SERVICE_OFFERING.value,
            "error-case-400",
        ),
        (
            HTTPStatus.FORBIDDEN,
            {},
            None,
            NotAuthorizedException(""),
            "test:ServiceOffering",
            OntologyType.SERVICE_OFFERING.value,
            "error-case-403",
        ),
    ],
)
async def test_user_agent_client_sign_vc(
    respx_mock,
    create_directory,
    status_code,
    response_content,
    thrown_exception,
    expected_exception,
    object_type,
    ontology_type,
    test_id,
):
    filename = Path(create_directory, "test_store_object.json")
    if object_type is not None:
        with open(file=filename, mode="w", newline="", encoding="utf-8") as input_file:
            json.dump(fp=input_file, obj={"@type": object_type})

    # Arrange
    if thrown_exception is not None:
        respx_mock.put("http://example.com/api/vc-request").mock(side_effect=thrown_exception)
    else:
        respx_mock.put("http://example.com/api/vc-request").mock(
            return_value=httpx.Response(status_code=status_code, json=response_content)
        )

    client = UserAgentClient(url="example.com", port_number=80, api_key="123")

    # Act and Assert
    if expected_exception is not None:
        with pytest.raises(Exception):
            await client.sign_vc(filename)
    else:
        result = await client.sign_vc(filename)
        assert result == {"msg": "OK"}, test_id

    if filename.exists():
        filename.unlink()

@pytest.mark.anyio
@pytest.mark.parametrize(
    "status_code, response_content, thrown_exception, expected_exception, test_id",
    [
        # Happy path tests with various realistic test values
        (
            HTTPStatus.OK,
            "Provider Catalogue for ovhcloud OK",
            None,
            None,
            "happy-path-default-version",
        ),
        # Error cases
        (
            -1,
            {},
            SSLCertVerificationError,
            HostNotReachable(""),
            "error-case-ssl",
        ),
        (HTTPStatus.BAD_REQUEST, {}, None, BadRequestException, "error-case-400"),
        (
            HTTPStatus.IM_A_TEAPOT,
            {},
            None,
            HostNotReachable(""),
            "error-case-418",
        ),
        (
            HTTPStatus.INTERNAL_SERVER_ERROR,
            {},
            None,
            HostNotReachable(""),
            "error-case-500",
        ),
    ],
)
async def test_catalogue_client_ping(
    respx_mock, status_code, response_content, expected_exception, thrown_exception, test_id
):
    # Arrange
    if thrown_exception is not None:
        respx_mock.get("http://catalogue-api.example.com/healthcheck").mock(side_effect=thrown_exception)
    else:
        respx_mock.get("http://catalogue-api.example.com/healthcheck").mock(
            return_value=httpx.Response(status_code=status_code, json=response_content)
        )

    client = ProviderCatalogueClient(base_url="example.com", port_number=80, api_key="123")

    # Act and Assert
    if expected_exception is not None:
        with pytest.raises(Exception):
            await client.ping()
    else:
        result = await client.ping()
        assert result is True, test_id

@pytest.mark.anyio
@pytest.mark.parametrize(
    "status_code, response_content, thrown_exception, expected_exception, did_web, enable_ces_sync, test_id",
    [
        # Happy path tests with various realistic test values
        (
            HTTPStatus.OK,
            {"msg": "OK"},
            None,
            None,
            "did:web:example.com:1",
            False,
            "happy-path-index-vc-without-ces-sync",
        ),
        (
            HTTPStatus.OK,
            {"msg": "OK"},
            None,
            None,
            "did:web:example.com:1",
            True,
            "happy-path-index-vc-with-ces-sync",
        ),
        # Error cases
        (
            -1,
            {},
            SSLCertVerificationError,
            HostNotReachable(""),
            "did:web:example.com:1",
            False,
            "error-case-ssl",
        ),
        (
            HTTPStatus.BAD_REQUEST,
            {},
            None,
            BadRequestException(""),
            "did:web:example.com:1",
            False,
            "error-case-400",
        ),
        (
            HTTPStatus.FORBIDDEN,
            {},
            None,
            NotAuthorizedException(""),
            "did:web:example.com:1",
            False,
            "error-case-403",
        ),
    ],
)
async def test_provider_catalogue_index_vc(
    respx_mock,
    create_directory,
    status_code,
    response_content,
    thrown_exception,
    expected_exception,
    did_web,
    enable_ces_sync,
    test_id,
):
    # Arrange
    if thrown_exception is not None:
        (respx_mock
         .post(f"http://catalogue-api.example.com:80/catalog/items/?publish_to_catalog=True&publish_to_ces={enable_ces_sync}")
         .mock(side_effect=thrown_exception)
        )
    else:
        (respx_mock.post(f"http://catalogue-api.example.com:80/catalog/items/?publish_to_catalog=True&publish_to_ces={enable_ces_sync}")
        .mock(
            return_value=httpx.Response(status_code=status_code, json=response_content)
        ))

    client = ProviderCatalogueClient(base_url="example.com", port_number=80, api_key="123")

    # Act and Assert
    if expected_exception is not None:
        with pytest.raises(Exception):
            await client.index_vc(did_web=did_web, enable_ces_sync=enable_ces_sync)
    else:
        result = await client.index_vc(did_web=did_web, enable_ces_sync=enable_ces_sync)
        assert result == '{"msg": "OK"}', test_id

@pytest.mark.anyio
@pytest.mark.parametrize(
    "status_code, response_content, thrown_exception, expected_exception, did_web_list, enable_ces_sync, test_id",
    [
        # Happy path tests with various realistic test values
        (
            HTTPStatus.OK,
            {"msg": "OK"},
            None,
            None,
            ["did:web:example.com:1"],
            False,
            "happy-path-index-vc-without-ces-sync",
        ),
        (
            HTTPStatus.OK,
            {"msg": "OK"},
            None,
            None,
            ["did:web:example.com:1"],
            True,
            "happy-path-index-vc-with-ces-sync",
        ),
        # Error cases
        (
            -1,
            {},
            SSLCertVerificationError,
            HostNotReachable(""),
            ["did:web:example.com:1"],
            False,
            "error-case-ssl",
        ),
        (
            HTTPStatus.BAD_REQUEST,
            {},
            None,
            BadRequestException(""),
            ["did:web:example.com:1"],
            False,
            "error-case-400",
        ),
        (
            HTTPStatus.FORBIDDEN,
            {},
            None,
            NotAuthorizedException(""),
            ["did:web:example.com:1"],
            False,
            "error-case-403",
        ),
    ],
)
async def test_provider_catalogue_index_vcs(
    respx_mock,
    status_code,
    response_content,
    thrown_exception,
    expected_exception,
    did_web_list,
    enable_ces_sync,
    test_id,
):
    # Arrange
    if thrown_exception is not None:
        (respx_mock
         .post(f"http://catalogue-api.example.com:80/catalogue/items/?publish_to_catalogue=True&publish_to_ces={enable_ces_sync}")
         .mock(side_effect=thrown_exception)
        )
    else:
        (respx_mock.post(f"http://catalogue-api.example.com:80/catalogue/items/?publish_to_catalogue=True&publish_to_ces={enable_ces_sync}")
        .mock(
            return_value=httpx.Response(status_code=status_code, json=response_content)
        ))

    client = ProviderCatalogueClient(base_url="example.com", port_number=80, api_key="123")

    # Act and Assert
    if expected_exception is not None:
        with pytest.raises(Exception):
            await client.index_vcs(did_webs=did_web_list, enable_ces_sync=enable_ces_sync)
    else:
        result = await client.index_vcs(did_webs=did_web_list, enable_ces_sync=enable_ces_sync)
        assert result == '{"msg": "OK"}', test_id

@pytest.mark.anyio
@pytest.mark.parametrize(
    "status_code, response_content, thrown_exception, expected_exception, vp_request, test_id",
    [
        # Happy path tests with various realistic test values
        (
            HTTPStatus.OK,
            {"msg": "OK"},
            None,
            None,
            {"test": True},
            "happy-path-index-vc-without-ces-sync",
        ),
        (
            HTTPStatus.OK,
            {"msg": "OK"},
            None,
            None,
            {"test": True},
            "happy-path-index-vc-with-ces-sync",
        ),
        # Error cases
        (
            -1,
            {},
            SSLCertVerificationError,
            HostNotReachable(""),
            {"test": True},
            "error-case-ssl",
        ),
        (
            HTTPStatus.BAD_REQUEST,
            {},
            None,
            BadRequestException(""),
            {"test": True},
            "error-case-400",
        ),
        (
            HTTPStatus.FORBIDDEN,
            {},
            None,
            NotAuthorizedException(""),
            {"test": True},
            "error-case-403",
        ),
    ],
)
async def test_labelling_client_check_labels(
    respx_mock,
    status_code,
    response_content,
    thrown_exception,
    expected_exception,
    vp_request,
    test_id,
):
    # Arrange
    if thrown_exception is not None:
        (respx_mock
         .post(f"http://labelling.example.com:80")
         .mock(side_effect=thrown_exception)
        )
    else:
        (respx_mock.post(f"http://labelling.example.com:80")
        .mock(
            return_value=httpx.Response(status_code=status_code, json=response_content)
        ))

    client = LabellingAgentClient(base_url="example.com", port_number=80)

    # Act and Assert
    if expected_exception is not None:
        with pytest.raises(Exception):
            await client.check_labels(vp_request)
    else:
        result = await client.check_labels(vp_request)
        assert result == {"msg": "OK"}, test_id
