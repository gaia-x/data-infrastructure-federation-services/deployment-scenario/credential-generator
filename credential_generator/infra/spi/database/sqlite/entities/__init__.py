# -*- coding: utf-8 -*-
"""
This module declares compliance entities.
"""
from typing import List, Optional

from sqlalchemy import Boolean, Column, ForeignKey, String, Table
from sqlalchemy.orm import DeclarativeBase, Mapped, mapped_column, relationship


class Base(DeclarativeBase):
    """
    Base class is the base class for SQLAlchemy ORM mapping
    """


service_layer_link = Table(
    "service_layer_link",
    Base.metadata,
    Column("service_id", ForeignKey("service.id"), primary_key=True),
    Column("layer_id", ForeignKey("layer.id"), primary_key=True),
)

service_location_link = Table(
    "service_location_link",
    Base.metadata,
    Column("service_id", ForeignKey("service.id"), primary_key=True),
    Column("location_id", ForeignKey("location.id"), primary_key=True),
)

service_keyword_link = Table(
    "service_keyword_link",
    Base.metadata,
    Column("service_id", ForeignKey("service.id"), primary_key=True),
    Column("keyword_id", ForeignKey("keyword.id"), primary_key=True),
)

compliance_criterion_level_link = Table(
    "compliance_criterion_level_link",
    Base.metadata,
    Column("level_id", ForeignKey("level.level_id"), primary_key=True),
    Column("compliance_criterion_id", ForeignKey("compliance_criterion.criterion_id"), primary_key=True),
)

service_compliance_reference_link = Table(
    "service_compliance_reference_link",
    Base.metadata,
    Column("service_id", ForeignKey("service.id"), primary_key=True),
    Column("compliance_reference_id", ForeignKey("compliance_reference.compliance_reference_id"), primary_key=True),
)

service_self_assessed_criterions_link = Table(
    "service_self_assessed_criterions_link",
    Base.metadata,
    Column("service_id", ForeignKey("service.id"), primary_key=True),
    Column("criterion_id", ForeignKey("compliance_criterion.criterion_id"), primary_key=True),
)


class ThirdPartyComplianceCriterionComplianceCertificationSchemeLink(Base):
    """
    This entity represents a ThirdPartyComplianceCriterionComplianceCertificationSchemeLink
    """

    __tablename__ = "third_party_compliance_criterion_compliance_certification_scheme_link"
    compliance_criterion_id: Mapped[str] = mapped_column(
        String,
        ForeignKey("compliance_criterion_level_link.compliance_criterion_id"),
        primary_key=True,
        nullable=False,
    )

    compliance_criterion_level_id: Mapped[str] = mapped_column(
        String,
        ForeignKey("compliance_criterion_level_link.level_id"),
        primary_key=True,
        nullable=False,
    )

    compliance_reference_id: Mapped[str] = mapped_column(
        String,
        ForeignKey("third_party_compliance_certification_scheme.compliance_reference_id"),
        primary_key=True,
        nullable=False,
    )


class ComplianceCertificationScheme(Base):
    """
    This entity represents a ComplianceCertificationScheme
    """

    __tablename__ = "compliance_certification_scheme"

    compliance_reference_id: Mapped[str] = mapped_column(
        String, ForeignKey("compliance_reference.compliance_reference_id"), primary_key=True, nullable=False
    )


class ThirdPartyComplianceCertificationScheme(Base):
    """
    This entity represents a ThirdPartyComplianceCertificationScheme
    """

    __tablename__ = "third_party_compliance_certification_scheme"

    compliance_reference_id: Mapped[str] = mapped_column(
        String, ForeignKey("compliance_reference.compliance_reference_id"), primary_key=True, nullable=False
    )


class ComplianceCriterionComplianceCertificationSchemeLink(Base):
    """
    This entity represents a ComplianceCriterionComplianceCertificationSchemeLink
    """

    __tablename__ = "compliance_criterion_compliance_certification_scheme_link"

    compliance_criterion_id: Mapped[str] = mapped_column(
        String,
        ForeignKey("compliance_criterion_level_link.compliance_criterion_id"),
        primary_key=True,
        nullable=False,
    )
    compliance_criterion_level_id: Mapped[str] = mapped_column(
        String,
        ForeignKey("compliance_criterion_level_link.level_id"),
        primary_key=True,
        nullable=False,
    )
    compliance_reference_id: Mapped[str] = mapped_column(
        String,
        ForeignKey("compliance_certification_scheme.compliance_reference_id"),
        primary_key=True,
        nullable=False,
    )


class ThirdPartyComplianceCertificationSchemeCABProviderLink(Base):
    """
    This entity represents a ThirdPartyComplianceCertificationSchemeCABProviderLink
    """

    __tablename__ = "third_party_compliance_certification_scheme_cab_provider_link"

    compliance_reference_id: Mapped[str] = mapped_column(
        String,
        ForeignKey("third_party_compliance_certification_scheme.compliance_reference_id"),
        primary_key=True,
        nullable=False,
    )

    provider_name: Mapped[str] = mapped_column(
        String,
        ForeignKey("provider.name"),
        primary_key=True,
        nullable=False,
    )

    cab_id: Mapped[str] = mapped_column(
        String,
        ForeignKey("compliance_assessments_body.cab_id"),
        primary_key=True,
        nullable=False,
    )


class ComplianceCertificationSchemeProviderLink(Base):
    """
    The ComplianceCertificationSchemeProviderLink class is a SQLModel class that represents a link between
    ComplianceCertificationScheme and a Provider.

    """

    __tablename__ = "compliance_certification_scheme_provider_link"

    compliance_reference_id: Mapped[str] = mapped_column(
        String,
        ForeignKey("compliance_certification_scheme.compliance_reference_id"),
        primary_key=True,
        nullable=False,
    )

    provider_name: Mapped[str] = mapped_column(
        String,
        ForeignKey("provider.name"),
        primary_key=True,
        nullable=False,
    )


class ComplianceAssessmentsBody(Base):
    """
    This entity represents a ComplianceAssessmentsBody
    """

    __tablename__ = "compliance_assessments_body"

    cab_id: Mapped[str] = mapped_column(
        String,
        primary_key=True,
        nullable=False,
    )
    name: Mapped[str] = mapped_column(String, index=True, unique=True, nullable=False)
    lei_code: Mapped[Optional[str]] = mapped_column(String, nullable=True)
    street: Mapped[Optional[str]] = mapped_column(String, nullable=True)
    locality: Mapped[Optional[str]] = mapped_column(String, nullable=True)
    postal_code: Mapped[Optional[str]] = mapped_column(String, nullable=True)
    country: Mapped[Optional[str]] = mapped_column(String, nullable=True)
    registration_number: Mapped[Optional[str]] = mapped_column(String, nullable=True)
    designation: Mapped[Optional[str]] = mapped_column(String, nullable=True)
    terms_and_conditions: Mapped[Optional[str]] = mapped_column(String, nullable=True)


class ComplianceReferenceManager(Base):
    """
    This entity represents a ComplianceReferenceManager
    """

    __tablename__ = "compliance_reference_manager"

    name: Mapped[str] = mapped_column(String, primary_key=True, index=True, unique=True, nullable=False)
    lei_code: Mapped[Optional[str]] = mapped_column(String, nullable=True)
    street: Mapped[Optional[str]] = mapped_column(String, nullable=True)
    locality: Mapped[Optional[str]] = mapped_column(String, nullable=True)
    postal_code: Mapped[Optional[str]] = mapped_column(String, nullable=True)
    country: Mapped[Optional[str]] = mapped_column(String, nullable=True)
    registration_number: Mapped[Optional[str]] = mapped_column(String, nullable=True)
    designation: Mapped[Optional[str]] = mapped_column(String, nullable=True)
    terms_and_conditions: Mapped[Optional[str]] = mapped_column(String, nullable=True)


class Provider(Base):
    """
    This entity represents a Provider
    """

    __tablename__ = "provider"

    name: Mapped[str] = mapped_column(String, primary_key=True, index=True, unique=True, nullable=False)
    lei_code: Mapped[Optional[str]] = mapped_column(String, nullable=True)
    street: Mapped[Optional[str]] = mapped_column(String, nullable=True)
    locality: Mapped[Optional[str]] = mapped_column(String, nullable=True)
    postal_code: Mapped[Optional[str]] = mapped_column(String, nullable=True)
    country: Mapped[Optional[str]] = mapped_column(String, nullable=True)
    registration_number: Mapped[Optional[str]] = mapped_column(String, nullable=True)
    designation: Mapped[Optional[str]] = mapped_column(String, nullable=True)
    terms_and_conditions: Mapped[Optional[str]] = mapped_column(String, nullable=True)


class Keyword(Base):
    """
    This entity represents a Keyword
    """

    __tablename__ = "keyword"

    id: Mapped[str] = mapped_column(String, primary_key=True, nullable=False)
    services: Mapped[List["Service"]] = relationship(
        "Service", secondary=service_keyword_link, back_populates="keywords"
    )


class Layer(Base):
    """
    This entity represents a Layer
    """

    __tablename__ = "layer"

    id: Mapped[str] = mapped_column(String, primary_key=True, nullable=False)
    services: Mapped[List["Service"]] = relationship("Service", secondary=service_layer_link, back_populates="layers")


class Location(Base):
    """
    This entity represents a Location
    """

    __tablename__ = "location"

    id: Mapped[str] = mapped_column(String, primary_key=True, nullable=False)
    provider_name: Mapped[str] = mapped_column(String, index=True, nullable=False)
    location_external_id: Mapped[str] = mapped_column(String, index=True, nullable=False)
    designation: Mapped[str] = mapped_column(String, nullable=False)
    city: Mapped[str] = mapped_column(String, nullable=False)
    state: Mapped[str] = mapped_column(String, nullable=False)
    country: Mapped[str] = mapped_column(String, nullable=False)
    services: Mapped[List["Service"]] = relationship(
        "Service", secondary=service_location_link, back_populates="locations"
    )


class Service(Base):
    """
    This entity represents a Service
    """

    __tablename__ = "service"

    id: Mapped[str] = mapped_column(String, primary_key=True, nullable=False)
    provider_name: Mapped[str] = mapped_column(String, index=True, nullable=False)
    service_external_id: Mapped[str] = mapped_column(String, nullable=False)
    designation: Mapped[str] = mapped_column(String, nullable=False)
    service_version: Mapped[str] = mapped_column(String, nullable=False)
    description: Mapped[Optional[str]] = mapped_column(String, nullable=True, default=None)
    layers: Mapped[List["Layer"]] = relationship("Layer", secondary=service_layer_link, back_populates="services")
    keywords: Mapped[List["Keyword"]] = relationship(
        "Keyword", secondary=service_keyword_link, back_populates="services"
    )
    locations: Mapped[List["Location"]] = relationship(
        "Location", secondary=service_location_link, back_populates="services"
    )
    compliance_references: Mapped[List["ComplianceReference"]] = relationship(
        "ComplianceReference", secondary=service_compliance_reference_link, back_populates="services"
    )

    self_assessed_criterions: Mapped[List["ComplianceCriterion"]] = relationship(
        "ComplianceCriterion", secondary=service_self_assessed_criterions_link, back_populates="services"
    )

    level_id: Mapped[Optional[str]] = mapped_column(
        String,
        ForeignKey("level.level_id"),
        nullable=True,
        default=None,
    )


class ComplianceCriterion(Base):
    """
    This entity represents a ComplianceCriterion
    """

    __tablename__ = "compliance_criterion"

    criterion_id: Mapped[str] = mapped_column(String, primary_key=True, nullable=False)
    name: Mapped[str] = mapped_column(String, nullable=False)
    category: Mapped[str] = mapped_column(String, nullable=False)
    description: Mapped[str] = mapped_column(String, nullable=False)
    self_assessed: Mapped[bool] = mapped_column(Boolean, nullable=False)

    levels: Mapped[List["Level"]] = relationship(
        "Level", secondary=compliance_criterion_level_link, back_populates="compliance_criterions"
    )
    services: Mapped[List["Service"]] = relationship(
        "Service", secondary=service_self_assessed_criterions_link, back_populates="self_assessed_criterions"
    )


class ComplianceReference(Base):
    """
    This entity represents a ComplianceReference
    """

    __tablename__ = "compliance_reference"

    compliance_reference_id: Mapped[str] = mapped_column(String, primary_key=True, nullable=False)
    compliance_reference_manager_name: Mapped[str] = mapped_column(
        String, ForeignKey("compliance_reference_manager.name"), primary_key=True, nullable=False
    )
    services: Mapped[List["Service"]] = relationship(
        "Service", secondary=service_compliance_reference_link, back_populates="compliance_references"
    )
    designation: Mapped[str] = mapped_column(String, nullable=False)
    version: Mapped[str] = mapped_column(String, nullable=False)
    category: Mapped[str] = mapped_column(String, nullable=False)
    valid_from: Mapped[Optional[str]] = mapped_column(String, nullable=True)
    valid_until: Mapped[Optional[str]] = mapped_column(String, nullable=True)
    reference: Mapped[Optional[str]] = mapped_column(String, nullable=True)
    reference_hash: Mapped[Optional[str]] = mapped_column(String, nullable=True)
    assertion_method: Mapped[Optional[str]] = mapped_column(String, nullable=True)
    valid_date: Mapped[Optional[str]] = mapped_column(String, nullable=True)
    issuer_designation: Mapped[Optional[str]] = mapped_column(String, nullable=True)


class Level(Base):
    """
    This entity represents a Level
    """

    __tablename__ = "level"

    level_id: Mapped[str] = mapped_column(String, primary_key=True, nullable=False)
    compliance_criterions: Mapped[List["ComplianceCriterion"]] = relationship(
        "ComplianceCriterion", secondary=compliance_criterion_level_link, back_populates="levels"
    )
