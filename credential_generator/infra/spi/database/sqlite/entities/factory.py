# -*- coding: utf-8 -*-
"""
It defines factories to create Entities from dict.
"""
# Standard Library
import logging
from typing import Any, Generator

# data-initialiser Library
from credential_generator.infra.spi.database.sqlite.entities import (
    ComplianceAssessmentsBody,
    ComplianceCertificationScheme,
    ComplianceCertificationSchemeProviderLink,
    ComplianceCriterion,
    ComplianceReference,
    ComplianceReferenceManager,
    Keyword,
    Layer,
    Level,
    Location,
    Provider,
    Service,
    ThirdPartyComplianceCertificationScheme,
    ThirdPartyComplianceCertificationSchemeCABProviderLink,
)

PARTICIPANT_MUST_BE_NOT_NONE_ERROR_MESSAGE = "participant dictionary cannot be None."

log = logging.getLogger(__name__)


def __check_required_properties(object_dict: dict, required_keys: list):
    """
    The `__check_required_properties` function checks if a given dictionary contains all the required keys.
    If any required key is missing, it raises a `ValueError` with a message indicating the missing key.

    ### Example Usage
    ```python
    object_dict = {
        "key1": "value1",
        "key2": "value2",
        "key3": "value3"
    }
    required_keys = ["key1", "key2", "key3", "key4"]

    __check_required_properties(object_dict, required_keys)
    # Output: ValueError: Missing required key: key4
    ```
    """
    for key in required_keys:
        if key not in object_dict:
            raise ValueError(f"Missing required key: {key}")


def to_compliance_reference_entity(compliance_reference: dict) -> ComplianceReference:
    """
    The to_compliance_reference_entity function takes a dictionary representing a compliance reference and
    converts it into an instance of the ComplianceReferenceEntity class. It maps the values from the dictionary
    to the corresponding attributes of the  class.

    Example Usage
    ```python
    # Creating a dictionary representing a compliance reference
    compliance_reference = {
        "criterion_reference_id": "reference1",
        "title": "designation1",
        "version": "version1",
        "category": "category1",
        "valid_from": "2022-01-01",
        "valid_until": "2022-12-31",
        "reference": "https://example.com",
        "assertion_method": "method1",
        "assertion_date": "2022-06-30",
        "issuer_designation": "issuer1"
    }

    # Converting the dictionary to a ComplianceReferenceEntity instance
    entity = to_compliance_reference_entity(compliance_reference)

    # Accessing the attributes of the entity
    print(entity.compliance_reference_id)  # Output: "reference1"
    print(entity.designation)  # Output: "designation1"
    print(entity.version)  # Output: "version1"
    print(entity.category)  # Output: "category1"
    print(entity.valid_from)  # Output: "2022-01-01"
    print(entity.valid_until)  # Output: "2022-12-31"
    print(entity.reference)  # Output: "https://example.com"
    print(entity.assertion_method)  # Output: "method1"
    print(entity.valid_date)  # Output: "2022-06-30"
    print(entity.issuer_designation)  # Output: "issuer1"
    ```
    :param compliance_reference:
    :return:
    """
    required_keys = [
        "criterion_reference_id",
        "compliance_reference_manager_name",
        "title",
        "version",
        "category",
    ]
    __check_required_properties(compliance_reference, required_keys)

    return ComplianceReference(
        compliance_reference_id=compliance_reference.get("criterion_reference_id"),
        compliance_reference_manager_name=compliance_reference.get("compliance_reference_manager_name"),
        designation=compliance_reference.get("title"),
        version=compliance_reference.get("version"),
        category=compliance_reference.get("category"),
        valid_from=compliance_reference.get("valid_from"),
        valid_until=compliance_reference.get("valid_until"),
        reference=compliance_reference.get("reference"),
        assertion_method=compliance_reference.get("assertion_method"),
        valid_date=compliance_reference.get("assertion_date"),
        issuer_designation=compliance_reference.get("issuer_designation"),
        reference_hash=compliance_reference.get("document_sha256"),
    )


def to_location_entity(location: dict) -> Location:
    """
    The `to_location_entity` function is a factory function that takes a dictionary representing a location
    and returns an instance of the `Location` class.
    It extracts the necessary information from the input dictionary and uses it to create the `Location` object.

    ### Example Usage
    ```python
    location = {
        "provider_name": "ABC",
        "location_id": "123",
        "designation": "Headquarters",
        "city": "New York",
        "state": "NY",
        "country": "USA"
    }

    entity = to_location_entity(location)
    print(entity)
    ```
    :param location: a dictionary representing a location.
    :return: a ComplianceReference instance
    """
    if location is None:
        raise ValueError("participant dictionary cannot be None.")

    required_keys = ["provider_name", "location_id", "designation", "city", "state", "country"]
    __check_required_properties(location, required_keys)

    provider_name = location.get("provider_name")
    location_id = location.get("location_id")
    return Location(
        id=get_entity_id(provider_name=provider_name, entity_external_id=location_id),
        provider_name=provider_name,
        location_external_id=location_id,
        designation=location.get("designation"),
        city=location.get("city"),
        state=location.get("state"),
        country=location.get("country"),
    )


def get_entity_id(provider_name: str, entity_external_id: str) -> str:
    """
    The `get_entity_id` function takes in a provider name and a entity external ID and returns a string representing
    the entity ID. It concatenates the provider name and entity external ID with a double hyphen in between.

    ### Example Usage
    ```python
    provider_name = "ABC"
    entity_external_id = "123"
    entity_id = get_entity_id(provider_name, entity_external_id)
    print(entity_id)  # Output: "'abc__123'"
    ```
    :param provider_name: name of provider
    :param entity_external_id: external id
    :return: a string representing entity id
    :raise TypeError if provider_name or entity_external_id is not a string
    :raise ValueError if provider_name or entity_external_id is None or empty strings
    """
    if provider_name is None or entity_external_id is None:
        raise ValueError("Input strings cannot be None.")

    if not isinstance(provider_name, str):
        raise TypeError("provider_name must be a string.")

    if not isinstance(entity_external_id, str):
        raise TypeError("location_external_id must be a string.")

    if not provider_name or not entity_external_id:
        raise ValueError("Input strings cannot be empty.")

    provider_name = provider_name.lower().strip()
    entity_external_id = entity_external_id.lower().strip()

    return f"{provider_name}__{entity_external_id}"


def get_entity_external_id(provider_name: str, entity_id: str) -> str:
    """
    This function, get_entity_external_id, takes in two string inputs, provider_name and entity_id, and returns a
    modified version of entity_id. The function first checks if the inputs are valid and not empty. It then converts
    provider_name to lowercase and removes any leading or trailing whitespace. Finally, it removes the provider_name
    followed by two underscores from the beginning of entity_id and converts the result to uppercase before returning
    it.
    ## Example Usage
    ```
    external_id = get_entity_external_id("ProviderA", "ProviderA__12345")
    print(external_id)
    ```
    :param provider_name: The name of the provider.
    :param entity_id: The ID of the entity.
    :return: The modified version of entity_id with the provider_name removed and converted to uppercase.
    """
    if provider_name is None or entity_id is None:
        raise ValueError("Input strings cannot be None.")

    if not isinstance(provider_name, str):
        raise TypeError("provider_name must be a string.")

    if not isinstance(entity_id, str):
        raise TypeError("location_external_id must be a string.")

    if not provider_name or not entity_id:
        raise ValueError("Input strings cannot be empty.")

    provider_name = provider_name.lower().strip()
    return entity_id.removeprefix(f"{provider_name}__").upper()


def get_self_assessments_id(provider_name: str, criterion: str) -> str:
    """
    The `get_self_assessments_id` function takes in two parameters: `provider_name` and `criterion`.
    It checks if the input strings are valid and not empty. It then converts the `provider_name` to lowercase and
    removes any leading or trailing whitespace.
    It extracts the criterion ID from the `criterion` string by splitting it at the colon (:) and taking the first part.
    Finally, it returns a string combining the lowercase `provider_name` and the extracted `criterion_id`
    separated by two underscores (__).

    ### Example Usage
    ```python
    provider_name = "ABC Provider"
    criterion = "CRITERION:12345"

    result = get_self_assessments_id(provider_name, criterion)
    print(result)
    ```
    Output:
    ```
    abc provider__criterion
    ```
    :param provider_name:
    :param criterion:
    :return:
    """
    if provider_name is None or criterion is None:
        raise ValueError("Input strings cannot be None.")

    if not isinstance(provider_name, str):
        raise TypeError("provider_name must be a string.")

    if not isinstance(criterion, str):
        raise TypeError("location_external_id must be a string.")

    if not provider_name or not criterion:
        raise ValueError("Input strings cannot be empty.")

    provider_name = provider_name.lower().strip()
    criterion_id = criterion.strip().split(":")[0]

    return f"{provider_name}__{criterion_id}"


def to_layer_entity(layer: str) -> Layer:
    """
    The `to_layer_entity` function is a factory function that creates an instance of the `Layer` class from
    a string representation of a layer name.

    ### Example Usage
    ```python
    layer_name = "Layer 1"
    layer_entity = to_layer_entity(layer_name)
    print(layer_entity.id)  # Output: "Layer 1"
    ```
    :param layer: a layer name
    :return: a Layer instance
    :raise TypeError if layer_name is not a string
    :raise ValueError if layer_name is an empty string
    """
    if not isinstance(layer, str):
        raise TypeError("Input 'layer' must be a string")

    if not layer:
        raise ValueError("Invalid layer input")

    return Layer(id=layer.strip())


def to_service_entity(service: dict) -> Service:
    """
    The `to_service_entity` function is a factory function that takes in a dictionary representing a service
    and returns an instance of the `Service` class. It extracts the necessary properties from the dictionary
    and creates a new `Service` object with those properties.

    ### Example Usage
    ```python
    service = {
        "provider_name": "ABC",
        "service_id": "123",
        "designation": "Service 1",
        "service_version": "1.0",
        "description": "This is a sample service"
    }

    entity = to_service_entity(service)
    print(entity.id)  # Output: "abc__123"
    print(entity.provider_name)  # Output: "ABC"
    print(entity.service_external_id)  # Output: "123"
    print(entity.designation)  # Output: "Service 1"
    print(entity.service_version)  # Output: "1.0"
    print(entity.description)  # Output: "This is a sample service"
    print(entity.locations)  # Output: []
    print(entity.layers)  # Output: []
    ```
    :param service: a dict with service properties
    :return: a Service instance
    :raise ValueError if provider compliance properties are not found in dictionary
    """

    if service is None:
        raise ValueError("Service dictionary cannot be None.")

    required_keys = ["provider_name", "service_id", "designation", "service_version", "description"]
    __check_required_properties(service, required_keys)

    provider_name = service["provider_name"]
    service_id = service["service_id"]
    return Service(
        id=get_entity_id(provider_name=provider_name, entity_external_id=service_id),
        provider_name=provider_name.strip(),
        service_external_id=service_id.strip(),
        designation=service.get("designation"),
        service_version=service.get("service_version"),
        description=service.get("description"),
        level_id=service.get("level"),
        locations=[],
        layers=[],
        keywords=[],
    )


def to_keyword_entity(keyword_name: str) -> Keyword:
    """
    The `to_keyword_entity` function is a factory function that creates an instance of the `Keyword` class from
    a string representation of a keyword name.

    ### Example Usage
    ```python
    keyword_name = "Keyword 1"
    keyword_entity = to_keyword_entity(keyword_name)
    print(keyword_entity.id)  # Output: "Keyword 1"
    ```
    :param keyword_name: a Keyword name
    :return: a Keyword instance
    :raise TypeError if keyword_name is not a string
    :raise ValueError if keyword_name is an empty string
    """
    if not isinstance(keyword_name, str):
        raise TypeError("Input 'keyword_name' must be a string")

    if not keyword_name:
        raise ValueError("Invalid keyword_name input")

    return Keyword(id=keyword_name.strip())


def to_keyword_entity_list(keyword_list: list[str]) -> Generator[Keyword, Any, None]:
    """
    The `to_keyword_entity_list` function takes a list of keyword names as input and returns a generator that yields
    instances of the `Keyword` class. It first checks if the input is a valid list of strings, and then calls the
    `to_keyword_entity` function for each keyword name in the list to create the `Keyword` instances. If the input
    list is empty, an empty generator is returned.

    ### Example Usage
    ```python
    keyword_list = ["keyword 1", "keyword 2", "keyword 3"]
    keyword_entity_list = to_keyword_entity_list(keyword_list)
    for keyword_entity in keyword_entity_list:
        print(keyword_entity.id)
    ```
    Output:
    ```
    keyword 1
    keyword 2
    keyword 3
    ```
    :param keyword_list: a list of keyword string
    :return: a list of  keyword instance
    :raise TypeError if keyword_list is not a list of string
    """
    if not isinstance(keyword_list, list):
        raise TypeError("Input 'keyword_list' must be a list")

    if not all(isinstance(keyword, str) for keyword in keyword_list):
        raise TypeError("Elements of 'keyword_list' must be strings")

    return (Keyword(id=keyword.strip()) for keyword in keyword_list if keyword != "")


# def to_provider_compliance(provider_compliance: dict) -> ProviderCompliance:
#     """
#     The `to_provider_compliance` function takes in a dictionary representing provider compliance information and
#     returns an instance of the `ProviderCompliance` class. It performs validation on the input dictionary and
#     constructs the `ProviderCompliance` object using the provided values.
#
#     ### Example Usage
#     ```python
#     provider_compliance = {
#         "provider_name": "ABC",
#         "compliance_reference_id": "123",
#         "cab_id": "456",
#         "issuer_designation": "Issuer",
#         "assertion_date": "2022-01-01",
#         "assertion_method": "Method",
#         "reference_doc": "Doc",
#         "valid_until": "2022-12-31",
#         "valid_from": "2022-01-01",
#         "version": "1.0",
#         "compliance_reference_designation": "Reference",
#     }
#     entity = to_provider_compliance(provider_compliance)
#     print(entity.id)  # Output: "abc__123"
#     ```
#     :param provider_compliance: a dictionary with provider compliance properties
#     :return:a ProviderCompliance instance
#     :raise ValueError if provider compliance properties are not found in dictionary
#     """
#     if provider_compliance is None:
#         raise ValueError("provider_compliance dictionary cannot be None.")
#
#     required_keys = [
#         "provider_name",
#         "cab_id",
#         "issuer_designation",
#         "assertion_date",
#         "assertion_method",
#         "reference_doc",
#         "valid_until",
#         "valid_from",
#         "version",
#         "compliance_reference_designation",
#         "compliance_reference_id",
#     ]
#
#     __check_required_properties(provider_compliance, required_keys)
#
#     provider_name = provider_compliance["provider_name"]
#     compliance_reference_id = provider_compliance["compliance_reference_id"]
#     return ProviderCompliance(
#         id=get_entity_id(provider_name=provider_name, entity_external_id=compliance_reference_id),
#         provider_name=provider_name.strip(),
#         cab_id=provider_compliance["cab_id"],
#         issuer_designation=provider_compliance["issuer_designation"],
#         assertion_date=provider_compliance["assertion_date"],
#         assertion_method=provider_compliance["assertion_method"],
#         reference_doc=provider_compliance["reference_doc"],
#         valid_until=provider_compliance["valid_until"],
#         valid_from=provider_compliance["valid_from"],
#         version=provider_compliance["version"],
#         compliance_reference_id=compliance_reference_id.strip(),
#     )
#
#
# def to_provider_self_assessments(provider_self_assessments: dict) -> ProviderSelfAssessments:
#     """
#     The `to_provider_self_assessments` function takes in a dictionary `provider_self_assessments` as input and
#     converts it into an instance of the `ProviderSelfAssessments` class. It performs some validation checks on
#     the input dictionary and extracts necessary information to create the entity object.
#
#     ### Example Usage
#     ```python
#     provider_self_assessments = {
#         "provider_name": "ABC Provider",
#         "section": "Section 1",
#         "criterion": "CRITERION:12345",
#         "service_ids": [],
#         "compliance_reference_ids": []
#     }
#
#     result = to_provider_self_assessments(provider_self_assessments)
#     print(result.id)  # Output: "abc provider__criterion"
#     print(result.provider_name)  # Output: "ABC Provider"
#     print(result.section)  # Output: "Section 1"
#     print(result.criterion)  # Output: "CRITERION:12345"
#     print(result.services)  # Output: []
#     print(result.compliance_references)  # Output: []
#     ```
#
#     :param provider_self_assessments:
#     :return:
#     """
#     if provider_self_assessments is None:
#         raise ValueError("provider_compliance dictionary cannot be None.")
#
#     required_keys = [
#         "provider_name",
#         "section",
#         "criterion",
#         "service_ids",
#         "compliance_reference_ids",
#     ]
#
#     log.debug(f'Creating self assessments for provider : {provider_self_assessments["provider_name"]}')
#     __check_required_properties(provider_self_assessments, required_keys)
#
#     provider_name = provider_self_assessments["provider_name"]
#     criterion = provider_self_assessments["criterion"]
#
#     provider_self_assessments_id = get_self_assessments_id(provider_name, criterion)
#     return ProviderSelfAssessments(
#         id=provider_self_assessments_id,
#         provider_name=provider_name,
#         section=provider_self_assessments["section"],
#         criterion=criterion,
#         services=[],
#         compliance_references=[],
#     )


def to_compliance_criterion(compliance_criterion: dict) -> ComplianceCriterion:
    """
    The to_compliance_criterion function is used to convert a dictionary representing a compliance criterion into an
    instance of the ComplianceCriterion class. It handles converting the level and category values from their
    string representations to their corresponding enum values, if necessary.

    ### Example Usage
    ```python
    criterion = {
        "name": "Example Criterion",
        "level": "Level 1",
        "category": "Security",
        "description": "This is an example criterion",
        "self_assessed": True
    }
    entity = to_compliance_criterion(criterion)
    print(entity)
    ```

    :param compliance_criterion:
    :return:
    """
    if compliance_criterion is None:
        raise ValueError("compliance_criterion dictionary cannot be None.")

    required_keys = ["id", "name", "category", "description", "self_assessed"]
    __check_required_properties(compliance_criterion, required_keys)

    return ComplianceCriterion(
        criterion_id=compliance_criterion.get("id"),
        name=compliance_criterion.get("name"),
        category=compliance_criterion.get("category"),
        description=compliance_criterion.get("description"),
        levels=[],
        self_assessed=compliance_criterion.get("self_assessed"),
    )


def to_level_entity(level: str) -> Level:
    """
    The to_level_entity function is used to convert a string representing a gaia-x label level into an instance of the
    Level class.

    ### Example Usage
    ```python
    level = "Level 1"
    entity = to_level_entity(level)
    print(entity)
    ```

    :param level: a string representing a Gaia-X Label level
    :return: a Level instance
    """
    if not isinstance(level, str):
        raise TypeError("Input 'level' must be a string")

    if not level:
        raise ValueError("Invalid level input")

    level_id = level.removeprefix("Level ")
    return Level(level_id=level_id)


def to_compliance_reference_manager(participant: dict) -> ComplianceReferenceManager:
    """
    The to_compliance_reference_manager function is used to convert a dict representing a ComplianceReferenceManager
    into an instance of the ComplianceAssessmentsBody class.

    ### Example Usage
    ```python
    crm = {
            "name": "ABC-FEDERATION",
            "designation": "abc-federation",
            "registrationNumber": "1234567890",
            "legalAddress": {
                "country-name": "BE",
                "street-address": "Avenue des Arts 6-9",
                "locality": "Bruxelles",
                "postal-code": "1210",
            },
            "leiCode": "abc-federation",
            "termsAndConditions": "https://abc-federation.gaia-x.community/terms",
        },

    entity = to_compliance_reference_manager(crm)
    print(entity)
    ```

    :param participant: a dict representing a ComplianceReferenceManager
    :return: a ComplianceReferenceManager instance
    """
    if participant is None:
        raise ValueError(PARTICIPANT_MUST_BE_NOT_NONE_ERROR_MESSAGE)

    required_keys = [
        "name",
    ]

    __check_required_properties(participant, required_keys)

    return ComplianceReferenceManager(
        name=participant.get("name"),
        designation=participant.get("designation"),
        registration_number=participant.get("vat_id"),
        lei_code=participant.get("lei_code"),
        terms_and_conditions=participant.get("terms_and_conditions"),
        street=participant.get("street_address"),
        locality=participant.get("city"),
        postal_code=participant.get("postal_code"),
        country=participant.get("country"),
    )


def to_provider(participant: dict) -> Provider:
    """
    The to_provider function is used to convert a dict representing a provider into an instance of the
    Provider class.

    ### Example Usage
    ```python
    provider = {
        "name": "aruba",
        "designation": "ARUBA S.P.A.",
        "registrationNumber": "ITRI.04552920482",
        "legalAddress": {
            "country-name": "IT-BG",
            "street-address": "via San Clemente, 53",
            "locality": "Ponte San Pietro",
            "postal-code": "24036",
        },
        "leiCode": "81560069E326FF3BE121",
        "termsAndConditions": "https://hosting.aruba.it/en/terms-conditions.aspx",
    }
    entity = to_provider_entity(provider)
    print(entity)
    ```

    :param participant: a dict representing a provider
    :return: a Provider instance
    """
    if participant is None:
        raise ValueError("participant dictionary cannot be None.")

    required_keys = [
        "name",
    ]

    __check_required_properties(participant, required_keys)

    return Provider(
        name=participant.get("name"),
        designation=participant.get("designation"),
        registration_number=participant.get("vat_id"),
        lei_code=participant.get("lei_code"),
        terms_and_conditions=participant.get("terms_and_conditions"),
        street=participant.get("street_address"),
        locality=participant.get("city"),
        postal_code=participant.get("postal_code"),
        country=participant.get("country"),
    )


def to_cab(participant: dict) -> ComplianceAssessmentsBody:
    """
    The to_cab function is used to convert a dict representing a CAB into an instance of the
    ComplianceAssessmentsBody class.

    ### Example Usage
    ```python
    cab = {
            "cab_id": "CAB-00"
            "name": "ABC-FEDERATION",
            "designation": "abc-federation",
            "registrationNumber": "1234567890",
            "legalAddress": {
                "country-name": "BE",
                "street-address": "Avenue des Arts 6-9",
                "locality": "Bruxelles",
                "postal-code": "1210",
            },
            "leiCode": "abc-federation",
            "termsAndConditions": "https://abc-federation.gaia-x.community/terms",
        },

    entity = to_cab_entity(cab)
    print(entity)
    ```

    :param participant: a dict representing a CAB
    :return: a ComplianceAssessmentsBody instance
    """
    if participant is None:
        raise ValueError("participant dictionary cannot be None.")

    required_keys = ["name", "id"]

    __check_required_properties(participant, required_keys)

    return ComplianceAssessmentsBody(
        cab_id=participant.get("id"),
        name=participant.get("name"),
        designation=participant.get("designation"),
        registration_number=participant.get("vat_id"),
        lei_code=participant.get("lei_code"),
        terms_and_conditions=participant.get("terms_and_conditions"),
        street=participant.get("street_address"),
        locality=participant.get("city"),
        postal_code=participant.get("postal_code"),
        country=participant.get("country"),
    )


def to_compliance_certification_scheme(
    compliance_certification_schemes: dict,
) -> list[ComplianceCertificationScheme]:
    """
    This code defines a function named to_compliance_certification_scheme that takes a dictionary of compliance
    certification schemes as input and returns a list of ComplianceCertificationScheme objects.

    ### Example Usage
    ```python
    compliance_certification_schemes = {
        "cr_id1": {...},
        "cr_id2": {...},
        ...
    }
    entities = to_compliance_certification_scheme(compliance_certification_schemes)
    print(entities)
    ```
    :param compliance_certification_schemes: A dictionary containing compliance certification schemes. Each key
    represents a compliance reference ID and the corresponding value is a dictionary representing the details of the
    scheme.
    :return: A list of ComplianceCertificationScheme objects, where each object represents a compliance
    certification scheme with the corresponding compliance reference ID.
    """
    if compliance_certification_schemes is None:
        raise ValueError("compliance_certification_schemes dictionary cannot be None.")

    entities: list[ComplianceCertificationScheme] = [
        ComplianceCertificationScheme(compliance_reference_id=cr_id) for cr_id in compliance_certification_schemes
    ]
    return entities


def to_third_party_compliance_certification_scheme(
    compliance_certification_schemes: dict,
) -> list[ThirdPartyComplianceCertificationScheme]:
    """
    This code defines a function named to_compliance_certification_scheme that takes a dictionary of third party
    compliance certification schemes as input and returns a list of ComplianceCertificationScheme objects.

    ### Example Usage
    ```python
    compliance_certification_schemes = {
        "cr_id1": {...},
        "cr_id2": {...},
        ...
    }
    entities = to_third_party_compliance_certification_scheme(compliance_certification_schemes)
    print(entities)
    ```
    :param compliance_certification_schemes: A dictionary containing compliance certification schemes. Each key
    represents a compliance reference ID and the corresponding value is a dictionary representing the details of the
    scheme.
    :return: A list of ThirdPartyComplianceCertificationScheme objects, where each object represents a third party
    compliance certification scheme with the corresponding compliance reference ID.
    """
    if compliance_certification_schemes is None:
        raise ValueError("compliance_certification_schemes dictionary cannot be None.")

    entities: list[ThirdPartyComplianceCertificationScheme] = [
        ThirdPartyComplianceCertificationScheme(compliance_reference_id=cr_id)
        for cr_id in compliance_certification_schemes
    ]
    return entities


def to_third_party_certification_compliance_scheme_cab_provider_link(
    provider_compliances_schemes: list[dict],
) -> list[ThirdPartyComplianceCertificationSchemeCABProviderLink]:
    """
    The to_third_party_certification_compliance_scheme_cab_provider_link function takes a list of dictionaries
    representing provider compliance schemes as input. It creates instances of the
    ThirdPartyComplianceCertificationSchemeCABProviderLink class for each dictionary in the input list, filtering
    out the ones with an "assertion_method" value of "Certified". The function returns a list of these instances.

    ### Example Usage
    ```python
    provider_compliances_schemes = [
        {
            "cab_id": "CAB-01",
            "provider_name": "Provider A",
            "compliance_reference_id": "Ref-01",
            "assertion_method": "Certified"
        },
        {
            "cab_id": "CAB-02",
            "provider_name": "Provider B",
            "compliance_reference_id": "Ref-02",
            "assertion_method": "Not Certified"
        },
        {
            "cab_id": "CAB-03",
            "provider_name": "Provider C",
            "compliance_reference_id": "Ref-03",
            "assertion_method": "Certified"
        }
    ]

    result = to_third_party_certification_compliance_scheme_cab_provider_link(provider_compliances_schemes)
    print(result)
    Output:
    [
        ThirdPartyComplianceCertificationSchemeCABProviderLink(
            cab_id="CAB-01",
            provider_name="Provider A",
            compliance_reference_id="Ref-01"
        ),
        ThirdPartyComplianceCertificationSchemeCABProviderLink(
            cab_id="CAB-03",
            provider_name="Provider C",
            compliance_reference_id="Ref-03"
        )
    ]
    ```
    :param provider_compliances_schemes: A list of dictionaries representing provider compliance schemes. Each
    dictionary should have the following keys: "cab_id", "provider_name", "compliance_reference_id", and
    "assertion_method".
    :return: A list of instances of the ThirdPartyComplianceCertificationSchemeCABProviderLink class, representing the
    filtered provider compliance schemes.
    """
    if provider_compliances_schemes is None:
        raise ValueError("provider_compliances_schemes list cannot be None.")

    return [
        ThirdPartyComplianceCertificationSchemeCABProviderLink(
            cab_id=link["cab_id"] if link["cab_id"] is not None and link["cab_id"] != "" else "CAB-00",
            provider_name=link["provider_name"],
            compliance_reference_id=link["compliance_reference_id"],
        )
        for link in provider_compliances_schemes
        if "cab_id" in link
        and "assertion_method" in link
        and "provider_name" in link
        and link["assertion_method"] == "Certified"
    ]


#
def to_compliance_certification_scheme_provider_link(
    provider_compliances_schemes: list[dict],
) -> list[ComplianceCertificationSchemeProviderLink]:
    """
    The to_compliance_certification_scheme_provider_link function takes a list of dictionaries representing provider
    compliance schemes as input. It creates instances of the ComplianceCertificationSchemeProviderLink class for each
    dictionary in the input list, filtering out certain links based on specific conditions. The function returns a
    list of these instances.

    ### Example Usage
    ```python
    provider_compliances_schemes = [
        {
            "provider_name": "Provider A",
            "compliance_reference_id": "123",
            "cab_id": "456",
            "assertion_method": "Certified"
        },
        {
            "provider_name": "Provider B",
            "compliance_reference_id": "789",
            "cab_id": "101112",
            "assertion_method": "Not Certified"
        }
    ]

    result = to_compliance_certification_scheme_provider_link(provider_compliances_schemes)
    print(result)
    ```
    :param provider_compliances_schemes: A list of dictionaries representing provider compliance schemes. Each
    dictionary should have the keys provider_name, compliance_reference_id, cab_id, and assertion_method.
    :return: A list of instances of the ComplianceCertificationSchemeProviderLink class, filtered based on the
    conditions specified in the function.
    """
    if provider_compliances_schemes is None:
        raise ValueError("provider_compliances_schemes list cannot be None.")

    return [
        ComplianceCertificationSchemeProviderLink(
            provider_name=link["provider_name"], compliance_reference_id=link["compliance_reference_id"]
        )
        for link in provider_compliances_schemes
        if "cab_id" in link
        and "assertion_method" in link
        and "provider_name" in link
        and link["assertion_method"] != "Certified"
    ]
