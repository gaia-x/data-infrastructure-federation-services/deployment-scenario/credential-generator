# -*- coding: utf-8 -*-
"""
It defines classes to interact with sqlite database.
"""
#pylint: disable=too-many-lines
import logging
from typing import Optional

from sqlalchemy import create_engine, select
from sqlalchemy.orm import selectinload, sessionmaker

from credential_generator.infra.spi.database.sqlite.entities import (
    Base,
    ComplianceAssessmentsBody,
    ComplianceCertificationScheme,
    ComplianceCertificationSchemeProviderLink,
    ComplianceCriterion,
    ComplianceCriterionComplianceCertificationSchemeLink,
    ComplianceReference,
    ComplianceReferenceManager,
    Keyword,
    Layer,
    Level,
    Location,
    Provider,
    Service,
    ThirdPartyComplianceCertificationScheme,
    ThirdPartyComplianceCertificationSchemeCABProviderLink,
    ThirdPartyComplianceCriterionComplianceCertificationSchemeLink,
)
from credential_generator.infra.spi.database.sqlite.entities.factory import (
    to_cab,
    to_compliance_certification_scheme,
    to_compliance_certification_scheme_provider_link,
    to_compliance_criterion,
    to_compliance_reference_entity,
    to_compliance_reference_manager,
    to_keyword_entity,
    to_layer_entity,
    to_level_entity,
    to_location_entity,
    to_provider,
    to_service_entity,
    to_third_party_certification_compliance_scheme_cab_provider_link,
    to_third_party_compliance_certification_scheme,
)

log = logging.getLogger(__name__)


class SQLiteRepository:
    """
    The `SQLiteRepository` class is used to interact with a SQLite compliance database. It provides methods to create
    the database, add entities to the database, retrieve entities from the database, and clean the database by deleting
    all data.
    """

    def __init__(self, database_url: Optional[str]):
        # pylint: disable=unused-private-member
        self.__engine = create_engine(database_url, echo=False)

    def create_database(self):
        """
        The `create_database` method in the `SQLiteRepository` class is used to create tables in a SQLite database based
        on the entities defined in the `.entities` package.
        """
        log.debug("creating database")
        Base.metadata.drop_all(self.__engine, checkfirst=True)
        Base.metadata.create_all(self.__engine, checkfirst=True)

    def add_compliance_reference_manager(self, participant: dict) -> None:
        """
        The `add_compliance_reference_manager` method in the `SQLiteRepository` class is used to add a
        ComplianceReferenceManager in database.
        :param participant: a dict representing a compliance reference manager
        :return None
        """
        if not isinstance(participant, dict):
            raise TypeError("participant must be a dict")

        session_class = sessionmaker(self.__engine)
        with session_class() as session:
            entity: ComplianceReferenceManager = to_compliance_reference_manager(participant)
            session.add(entity)
            session.commit()

    def add_provider(self, participant: dict) -> None:
        """
        The `add_provider` method in the `SQLiteRepository` class is used to add a
        Provider in database.
        :param participant: a dict representing a provider
        :return None
        """
        if not isinstance(participant, dict):
            raise TypeError("participant must be a dict")

        session_class = sessionmaker(self.__engine)
        with session_class() as session:
            entity: Provider = to_provider(participant)
            session.add(entity)
            session.commit()

    def add_cab(self, participant: dict) -> None:
        """
        The `add_cab` method in the `SQLiteRepository` class is used to add a
        ComplianceAssessmentsBody in database.
        :param participant: a dict representing a cab
        :return None
        """
        if not isinstance(participant, dict):
            raise TypeError("participant must be a dict")

        session_class = sessionmaker(self.__engine)
        with session_class() as session:
            entity: ComplianceAssessmentsBody = to_cab(participant)
            session.add(entity)
            session.commit()

    def add_compliance_references(self, compliance_references: list[dict]) -> None:
        """
        The `add_compliance_references` method in the `SQLiteRepository` class is used to add a list of
        ComplianceReference in database.
        :param compliance_references: a list of dict representing a list of compliance references
        :return None
        """
        if not isinstance(compliance_references, list):
            raise TypeError("compliance_references must be a list")

        if any(not isinstance(reference, dict) for reference in compliance_references):
            raise TypeError("reference must be a dict")

        session_class = sessionmaker(self.__engine)
        with session_class() as session:
            for compliance_reference in compliance_references:
                entity: ComplianceReference = to_compliance_reference_entity(compliance_reference)
                session.add(entity)

            session.commit()

    def add_levels(self, levels: list[str]) -> None:
        """
        The `add_levels` method in the `SQLiteRepository` class is used to add a list of
        Level in database.
        :param levels: a list of str representing a list of levels
        :return None
        """
        if not isinstance(levels, list):
            raise TypeError("levels must be a list")

        if any(not isinstance(level, str) for level in levels):
            raise TypeError("level must be a dict")

        session_class = sessionmaker(self.__engine)
        with session_class() as session:
            for level in levels:
                entity: Level = to_level_entity(level)
                session.add(entity)
            session.commit()

    def add_compliance_criterions(self, compliance_criterions: list[dict]) -> None:
        """
        The `add_compliance_criterions` method in the `SQLiteRepository` class is used to add a list of
        ComplianceCriterion in database.
        :param compliance_criterions: a list of dict representing a list of compliance criterions
        :return None
        """
        if not isinstance(compliance_criterions, list):
            raise TypeError("compliance_criterions must be a list")

        if any(not isinstance(criterion, dict) for criterion in compliance_criterions):
            raise TypeError("criterions must be a dict")

        session_class = sessionmaker(bind=self.__engine, autoflush=False)
        with session_class() as session:
            for compliance_criterion in compliance_criterions:
                entity: ComplianceCriterion = to_compliance_criterion(compliance_criterion)

                for level in compliance_criterion["levels"]:
                    level_id = level.removeprefix("Level ")

                    level_entity = session.query(Level).get(level_id)
                    if level_entity is not None:
                        entity.levels.append(level_entity)

                session.add(entity)
                session.commit()

    def add_compliance_certification_scheme(self, compliance_certification_schemes: dict) -> None:
        """
        The add_compliance_certification_scheme method in the ComplianceSQLiteRepository class is used to add
        compliance certification schemes to the SQLite database. It takes a dictionary of compliance certification
        schemes as input and creates corresponding entities in the database.
        :param compliance_certification_schemes: (dict): A dictionary containing compliance reference IDs as keys and
        lists of criterion IDs as values.
        :return: None. The method adds the compliance certification scheme entities to the SQLite database.
        """
        if not isinstance(compliance_certification_schemes, dict):
            raise TypeError("compliance_certification_schemes must be a dict")

        session_class = sessionmaker(bind=self.__engine, autoflush=False)
        with session_class() as session:
            entities: list[ComplianceCertificationScheme] = to_compliance_certification_scheme(
                compliance_certification_schemes=compliance_certification_schemes
            )

            for entity in entities:
                criterions = compliance_certification_schemes.get(entity.compliance_reference_id)

                for criterion_key in criterions:
                    criterion_key_parts = criterion_key.split("-")
                    criterion_id = criterion_key_parts[0]
                    level_id = criterion_key_parts[1].removeprefix("Level ")

                    criterion_entity = ComplianceCriterionComplianceCertificationSchemeLink(
                        compliance_criterion_id=criterion_id,
                        compliance_criterion_level_id=level_id,
                        compliance_reference_id=entity.compliance_reference_id,
                    )
                    session.add(criterion_entity)

                session.add(entity)
                session.commit()

    def add_third_party_compliance_certification_scheme(self, compliance_certification_schemes: dict) -> None:
        """
        The add_third_partycompliance_certification_scheme method in the ComplianceSQLiteRepository class is used to
        add third-party compliance certification schemes to the SQLite database. It takes a dictionary of compliance
        certification schemes as input and creates corresponding entities in the database.
        :param compliance_certification_schemes: (dict): A dictionary containing compliance reference IDs as keys and
        lists of criterion IDs as values.
        :return: None. The method adds the compliance certification scheme entities to the SQLite database.
        """
        if not isinstance(compliance_certification_schemes, dict):
            raise TypeError("compliance_certification_schemes must be a dict")

        session_class = sessionmaker(bind=self.__engine, autoflush=False)
        with session_class() as session:
            entities: list[ThirdPartyComplianceCertificationScheme] = to_third_party_compliance_certification_scheme(
                compliance_certification_schemes=compliance_certification_schemes
            )

            for entity in entities:
                criterions = compliance_certification_schemes.get(entity.compliance_reference_id)

                for criterion_key in criterions:
                    criterion_key_parts = criterion_key.split("-")
                    criterion_id = criterion_key_parts[0]
                    level_id = criterion_key_parts[1].removeprefix("Level ")

                    criterion_entity = ThirdPartyComplianceCriterionComplianceCertificationSchemeLink(
                        compliance_criterion_id=criterion_id,
                        compliance_criterion_level_id=level_id,
                        compliance_reference_id=entity.compliance_reference_id,
                    )
                    session.add(criterion_entity)

                session.add(entity)
                session.commit()

    def add_keywords(self, keywords: list[str]) -> None:
        """
        The `add_keywords` method in the `SQLiteRepository` class is used to add a list of keyword names to a SQLite
        database. It creates a `Keyword` object for each keyword name.

        :param keywords: list of keywords
        """
        if not isinstance(keywords, list):
            raise TypeError("keywords must be a list")

        if any(not isinstance(keyword, str) for keyword in keywords):
            raise TypeError("keywords must be a str")

        session_class = sessionmaker(bind=self.__engine)
        with session_class() as session:
            entities = [to_keyword_entity(keyword) for keyword in keywords if keyword is not None and keyword != ""]
            session.add_all(entities)
            session.commit()

    def add_layers(self, layers: list[str]) -> None:
        """
        The `add_layer` method in the `SQLiteRepository` class is used to add a list of layer names to a SQLite
        database. It creates a `LayerEntity` object for each layer name using the `to_layer_entity` function, adds all
        the entities to a session, and commits the changes to the database.
        ```
        :param layers: list of layer instances.
        """
        if not isinstance(layers, list):
            raise TypeError("layers must be a list")

        if any(not isinstance(layer, str) for layer in layers):
            raise TypeError("layer must be a dict")

        session_class = sessionmaker(bind=self.__engine)
        with session_class() as session:
            entities = [to_layer_entity(layer) for layer in layers if layer is not None and layer != ""]
            session.add_all(entities)
            session.commit()

    def add_locations(self, locations: list[dict]) -> None:
        """
        The `add_locations` method in the `SQLiteRepository` class is used to add a list of location dictionaries to a
        SQLite database. It iterates over the list of locations and calls the `add_location` method to add each
        location to the database.
        ### Example Usage
        ```python
        repository = SQLiteRepository("database.db")
        locations = [
            {"id": "1", "name": "Location 1"},
            {"id": "2", "name": "Location 2"},
            {"id": "3", "name": "Location 3"}
        ]
        repository.add_locations(locations)
        ```
        :param locations: list of locations instances.
        """
        if not isinstance(locations, list):
            raise TypeError("locations must be a list")

        if any(not isinstance(location, dict) for location in locations):
            raise TypeError("location must be a dict")

        session_class = sessionmaker(bind=self.__engine)
        with session_class() as session:
            entities = [to_location_entity(location) for location in locations]
            session.add_all(entities)
            session.commit()

    def add_provider_compliances_schemes(self, provider_compliances_schemes: list[dict]) -> None:
        """
        The add_provider_compliances_schemes method in the ComplianceSQLiteRepository class is used to
        add compliance certification schemes to the SQLite database. It takes a dictionary of compliance
        certification schemes as input and creates corresponding entities in the database.

        :param provider_compliances_schemes:
        :param compliance_certification_schemes: (dict): A dictionary containing compliance reference IDs as keys and
        lists of criterion IDs as values.
        :return: None. The method adds the compliance certification scheme entities to the SQLite database.
        """
        if not isinstance(provider_compliances_schemes, list):
            raise TypeError("provider_compliances_schemes must be a list")

        if any(not isinstance(scheme, dict) for scheme in provider_compliances_schemes):
            raise TypeError("scheme must be a dict")

        session_class = sessionmaker(bind=self.__engine, autoflush=True)
        with session_class() as session:
            third_party_schemes: list[
                ThirdPartyComplianceCertificationSchemeCABProviderLink
            ] = to_third_party_certification_compliance_scheme_cab_provider_link(
                provider_compliances_schemes=provider_compliances_schemes
            )
            session.add_all(third_party_schemes)
            session.commit()

            schemes: list[ComplianceCertificationSchemeProviderLink] = to_compliance_certification_scheme_provider_link(
                provider_compliances_schemes=provider_compliances_schemes
            )
            for entity in schemes:
                session.add(entity)
                session.commit()

    def add_services(self, services: list[dict]):
        """
        The `add_services` method in the `SQLiteRepository` class adds a new service to the database based on the
        properties found in the given dictionary.
        :param services: a list of dict with service properties
        """
        if not isinstance(services, list):
            raise TypeError("services must be a list")

        if any(not isinstance(service, dict) for service in services):
            raise TypeError("service must be a dict")

        session_class = sessionmaker(bind=self.__engine, autoflush=True)
        with session_class() as session:
            for service in services:
                provider_name = service["provider_name"]
                service_entity: Service = to_service_entity(service)
                session.add(service_entity)

                log.debug(f"Linking Layers to ({service_entity.id})")
                for layer_id in service.get("layers", []):
                    layer = session.query(Layer).filter_by(id=layer_id).first()
                    if layer is not None:
                        service_entity.layers.append(layer)

                log.debug(f"Linking Keywords to ({service_entity.id})")
                for keyword_id in service.get("keywords", []):
                    keyword = session.query(Keyword).filter_by(id=keyword_id).first()
                    if keyword is not None:
                        service_entity.keywords.append(keyword)

                log.debug(f"Linking locations to ({service_entity.id})")
                for location_external_id in service.get("location_ids", []):
                    if location_external_id != "":
                        location = (
                            session.query(Location)
                            .filter_by(provider_name=provider_name, location_external_id=location_external_id)
                            .first()
                        )

                        if location is not None:
                            # log.debug(f"Linking location {location.id} to ({service_entity.id})")
                            service_entity.locations.append(location)

                for compliance_reference_id in service.get("compliance_reference_ids", set()):
                    if compliance_reference_id != "":
                        compliance_reference = (
                            session.query(ComplianceReference)
                            .filter_by(compliance_reference_id=compliance_reference_id)
                            .first()
                        )
                        if compliance_reference is not None:
                            service_entity.compliance_references.append(compliance_reference)

                for criterion_id in service.get("self_assessments", set()):
                    if criterion_id != "":
                        criterion = session.query(ComplianceCriterion).filter_by(criterion_id=criterion_id).first()
                        if criterion is not None:
                            service_entity.self_assessed_criterions.append(criterion)

                session.commit()

    def get_level(self, level_id: str) -> Optional[Level]:
        """
        The `get_level` method in the `SQLiteRepository` class is used to get a Level from its id.
        :param level_id: a string representing level ID.
        :return a Level entity if found, None otherwise
        """
        session_class = sessionmaker(self.__engine)
        with session_class() as session:
            results = session.query(Level).where(Level.level_id == level_id)

            return results.one_or_none()

    def get_compliance_assessments_body_from_name(self, name: str) -> Optional[ComplianceAssessmentsBody]:
        """
        The `get_compliance_assessments_body_from_name` method retrieves a compliance assessments body entity from the
        SQLite database based on the provided CAB name.

        :param name: The name of the compliance assessments body to retrieve.
        :return:Optional[ComplianceAssessmentsBody]: The retrieved compliance assessments body entity from the
        database, or None if no entity is found.
        """
        session_class = sessionmaker(self.__engine)
        with session_class() as session:
            results = session.query(ComplianceAssessmentsBody).where(ComplianceAssessmentsBody.name == name)

            return results.one_or_none()

    def get_compliance_reference_manager_names(self) -> list[str]:
        """
        The get_compliance_reference_manager_names method retrieves the names of all compliance reference managers from
        the SQLite database.
        ## Example Usage
        ```
        repository = ComplianceSQLiteRepository("database.db")
        names = repository.get_compliance_reference_manager_names()
        print(names)
        ```
        :return:A list of strings representing the names of all compliance reference managers in the database.
        """
        session_class = sessionmaker(self.__engine)
        with session_class() as session:
            results = session.query(ComplianceReferenceManager.name)
            return [result.name for result in results.all()]

    def get_compliance_reference_manager_from_name(self, name: str) -> Optional[ComplianceReferenceManager]:
        """
        The get_compliance_reference_manager_from_name method is used to retrieve a ComplianceReferenceManagerEntity
        object from the SQLite database based on the provided name.


        :param name: The name of the compliance reference manager to retrieve.
        :return: reference_manager (Optional[ComplianceReferenceManagerEntity]): The retrieved
        ComplianceReferenceManagerEntity object or None if no object is found.
        """
        session_class = sessionmaker(self.__engine)
        with session_class() as session:
            results = session.query(ComplianceReferenceManager).where(ComplianceReferenceManager.name == name)
            return results.one_or_none()

    def get_provider_from_name(self, name: str) -> Optional[Provider]:
        """
        The get_provider_from_name method is used to retrieve a ProviderEntity
        object from the SQLite database based on the provided name.

        :param name: The name of the provider to retrieve.
        :return: provider (Optional[ProviderEntity]): The retrieved
        ProviderEntity object or None if no object is found.
        """
        session_class = sessionmaker(self.__engine)
        with session_class() as session:
            results = session.query(Provider).where(Provider.name == name)
            return results.one_or_none()

    def get_compliance_references_from_manager_name(self, manager_name: str) -> list[ComplianceReference]:
        """
        The get_compliance_references_from_manager_name method retrieves a list of ComplianceReferenceEntity objects
        from the SQLite database based on the provided manager_name.

        :param manager_name: A string representing the name of the compliance reference manager.
        :return: compliance_references: A list of ComplianceReferenceEntity objects that match the provided
        manager_name.
        """
        session_class = sessionmaker(self.__engine)
        with session_class() as session:
            results = session.query(ComplianceReference).where(
                ComplianceReference.compliance_reference_manager_name == manager_name
            )
            return results.all()

    def get_compliance_certification_scheme_from_id(
        self, compliance_reference_id
    ) -> Optional[ComplianceCertificationScheme]:
        """
        The get_compliance_certification_scheme_from_id method retrieves a ComplianceCertificationSchemeEntity from the
        SQLite database based on the provided compliance_reference_id.


        :param compliance_reference_id: A string representing the ID of the compliance certification scheme to retrieve.
        :return: scheme: An optional ComplianceCertificationSchemeEntity object representing the compliance
        certification scheme with the provided ID.
        """
        session_class = sessionmaker(self.__engine)
        with session_class() as session:
            results = session.query(ComplianceCertificationScheme).where(
                ComplianceCertificationScheme.compliance_reference_id == compliance_reference_id
            )

            return results.one_or_none()

    def get_third_party_compliance_certification_scheme_from_id(
        self, compliance_reference_id
    ) -> Optional[ThirdPartyComplianceCertificationScheme]:
        """
        The get_third_party_compliance_certification_scheme_from_id method retrieves a
        ThirdPartyComplianceCertificationSchemeEntity from the SQLite database based on the provided
        compliance_reference_id.

        ### Example Usage
        ```python
        repository = ComplianceSQLiteRepository("database.db")
        compliance_reference_id = "123"
        scheme = repository.get_third_party_compliance_certification_scheme_from_id(compliance_reference_id)
        print(scheme)
        ```

        :param compliance_reference_id: A string representing the ID of the compliance reference.
        :return: scheme: An optional ThirdPartyComplianceCertificationSchemeEntity object representing the compliance
        certification scheme with the provided compliance_reference_id.
        """
        session_class = sessionmaker(self.__engine)
        with session_class() as session:
            results = session.query(ThirdPartyComplianceCertificationScheme).where(
                ThirdPartyComplianceCertificationScheme.compliance_reference_id == compliance_reference_id
            )

            return results.one_or_none()

    def get_services_of_provider(self, provider_name: str, fetch_keywords: bool = True) -> list[Service]:
        """
        The `get_services_of_provider` method retrieves a list of `ServiceEntity` objects from a SQLite database based
        on the `provider_name`. It uses the `Session` object to execute a select query and returns all the matching
        service entities.
        :param provider_name: name of provider
        :param fetch_keywords: if True, fetch associated keywords
        :return:
        """
        session_class = sessionmaker(self.__engine)
        with session_class() as session:
            if not fetch_keywords:
                return session.query(Service).where(Service.provider_name == provider_name).all()

            return (
                session.query(Service)
                .options(selectinload(Service.keywords))
                .where(Service.provider_name == provider_name)
                .all()
            )

    def get_layer_of_service(self, service_id: str) -> list[str]:
        """
        The get_layer_of_service method retrieves the layer IDs associated with a service from a SQLite database.
        :param  service_id (str): The ID of the service.
        :return: List of layer IDs associated with the service.
        """
        session_class = sessionmaker(self.__engine)
        with session_class() as session:
            results = session.query(Layer.id).join(Layer.services).where(Service.id == service_id).distinct()

            return [result.id for result in results.all()]

    def get_location_from_provider(self, provider_name: str, fetch_services: bool = False) -> Optional[list[Location]]:
        """
        The get_location_from_provider method in the SQLiteRepository class retrieves a LocationEntity object from a
        SQLite database based on the provider_name. If no instance is found in the database, it returns None.

        :param location_id: a location ID
        :param fetch_services: if True, fetch services entities linked to each location. Default is False
        :return LocationEntity if found, None otherwise
        """
        session_class = sessionmaker(self.__engine)
        with session_class() as session:
            if fetch_services:
                results = (
                    session.query(Location)
                    .options(selectinload(Location.services))
                    .where(Location.provider_name == provider_name)
                )
                return results.all()

            return session.query(Location).where(Location.provider_name == provider_name).all()

    def get_location(self, location_id) -> Optional[Location]:
        """
        The `get_location` method in the `SQLiteRepository` class retrieves a `LocationEntity` object from a
        SQLite database based on its `location_id`. If no instance is found in the database, it returns `None`.

        :param location_id: a location ID
        :return LocationEntity if found, None otherwise
        """
        session_class = sessionmaker(self.__engine)
        with session_class() as session:
            results = session.query(Location).where(Location.id == location_id)
            return results.one_or_none()

    def get_compliance_certificate_claim_for_provider(self, provider_name: str) -> list[(str, str, str)]:
        """
        The get_compliance_certificate_claim_for_provider method retrieves a list of compliance certificate claims for
        a given provider name from a SQLite database. It returns a list of tuples containing the service external ID,
        compliance certification scheme ID, and location external ID for each claim.
        :param provider_name (str): The name of the provider for which to retrieve the compliance certificate claims.
        :return:claims (list[(str, str, str)]): A list of tuples containing the service external ID, compliance
         certification scheme ID, and location external ID for each compliance certificate claim.
        """
        session_class = sessionmaker(self.__engine)
        with session_class() as session:
            statement = (
                select(
                    Service.service_external_id,
                    ComplianceReference.compliance_reference_id,
                    Location.location_external_id,
                )
                .join(Location, Service.locations)
                .join(ComplianceReference, Service.compliance_references)
                .join(
                    ComplianceCertificationSchemeProviderLink,
                    ComplianceCertificationSchemeProviderLink.provider_name == Service.provider_name,
                )
                .where(
                    ComplianceReference.compliance_reference_id
                    == ComplianceCertificationSchemeProviderLink.compliance_reference_id
                )
                .where(Service.provider_name == provider_name)
                .order_by(
                    Service.service_external_id,
                    ComplianceReference.compliance_reference_id,
                    Location.location_external_id,
                )
                .distinct()
            )

            return session.execute(statement).all()

    def get_third_party_compliance_certificate_claim_for_provider(
        self, provider_name: str
    ) -> list[(str, str, str, str)]:
        """
         The get_third_party_compliance_certificate_claim_for_provider method retrieves a list of tuples containing
         information about third-party compliance certificate claims for a given provider. It queries the SQLite
         database to fetch the service external ID, location external ID, third-party compliance certification scheme ID
         , and compliance assessments body name for each claim associated with the provider.

        ```
         :param provider_name (str): The name of the provider for which to retrieve the compliance certificate claims.
         :return: claims (list[(str, str, str, str)]): A list of tuples, where each tuple represents a compliance
         certificate claim and contains the service external ID, location external ID, third-party compliance
         certification scheme ID, and compliance assessments body name.
        """
        session_class = sessionmaker(self.__engine)
        with session_class() as session:
            statement = (
                select(
                    Service.service_external_id,
                    Location.location_external_id,
                    ComplianceReference.compliance_reference_id,
                    ComplianceAssessmentsBody.name,
                )
                .join(Location, Service.locations)
                .join(ComplianceReference, Service.compliance_references)
                .join(
                    ThirdPartyComplianceCertificationSchemeCABProviderLink,
                    ComplianceReference.compliance_reference_id
                    == ThirdPartyComplianceCertificationSchemeCABProviderLink.compliance_reference_id,
                )
                .join(
                    ComplianceAssessmentsBody,
                    ComplianceAssessmentsBody.cab_id == ThirdPartyComplianceCertificationSchemeCABProviderLink.cab_id,
                )
                .where(
                    Service.provider_name == provider_name,
                    Location.provider_name == Service.provider_name,
                    ThirdPartyComplianceCertificationSchemeCABProviderLink.provider_name == Service.provider_name,
                )
            )

            return session.execute(statement).all()

    def get_self_assessed_compliance_criteria_for_service(self, service_id: str) -> list[tuple[str, str]]:
        """
        The get_self_assessed_compliance_criteria_for_service method retrieves the names of self-assessed compliance
        criteria for a given service from a SQLite database.

        :param service_id: The ID of the service for which to retrieve self-assessed compliance criteria.
        :return: criteria (list[str]): A list of strings representing the names of self-assessed compliance criteria
        for the specified service.
        """
        session_class = sessionmaker(self.__engine)
        with session_class() as session:
            statement = (
                select(ComplianceCriterion.name, Level.level_id)
                .join(ComplianceCriterion, Service.self_assessed_criterions)
                .join(Level, ComplianceCriterion.levels)
                .where(Service.id == service_id)
                .group_by(
                    ComplianceCriterion.name,
                    Level.level_id,
                )
                .order_by(
                    ComplianceCriterion.name,
                    Level.level_id,
                )
            )

            results = session.execute(statement)
            # pylint: disable = unnecessary-comprehension
            return [result for result in results.all()]

    def get_service_location_from_service_id(self, service_id: str) -> list[tuple[str, str]]:
        """
        The get_service_location_from_service_id method retrieves the service external ID and location external ID for
        a given service ID from the SQLite database.

        ### Example Usage
        ```
        repository = ServiceSQLiteRepository("database.db")
        service_id = "123"
        results = repository.get_service_location_from_service_id(service_id)
        for result in results:
            print(result)
        Output:
        ("123", "location1")
        ("123", "location2")
        ```

        :param service (str): The ID of the service for which to retrieve the location information.
        :return: results (list[tuple[str, str]]): A list of tuples containing the service external ID and location
        external ID for the given service ID.
        """
        session_class = sessionmaker(self.__engine)
        with session_class() as session:
            statement = (
                select(Service.service_external_id, Location.location_external_id)
                .join(Location, Service.locations)
                .where(Service.id == service_id)
            )

            results = session.execute(statement)
            # pylint: disable = unnecessary-comprehension
            return [result for result in results.all()]

    def get_service_location_of_provider(self, provider_name: str) -> list[tuple[str, str]]:
        """
        The get_service_location_of_provider method retrieves a list of tuples containing the service external ID and
        location external ID for a given provider name.
        ## Example Usage
        ```
        repository = ServiceSQLiteRepository("database.db")
        provider_name = "ABC"
        results = repository.get_service_location_of_provider(provider_name)
        for result in results:
            print(result)
        ```
        :param provider_name:
        :return:
        """
        session_class = sessionmaker(self.__engine)
        with session_class() as session:
            statement = (
                select(Service.service_external_id, Location.location_external_id)
                .join(Location, Service.locations)
                .where(Service.provider_name == provider_name, Location.provider_name == provider_name)
            )

            results = session.execute(statement)
            # pylint: disable = unnecessary-comprehension
            return [result for result in results.all()]

    def get_compliance_certificate_claim_for_service(self, service_id: str) -> list[(str, str, str)]:
        """
        The get_compliance_certificate_claim_for_service method retrieves the compliance certification scheme IDs
        associated with a given service ID from a SQLite database.
        ```
        :param service_id (str): The ID of the service for which to retrieve the compliance certificate claims.
        :return:claims (list[(str,)]): A list of tuples, where each tuple contains a single element representing a
        compliance certification scheme ID associated with the given service ID.
        """
        session_class = sessionmaker(self.__engine)
        with session_class() as session:
            statement = (
                select(ComplianceReference.compliance_reference_id)
                .join(ComplianceReference, Service.compliance_references)
                .join(
                    ComplianceCertificationSchemeProviderLink,
                    ComplianceCertificationSchemeProviderLink.compliance_reference_id
                    == ComplianceReference.compliance_reference_id,
                )
                .where(
                    Service.id == service_id,
                    Service.provider_name == ComplianceCertificationSchemeProviderLink.provider_name,
                )
                .distinct()
            )

            results = session.execute(statement)
            values = [result.compliance_reference_id for result in results.all()]
            return values

    def get_third_party_compliance_certificate_claim_for_service(self, service_id: str) -> list[str]:
        """
        The get_third_party_compliance_certificate_claim_for_service method retrieves the third-party compliance
        certificate claims for a given service from the SQLite database.
        ## Example Usage
        ```
        repository = ComplianceSQLiteRepository("database.db")
        service_id = "123"
        claims = repository.get_third_party_compliance_certificate_claim_for_service(service_id)
        ``
        :param `service_id (str): The ID of the service for which to retrieve the third-party compliance certificate
        claims.
        :return: claims (list[(str, str, str)]): A list of tuples representing the third-party compliance certificate
        claims for the service. Each tuple contains three strings representing the claims.
        """
        session_class = sessionmaker(self.__engine)
        with session_class() as session:
            statement = (
                select(ComplianceReference.compliance_reference_id)
                .join(ComplianceReference, Service.compliance_references)
                .join(
                    ThirdPartyComplianceCertificationSchemeCABProviderLink,
                    ThirdPartyComplianceCertificationSchemeCABProviderLink.compliance_reference_id
                    == ComplianceReference.compliance_reference_id,
                )
                .where(
                    Service.id == service_id,
                    Service.provider_name == ThirdPartyComplianceCertificationSchemeCABProviderLink.provider_name,
                )
                .distinct()
            )

            results = session.execute(statement)
            values = [result.compliance_reference_id for result in results.all()]
            return values

    def get_compliance_reference(self, compliance_reference_id: str) -> Optional[ComplianceReference]:
        """
        The `get_compliance_reference` method retrieves a `ComplianceReferenceEntity` object from a SQLite database
        based on its `compliance_reference_id`. If no instance is found in the database, it returns `None`.

        ### Example Usage
        ```python
        repository = SQLiteRepository("database.db")
        compliance_reference_id = "456"
        compliance_reference = repository.get_compliance_reference(compliance_reference_id)
        print(compliance_reference)
        ```
        Output:
        ```
        ComplianceReferenceEntity(
                                compliance_reference_id='456',
                                designation='Example Designation',
                                version='1.0',
                                category='Example Category',
                                valid_from='2022-01-01',
                                valid_until='2022-12-31',
                                reference='https://example.com',
                                assertion_method='Example Assertion Method',
                                valid_date='2022-06-30',
                                issuer_designation='Example Issuer Designation')
        ```
        :param compliance_reference_id: a compliance_reference ID
        :return ComplianceReferenceEntity if found, None otherwise
        """
        session_class = sessionmaker(self.__engine)
        with session_class() as session:
            results = session.query(ComplianceReference).where(
                ComplianceReference.compliance_reference_id == compliance_reference_id
            )
            return results.one_or_none()


    def get_compliance_certification_scheme_with_compliance_reference(
        self,
    ) -> (list[str], list[ComplianceReference]):
        """
        get compliance certification scheme from database
        :return:
        """
        session_class = sessionmaker(self.__engine)
        with session_class() as session:
            results = (session
                       .query(ComplianceCertificationSchemeProviderLink.compliance_reference_id)
                       .distinct()
                       .order_by(ComplianceCertificationSchemeProviderLink.compliance_reference_id)
                       .all())
            compliance_certification_scheme_ids = [ result[0] for result in results]
            compliance_references = []
            for cr_id in compliance_certification_scheme_ids:
                compliance_reference = self.get_compliance_reference(compliance_reference_id=cr_id)
                compliance_references.append(compliance_reference)

            return compliance_certification_scheme_ids, compliance_references

    def get_compliance_criterion_from_compliance_scheme_id(self, compliance_scheme_id: str) -> list[tuple[str, str]]:
        """
        get compliance criterion associated to compliance scheme
        :param compliance_scheme_id:
        :return:
        """
        session_class = sessionmaker(self.__engine)
        with session_class() as session:
            rows = (
                session.query(
                    ComplianceCriterionComplianceCertificationSchemeLink.compliance_criterion_id,
                    ComplianceCriterionComplianceCertificationSchemeLink.compliance_criterion_level_id,
                )
                .join(
                    ComplianceCertificationSchemeProviderLink,
                    ComplianceCertificationSchemeProviderLink.compliance_reference_id
                    == ComplianceCriterionComplianceCertificationSchemeLink.compliance_reference_id,
                )
                .where(ComplianceCertificationSchemeProviderLink.compliance_reference_id == compliance_scheme_id)
                .distinct()
                .all()
            )

            return [(str(row[0]), str(row[1])) for row in rows]
    def get_compliance_criterions(self, fetch_levels: bool = True) -> Optional[list[ComplianceCriterion]]:
        """
        The get_compliance_criterions method retrieves a list of ComplianceCriterion objects from a SQLite
        database.
        ### Example Usage
        ```python
        repository = SQLiteRepository("database.db")
        criterions = repository.get_compliance_criterions()
        print(criterions)
        ```

        :return:
        """
        session_class = sessionmaker(self.__engine)
        with session_class() as session:
            return (
                session.query(ComplianceCriterion).all()
                if not fetch_levels
                else session.query(ComplianceCriterion)
                .options(selectinload(ComplianceCriterion.levels))
                .all()
            )

    def get_compliance_criterion(self, criterion_id: str, fetch_levels: bool = True) -> Optional[ComplianceCriterion]:
        """
        The get_compliance_criterion method retrieves a ComplianceCriterion object from a SQLite
        database thanks to its id.
        ### Example Usage
        ```python
        repository = SQLiteRepository("database.db")
        criterion = repository.get_compliance_criterion(criterion_id="Criterion 1")
        print(criterion)
        ```

        :return:
        """
        session_class = sessionmaker(self.__engine)
        with session_class() as session:
            results = (
                session.query(ComplianceCriterion)
                .where(ComplianceCriterion.criterion_id == criterion_id)
                .options(selectinload(ComplianceCriterion.levels))
            )
            if not fetch_levels:
                results = session.query(ComplianceCriterion).where(ComplianceCriterion.criterion_id == criterion_id)

            return results.one_or_none()

    def get_third_party_compliance_certification_scheme_with_compliance_reference(
        self,
    ) -> (list[str], list[ComplianceReference]):
        """
        The get_third_party_compliance_certification_scheme_with_compliance_reference method retrieves a list of
        ThirdPartyComplianceCertificationSchemeEntity objects along with their associated ComplianceReferenceEntity
        objects from the SQLite database. It also has an option to fetch the associated ComplianceCriterion
        objects if specified.

        ### Example Usage
        ```python
        repository = ComplianceSQLiteRepository("database.db")
        schemes, references = repository.get_third_party_compliance_certification_scheme_with_compliance_reference(
            fetch_compliance_criterions=True)
        print(schemes)
        print(references)
        ```

        :param fetch_compliance_criterions (bool): A flag indicating whether to fetch the associated
        ComplianceCriterion objects or not.
        :return: compliance_certification_schemes (list[ThirdPartyComplianceCertificationSchemeEntity]): A list of
        ThirdPartyComplianceCertificationSchemeEntity objects retrieved from the database.
        :return compliance_references (list[ComplianceReferenceEntity]): A list of associated ComplianceReferenceEntity
        objects for the retrieved ThirdPartyComplianceCertificationSchemeEntity objects.
        """
        session_class = sessionmaker(self.__engine)
        with session_class() as session:
            results = session.query(
                ThirdPartyComplianceCertificationSchemeCABProviderLink.compliance_reference_id
            ).distinct().order_by(ThirdPartyComplianceCertificationSchemeCABProviderLink.compliance_reference_id).all()

            compliance_certification_scheme_ids = [ result[0] for result in results]
            compliance_references = []
            for cr_id in compliance_certification_scheme_ids:
                compliance_reference = self.get_compliance_reference(compliance_reference_id=cr_id)
                compliance_references.append(compliance_reference)

            return compliance_certification_scheme_ids, compliance_references

    def get_cabs_from_third_party_compliance_certification_scheme_id(
        self, compliance_reference_id: str
    ) -> list[ComplianceAssessmentsBody]:
        """
        The get_cabs_from_third_party_compliance_certification_scheme_id method retrieves a list of
        ComplianceAssessmentsBody objects associated with a given compliance reference ID from the SQLite
        database.

        ### Example Usage
        ```python
        repository = ComplianceSQLiteRepository("database.db")
        compliance_reference_id = "123"
        cabs = repository.get_cabs_from_third_party_compliance_certification_scheme_id(compliance_reference_id)
        print(cabs)
        ```
        :param compliance_reference_id: A string representing the compliance reference ID.
        :return: cabs: A list of ComplianceAssessmentsBody objects associated with the given compliance reference
        ID.
        """
        session_class = sessionmaker(self.__engine)
        with session_class() as session:
            results = (
                session.query(ThirdPartyComplianceCertificationSchemeCABProviderLink.cab_id)
                .where(
                    ThirdPartyComplianceCertificationSchemeCABProviderLink.compliance_reference_id
                    == compliance_reference_id
                )
                .distinct().all()
            )

            results = [ result[0] for result in results]
            cabs = []
            for cab_id in results:
                cab = self.get_compliance_assessments_body_from_id(cab_id)
                if cab is not None:
                    cabs.append(cab)

            return cabs

    def get_compliance_assessments_body_from_id(self, cab_id: str) -> Optional[ComplianceAssessmentsBody]:
        """
        The get_compliance_assessments_body_from_id method retrieves a compliance assessments body entity from the
        SQLite database based on the provided CAB ID.

        ### Example Usage
        ```python
        repository = ComplianceSQLiteRepository("database.db")
        cab_id = "123"
        body = repository.get_compliance_assessments_body_from_id(cab_id)
        print(body)  # Output: ComplianceAssessmentsBody object or None
        ```

        :param cab_id (str): The ID of the compliance assessments body to retrieve.
        :return:Optional[ComplianceAssessmentsBody]: The retrieved compliance assessments body entity from the
        database, or None if no entity is found.
        """
        session_class = sessionmaker(self.__engine)
        with session_class() as session:
            results = session.query(ComplianceAssessmentsBody).where(ComplianceAssessmentsBody.cab_id == cab_id)

            return results.one_or_none()

    def get_third_party_compliance_criterion_from_compliance_scheme_id(
        self, compliance_scheme_id: str
    ) -> list[tuple[str, str]]:
        """
        get third party criterion from compliance reference id
        :param compliance_scheme_id:
        :return:
        """
        session_class = sessionmaker(self.__engine)
        with session_class() as session:
            rows = (
                session.query(
                    ThirdPartyComplianceCriterionComplianceCertificationSchemeLink.compliance_criterion_id,
                    ThirdPartyComplianceCriterionComplianceCertificationSchemeLink.compliance_criterion_level_id,
                )
                .join(
                    ThirdPartyComplianceCertificationSchemeCABProviderLink,
                    ThirdPartyComplianceCertificationSchemeCABProviderLink.compliance_reference_id
                    == ThirdPartyComplianceCriterionComplianceCertificationSchemeLink.compliance_reference_id,
                )
                .where(
                    ThirdPartyComplianceCertificationSchemeCABProviderLink.compliance_reference_id
                    == compliance_scheme_id
                )
                .distinct()
                .all()
            )


            return [(str(row[0]), str(row[1])) for row in rows]
