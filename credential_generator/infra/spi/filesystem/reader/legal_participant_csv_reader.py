# -*- coding: utf-8 -*-
"""
It defines helper class to read csv files
"""
import csv
from pathlib import Path

from credential_generator.utilities.csv_helpers import get_value_of


def read_legal_participant(input_file: Path, participant_name: str) -> dict[str, str]:
    """
    Reads the legal participant CSV file and retrieves participant information.

    :return
        A dictionary containing the participant information with the following keys:
        - "role": The role of the participant. (type: str)
        - "id": The ID of the participant. (type: str)
        - "designation": The designation of the participant. (type: str)
        - "street_address": The street address of the participant. (type: str)
        - "postal_code": The postal code of the participant. (type: str)
        - "city": The city of the participant. (type: str)
        - "country": The country of the participant. (type: str)
        - "vatID": The VAT ID of the participant. (type: str)
        - "lei_code": The LEI code of the participant. (type: str)
        - "terms_and_conditions": The terms and conditions of the participant. (type: str)

    """
    if not input_file.exists() or not input_file.is_file():
        return {}

    with open(input_file, newline="", encoding="utf-8-sig") as csvfile:
        reader = csv.DictReader(csvfile, delimiter=";")

        for row in reader:
            return {
                "role": get_value_of(row, "Role"),
                "name": participant_name,
                "id": get_value_of(row, "Id"),
                "designation": get_value_of(row, "Designation"),
                "street_address": get_value_of(row, "Street Address"),
                "postal_code": get_value_of(row, "Postal Code"),
                "city": get_value_of(row, "Locality"),
                "country": get_value_of(row, "Country Name"),
                "vat_id": get_value_of(row, "VatID"),
                "lei_code": get_value_of(row, "LeiCode"),
                "terms_and_conditions": get_value_of(row, "Terms And Conditions"),
            }

    return {}
