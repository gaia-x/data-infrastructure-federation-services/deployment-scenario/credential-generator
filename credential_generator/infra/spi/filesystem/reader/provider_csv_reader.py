# -*- coding: utf-8 -*-
"""
It defines how to read location csv file
"""
# Standard Library
import csv
from pathlib import Path

# data-initialiser Library
from credential_generator.utilities.csv_helpers import get_splitted_values_of, get_value_of


def read_keywords(input_file: Path, participant_name: str) -> list[dict]:
    """
    The read function reads the keywords from a CSV file and generates
    keyword list.

    :param participant_name:
    :param input_file:
    :return: A list of keywords
    """
    values = []
    if input_file.is_file():
        with open(input_file, newline="", encoding="utf-8-sig") as csvfile:
            reader = csv.DictReader(csvfile, delimiter=";")
            values = [
                {
                    "provider_name": participant_name,
                    "keyword": get_value_of(row, "Keyword"),
                    "service_ids": get_splitted_values_of(get_value_of(row, "Service IDs")),
                }
                for row in reader
            ]
    return values


def read_layers(input_file: Path, participant_name: str) -> list[dict]:
    """
    The read function reads the services from a CSV file and generates
    layers list.

    :param participant_name:
    :param input_file:
    :param self: Refer to the object itself
    :return: A list of layers
    """

    values = []
    if input_file.is_file():
        with open(input_file, newline="", encoding="utf-8-sig") as csvfile:
            reader = csv.DictReader(csvfile, delimiter=";")
            values = [
                {
                    "provider_name": participant_name,
                    "service_ids": get_splitted_values_of(get_value_of(row, "Service ID")),
                    "layer": get_value_of(row, "Layer"),
                }
                for row in reader
            ]
    return values


def read_location(input_file: Path, participant_name: str) -> list[dict]:
    """
    The read function reads the services from a CSV file and generates
    location list.

    :param participant_name:
    :param input_file:
    :return: A list of location
    """

    if not input_file.is_file():
        return []

    with open(input_file, newline="", encoding="utf-8-sig") as csvfile:
        reader = csv.DictReader(csvfile, delimiter=";")

        return [
            {
                "provider_name": participant_name,
                "location_id": get_value_of(row, "Location Id"),
                "designation": get_value_of(row, "Provider Designation"),
                "city": get_value_of(row, "City"),
                "state": get_value_of(row, "State/Dpt"),
                "country": get_value_of(row, "Country"),
            }
            for row in reader
        ]


def read_services(input_file: Path, participant_name: str) -> list[dict]:
    """
    The read function reads the services from a CSV file and generates
    service list.

    :param self: Refer to the object itself
    :return: A list of Services
    """
    values = []
    if input_file.is_file():
        with open(input_file, newline="", encoding="utf-8-sig") as csvfile:
            reader = csv.DictReader(csvfile, delimiter=";")
            values = [
                {
                    "provider_name": participant_name,
                    "service_id": get_value_of(row, "Service Id"),
                    "designation": get_value_of(row, "Designation"),
                    "service_version": get_value_of(row, "Service Version"),
                    "compliance_reference_ids": get_splitted_values_of(get_value_of(row, "Comply With")),
                    "location_ids": get_splitted_values_of(get_value_of(row, "Available In")),
                    "description": get_value_of(row, "Description"),
                    "level": get_value_of(row, "Level of Label"),
                }
                for row in reader
            ]
    return values


def read_self_assessments(input_file: Path, participant_name: str) -> list[dict]:
    """
    The `read_self_assessments` method reads data from a CSV file named "self-assessment.csv" and
    returns a list of dictionaries. Each dictionary represents a row in the CSV file and contains information
    about a self-assessment. The method uses the `csv.DictReader` class to read the CSV file, and the `
    get_value_of` and `get_splitted_values_of` functions to extract values from the rows.

    ### Example Usage
    ```python
    reader = ProviderCSVReader(directory, participant_name)
    self_assessments = reader.read_self_assessments()
    print(self_assessments)
    ```
    :param participant_name:
    :param input_file:
    :return: A list of self assessments
    """
    values = []
    if input_file.is_file():
        with open(input_file, newline="", encoding="utf-8-sig") as csvfile:
            reader = csv.DictReader(csvfile, delimiter=";")
            values = []
            for row in reader:
                criterion = get_value_of(row, "Criterion")
                if criterion is not None and criterion != "":
                    self_assessment = {
                        "provider_name": participant_name,
                        "section": get_value_of(row, "Section"),
                        "criterion": criterion.split(":")[0].strip(),
                        "service_ids": get_splitted_values_of(
                            get_value_of(row, "Service(s)  Id- for which the  self-assesment of the criterion is  OK")
                        ),
                        "compliance_reference_ids": get_splitted_values_of(
                            get_value_of(row, "External available proof - Optinoal  (if any)")
                        ),
                    }
                    values.append(self_assessment)
    return values


def read_provider_compliance_schemes(input_file: Path, participant_name: str) -> list[dict]:
    """
    The read function reads the cab from a CSV file and generates
    cab list.

    :param participant_name:
    :param input_file:
    :param self: Refer to the object itself
    :return: A list of cab
    """
    values = []
    if input_file.is_file():
        with open(input_file, newline="", encoding="utf-8-sig") as csvfile:
            reader = csv.DictReader(csvfile, delimiter=";")
            values = [
                {
                    "provider_name": participant_name,
                    "cab_id": get_value_of(row, "CAB-ID"),
                    "issuer_designation": get_value_of(row, "Issuer Designation"),
                    "assertion_date": get_value_of(row, "Assertion Date"),
                    "assertion_method": get_value_of(row, "Assertion Method"),
                    "reference_doc": get_value_of(row, "Reference Doc"),
                    "valid_until": get_value_of(row, "Valid Until"),
                    "valid_from": get_value_of(row, "Valid from"),
                    "version": get_value_of(row, "Version"),
                    "compliance_reference_designation": get_value_of(row, "Ref Designation"),
                    "compliance_reference_id": get_value_of(row, "CR-ID"),
                }
                for row in reader
            ]
    return values
