# -*- coding: utf-8 -*-
"""
It defines how to read compliance criterion csv file
"""
# Standard Library
import csv
from pathlib import Path

from credential_generator.utilities.csv_helpers import get_value_of


class ComplianceCriterionCSVReader:
    """
    ComplianceCriterionCSVReader is helper class to read a compliance criterion csv file
    and retrieve ComplianceCriterion and ComplianceLabels objects from this file.
    """

    def __init__(self, datasource_root_directory: Path, filename: str):
        self.datasource = Path(datasource_root_directory, filename).absolute()

    def read_criterions_and_labels(self) -> (list[dict], list[str]):
        """
        The ComplianceCriterionCSVReader class is a helper class that reads a CSV file containing compliance criteria
        and retrieves the compliance criteria and labels from the file.
        :return: a tuple corresponding to list of criterions and list of levels
        """
        compliance_dict: dict[str, dict] = {}
        levels = set()
        with open(self.datasource, newline="", encoding="utf-8-sig") as csvfile:
            reader = csv.DictReader(csvfile, delimiter=";")
            for row in reader:
                if row.get("applicable", "") == "":
                    criterion_name = get_value_of(row, "criterion-name")
                    criterion_id = get_value_of(row, "criterion").split(":")

                    level = get_value_of(row, "used for label level")

                    criterion = (
                        compliance_dict[criterion_name]
                        if criterion_name in compliance_dict
                        else {
                            "id": criterion_id[0],
                            "name": criterion_name,
                            "category": get_value_of(row, "Category"),
                            "description": get_value_of(row, "description"),
                            "levels": set(),
                            "self_assessed": get_value_of(row, "self-assessed") != "",
                        }
                    )

                    criterion["levels"].add(level)
                    levels.add(level)
                    compliance_dict[criterion_name] = criterion

        return list(compliance_dict.values()), list(levels)

    def read_compliance_certification_schemes(self) -> (dict, dict):
        """
        The read_compliance_certification_schemes method reads a CSV file containing compliance criteria and retrieves
        the compliance certification schemes from the file.
        :return: compliance_certification_schemes (dict): A dictionary containing compliance certification schemes
        where the keys are compliance reference IDs and the values are lists of criterion IDs.
                third_party_compliance_certification_schemes (dict): A dictionary containing third-party compliance
                certification schemes where the keys are compliance reference IDs and the values are lists of
                criterion IDs.
        """
        with open(self.datasource, newline="", encoding="utf-8-sig") as csvfile:
            reader = csv.DictReader(csvfile, delimiter=";")

            compliance_certification_schemes = {}
            third_party_compliance_certification_schemes = {}
            for row in reader:
                if row.get("applicable", "") == "":
                    criterion_name = get_value_of(row, "criterion")
                    criterion_id = criterion_name.split(":")[0]
                    level = get_value_of(row, "used for label level")

                    criterion_key = f"{criterion_id}-{level}"

                    for key, value in row.items():
                        if "CR-" in key and isinstance(value, str) and value.strip().lower() == "x":
                            compliance_reference_list = list(filter(lambda x: x.count("CR-") != 0, key.split(":::")))
                            for compliance_reference in compliance_reference_list:
                                cr_id = compliance_reference.split(":")[0].strip()
                                certification_type = compliance_reference.split(":")[1].strip().lower()
                                match certification_type:
                                    case "self-declared":
                                        if cr_id not in compliance_certification_schemes:
                                            compliance_certification_schemes[cr_id] = []

                                        if criterion_key not in compliance_certification_schemes[cr_id]:
                                            compliance_certification_schemes[cr_id].append(criterion_key)
                                    case "certified":
                                        if cr_id not in third_party_compliance_certification_schemes:
                                            third_party_compliance_certification_schemes[cr_id] = []

                                        if criterion_key not in third_party_compliance_certification_schemes[cr_id]:
                                            third_party_compliance_certification_schemes[cr_id].append(criterion_key)

            return compliance_certification_schemes, third_party_compliance_certification_schemes
