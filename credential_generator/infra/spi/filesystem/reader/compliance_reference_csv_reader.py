# -*- coding: utf-8 -*-
"""
It defines how to read compliance criterion csv file
"""
# Standard Library
import csv
from pathlib import Path
from typing import List

# data-initialiser Library
from credential_generator.utilities.csv_helpers import get_value_of


class ComplianceReferenceCSVReader:
    """
    ComplianceReferenceCSVReader is helper class to read a compliance criterion reference csv file
    and retrieve ComplianceCriterion and ComplianceLabels objects from this file.
    """

    def __init__(self, datasource_root_directory: Path, filename: str):
        self.datasource = Path(datasource_root_directory, filename).absolute()

    def read(self) -> list[dict]:
        """
        The read function reads the compliance criteria from a CSV file and generates
        ComplianceLabel list and ComplianceReference list.

        :param self: Refer to the object itself
        :return: A list of ComplianceReference
        """
        compliance_reference_list: List[dict] = []
        with open(self.datasource, newline="", encoding="utf-8-sig") as csvfile:
            reader = csv.DictReader(csvfile, delimiter=";")
            for row in reader:
                compliance_reference = {
                    "title": get_value_of(row, "Ref Designation"),
                    "version": get_value_of(row, "Extrait"),
                    "criterion_reference_id": get_value_of(row, "CR-ID"),
                    "category": get_value_of(row, "Category"),
                    "valid_from": get_value_of(row, "Valid From"),
                    "valid_until": get_value_of(row, "Valid Until"),
                    "reference": get_value_of(row, "Reference Doc"),
                    "assertion_method": get_value_of(row, "Assertion Method"),
                    "assertion_date": get_value_of(row, "Assertion Date"),
                    "issuer_designation": get_value_of(row, "Issuer Designation"),
                }
                compliance_reference_list.append(compliance_reference)

        return compliance_reference_list
