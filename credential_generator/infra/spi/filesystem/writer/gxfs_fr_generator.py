# -*- coding: utf-8 -*-
"""
It defines generators to dump gx-participant objects into json-ld
"""
# Standard Library
import logging
from pathlib import Path

from credential_generator.config import (
    ASTER_CONFORMITY_CONTEXT,
    ASTER_CONFORMITY_PREFIX,
    DEFAULT_CREDENTIAL_CONTEXT,
    GX_TRUST_FRAMEWORK_CONTEXT,
    GX_TRUST_FRAMEWORK_PREFIX,
)

# data-initialiser Library
from credential_generator.domain._2210.gxfs.gx_compliance import (
    ComplianceAssessmentBody,
    ComplianceCertificateClaim,
    ComplianceCertificationScheme,
    ComplianceCriterion,
    ComplianceLabel,
    ComplianceReference,
    ComplianceReferenceManager,
    SelfAssessedComplianceCriteriaClaim,
    ThirdPartyComplianceCertificateClaim,
    ThirdPartyComplianceCertificationScheme,
)
from credential_generator.domain._2210.gxfs.gx_participant import Location
from credential_generator.domain._2210.gxfs.gx_service import LocatedServiceOffering
from credential_generator.infra.spi.filesystem.writer import (
    dump_gaiax_object_to_json_ld,
)

log = logging.getLogger(__name__)


def dump_gxfs_location_to_json_ld(location: Location, output_file: Path) -> None:
    """
    The dump_gxfs_location_to_json_ld function takes a Location object and an output file path as input. It converts
    the Location object into a dictionary, adds a prefix to the keys, and saves the modified object as a JSON-LD file
    on disk.

    ### Example usage
    ```python
    from pathlib import Path

    # Creating a Location object
    location = Location(
                did_web="id": "did:web:example.com/location/1",
                country="US",
                state="CA",
                urbanArea="LA",
                providerDesignation="ABC"
                )

    # Calling the dump_gxfs_location_to_json_ld function
    output_file = Path("location.jsonld")
    dump_gxfs_location_to_json_ld(location, output_file)
    ```

    Output: A JSON-LD file named "location.jsonld" is created on disk with the following content:

    ```python
    {
        "@context": {
            "gx-participant": "https://schemas.abc-federation.gaia-x.community/wip/vocab/participant#"
        },
        "id": "did:web:example.com/location/1",
        "@type": "gx-participant:Location",
        "gx-participant:country": "US",
        "gx-participant:state": "CA",
        "gx-participant:urbanArea": "LA",
        "gx-participant:providerDesignation": "ABC"
    }
    ```



    :param location:
    :param output_file:
    :return:
    """
    if not isinstance(location, Location):
        raise TypeError("location must be a Location object")

    log.debug("Generating json-ld from GXFS-FR location object")

    location_dict = location.model_dump(by_alias=True, exclude_unset=True, exclude_none=True)
    dump_gaiax_object_to_json_ld(
        gaia_x=location_dict,
        object_type=f"{ASTER_CONFORMITY_PREFIX}:Location",
        context=[ASTER_CONFORMITY_CONTEXT],
        output_file=output_file,
    )


def dump_gxfs_located_service_offering_to_json_ld(
    located_service_offering: LocatedServiceOffering, output_file: Path
) -> None:
    """
    The dump_gxfs_located_service_offering_to_json_ld function takes a LocatedServiceOffering object and an output file
    path as input. It converts the LocatedServiceOffering object into a dictionary, and then calls the
    dump_gaiax_object_to_json_ld function to convert the dictionary into a JSON-LD file. The JSON-LD file is saved to
    the specified output file path.

    ### Example Usage
    ```python
    located_service_offering = LocatedServiceOffering(isImplementationOf=..., isHostedOn=...)
    output_file = Path("located_service_offering.jsonld")

    dump_gxfs_located_service_offering_to_json_ld(located_service_offering, output_file)
    ```
    :param located_service_offering:
    :param output_file:
    :return:
    """

    if not isinstance(located_service_offering, LocatedServiceOffering):
        raise TypeError("located_service_offering must be a LocatedServiceOffering object")

    log.debug("Generating json-ld from GXFS-FR located service offering object")

    located_service_offering_dict = located_service_offering.model_dump(
        by_alias=True, exclude_unset=True, exclude_none=True
    )
    dump_gaiax_object_to_json_ld(
        gaia_x=located_service_offering_dict,
        object_type=f"{ASTER_CONFORMITY_PREFIX}:LocatedServiceOffering",
        context=[ASTER_CONFORMITY_CONTEXT],
        output_file=output_file,
    )


def dump_gxfs_compliance_criterion_to_json_ld(compliance_criterion: ComplianceCriterion, output_file: Path) -> None:
    """
    The dump_gxfs_compliance_criterion_to_json_ld function takes a ComplianceCriterion object and an output file path
    as input. It generates a JSON-LD representation of the ComplianceCriterion object and saves it as a file on disk.

    ### Example Usage
    ```python
    compliance_criterion = ComplianceCriterion(
        name="Example Criterion",
        level=ComplianceLevel.HIGH,
        category=CriterionCategory.SECURITY,
        description="Example description",
        self_assessed=True,
        did_web="did:web:example"
    )
    output_file = Path("compliance_criterion.jsonld")

    dump_gxfs_compliance_criterion_to_json_ld(compliance_criterion, output_file)
    ```
    :param compliance_criterion:
    :param output_file:
    :return:
    """
    if not isinstance(compliance_criterion, ComplianceCriterion):
        raise TypeError("compliance_criterion must be a ComplianceCriterion object")

    log.debug(f"Generating json-ld from GXFS-FR compliance criterion object {compliance_criterion.did_web}")

    compliance_criterion_dict = compliance_criterion.model_dump(by_alias=True, exclude_unset=True, exclude_none=True)

    dump_gaiax_object_to_json_ld(
        gaia_x=compliance_criterion_dict,
        object_type=f"{ASTER_CONFORMITY_PREFIX}:ComplianceCriterion",
        context=[ASTER_CONFORMITY_CONTEXT, DEFAULT_CREDENTIAL_CONTEXT],
        output_file=output_file,
    )


def dump_gxfs_compliance_label_to_json_ld(compliance_label: ComplianceLabel, output_file: Path) -> None:
    """
    The dump_gxfs_compliance_label_to_json_ld function takes a ComplianceLabel object and an output file path as input.
    It converts the ComplianceLabel object into a dictionary, and then calls the dump_gaiax_object_to_json_ld function
    to save the modified object as a JSON-LD file on disk.

    ### Example Usage
    ```python
    compliance_label = ComplianceLabel(
        did_web="did:web:example",
        level="High",
        name="Label 1",
        description="Description of Label 1",
        criteria_list=[
            GaiaxObjectWithDidWeb(did_web="did:web:criteria1"),
            GaiaxObjectWithDidWeb(did_web="did:web:criteria2"),
        ]
    )
    output_file = Path("compliance_label.jsonld")

    dump_gxfs_compliance_label_to_json_ld(compliance_label, output_file)
    ```
    :param compliance_label:
    :param output_file:
    :return:
    """
    if not isinstance(compliance_label, ComplianceLabel):
        raise TypeError("compliance_label must be a ComplianceLabel object")

    log.debug(f"Generating json-ld from GXFS-FR compliance label object {compliance_label.did_web}")

    compliance_label_dict = compliance_label.model_dump(by_alias=True, exclude_unset=True, exclude_none=True)

    dump_gaiax_object_to_json_ld(
        gaia_x=compliance_label_dict,
        object_type=f"{ASTER_CONFORMITY_PREFIX}:ComplianceLabel",
        context=[ASTER_CONFORMITY_CONTEXT, DEFAULT_CREDENTIAL_CONTEXT],
        output_file=output_file,
    )


def dump_gxfs_cab_to_json_ld(cab: ComplianceAssessmentBody, output_file: Path) -> None:
    """
    The dump_gxfs_cab_to_json_ld function takes a ComplianceAssessmentBody object and an output file path as input. It
    converts the ComplianceAssessmentBody object into a dictionary, and then calls the dump_gaiax_object_to_json_ld
    function to save the modified object as a JSON-LD file on disk.

    ### Example Usage
    ```python
    cab = ComplianceAssessmentBody(cab_id="123", name="Example CAB")
    output_file = Path("cab.jsonld")

    dump_gxfs_cab_to_json_ld(cab, output_file)
    ```

    :param cab: A ComplianceAssessmentBody object representing a compliance assessment body.
    :param output_file: A Path object representing the file path where the JSON-LD file will be saved.
    :return: The function does not return any value. It saves the modified ComplianceAssessmentBody object as a JSON-LD
    file on disk.
    """
    if not isinstance(cab, ComplianceAssessmentBody):
        raise TypeError("cab must be a ComplianceAssessmentBody object")

    log.debug(f"Generating json-ld from GXFS-FR compliance assessment body object {cab.did_web}")

    legal_registration_number_dict = cab.legalRegistrationNumber.model_dump(
        by_alias=True,
        exclude_unset=True,
        exclude_none=True,
        include={
            "did_web",
        },
    )

    cab_dict = cab.model_dump(
        by_alias=True, exclude_unset=True, exclude_none=True, exclude={"gx:legalRegistrationNumber"}
    ) | {"gx:legalRegistrationNumber": legal_registration_number_dict}

    dump_gaiax_object_to_json_ld(
        gaia_x=cab_dict,
        object_type=f"{GX_TRUST_FRAMEWORK_PREFIX}:LegalParticipant",
        context=[GX_TRUST_FRAMEWORK_CONTEXT],
        output_file=output_file,
    )


def dump_gxfs_compliance_reference_manager_to_json_ld(manager: ComplianceReferenceManager, output_file: Path) -> None:
    """
    The dump_gxfs_compliance_reference_manager_to_json_ld function takes a ComplianceReferenceManager object and an
    output file path as input. It converts the ComplianceReferenceManager object into a dictionary, and then calls the
    dump_gaiax_object_to_json_ld function to save the modified object as a JSON-LD file on disk.

    ### Example Usage
    ```python
    cab = ComplianceReferenceManager(cab_id="123", name="Example CAB")
    output_file = Path("cab.jsonld")

    dump_gxfs_cab_to_json_ld(cab, output_file)
    ```

    :param manager: A ComplianceReferenceManager object representing a compliance reference manager.
    :param output_file: A Path object representing the file path where the JSON-LD file will be saved.
    :return: The function does not return any value. It saves the modified ComplianceAssessmentBody object as a JSON-LD
    file on disk.
    """
    if not isinstance(manager, ComplianceReferenceManager):
        raise TypeError("manager must be a ComplianceReferenceManager object")

    log.debug(f"Generating json-ld from GXFS-FR compliance reference manager object {manager.did_web}")
    legal_registration_number_dict = manager.legalRegistrationNumber.model_dump(
        by_alias=True,
        exclude_unset=True,
        exclude_none=True,
        include={
            "did_web",
        },
    )
    manager_dict = manager.model_dump(
        by_alias=True, exclude_unset=True, exclude_none=True, exclude={"gx:legalRegistrationNumber"}
    ) | {"gx:legalRegistrationNumber": legal_registration_number_dict}

    dump_gaiax_object_to_json_ld(
        gaia_x=manager_dict,
        object_type=f"{GX_TRUST_FRAMEWORK_PREFIX}:LegalParticipant",
        context=[GX_TRUST_FRAMEWORK_CONTEXT],
        output_file=output_file,
    )


def dump_gxfs_compliance_reference_to_json_ld(compliance_reference: ComplianceReference, output_file: Path) -> None:
    """
    The dump_gxfs_compliance_reference_to_json_ld function takes a ComplianceReference object and an output file path
    as input. It generates a JSON-LD representation of the ComplianceReference object and saves it as a file on disk.
    ### Example Usage
    ```python
    compliance_reference = ComplianceReference(...)
    output_file = Path("compliance_reference.jsonld")

    dump_gxfs_compliance_reference_to_json_ld(compliance_reference, output_file)
    ```
    :param compliance_reference: A ComplianceReference object representing a compliance reference.
    :param output_file: A Path object representing the file path where the JSON-LD representation will be saved
    :return: None. The function saves the JSON-LD representation of the compliance_reference object as a file on disk.
    """
    if not isinstance(compliance_reference, ComplianceReference):
        raise TypeError("compliance_reference must be a ComplianceReference object")

    log.debug(f"Generating json-ld from GXFS-FR compliance reference object {compliance_reference.did_web}")

    compliance_reference_dict = compliance_reference.model_dump(
        by_alias=True, exclude_unset=True, exclude_none=True, exclude={"compliance_reference_id"}
    )

    dump_gaiax_object_to_json_ld(
        gaia_x=compliance_reference_dict,
        object_type=f"{ASTER_CONFORMITY_PREFIX}:ComplianceReference",
        context=[ASTER_CONFORMITY_CONTEXT, DEFAULT_CREDENTIAL_CONTEXT],
        output_file=output_file,
    )


def dump_gxfs_compliance_certification_scheme_to_json_ld(
    compliance_certification_scheme: ComplianceCertificationScheme, output_file: Path
) -> None:
    """
    This code defines a function named dump_gxfs_compliance_certification_scheme_to_json_ld that takes a
    ComplianceCertificationScheme object and an output file path as input. It generates a JSON-LD representation
    of the ComplianceCertificationScheme object and saves it to the specified output file.

    ```python
    Example Usage
    compliance_certification_scheme = ComplianceCertificationScheme(...)
    output_file = Path("output.jsonld")
    dump_gxfs_compliance_certification_scheme_to_json_ld(compliance_certification_scheme, output_file)
    ```
    :param compliance_certification_scheme: A ComplianceCertificationScheme object representing a compliance
    certification scheme.
    :param output_file: A Path object representing the path to the output file where the JSON-LD representation will be
    saved.
    :return: None. The function saves the JSON-LD representation of the compliance_certification_scheme object to the
    specified output file.
    """
    if not isinstance(compliance_certification_scheme, ComplianceCertificationScheme):
        raise TypeError("compliance_certification_scheme must be a ComplianceCertificationScheme object")

    log.debug(
        f"Generating json-ld from GXFS-FR compliance certification scheme object "
        f"{compliance_certification_scheme.did_web}"
    )

    compliance_certification_scheme_dict = compliance_certification_scheme.model_dump(
        by_alias=True, exclude_unset=True, exclude_none=True, include={"did_web"}
    ) | {
        f"{ASTER_CONFORMITY_PREFIX}:hasComplianceReference": {
            "id": compliance_certification_scheme.compliance_reference.did_web
        },
        f"{ASTER_CONFORMITY_PREFIX}:grantsComplianceCriteria": [
            {"id": criteria.did_web} for criteria in compliance_certification_scheme.criteria_list
        ]
        if compliance_certification_scheme.criteria_list is not None
        else [],
    }

    dump_gaiax_object_to_json_ld(
        gaia_x=compliance_certification_scheme_dict,
        object_type=f"{ASTER_CONFORMITY_PREFIX}:ComplianceCertificationScheme",
        context=[ASTER_CONFORMITY_CONTEXT],
        output_file=output_file,
    )


def dump_gxfs_third_party_compliance_certification_scheme_to_json_ld(
    compliance_certification_scheme: ThirdPartyComplianceCertificationScheme, output_file: Path
) -> None:
    """
    This code defines a function named dump_gxfs_third_party_compliance_certification_scheme_to_json_ld that takes a
    ThirdPartyComplianceCertificationScheme object and an output file path as inputs. The function generates a JSON-LD
    representation of the compliance certification scheme object and saves it to the specified output file.

    ### Example Usage
    ```python
    compliance_certification_scheme = ThirdPartyComplianceCertificationScheme(...)
    output_file = Path("output.jsonld")
    dump_gxfs_third_party_compliance_certification_scheme_to_json_ld(compliance_certification_scheme, output_file)
    ```
    :param compliance_certification_scheme: A ThirdPartyComplianceCertificationScheme object representing a compliance
    certification scheme.
    :param output_file: A Path object representing the file path where the JSON-LD representation will be saved.
    :return: The function does not return any value. It generates a JSON-LD file representing the compliance
    certification scheme object and saves it to the specified output file path.
    """

    if not isinstance(compliance_certification_scheme, ThirdPartyComplianceCertificationScheme):
        raise TypeError("compliance_certification_scheme must be a ThirdPartyComplianceCertificationScheme object")

    log.debug(
        f"Generating json-ld from GXFS-FR third party compliance certification scheme object "
        f"{compliance_certification_scheme.did_web}"
    )

    compliance_certification_scheme_dict = compliance_certification_scheme.model_dump(
        by_alias=True, exclude_unset=True, exclude_none=True, include={"did_web"}
    ) | {
        f"{ASTER_CONFORMITY_PREFIX}:hasComplianceReference": {
            "id": compliance_certification_scheme.compliance_reference.did_web
        },
        f"{ASTER_CONFORMITY_PREFIX}:grantsComplianceCriteria": [
            {"id": criteria.did_web} for criteria in compliance_certification_scheme.criteria_list
        ]
        if compliance_certification_scheme.criteria_list is not None
        else [],
        f"{ASTER_CONFORMITY_PREFIX}:hasComplianceAssessmentBody": [
            {"id": cab.did_web} for cab in compliance_certification_scheme.compliance_assessment_bodies
        ],
    }

    dump_gaiax_object_to_json_ld(
        gaia_x=compliance_certification_scheme_dict,
        object_type=f"{ASTER_CONFORMITY_PREFIX}:ThirdPartyComplianceCertificationScheme",
        context=[ASTER_CONFORMITY_CONTEXT],
        output_file=output_file,
    )


def dump_gxfs_compliance_certificate_claim_to_json_ld(
    compliance_certificate_claim: ComplianceCertificateClaim, output_file: Path
) -> None:
    """
    This code defines a function named dump_gxfs_compliance_certificate_claim_to_json_ld that takes a
    ComplianceCertificateClaim object and an output file path as inputs. It generates a JSON-LD representation of the
    ComplianceCertificateClaim object and saves it to the specified output file.

    ## Example Usage
    ```python
    compliance_certificate_claim = ComplianceCertificateClaim(...)
    output_file = Path("output.jsonld")

    dump_gxfs_compliance_certificate_claim_to_json_ld(compliance_certificate_claim, output_file)
    ```
    :param compliance_certificate_claim: A ComplianceCertificateClaim object representing a compliance certificate
    claim.
    :param output_file: A Path object specifying the output file path.
    :return:None. The function saves a JSON-LD representation of the compliance_certificate_claim object to the
    specified output file.
    """
    if not isinstance(compliance_certificate_claim, ComplianceCertificateClaim):
        raise TypeError("compliance_certificate_claim must be a ComplianceCertificateClaim object")

    log.debug(
        f"Generating json-ld from GXFS-FR compliance certificate claim object" f"{compliance_certificate_claim.did_web}"
    )

    compliance_certification_scheme_dict = compliance_certificate_claim.model_dump(
        by_alias=True, exclude_unset=True, exclude_none=True, include={"did_web"}
    ) | {
        f"{ASTER_CONFORMITY_PREFIX}:hasLocatedServiceOffering": {
            "id": compliance_certificate_claim.located_service_offering.did_web
        },
        # pylint: disable=line-too-long
        f"{ASTER_CONFORMITY_PREFIX}:hasComplianceCertificationScheme": {
            "id": compliance_certificate_claim.compliance_certification_scheme.did_web
        }
        if compliance_certificate_claim.compliance_certification_scheme is not None
        else {},
    }

    dump_gaiax_object_to_json_ld(
        gaia_x=compliance_certification_scheme_dict,
        object_type=f"{ASTER_CONFORMITY_PREFIX}:ComplianceCertificateClaim",
        context=[ASTER_CONFORMITY_CONTEXT],
        output_file=output_file,
    )


def dump_gxfs_third_party_compliance_certificate_claim_to_json_ld(
    compliance_certificate_claim: ThirdPartyComplianceCertificateClaim, output_file: Path
) -> None:
    """
    This code defines a function named dump_gxfs_third_party_compliance_certificate_claim_to_json_ld that takes a
    ThirdPartyComplianceCertificateClaim object and an output file path as inputs. It generates a JSON-LD
    representation of the compliance certificate claim object and saves it to the specified output file.
    :param compliance_certificate_claim: A ThirdPartyComplianceCertificateClaim object representing a third-party
    compliance certificate claim.
    :param output_file: A Path object specifying the output file path where the JSON-LD representation will be saved.
    :return: None. The function generates a JSON-LD representation of the compliance certificate claim object and saves
    it to the specified output file.
    """
    if not isinstance(compliance_certificate_claim, ThirdPartyComplianceCertificateClaim):
        raise TypeError("compliance_certificate_claim must be a ThirdPartyComplianceCertificateClaim object")

    log.debug(
        f"Generating json-ld from GXFS-FR third party compliance certificate claim object"
        f"{compliance_certificate_claim.did_web}"
    )

    compliance_certification_scheme_dict = compliance_certificate_claim.model_dump(
        by_alias=True, exclude_unset=True, exclude_none=True, include={"did_web"}
    ) | {
        f"{ASTER_CONFORMITY_PREFIX}:hasLocatedServiceOffering": {
            "id": compliance_certificate_claim.located_service_offering.did_web
        },
        # pylint: disable = line-too-long
        f"{ASTER_CONFORMITY_PREFIX}:hasComplianceCertificationScheme": {
            "id": compliance_certificate_claim.compliance_certification_scheme.did_web
        }
        if compliance_certificate_claim.compliance_certification_scheme is not None
        else {},
        # pylint: disable = line-too-long
        f"{ASTER_CONFORMITY_PREFIX}:hasComplianceAssessmentBody": {
            "id": compliance_certificate_claim.compliance_assessment_body.did_web
        },
    }

    dump_gaiax_object_to_json_ld(
        gaia_x=compliance_certification_scheme_dict,
        object_type=f"{ASTER_CONFORMITY_PREFIX}:ThirdPartyComplianceCertificateClaim",
        context=[ASTER_CONFORMITY_CONTEXT],
        output_file=output_file,
    )


def dump_gxfs_self_assessed_compliance_certificate_claim_to_json_ld(
    compliance_certificate_claim: SelfAssessedComplianceCriteriaClaim, output_file: Path
) -> None:
    """
    This code defines a function named dump_gxfs_self_assessed_compliance_certificate_claim_to_json_ld that takes a
    SelfAssessedComplianceCriteriaClaim object and an output file path as inputs. It generates a JSON-LD representation
    of the compliance certificate claim object and saves it to the specified output file.

    ## Example Usage
    ```
    claim = SelfAssessedComplianceCriteriaClaim(...)
    output_file = Path("output.jsonld")
    dump_gxfs_self_assessed_compliance_certificate_claim_to_json_ld(claim, output_file)
    ```
    :param compliance_certificate_claim: A SelfAssessedComplianceCriteriaClaim object representing a compliance
    certificate claim.
    :param output_file: A Path object specifying the output file path.
    :return:None. The function saves the generated JSON-LD representation of the compliance certificate claim object to
    the specified output file.
    """
    if not isinstance(compliance_certificate_claim, SelfAssessedComplianceCriteriaClaim):
        raise TypeError("compliance_certificate_claim must be a SelfAssessedComplianceCriteriaClaim object")

    log.debug(
        f"Generating json-ld from GXFS-FR third party compliance certificate claim object"
        f"{compliance_certificate_claim.did_web}"
    )

    criterias = (
        [{"id": criteria.did_web} for criteria in compliance_certificate_claim.criteria]
        if compliance_certificate_claim.criteria is not None
        else []
    )

    compliance_certification_scheme_dict = compliance_certificate_claim.model_dump(
        by_alias=True, exclude_unset=True, exclude_none=True, include={"did_web"}
    ) | {
        f"{ASTER_CONFORMITY_PREFIX}:hasServiceOffering": [
            {"id": compliance.did_web} for compliance in compliance_certificate_claim.located_service_offering
        ],
        # pylint: disable = line-too-long
        f"{ASTER_CONFORMITY_PREFIX}:hasAssessedComplianceCriteria": criterias,
    }

    dump_gaiax_object_to_json_ld(
        gaia_x=compliance_certification_scheme_dict,
        object_type=f"{ASTER_CONFORMITY_PREFIX}:SelfAssessedComplianceCriteriaClaim",
        context=[ASTER_CONFORMITY_CONTEXT],
        output_file=output_file,
    )
