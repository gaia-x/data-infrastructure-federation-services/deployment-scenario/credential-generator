# -*- coding: utf-8 -*-
"""
it defines common functions to dump or read files from filesystem
"""
# Standard Library
from pathlib import Path

# data-initialiser Library
from credential_generator.utilities.file_helpers import save_json_on_disk_by_filepath


def dump_gaiax_object_to_json_ld(gaia_x: dict, object_type: object, context: list[str], output_file: Path) -> None:
    """
    The dump_gaiax_object_to_json_ld function takes a Gaia-X object, object type, prefix, context, and output file path
    as input. It adds a prefix to the keys in the Gaia-X object, replaces a specific key, and saves the modified object
    as a JSON-LD file on disk.

    ### Example Usage
    ```python
    gaia_x = {
        "did_web": "did:web:example",
        "name": "John",
        "age": 25,
        "address": {
            "street": "123 Main St",
            "city": "New York"
        }
    }
    object_type = "Person"
    prefix = "gx:"
    context = {
        "@context": ["http://example.com/gx#"]

    }
    output_file = Path("gaia_x.jsonld")

    dump_gaiax_object_to_json_ld(gaia_x, object_type, prefix, context, output_file)
    ```

    Output: A JSON-LD file named "gaia_x.jsonld" is created on disk with the following content:

    ```python
    {
        "@context": {
            "gx": "http://example.com/gx#"
        }
        "type": "Person",
        "id": "did:web:example.com/1",
        "gx:name": "John",
        "gx:age": 25,
        "gx:address": {
            "gx:street": "123 Main St",
            "gx:city": "New York"
        }
    }
    ```
    :param gaia_x:
    :param object_type:
    :param prefix:
    :param context:
    :param output_file:
    :return:
    """
    if not isinstance(output_file, Path):
        raise TypeError("output_file must be a Path object")

    save_json_on_disk_by_filepath(
        filepath=output_file,
        data=gaia_x | {"@context": context} | {"type": object_type},
    )
