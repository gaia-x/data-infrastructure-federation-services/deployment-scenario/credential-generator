# -*- coding: utf-8 -*-
"""
It defines generators to dump domain objects into json-ld
"""
# Standard Library
import logging
from pathlib import Path

from credential_generator.config import (
    ASTER_CONFORMITY_CONTEXT,
    GX_TRUST_FRAMEWORK_CONTEXT,
    GX_TRUST_FRAMEWORK_PREFIX,
)

# data-initialiser Library
from credential_generator.domain._2210.trust_framework.participant import LegalPerson, LegalRegistrationNumber
from credential_generator.domain._2210.trust_framework.service_offering import ServiceOffering
from credential_generator.domain._2210.trust_framework.terms_and_conditions import GaiaXTermsAndConditions
from credential_generator.infra.spi.filesystem.writer import dump_gaiax_object_to_json_ld
from credential_generator.utilities.file_helpers import save_json_on_disk_by_filepath

log = logging.getLogger(__name__)


def dump_gaiax_terms_and_conditions_to_json_ld(
    terms_and_conditions: GaiaXTermsAndConditions, output_file: Path
) -> None:
    """
    Dumps a GaiaXTermsAndConditions object to json-ld in a file located in participant output directory
    :param terms_and_conditions: GaiaXTermsAndConditions object to dump.
    :param output_file: path of file to be generated
    """
    if not isinstance(terms_and_conditions, GaiaXTermsAndConditions):
        raise TypeError("terms_and_conditions must be a GaiaXTermsAndConditions object")

    log.debug("Generating json-ld from Gaia-x terms and conditions object")

    terms_and_conditions_dict = terms_and_conditions.dict(by_alias=True, exclude_unset=True, exclude_none=True)
    dump_gaiax_object_to_json_ld(
        gaia_x=terms_and_conditions_dict,
        object_type=f"{GX_TRUST_FRAMEWORK_PREFIX}:GaiaXTermsAndConditions",
        context=[GX_TRUST_FRAMEWORK_CONTEXT],
        output_file=output_file,
    )


def dump_participant_to_json_ld(legal_participant: LegalPerson, output_file: Path) -> None:
    """
    Dumps a LegalParticipant object to json-ld in a file located in participant output directory
    :param legal_participant: LegalParticipant object to dump.
    :param output_file: path of file to be generated
    """
    if not isinstance(legal_participant, LegalPerson):
        raise TypeError("legal_participant must be a LegalPerson object")

    log.debug("Generating json-ld from legal person object")

    legal_registration_number_dict = legal_participant.legalRegistrationNumber.dict(
        by_alias=True,
        exclude_unset=True,
        exclude_none=True,
        include={
            "did_web",
        },
    )

    legal_participant_dict = legal_participant.dict(
        by_alias=True, exclude_unset=True, exclude_none=True, exclude={"gx:legalRegistrationNumber"}
    )

    legal_participant_dict = legal_participant_dict | {"gx:legalRegistrationNumber": legal_registration_number_dict}
    dump_gaiax_object_to_json_ld(
        gaia_x=legal_participant_dict,
        object_type=f"{GX_TRUST_FRAMEWORK_PREFIX}:LegalParticipant",
        context=[GX_TRUST_FRAMEWORK_CONTEXT],
        output_file=output_file,
    )


def dump_service_offering_to_json_ld(service_offering: ServiceOffering, output_file: Path) -> None:
    """
    Dumps a ServiceOffering object to json-ld in a file located in participant output directory
    :param output_file:
    :param service_offering: ServiceOffering object to dump.
    """

    if not isinstance(service_offering, ServiceOffering):
        raise TypeError("service_offering must be a ServiceOffering object")

    log.debug("Generating json-ld from service offering object")
    service_offering_dict = service_offering.dict(by_alias=True, exclude_unset=True, exclude_none=True)
    dump_gaiax_object_to_json_ld(
        gaia_x=service_offering_dict,
        object_type=f"{GX_TRUST_FRAMEWORK_PREFIX}:ServiceOffering",
        context=[GX_TRUST_FRAMEWORK_CONTEXT, ASTER_CONFORMITY_CONTEXT],
        output_file=output_file,
    )


def dump_legal_registration_number_vc(legal_registration_number_vc: dict, output_file: Path) -> None:
    """
    The `dump_legal_registration_number_vc` method is a static method that takes a dictionary
    `legal_registration_number_vc` and a file path `output_file` as input. It saves the dictionary as a JSON file
    at the specified file path.

    ### Example Usage
    ```python
    legal_registration_number_vc = {
        "registration_number": "1234567890",
        "registration_date": "2022-01-01"
    }
    output_file = Path("legal_registration.json")

    TrustFrameworkGenerator.dump_legal_registration_number(legal_registration_number_vc, output_file)
    ```
    :param legal_registration_number_vc:
    :param output_file:
    :return:
    """

    if not isinstance(legal_registration_number_vc, dict):
        raise TypeError("legal_registration_number_vc must be a dictionary")

    if not legal_registration_number_vc:
        raise ValueError("legal_registration_number_vc is empty")

    if not isinstance(output_file, Path):
        raise TypeError("output_file must be a Path object")

    log.debug("Dumping legal registration number to json-ld")
    save_json_on_disk_by_filepath(filepath=output_file, data=legal_registration_number_vc)


def dump_legal_registration_number_credential_subject(
    legal_registration_number: LegalRegistrationNumber, output_file: Path
) -> None:
    """
    This code defines a function named dump_legal_registration_number_credential_subject that takes a
    LegalRegistrationNumber object and an output file path as input. It converts the LegalRegistrationNumber object
    into a dictionary, and then calls the dump_gaiax_object_to_json_ld function to save the dictionary as a JSON-LD
    file on disk.

    ```python
    legal_registration_number = LegalRegistrationNumber(...)
    output_file = Path("output.jsonld")
    dump_legal_registration_number_credential_subject(legal_registration_number, output_file)
    ``
    :param legal_registration_number: A LegalRegistrationNumber object representing the legal registration number of
    an entity.
    :param output_file: A Path object representing the file path where the JSON-LD output will be saved.
    :return: None. The function saves the converted LegalRegistrationNumber object as a JSON-LD file on disk.
    """
    if not isinstance(legal_registration_number, LegalRegistrationNumber):
        raise TypeError("legal_registration_number must be a LegalRegistrationNumber")

    if not isinstance(output_file, Path):
        raise TypeError("output_file must be a Path object")

    log.debug("Dumping legal registration number credential subject to json-ld")
    legal_registration_number_dict = legal_registration_number.dict(
        by_alias=True, exclude_unset=True, exclude_none=True
    )

    dump_gaiax_object_to_json_ld(
        gaia_x=legal_registration_number_dict,
        object_type=f"{GX_TRUST_FRAMEWORK_PREFIX}:legalRegistrationNumber",
        context=[GX_TRUST_FRAMEWORK_CONTEXT],
        output_file=output_file,
    )
