# -*- coding: utf-8 -*-
"""
Gaia-X Registry client functions
"""
from __future__ import annotations

import ssl
# Standard Library
from abc import ABC
from dataclasses import dataclass
from enum import Enum
import logging
from typing import Final, Optional
from urllib.parse import quote_plus

import httpx
from tenacity import retry_if_exception_type, stop_after_attempt, wait_exponential, retry

from credential_generator.config import (
    GAIA_X_CLEARING_HOUSE_NOTARY_BASE_URL,
    GAIA_X_CLEARING_HOUSE_REGISTRY_BASE_URL,
    GX_TRUST_FRAMEWORK_CONTEXT,
)

# data-initialiser Library
from credential_generator.domain._2210.trust_framework.participant import LegalRegistrationNumber
from credential_generator.infra.spi.clients import log_attempt_number
from credential_generator.use_cases import RemoteProtocolErrorException, HostNotReachable
from credential_generator.utilities.did_helpers import compute_hash_value

log = logging.getLogger(__name__)

def to_registration_number_request(registration_number: LegalRegistrationNumber) -> dict:
    """
    The `to_registration_number_request` function takes a `LegalRegistrationNumber` object as input and converts it
    into a dictionary with default prefixes added to the keys. It then combines this dictionary with a default context
    and a type annotation to create a JSON-LD representation of the registration number request.

    ### Example Usage
    ```python
    registration_number = LegalRegistrationNumber(local="123456789")
    result = to_registration_number_request(registration_number)
    print(result)
    ```

    Output:
    ```python
    {
        "gx:local": "123456789",
        "@context": {
            "@context": {
                "gx":
                "https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#"
            }
        },
        "@type": "gx:legalRegistrationNumber"
    }
    ```
    :param registration_number:
    :return:
    """
    registration_number_dict = registration_number.model_dump(by_alias=True, exclude_unset=True, exclude_none=True)

    return registration_number_dict | {"@context": GX_TRUST_FRAMEWORK_CONTEXT} | {"type": "gx:legalRegistrationNumber"}


class GaiaXClearingHouseApiVersion(Enum):
    """
    GaiaXClearingHouseApiVersion gives valid values for compliance / registry api version
    """

    MAIN = "main"
    DEVELOPMENT = "development"
    V1_STAGING = "v1-staging"
    V1 = "v1"


class GaiaXClearingHouseRegistryException(Exception):
    """
    GaiaXClearingHouseRegistryException is raised when given Api version is not in GaiaXClearingHouseApiVersion list.
    """


class GaiaXClearingHouseNotaryException(Exception):
    """
    GaiaXClearingHouseNotaryException is raised when given Api version is not in GaiaXClearingHouseApiVersion list.
    """


@dataclass
class TermsAndConditions:
    """
    defines a TermsAndCondition object.
    """

    version: str
    text: str
    hash: Optional[str] = None


class GXCHClient(ABC):
    """
    The `GXCHClient` class is an abstract base class that provides common functionality for accessing the Gaia-X
    Clearing House API. It has two subclasses: `GXCHRegistryClient` and `GXCHNotaryClient`.
    - The `GXCHRegistryClient` class is used to retrieve the terms and conditions from the Gaia-X Clearing House
    Registry API. It has a method `get_gaix_terms_and_conditions` that sends a GET request to the API endpoint and
    returns the terms and conditions as a `TermsAndConditions` object.

    - The `GXCHNotaryClient` class is used to interact with the Gaia-X Clearing House Notary API. It has a method
    `get_registration_number_vc` that sends a POST request to the API to retrieve a registration number verification
    code (VC) for a given legal person. The response is returned as a dictionary.

    ### Example Usage
    ```python
    # Example usage of GXCHRegistryClient
    client = GXCHRegistryClient()
    terms_and_conditions = client.get_gaix_terms_and_conditions()
    print(terms_and_conditions.text)

    # Example usage of GXCHNotaryClient
    legal_person = LegalPerson(
        legalRegistrationNumber=LegalRegistrationNumber(local="123456789"),
        relatedOrganization=None,
        headquarterAddress=Address(...),
        legalAddress=Address(...)
    )

    client = GXCHNotaryClient()
    result = client.get_registration_number_vc(legal_person)
    print(result)
    ```
    """

    _DEFAULT_TIMEOUT: Final = 10.0
    _base_url: Optional[str] = None

    def __init__(self):
        self._client = httpx.Client()


class GXCHRegistryClient(GXCHClient):
    """
    The `GXCHRegistryClient` class is a client for accessing the Gaia-X Clearing House Registry API. It provides a
    method `get_gaix_terms_and_conditions` to retrieve the terms and conditions from the registry.

    ### Example Usage
    ```python
    client = GXCHRegistryClient()
    terms_and_conditions = client.get_gaix_terms_and_conditions()
    print(terms_and_conditions.text)
    ```
    """

    def __init__(self, api_version: GaiaXClearingHouseApiVersion = GaiaXClearingHouseApiVersion.V1_STAGING):
        super().__init__()
        self._base_url = f"{GAIA_X_CLEARING_HOUSE_REGISTRY_BASE_URL}/{api_version.value}/api"

    def get_gaix_terms_and_conditions(self, version: str = "22.10") -> TermsAndConditions:
        """
        The `get_gaix_terms_and_conditions` method is a part of the `GXCHRegistryClient` class. It retrieves the terms
        and conditions from the Gaia-X Clearing House Registry API based on the specified version. It uses the `httpx`
        library to send a GET request to the API endpoint and handles the response based on the status code. If the
        status code is 200 (HTTP.OK), it parses the response JSON and returns a `TermsAndConditions` object.
        If the status code is 400, it raises a `GaiaXClearingHouseRegistryException`. Otherwise, it also raises a
        `GaiaXClearingHouseRegistryException`.

        ### Example Usage
        ```python
        client = GXCHRegistryClient()
        terms_and_conditions = client.get_gaix_terms_and_conditions()
        print(terms_and_conditions.text)
        ```
        :param version: version of registry. By default, it is set to 22.10
        :return: GaiaXTermsAndConditions object
        :raises GaiaXRegistryException if api returns error code different from 200 (HTTP.OK)
        """
        response = self._client.get(
            url=f"{self._base_url}/termsAndConditions?version={version}", timeout=self._DEFAULT_TIMEOUT
        )

        match response.status_code:
            case 200:
                terms_and_conditions = response.json()

                return TermsAndConditions(
                    text=terms_and_conditions["text"],
                    version=terms_and_conditions["version"],
                    hash=terms_and_conditions["hash"]
                    if "hash" in terms_and_conditions
                    else compute_hash_value(bytes(terms_and_conditions["text"], "utf-8")),
                )
            case 400:
                raise GaiaXClearingHouseRegistryException()
            case _:
                raise GaiaXClearingHouseRegistryException()


class GXCHNotaryClient(GXCHClient):
    """
    The `GXCHNotaryClient` class is a client for interacting with the Gaia-X Clearing House Notary API. It provides a
    method `get_registration_number_vc` that sends a POST request to the API to retrieve a registration number
    verification code (VC) for a given legal person. The VC is obtained by constructing a well-known DID web URL and
    sending a JSON payload containing the legal registration number to the API. The response is returned as a
    dictionary.

    ### Example Usage
    ```python
    legal_person = LegalPerson(
        legalRegistrationNumber=LegalRegistrationNumber(local="123456789"),
        relatedOrganization=None,
        headquarterAddress=Address(...),
        legalAddress=Address(...)
    )

    client = GXCHNotaryClient()
    result = client.get_registration_number_vc(legal_person)
    print(result)
    ```

    Output:
    ```
    {
        "vc": "..."
    }
    ```
    """

    def __init__(self, api_version: GaiaXClearingHouseApiVersion = GaiaXClearingHouseApiVersion.V1_STAGING):
        super().__init__()
        self._base_url = f"{GAIA_X_CLEARING_HOUSE_NOTARY_BASE_URL}/{api_version.value}"


    @retry(retry=(retry_if_exception_type(Exception) | retry_if_exception_type(RemoteProtocolErrorException)), stop=stop_after_attempt(7),
           wait=wait_exponential(multiplier=3, min=6, max=120), after=log_attempt_number)
    def get_registration_number_vc(self, did_web: str, legal_registration_number: LegalRegistrationNumber) -> dict:
        """
        The get_registration_number_vc method is used to retrieve a registration number verification code (VC) for a
        given legal person from the Gaia-X Clearing House Notary API.
        ### Example Usage
        ```python
        legal_person = LegalPerson(
            legalRegistrationNumber=LegalRegistrationNumber(local="123456789"),
            relatedOrganization=None,
            headquarterAddress=Address(...),
            legalAddress=Address(...)
        )

        client = GXCHNotaryClient()
        result = client.get_registration_number_vc(legal_person)
        print(result)
        ```

        :param did_web: A string representing the DID web URL.
        :param legal_registration_number: An instance of the LegalRegistrationNumber class representing the legal
        registration number of a legal person.
        :return: A dictionary representing the response JSON from the API, which contains the registration number
        verification code (VC).
        """
        did_web_encoded = quote_plus(did_web)

        body = legal_registration_number.model_dump(
            by_alias=True, exclude_unset=True, exclude_none=True, exclude={"did_web"}
        ) | {"id": legal_registration_number.did_web}

        url = f"{self._base_url}/registrationNumberVC?vcid={did_web_encoded}"

        log.debug(f"Sending request to {url}")
        log.debug(f"Payload {body}")

        try:
            response = self._client.post(url=url, timeout=self._DEFAULT_TIMEOUT, json=body)

            match response.status_code:
                case 200:
                    return response.json()
                case 400:
                    log.debug(response.json())
                    raise GaiaXClearingHouseNotaryException()
                case _:
                    raise GaiaXClearingHouseNotaryException()
        except ssl.SSLCertVerificationError as err:
            raise HostNotReachable(f"{self._base_url} is not reachable: SSL Certificate issue") from err
        except httpx.ConnectError as err:
            raise HostNotReachable(f"{self._base_url} is not reachable: Connection Error") from err
        except httpx.RemoteProtocolError as err:
            raise RemoteProtocolErrorException(f"{self._base_url} is not reachable: Remote Protocol Error") from err
        except Exception as err:
            raise HostNotReachable(f"{self._base_url} is not reachable") from err
        finally:
            self._client.close()
        #
