# -*- coding: utf-8 -*-
"""
Aster-X clients
"""

import http
import json
import logging
import ssl
from pathlib import Path
from typing import Any, Final, Optional

import aiofiles
import httpx
from tenacity import retry, stop_after_attempt, wait_random_exponential, retry_if_exception_type, wait_exponential

from credential_generator.config import GAIA_X_CLEARING_HOUSE_COMPLIANCE_BASE_URL, ASTER_X_GATEWAY_BASE_URL
from credential_generator.domain import OntologyType
from credential_generator.infra.spi.clients import BadRequestException, HostNotReachable, NotAuthorizedException, \
    ResourceConflictException, log_attempt_number
from credential_generator.infra.spi.clients.gaiax_clearing_house import GaiaXClearingHouseApiVersion

log = logging.getLogger(__name__)


class FederatedCatalogueClient:
    """
    The FederatedCatalogueClient class is responsible for interacting with a federated catalogue API. It handles the
    querying federated catalogue database.
    """

    _DEFAULT_TIMEOUT: Final = 30.0

    def __init__(self, base_url: str, port_number: int = 443):
        scheme = "https" if port_number == 443 else "http"
        self._base_url = f"{scheme}://federated-catalogue-api.{base_url}:{port_number}"
        self._default_headers = {"Content-Type": "text/plain", "Accept": "application/json"}
        self._limits = httpx.Limits(max_connections=10, max_keepalive_connections=5)

    def get_provider_list(self) -> list[dict]:
        """
        Get the list of providers.

        :return: list[dict]: The list of providers as JSON objects.
        :except: BadRequestException: If the request is malformed.
        :except: NotAuthorizedException: If the request is not authorized.
        :except: HostNotReachable: If the host is not reachable.

        """
        log.debug("Get provider list")

        transport = httpx.AsyncHTTPTransport(retries=5)
        client = httpx.AsyncClient(transport=transport)

        try:
            api_url = f"{self._base_url}/api/providers"
            response = client.get(url=api_url, timeout=self._DEFAULT_TIMEOUT, headers=self._default_headers)

            match response.status_code:
                case httpx.codes.OK:
                    return response.json()
                case httpx.codes.BAD_REQUEST:
                    raise BadRequestException(str(response.content))
                case httpx.codes.FORBIDDEN:
                    raise NotAuthorizedException(response.json()["message"])
        except ssl.SSLCertVerificationError as err:
            raise HostNotReachable(f"{self._base_url} is not reachable: SSL Certificate issue") from err
        except httpx.ConnectError as err:
            raise HostNotReachable(f"{self._base_url} is not reachable: Connection Error") from err
        except Exception as err:
            raise HostNotReachable(f"{self._base_url} is not reachable") from err
        finally:
            client.close()

    def ping(self) -> bool:
        """
        The ping method in the FederatedCatalogyeClient class is responsible for pinging the API to check its
        health status. It sends a GET request to the /healthcheck endpoint and handles different HTTP status codes and
        connection errors.

        :return: True if the request is successful.
        :raises: Raises a BadRequestException if the status code is 400 (Bad Request).
        :raises: Raises a NotAuthorizedException if the status code is 403 (Forbidden).
        :raises: Raises a HostNotReachable exception if there is an SSL or connection error, or any other exception
        occurs.
        """

        api_url = f"{self._base_url}/healthcheck"
        transport = httpx.AsyncHTTPTransport(retries=5)
        client = httpx.AsyncClient(transport=transport)

        try:
            response = client.get(
                url=api_url,
                timeout=self._DEFAULT_TIMEOUT,
                headers=self._default_headers,
            )

            match response.status_code:
                case http.HTTPStatus.OK:
                    return True
                case http.HTTPStatus.BAD_REQUEST:
                    raise BadRequestException(str(response.content))
                case http.HTTPStatus.FORBIDDEN:
                    raise NotAuthorizedException(response.json()["message"])
                case _:
                    raise HostNotReachable(f"{self._base_url} is not reachable: {response.status_code}")
        except ssl.SSLCertVerificationError as err:
            raise HostNotReachable(f"{self._base_url} is not reachable: SSL Certificate issue") from err
        except httpx.ConnectError as err:
            raise HostNotReachable(f"{self._base_url} is not reachable: Connection Error") from err
        except Exception as err:
            raise HostNotReachable(f"{self._base_url} is not reachable") from err
        finally:
            client.close()


class ProviderCatalogueClient:
    """
    The ProviderCatalogueClient class is responsible for interacting with a provider catalogue API. It handles the
    indexing of virtual credentials (VCs) and provides error handling for various scenarios.

    ## Example Usage
    ```python
    client = ProviderCatalogueClient(base_url="example.com", api_key="12345")
    await client.index_vc(did_web="did:example:12345", enable_ces_sync=True)
    ```

    ## Main functionalities
    - Initializes the client with the base URL, API key, and optional port number.
    - Handles the indexing of VCs by making a POST request to the catalogue API.
    - Sets default headers for the request, including the API key and content type.
    - Handles different HTTP response codes and raises custom exceptions accordingly.
    - Handles SSL certificate verification errors and connection errors.
    """

    _DEFAULT_TIMEOUT: Final = 30.0

    def __init__(self, base_url: str, api_key: str, port_number: int = 443):
        scheme = "https" if port_number == 443 else "http"
        self._base_url = f"{scheme}://catalogue-api.{base_url}:{port_number}"
        self._default_headers = {
            "x-api-key": api_key,
            "Content-Type": "text/plain",
        }
        self._limits = httpx.Limits(max_connections=10, max_keepalive_connections=5)

    async def index_vc(self, did_web: str, enable_ces_sync: bool = False) -> str:
        """
        The index_vc method is responsible for indexing virtual credentials (VCs) by making a POST request to a
        provider catalogue API. It handles different HTTP response codes and raises custom exceptions accordingly.

        ## Example Usage
        ```python
        client = ProviderCatalogueClient(base_url="example.com", api_key="12345")
        await client.index_vc(did_web="did:example:12345", enable_ces_sync=True)
        ```

        :param did_web (str): The DID web to be indexed.
        :param enable_ces_sync (bool): Flag to enable CES synchronization (default is False).
        :return:
        """
        log.debug(f"Index VC {did_web} ")

        transport = httpx.AsyncHTTPTransport(retries=5)
        client = httpx.AsyncClient(transport=transport)

        try:
            api_url = f"{self._base_url}/catalog/items/?publish_to_catalog=True&publish_to_ces={enable_ces_sync}"
            response = await client.post(
                url=api_url, timeout=self._DEFAULT_TIMEOUT, headers=self._default_headers, content=did_web
            )

            match response.status_code:
                case httpx.codes.OK:
                    return response.text
                case httpx.codes.BAD_REQUEST:
                    raise BadRequestException(str(response.content))
                case httpx.codes.FORBIDDEN:
                    raise NotAuthorizedException(response.json()["message"])
        except ssl.SSLCertVerificationError as err:
            raise HostNotReachable(f"{self._base_url} is not reachable: SSL Certificate issue") from err
        except httpx.ConnectError as err:
            raise HostNotReachable(f"{self._base_url} is not reachable: Connection Error") from err
        except Exception as err:
            raise HostNotReachable(f"{self._base_url} is not reachable") from err
        finally:
            await client.aclose()

    async def index_vcs(self, did_webs: list, enable_catalogue_sync: bool = True, enable_ces_sync: bool = False) -> str:
        """
        The index_vc method is responsible for indexing virtual credentials (VCs) by making a POST request to a
        provider catalogue API. It handles different HTTP response codes and raises custom exceptions accordingly.

        ## Example Usage
        ```python
        client = ProviderCatalogueClient(base_url="example.com", api_key="12345")
        await client.index_vc(did_web="did:example:12345", enable_ces_sync=True)
        ```

        :param did_webs: (list) The DID web to be indexed.
        :param enable_ces_sync: (bool) Flag to enable CES synchronization (default is False).
        :return:
        """
        log.debug(f"Index VCs {did_webs} ")

        if did_webs is None or not isinstance(did_webs, list):
            raise TypeError

        transport = httpx.AsyncHTTPTransport(retries=5)
        client = httpx.AsyncClient(transport=transport)

        try:
            headers = self._default_headers
            headers["Content-Type"] = "application/json"
            api_url = (
                f"{self._base_url}/catalogue/items/"
                f"?publish_to_catalogue={enable_catalogue_sync}&publish_to_ces={enable_ces_sync}"
            )
            response = await client.post(
                url=api_url, timeout=self._DEFAULT_TIMEOUT, headers=headers, content=json.dumps(did_webs)
            )

            match response.status_code:
                case httpx.codes.OK:
                    return response.text
                case httpx.codes.BAD_REQUEST:
                    raise BadRequestException(str(response.content))
                case httpx.codes.FORBIDDEN:
                    raise NotAuthorizedException(response.json()["message"])
        except ssl.SSLCertVerificationError as err:
            raise HostNotReachable(f"{self._base_url} is not reachable: SSL Certificate issue") from err
        except httpx.ConnectError as err:
            raise HostNotReachable(f"{self._base_url} is not reachable: Connection Error") from err
        except Exception as err:
            raise HostNotReachable(f"{self._base_url} is not reachable") from err
        finally:
            await client.aclose()

    async def ping(self) -> bool:
        """
        The ping method in the UserAgentClient class is responsible for pinging the participant agent API to check its
        health status. It sends a GET request to the /healthcheck endpoint and handles different HTTP status codes and
        connection errors.

        :return: True if the request is successful.
        :raises: Raises a BadRequestException if the status code is 400 (Bad Request).
        :raises: Raises a NotAuthorizedException if the status code is 403 (Forbidden).
        :raises: Raises a HostNotReachable exception if there is an SSL or connection error, or any other exception
        occurs.
        """

        api_url = f"{self._base_url}/healthcheck"
        transport = httpx.AsyncHTTPTransport(retries=5)
        client = httpx.AsyncClient(transport=transport)

        try:
            response = await client.get(
                url=api_url,
                timeout=self._DEFAULT_TIMEOUT,
                headers=self._default_headers,
            )

            match response.status_code:
                case http.HTTPStatus.OK:
                    return True
                case http.HTTPStatus.BAD_REQUEST:
                    raise BadRequestException(str(response.content))
                case http.HTTPStatus.FORBIDDEN:
                    raise NotAuthorizedException(response.json()["message"])
                case _:
                    raise HostNotReachable(f"{self._base_url} is not reachable: {response.status_code}")
        except ssl.SSLCertVerificationError as err:
            raise HostNotReachable(f"{self._base_url} is not reachable: SSL Certificate issue") from err
        except httpx.ConnectError as err:
            raise HostNotReachable(f"{self._base_url} is not reachable: Connection Error") from err
        except Exception as err:
            raise HostNotReachable(f"{self._base_url} is not reachable") from err
        finally:
            await client.aclose()


class UserAgentClient:
    """
    The UserAgentClient class is responsible for interacting with a participant agent API. It provides methods for
    pinging the agent and storing objects in the agent. It also handles exceptions for different HTTP status codes and
    connection errors.

    ### Main functionalities
    Pinging the participant agent to check its health status.
    Storing objects in the participant agent by sending a POST request to the corresponding API endpoint.
    """

    _DEFAULT_TIMEOUT: Final = 120.0

    def __init__(self, url: str, api_key: str, port_number: int = 443):
        scheme = "https" if port_number == 443 else "http"
        self._base_url = f"{scheme}://{url}:{port_number}"

        if api_key is not None:
            self._default_headers = {
                "x-api-key": api_key,
                "Content-Type": "application/json",
            }
        else:
            self._default_headers = {
                "Content-Type": "application/json",
            }

        self._limits = httpx.Limits(max_connections=10, max_keepalive_connections=5)

    async def ping(self) -> bool:
        """
        The ping method in the UserAgentClient class is responsible for pinging the participant agent API to check its
        health status. It sends a GET request to the /healthcheck endpoint and handles different HTTP status codes and
        connection errors.

        :return: True if the request is successful.
        :raises: Raises a BadRequestException if the status code is 400 (Bad Request).
        :raises: Raises a NotAuthorizedException if the status code is 403 (Forbidden).
        :raises: Raises a HostNotReachable exception if there is an SSL or connection error, or any other exception
        occurs.
        """

        api_url = f"{self._base_url}/healthcheck"
        transport = httpx.AsyncHTTPTransport(retries=5)
        client = httpx.AsyncClient(transport=transport)

        try:
            response = await client.get(
                url=api_url,
                timeout=self._DEFAULT_TIMEOUT,
                headers=self._default_headers,
            )

            match response.status_code:
                case http.HTTPStatus.OK:
                    return True
                case http.HTTPStatus.BAD_REQUEST:
                    raise BadRequestException(str(response.content))
                case http.HTTPStatus.FORBIDDEN:
                    raise NotAuthorizedException(response.json()["message"])
                case _:
                    raise HostNotReachable(f"{self._base_url} is not reachable: {response.status_code}")
        except ssl.SSLCertVerificationError as err:
            log.error("SSLCertVerificationError")
            raise HostNotReachable(f"{self._base_url} is not reachable: SSL Certificate issue") from err
        except httpx.ConnectError as err:
            log.error(f"ConnectError for {api_url}: {err}")
            raise HostNotReachable(f"{self._base_url} is not reachable: Connection Error") from err
        except Exception as err:
            log.error(f"Exception: {str(err)}")
            raise HostNotReachable(f"{self._base_url} is not reachable") from err
        finally:
            await client.aclose()

    async def bootstrap(self) -> str:
        """
        The bootstrap method in the UserAgentClient class is responsible for bootstrapping a participant agent by
        sending a POST request to the /api/bootstrap-provider API endpoint. It handles different HTTP status codes and
        raises exceptions accordingly.

        :return: content from user agent call
        """
        log.info(f"Bootrapping participant agent {self._base_url}")

        transport = httpx.AsyncHTTPTransport(retries=5)
        client = httpx.AsyncClient(transport=transport)

        try:
            api_url = f"{self._base_url}/api/bootstrap-provider"
            response = await client.post(
                url=api_url,
                timeout=self._DEFAULT_TIMEOUT,
                headers=self._default_headers,
            )

            match response.status_code:
                case http.HTTPStatus.OK:
                    return response.text
                case http.HTTPStatus.BAD_REQUEST:
                    raise BadRequestException(str(response.content))
                case http.HTTPStatus.FORBIDDEN:
                    raise NotAuthorizedException(response.json()["message"])
        except ssl.SSLCertVerificationError as err:
            raise HostNotReachable(f"{self._base_url} is not reachable: SSL Certificate issue") from err
        except httpx.ConnectError as err:
            raise HostNotReachable(f"{self._base_url} is not reachable: Connection Error") from err
        except Exception as err:
            raise HostNotReachable(f"{self._base_url} is not reachable") from err
        finally:
            await client.aclose()


    @retry(retry=retry_if_exception_type(Exception), stop=stop_after_attempt(5), wait=wait_exponential(multiplier=3, min=6, max=120), after=log_attempt_number)
    async def store_object(self, filename: Path) -> Any:
        """
        The store_object method is a part of the UserAgentClient class. It is responsible for storing an object in a
        participant agent by sending a POST request to the corresponding API endpoint. The method takes a file name as
        input, reads the JSON data from the file, extracts the object type from the JSON data, and sends the object
        data along with the object type to the API endpoint.
        Example Usage
        ```python
        # Initialize the UserAgentClient object
        client = UserAgentClient(participant_name="participant", parent_domain_name="example.com", api_key="API_KEY")

        # Call the store_object method
        result = await client.store_object(filename="data.json")

        # Print the result
        print(result)
        ```
        :param filename: The path to the JSON file containing the object data
        :return: The JSON response from the API endpoint if the request is successful.
        :raise: Raises a BadRequestException if the status code is BAD_REQUEST.
        :raise: Raises a NotAuthorizedException if the status code is FORBIDDEN.
        :raise: Raises a HostNotReachable exception if there is an SSL or connection error, or any other exception
        occurs.
        """
        log.info(f"Storing (file: {filename}) to participant agent {self._base_url}")
        if filename is None or not filename.exists():
            raise FileNotFoundError()

        transport = httpx.AsyncHTTPTransport(retries=5)
        client = httpx.AsyncClient(transport=transport)

        try:
            async with aiofiles.open(file=filename, mode="r", encoding="utf-8") as file:
                contents = await file.read()

            jsonld = json.loads(contents)
            object_type = None if jsonld is None else jsonld.get("@type", jsonld.get("type"))
            if object_type is None:
                await client.aclose()
                raise TypeError(f"{filename} does not contain JSON-LD.")

            ontology = OntologyType.from_jsonld_type(object_type=object_type)
            log.debug(f"!!! Here is the ID of the object to be downloaded: {jsonld['id']}")
            api_url = f"{self._base_url}/api/store_object/{ontology.value}"
            response = await client.post(
                url=api_url,
                timeout=self._DEFAULT_TIMEOUT,
                json={"objectjson": jsonld},
                headers=self._default_headers,
            )
            ## log.info(json.dumps(jsonld))
            log.debug(response.status_code)
            match response.status_code:
                case http.HTTPStatus.OK:
                    return response.json()
                case http.HTTPStatus.BAD_REQUEST:
                    raise BadRequestException(str(response.content))
                case http.HTTPStatus.FORBIDDEN:
                    raise NotAuthorizedException(response.json()["message"])
        except ssl.SSLCertVerificationError as err:
            raise HostNotReachable(f"{self._base_url} is not reachable: SSL Certificate issue") from err
        except httpx.ConnectError as err:
            #log.exception(err)
            raise HostNotReachable(f"{self._base_url} is not reachable: Connection Error") from err
        except Exception as err:
            #log.exception(err)
            raise HostNotReachable(f"{self._base_url} is not reachable") from err
        finally:
            await client.aclose()


    @retry(retry=retry_if_exception_type(Exception), stop=stop_after_attempt(5), wait=wait_exponential(multiplier=3, min=6, max=120), after=log_attempt_number)
    async def sign_vc_with_content(self, jsonld: dict[str, Any]) -> Any:
        """
        The sign_vc_with_content method in the UserAgentClient class is responsible for sending a PUT request to the
        /api/vc-request API endpoint to sign a Verifiable Credential (VC) with the provided JSON-LD content.
        It handles different HTTP status codes and raises exceptions accordingly.
        ## Example Usage
        ```
        client = UserAgentClient(url="example.com", api_key="API_KEY")
        jsonld = {
            "subject": "Alice",
            "issuer": "Bob",
            "data": "..."
        }
        result = await client.sign_vc_with_content(jsonld)
        print(result)
        ```
        :param jsonld: The JSON-LD content of the Verifiable Credential to be signed.
        :return:The method returns the JSON response from the API endpoint if the request is successful.
        """
        transport = httpx.AsyncHTTPTransport(retries=5)
        client = httpx.AsyncClient(transport=transport)

        try:
            api_url = f"{self._base_url}/api/vc-request"
            response = await client.put(
                url=api_url,
                timeout=self._DEFAULT_TIMEOUT,
                json=jsonld,
                headers=self._default_headers,
            )

            match response.status_code:
                case http.HTTPStatus.OK:
                    return response.json()
                case http.HTTPStatus.BAD_REQUEST:
                    raise BadRequestException(str(response.content))
                case http.HTTPStatus.FORBIDDEN:
                    raise NotAuthorizedException(response.json()["message"])

        except ssl.SSLCertVerificationError as err:
            raise HostNotReachable(f"{self._base_url} is not reachable: SSL Certificate issue") from err
        except httpx.ConnectError as err:
            raise HostNotReachable(f"{self._base_url} is not reachable: Connection Error") from err
        except Exception as err:
            raise HostNotReachable(f"{self._base_url} is not reachable") from err
        finally:
            await client.aclose()


    async def sign_vc(self, filename: Path) -> Any:
        """
        The sign_vc method in the UserAgentClient class is responsible for signing a Verifiable Credential (VC) by
        sending a PUT request to the /api/vc-request API endpoint. It reads the JSON data from a file, sends the data
        to the API endpoint, and handles different HTTP status codes.

        :param filename: The path to the JSON file containing the Verifiable Credential data.
        :return The method returns the JSON response from the API endpoint if the request is successful.
        Otherwise, it raises a BadRequestException if the status code is 400, or a NotAuthorizedException if the status
        code is 403.
        :raise: Raises a BadRequestException if the status code is BAD_REQUEST.
        :raise: Raises a NotAuthorizedException if the status code is FORBIDDEN.
        :raise: Raises a HostNotReachable exception if there is an SSL or connection error, or any other exception
        occurs.
        """
        log.info(f"Signing VC (file: {filename})")
        if filename is None or not filename.exists():
            raise FileNotFoundError()

        async with aiofiles.open(file=filename, mode="r", encoding="utf-8") as file:
            contents = await file.read()

        jsonld = json.loads(contents)
        return await self.sign_vc_with_content(jsonld=jsonld)

    @retry(retry=(retry_if_exception_type(Exception) | retry_if_exception_type(ResourceConflictException)), stop=stop_after_attempt(7), wait=wait_exponential(multiplier=3, min=6, max=120), after=log_attempt_number)
    async def call_compliance(
        self,
        object_type: str,
        compliance_url: str = GAIA_X_CLEARING_HOUSE_COMPLIANCE_BASE_URL,
        compliance_version: str = GaiaXClearingHouseApiVersion.V1_STAGING.value,
        vc_id: Optional[str] = "",
    ) -> Any:
        """
        The call_compliance method in the UserAgentClient class is responsible for making a POST request to a compliance
        API endpoint. It takes in the object type, compliance URL, compliance version, and VC ID as inputs. It handles
        different HTTP status codes and raises exceptions accordingly.

        ### Example Usage
        ```python
        client = UserAgentClient(participant_name="participant", parent_domain_name="example.com", api_key="API_KEY")
        result = await client.call_compliance(object_type="object_type", compliance_url="https://compliance.gaia-x.eu",
        compliance_version="development", vc_id="vc_id")
        print(result)
        ```

        :param object_type: The type of the object being sent to the compliance API.
        :param compliance_url: The URL of the compliance API. Default is "https://compliance.gaia-x.eu".
        :param compliance_version: The version of the compliance API. Default is "development".
        :param vc_id: The ID of the Verifiable Credential. Default is an empty string.
        :return: The method returns the JSON response from the compliance API if the request is successful. Otherwise,
        it raises a BadRequestException if the status code is 400, or a NotAuthorizedException if the status code is
        403.
        """
        log.info(f"Calling compliance (object_type: {object_type} and vc_id={vc_id})")

        transport = httpx.AsyncHTTPTransport(retries=5)
        client = httpx.AsyncClient(transport=transport)

        configuration = {
            "compliance_url": compliance_url,
            "compliance_version": compliance_version,
            "vc_id": vc_id,
        }

        log.debug(f"parameters = {configuration}")

        try:
            api_url = f"{self._base_url}/api/call_compliance/{object_type}"
            response = await client.post(
                url=api_url,
                timeout=self._DEFAULT_TIMEOUT,
                json=configuration,
                headers=self._default_headers,
            )

            log.info(f"=> status code = {response.status_code}")
            log.debug(f"=> response = {response.content}")
            match response.status_code:
                case http.HTTPStatus.OK | http.HTTPStatus.CREATED:
                    return response.json()
                case http.HTTPStatus.BAD_REQUEST:
                    raise BadRequestException(str(response.content))
                case http.HTTPStatus.FORBIDDEN:
                    raise NotAuthorizedException(response.json()["message"])
                case http.HTTPStatus.CONFLICT:
                    raise ResourceConflictException(response.json())

        except ssl.SSLCertVerificationError as err:
            raise HostNotReachable(f"{self._base_url} is not reachable: SSL Certificate issue") from err
        except httpx.ConnectError as err:
            raise HostNotReachable(f"{self._base_url} is not reachable: Connection Error") from err
        finally:
            await client.aclose()

    @retry(retry=retry_if_exception_type(Exception), stop=stop_after_attempt(5), wait=wait_exponential(multiplier=3, min=6, max=120), after=log_attempt_number)
    async def sign_vp_with_content(self, jsonld: list[dict], params: dict = None) -> Any:
        """
        The sign_vp_with_content method in the UserAgentClient class is responsible for making a POST request to a
        vp request API endpoint.

        :param params: query parameters
        :param jsonld: a jsonld representation including VCs to present in a VP
        :return: The method returns the JSON response from the compliance API if the request is successful. Otherwise,
        it raises a BadRequestException if the status code is 400, or a NotAuthorizedException if the status code is
        403.
        """
        transport = httpx.AsyncHTTPTransport(retries=5)
        client = httpx.AsyncClient(transport=transport)

        try:
            api_url = f"{self._base_url}/api/vp-request"
            response = await client.put(
                url=api_url,
                params=params,
                timeout=self._DEFAULT_TIMEOUT,
                json=jsonld,
                headers=self._default_headers,
            )

            match response.status_code:
                case http.HTTPStatus.OK:
                    return response.json()
                case http.HTTPStatus.BAD_REQUEST:
                    raise BadRequestException(str(response.content))
                case http.HTTPStatus.FORBIDDEN:
                    raise NotAuthorizedException(response.json()["message"])

        except ssl.SSLCertVerificationError as err:
            raise HostNotReachable(f"{self._base_url} is not reachable: SSL Certificate issue") from err
        except httpx.ConnectError as err:
            raise HostNotReachable(f"{self._base_url} is not reachable: Connection Error") from err
        except Exception as err:
            raise HostNotReachable(f"{self._base_url} is not reachable") from err
        finally:
            await client.aclose()

    @retry(retry=retry_if_exception_type(Exception), stop=stop_after_attempt(5), wait=wait_exponential(multiplier=3, min=6, max=120), after=log_attempt_number)
    async def get_vc(self, vc_url: str) -> dict:
        """
        The get_vc method is responsible to get a vc from its url

        :param vc_url: URL of a VC to get
        :return: A VC if found in server.
        """
        transport = httpx.AsyncHTTPTransport(retries=5)
        client = httpx.AsyncClient(transport=transport)

        try:
            response = await client.get(
                url=vc_url,
                timeout=self._DEFAULT_TIMEOUT,
                headers=self._default_headers,
            )

            match response.status_code:
                case http.HTTPStatus.OK:
                    return response.json()
                case http.HTTPStatus.BAD_REQUEST:
                    raise BadRequestException(str(response.content))
                case http.HTTPStatus.FORBIDDEN:
                    raise NotAuthorizedException(response.json()["message"])

        except ssl.SSLCertVerificationError as err:
            raise HostNotReachable(f"{self._base_url} is not reachable: SSL Certificate issue") from err
        except httpx.ConnectError as err:
            raise HostNotReachable(f"{self._base_url} is not reachable: Connection Error") from err
        except Exception as err:
            raise HostNotReachable(f"{self._base_url} is not reachable") from err
        finally:
            await client.aclose()

    @retry(retry=retry_if_exception_type(Exception), stop=stop_after_attempt(5), wait=wait_exponential(multiplier=3, min=6, max=120), after=log_attempt_number)
    async def clear_cache(
        self,
    ) -> Any:
        """
        The clear_cache method in the UserAgentClient class is responsible for making a DELETE request to a compliance
        API endpoint.
        :return: The method returns the JSON response from the compliance API if the request is successful. Otherwise,
        it raises a BadRequestException if the status code is 400, or a NotAuthorizedException if the status code is
        403.
        """
        log.info("Clearing cache on participant agent")

        transport = httpx.AsyncHTTPTransport(retries=5)
        client = httpx.AsyncClient(transport=transport)

        try:
            api_url = f"{self._base_url}/api/cache"
            response = await client.delete(
                url=api_url,
                timeout=self._DEFAULT_TIMEOUT,
                headers=self._default_headers,
            )

            log.info(f"=> status code = {response.status_code}")
            log.info(f"=> response = {response.content}")
            match response.status_code:
                case http.HTTPStatus.OK | http.HTTPStatus.CREATED:
                    return response.json()
                case http.HTTPStatus.BAD_REQUEST:
                    raise BadRequestException(str(response.content))
                case http.HTTPStatus.FORBIDDEN:
                    raise NotAuthorizedException(response.json()["message"])

        except ssl.SSLCertVerificationError as err:
            raise HostNotReachable(f"{self._base_url} is not reachable: SSL Certificate issue") from err
        except httpx.ConnectError as err:
            raise HostNotReachable(f"{self._base_url} is not reachable: Connection Error") from err
        except Exception as err:
            raise HostNotReachable(f"{self._base_url} is not reachable") from err
        finally:
            await client.aclose()


class LabellingAgentClient:
    """
    Client to call labelling microservice
    """

    _DEFAULT_TIMEOUT: Final = 30.0

    def __init__(self, base_url: str, port_number: int = 443):
        scheme = "https" if port_number == 443 else "http"
        self._base_url = f"{scheme}://labelling.{base_url}:{port_number}"
        self._default_headers = {
            "Content-Type": "application/json",
        }
        self._limits = httpx.Limits(max_connections=10, max_keepalive_connections=5)

    async def check_labels(self, vp_request: dict[str, Any]) -> dict[str, Any]:
        """
        Call labelling microservice to check if a service has gaia-x labelling
        :param vp_request:
        :return:
        """
        transport = httpx.AsyncHTTPTransport(retries=5)
        client = httpx.AsyncClient(transport=transport)

        try:
            response = await client.post(
                url=self._base_url, timeout=self._DEFAULT_TIMEOUT, headers=self._default_headers, json=vp_request
            )

            match response.status_code:
                case httpx.codes.OK:
                    return response.json()
                case httpx.codes.BAD_REQUEST:
                    raise BadRequestException(str(response.content))
                case httpx.codes.FORBIDDEN:
                    raise NotAuthorizedException(response.json()["message"])
        except ssl.SSLCertVerificationError as err:
            raise HostNotReachable(f"{self._base_url} is not reachable: SSL Certificate issue") from err
        except httpx.ConnectError as err:
            raise HostNotReachable(f"{self._base_url} is not reachable: Connection Error") from err
        except Exception as err:
            raise HostNotReachable(f"{self._base_url} is not reachable") from err
        finally:
            await client.aclose()


class AsterXGatewayClient:
    """
    The AsterXGatewayClient class is responsible for interacting with an Aster-X Gateway API. It provides methods for
    call services provided by Aster-X federation.

    ### Main functionalities
     - Authenticate
     - Initiate Aster-X compliance
     - Ask Aster-X compliance
    """

    _DEFAULT_TIMEOUT: Final = 120.0

    def __init__(self, port_number: int = 443):
        scheme = "https" if port_number == 443 else "http"
        self._base_url = f"{scheme}://{ASTER_X_GATEWAY_BASE_URL}:{port_number}"
        self._default_headers = {
            "Content-Type": "application/json",
        }
        self._limits = httpx.Limits(max_connections=10, max_keepalive_connections=5)

    async def authenticate(self, membership_vc: dict) -> dict:
        """
        The authenticate method is responsible to get access_token from Aster-X federation thanks to Membership VC

        :param membership_vc:
        :return: A VC if found in server.
        """
        api_url = f"{self._base_url}/authenticate"
        client = httpx.AsyncClient()

        try:
            response = await client.post(
                url=api_url,
                json=membership_vc,
                timeout=self._DEFAULT_TIMEOUT,
                headers=self._default_headers,
            )

            match response.status_code:
                case http.HTTPStatus.OK:
                    return response.json()
                case http.HTTPStatus.BAD_REQUEST:
                    raise BadRequestException(str(response.content))
                case http.HTTPStatus.FORBIDDEN:
                    raise NotAuthorizedException(response.json()["message"])

        except ssl.SSLCertVerificationError as err:
            raise HostNotReachable(f"{self._base_url} is not reachable: SSL Certificate issue") from err
        except httpx.ConnectError as err:
            raise HostNotReachable(f"{self._base_url} is not reachable: Connection Error") from err
        except Exception as err:
            raise HostNotReachable(f"{self._base_url} is not reachable") from err
        finally:
            await client.aclose()

    async def initiate_compliance(self) -> dict:
        """
        The initiate_compliance method is responsible to get a session token able to call compliance

        :return: A session token
        """
        api_url = f"{self._base_url}/compliance/initiate"
        client = httpx.AsyncClient()

        try:
            response = await client.post(
                url=api_url,
                timeout=self._DEFAULT_TIMEOUT,
                headers=self._default_headers,
            )

            match response.status_code:
                case http.HTTPStatus.OK:
                    return response.json()
                case http.HTTPStatus.BAD_REQUEST:
                    raise BadRequestException(str(response.content))
                case http.HTTPStatus.FORBIDDEN:
                    raise NotAuthorizedException(response.json()["message"])

        except ssl.SSLCertVerificationError as err:
            raise HostNotReachable(f"{self._base_url} is not reachable: SSL Certificate issue") from err
        except httpx.ConnectError as err:
            raise HostNotReachable(f"{self._base_url} is not reachable: Connection Error") from err
        except Exception as err:
            raise HostNotReachable(f"{self._base_url} is not reachable") from err
        finally:
            await client.aclose()

    async def call_aster_x_service_compliance(self, content: dict, vp_token: str) -> dict:
        """
        The call_aster_x_service_compliance method is responsible to ask compliance for a service

        :return: A VC if found in server.
        """
        api_url = f"{self._base_url}/compliance/service"
        client = httpx.AsyncClient()

        try:
            headers = {
                "Content-Type": "application/json",
                "Authorization": f"Bearer {vp_token}"
            }

            response = await client.post(
                url=api_url,
                content=json.dumps(content),
                timeout=self._DEFAULT_TIMEOUT,
                headers=headers
            )

            match response.status_code:
                case http.HTTPStatus.OK:
                    return response.json()
                case http.HTTPStatus.BAD_REQUEST:
                    raise BadRequestException(str(response.content))
                case http.HTTPStatus.FORBIDDEN:
                    raise NotAuthorizedException(response.json()["message"])

        except ssl.SSLCertVerificationError as err:
            raise HostNotReachable(f"{self._base_url} is not reachable: SSL Certificate issue") from err
        except httpx.ConnectError as err:
            raise HostNotReachable(f"{self._base_url} is not reachable: Connection Error") from err
        except Exception as err:
            log.error(err)
            raise HostNotReachable(f"{self._base_url} is not reachable") from err
        finally:
            await client.aclose()
