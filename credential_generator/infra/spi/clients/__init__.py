# -*- coding: utf-8 -*-
"""
It defines common exceptions
"""
import logging


class BadRequestException(Exception):
    """
    The BadRequestException class is a custom exception class that is raised when a HTTP request returns a status code
    of 400 (Bad Request). It inherits from the base Exception class and takes a message as input.
    """

    def __init__(self, message: str) -> None:
        super().__init__(message)


class NotAuthorizedException(Exception):
    """
    The NotAuthorizedException class is a custom exception class that is raised when the API returns a status code of
    403 (FORBIDDEN). It inherits from the base Exception class and overrides the __init__ method to set the error
    message.
    """

    def __init__(self, message: str) -> None:
        super().__init__(message)


class HostNotReachable(Exception):
    """
    The HostNotReachable class is a custom exception class that is raised when a host is not reachable. It inherits
    from the base Exception class and takes a message as input.
    """

    def __init__(self, message: str) -> None:
        super().__init__(message)

class ResourceConflictException(Exception):
    """
    The HostNotReachable class is a custom exception class that is raised when a host is not reachable. It inherits
    from the base Exception class and takes a message as input.
    """

    def __init__(self, message: str) -> None:
        super().__init__(message)


def log_attempt_number(retry_state):
    """return the result of the last call attempt"""
    logging.error(f"Retrying: {retry_state.attempt_number}...")
