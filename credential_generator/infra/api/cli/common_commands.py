# -*- coding: utf-8 -*-
"""
It defines command utilities
"""
import logging
from pathlib import Path
from typing import Optional

from click.exceptions import Exit
from rich.console import Console, ConsoleRenderable
from rich.panel import Panel
from typer import Option

from credential_generator.config import (
    DEFAULT_AUDITOR_PARENT_DOMAIN_NAME,
    DEFAULT_CSV_FILE_DIRECTORY,
    DEFAULT_DATABASE_FILENAME,
    DEFAULT_PARENT_DOMAIN_NAME,
    DEFAULT_PROVIDER_PARENT_DOMAIN_NAME,
    DEFAULT_PUBLIC_DIRECTORY,
    GaiaxParticipantRole,
)
from credential_generator.use_cases.auditor import AuditorUseCase
from credential_generator.use_cases.federation import FederationUseCase
from credential_generator.use_cases.gaiax_participant import LegalParticipantUseCase
from credential_generator.use_cases.provider import ProviderUseCase

log = logging.getLogger(__name__)

CATALOGUE_API_KEY_OPTION_HELP = "Catalogue api key"
FED_CATALOGUE_API_KEY_OPTION_HELP = "Federation Catalogue api key"
FEDERATION_NAME_OPTION_HELP = "Federation name."
USER_AGENT_API_KEY_OPTION_HELP = "User agent api key"
MEMBERSHIP_VC_ID_OPTION_HELP = "Membership VC id"
AUDITOR_USER_AGENT_API_KEY_OPTION_HELP = "Auditor agent api key to sign provider's VC"
CSV_ROOT_DIRECTORY_OPTION_HELP = "Directory where csv files can be found"
ENABLE_GAIAX_COMPLIANCE_OPTION_HELP = "Enable call to Gaia-X compliance"
ENABLE_GAIA_CES_CALL_OPTION_HELP = "Enable call to Gaia-X CES"
DATABASE_FILENAME_OPTION_HELP = "Name of database to generate"
AUDITOR_NAME_OPTION_HELP = "Auditor's name."
PARENT_DOMAIN_OPTION_HELP = "Parent domain name to generate proper did web"
FEDERATION_PARENT_DOMAIN_OPTION_HELP = "Parent domain name of federation to generate proper did web"
JSONLD_ROOT_DIRECTORY_OPTION_HELP = "Directory where generated files can be found"

CSV_ROOT_DIRECTORY_OPTION = Option(
    DEFAULT_CSV_FILE_DIRECTORY,
    "--input-root-directory",
    "-i",
    help=CSV_ROOT_DIRECTORY_OPTION_HELP,
)

JSONLD_ROOT_DIRECTORY_OPTION = Option(
    DEFAULT_PUBLIC_DIRECTORY,
    "--output-root-directory",
    "-o",
    help=JSONLD_ROOT_DIRECTORY_OPTION_HELP,
)

PARENT_DOMAIN_OPTION = Option(
    DEFAULT_PARENT_DOMAIN_NAME,
    "--parent-domain",
    "-p",
    help=PARENT_DOMAIN_OPTION_HELP,
)

PROVIDER_PARENT_DOMAIN_OPTION = Option(
    DEFAULT_PROVIDER_PARENT_DOMAIN_NAME,
    "--parent-domain",
    "-f",
    help=PARENT_DOMAIN_OPTION_HELP,
)

AUDITOR_PARENT_DOMAIN_OPTION = Option(
    DEFAULT_AUDITOR_PARENT_DOMAIN_NAME,
    "--parent-domain",
    "-f",
    help=PARENT_DOMAIN_OPTION_HELP,
)

FEDERATION_PARENT_DOMAIN_OPTION = Option(
    DEFAULT_PARENT_DOMAIN_NAME,
    "--federation-parent-domain",
    "-f",
    help=FEDERATION_PARENT_DOMAIN_OPTION_HELP,
)

DATABASE_FILENAME_OPTION = Option(
    DEFAULT_DATABASE_FILENAME,
    "--database",
    "-d",
    help=DATABASE_FILENAME_OPTION_HELP,
)

ENABLE_GAIAX_COMPLIANCE_OPTION: bool = Option(
    False, "--enable-gaiax-compliance", help=ENABLE_GAIAX_COMPLIANCE_OPTION_HELP
)

ENABLE_GAIAX_CES_OPTION: bool = Option(False, "--enable-gaiax-ces", help=ENABLE_GAIA_CES_CALL_OPTION_HELP)

USER_AGENT_API_KEY_OPTION = Option(
    ..., "--api-key", "-k", help=USER_AGENT_API_KEY_OPTION_HELP, prompt=True, hide_input=True
)

MEMBERSHIP_VC_ID_OPTION = Option(
    ..., "--membership-vc-id", help=MEMBERSHIP_VC_ID_OPTION_HELP, prompt=True, hide_input=True
)

AUDITOR_USER_AGENT_API_KEY_OPTION = Option(
    ..., "--auditor-api-key", help=AUDITOR_USER_AGENT_API_KEY_OPTION_HELP, prompt=True, hide_input=True
)

CATALOGUE_API_KEY_OPTION = Option(
    ..., "--api-key", "-k", help=CATALOGUE_API_KEY_OPTION_HELP, prompt=True, hide_input=True
)

FED_CATALOGUE_API_KEY_OPTION = Option(
    ..., "--api-key", "-k", help=FED_CATALOGUE_API_KEY_OPTION_HELP, prompt=True, hide_input=True
)

FEDERATION_NAME_OPTION = Option(
    ...,
    "--federation-name",
    "-fn",
    help=FEDERATION_NAME_OPTION_HELP,
)

PORT_NUMBER_OPTION = Option(
    443,
    "--port-number",
    help="Port number.",
)

NUMBER_OF_WORKERS_OPTION = Option(
    5,
    "--workers",
    "-w",
    help="number of workers",
)


def __display_error_messages(text: ConsoleRenderable) -> None:
    error_console = Console(stderr=True)
    error_console.print(Panel(text, title="Error", border_style="red", title_align="left"))
    raise Exit(1)


def __instantiate_participant_use_case(
    role: GaiaxParticipantRole,
    csv_root_directory: Optional[Path],
    jsonld_root_directory: Path,
    parent_domain: str,
    federation_parent_domain: Optional[str],
    name: str,
    database_name: str,
) -> LegalParticipantUseCase:
    match role:
        case GaiaxParticipantRole.AUDITOR:
            return AuditorUseCase(
                name=name,
                parent_domain_name=parent_domain,
                csv_root_directory=csv_root_directory,
                jsonld_root_directory=jsonld_root_directory,
                database_name=database_name,
            )
        case GaiaxParticipantRole.FEDERATION:
            return FederationUseCase(
                name=name,
                parent_domain_name=parent_domain,
                csv_root_directory=csv_root_directory,
                jsonld_root_directory=jsonld_root_directory,
                database_name=database_name,
            )
        case GaiaxParticipantRole.PROVIDER:
            return ProviderUseCase(
                name=name,
                parent_domain_name=parent_domain,
                federation_domain_name=federation_parent_domain,
                csv_root_directory=csv_root_directory,
                jsonld_root_directory=jsonld_root_directory,
                database_name=database_name,
            )
