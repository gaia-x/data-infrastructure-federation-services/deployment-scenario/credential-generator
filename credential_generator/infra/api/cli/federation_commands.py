# -*- coding: utf-8 -*-
"""
It defines federation command and its options
"""
import asyncio
import logging
from pathlib import Path
from typing import Optional

from rich.console import Group
from rich.padding import Padding
from rich.text import Text
from typer import Option, Typer

from credential_generator.config import (
    DEFAULT_DATABASE_FILENAME,
    GaiaxParticipantRole,
)
from credential_generator.infra.api.cli.common_commands import (
    CATALOGUE_API_KEY_OPTION,
    CSV_ROOT_DIRECTORY_OPTION,
    DATABASE_FILENAME_OPTION,
    ENABLE_GAIAX_COMPLIANCE_OPTION,
    JSONLD_ROOT_DIRECTORY_OPTION,
    NUMBER_OF_WORKERS_OPTION,
    PARENT_DOMAIN_OPTION,
    PORT_NUMBER_OPTION,
    USER_AGENT_API_KEY_OPTION,
    __display_error_messages,
    __instantiate_participant_use_case,
)
from credential_generator.infra.spi.clients import HostNotReachable

FEDERATION_NAME_OPTION_HELP = "Federation's name."
FEDERATION_NAME_OPTION = Option(
    ...,
    "--name",
    "-n",
    help=FEDERATION_NAME_OPTION_HELP,
)
app = Typer()

log = logging.getLogger(__name__)


@app.command(name="bootstrap")
def bootstrap(
    federation_name: str = FEDERATION_NAME_OPTION,
    csv_root_directory: Optional[str] = CSV_ROOT_DIRECTORY_OPTION,
    jsonld_root_directory: Optional[str] = JSONLD_ROOT_DIRECTORY_OPTION,
    parent_domain: Optional[str] = PARENT_DOMAIN_OPTION,
    database_name: Optional[str] = DATABASE_FILENAME_OPTION,
    enable_compliance_call: bool = ENABLE_GAIAX_COMPLIANCE_OPTION,
):
    """
    The `bootstrap` command reads a CSV File from provider_name directory and
    generates compliance-criterion json-ld files and compliance-labels json-ld files in public directory.
    """

    federation = __instantiate_participant_use_case(
        role=GaiaxParticipantRole.FEDERATION,
        csv_root_directory=Path(csv_root_directory),
        jsonld_root_directory=Path(jsonld_root_directory).absolute(),
        parent_domain=parent_domain,
        federation_parent_domain=None,
        name=federation_name,
        database_name=database_name,
    )
    federation.bootstrap(enable_compliance_call=enable_compliance_call)


@app.command(name="cleanup")
def cleanup(
    federation_name: str = FEDERATION_NAME_OPTION,
    jsonld_root_directory: Optional[str] = JSONLD_ROOT_DIRECTORY_OPTION,
    parent_domain: Optional[str] = PARENT_DOMAIN_OPTION,
):
    """
    The `cleanup` command removes all generated json files from output directory.
    """
    federation = __instantiate_participant_use_case(
        role=GaiaxParticipantRole.FEDERATION,
        csv_root_directory=None,
        jsonld_root_directory=Path(jsonld_root_directory).absolute(),
        parent_domain=parent_domain,
        federation_parent_domain=None,
        name=federation_name,
        database_name=DEFAULT_DATABASE_FILENAME,
    )
    federation.cleanup()


@app.command(name="upload")
def upload(
    federation_name: str = FEDERATION_NAME_OPTION,
    jsonld_root_directory: Optional[str] = JSONLD_ROOT_DIRECTORY_OPTION,
    parent_domain: Optional[str] = PARENT_DOMAIN_OPTION,
    port_number: int = PORT_NUMBER_OPTION,
    enable_compliance_call: bool = ENABLE_GAIAX_COMPLIANCE_OPTION,
    api_key: str = USER_AGENT_API_KEY_OPTION,
    number_of_workers: int = NUMBER_OF_WORKERS_OPTION,
):
    """
    The `upload` command is used to upload generated VCs into federation user agent catalogue.participant agent is not
    reachable, an error message is displayed.
    """

    federation = __instantiate_participant_use_case(
        role=GaiaxParticipantRole.FEDERATION,
        csv_root_directory=None,
        jsonld_root_directory=Path(jsonld_root_directory).absolute(),
        name=federation_name,
        database_name=DEFAULT_DATABASE_FILENAME,
        parent_domain=parent_domain,
        federation_parent_domain=None,
    )

    try:
        asyncio.run(
            federation.upload_self_description_objects(
                api_key=api_key, port_number=port_number, number_of_workers=number_of_workers
            )
        )

        if enable_compliance_call:
            asyncio.run(federation.call_compliance(api_key=api_key, port_number=port_number))
    except HostNotReachable:
        __display_error_messages(
            text=Group(
                Padding(
                    renderable=f"Cannot upload files to {federation}.{parent_domain}.", style="red", pad=(0, 0, 1, 0)
                ),
                Text(f" Check user agent of {federation}", justify="center"),
            )
        )


@app.command(name="sync")
def sync(
    federation_name: str = FEDERATION_NAME_OPTION,
    jsonld_root_directory: Optional[str] = JSONLD_ROOT_DIRECTORY_OPTION,
    parent_domain: Optional[str] = PARENT_DOMAIN_OPTION,
    port_number: int = PORT_NUMBER_OPTION,
    api_key: str = CATALOGUE_API_KEY_OPTION,
):
    """
    The `sync` command is used to index generated VCs into federated catalogue.
    """
    federation = __instantiate_participant_use_case(
        role=GaiaxParticipantRole.FEDERATION,
        csv_root_directory=None,
        jsonld_root_directory=Path(jsonld_root_directory).absolute(),
        parent_domain=parent_domain,
        federation_parent_domain=None,
        name=federation_name,
        database_name=DEFAULT_DATABASE_FILENAME,
    )

    try:
        asyncio.run(federation.index_vcs(api_key=api_key, port_number=port_number, enable_ces_sync=False))

    except HostNotReachable:
        __display_error_messages(
            text=Group(
                Padding(
                    renderable=f"Cannot upload files to {federation_name}.{parent_domain}.",
                    style="red",
                    pad=(0, 0, 1, 0),
                ),
                Text(text=" Check user agent", justify="center"),
            )
        )
