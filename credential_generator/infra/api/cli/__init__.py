# -*- coding: utf-8 -*-
"""
Main entry point for credential-generator cli
"""
import logging
from pathlib import Path
from typing import Optional

from rich.console import Console
from typer import Exit, Option, Typer, get_app_dir

from credential_generator import __app_name__, __version__
from credential_generator.infra.api.cli import auditor_commands, federation_commands, provider_commands
from credential_generator.infra.api.cli.common_commands import (
    CSV_ROOT_DIRECTORY_OPTION,
    DATABASE_FILENAME_OPTION,
)
from credential_generator.infra.api.cli.federation_commands import FEDERATION_NAME_OPTION
from credential_generator.use_cases.database_initialization import ComplianceDatabaseInitUseCase
from credential_generator.utilities.logger import configure_logging

configure_logging()

app = Typer(name=__app_name__, rich_markup_mode="markdown", add_completion=True)
app.add_typer(
    auditor_commands.app,
    name="auditor",
    help="The auditor command helps to generate, cleanup, upload and index auditor related self-description files.",
)
app.add_typer(
    federation_commands.app,
    name="federation",
    help="The federation command helps to generate, cleanup, upload and index "
    "federation related self-description files.",
)
app.add_typer(
    provider_commands.app,
    name="provider",
    help="The provider command helps to generate, cleanup, upload and index "
    "provider related self-description files.",
)

console = Console()
error_console = Console(stderr=True, style="bold red")

log = logging.getLogger(__app_name__)


def version_callback(value: bool) -> None:
    """
    Echoes the version of the Gaia-X credential generator and exits the program.
    :param value: A boolean value indicating whether the version should be displayed.
    :return: None
    :raise Exit: If the value is True, the function raises a Exit exception.
    """
    if value:
        console.print(f":flag_for_european_union: Gaia-X {__app_name__}")
        console.print(f"Version v{__version__}")
        raise Exit()


@app.callback()
# pylint: disable = unused-argument
# pylint: disable = empty-docstring
def version_option(
    version: Optional[bool] = Option(
        None,
        "--version",
        "-v",
        callback=version_callback,
        help="Give application version and exit.",
        is_eager=True,
    ),
):
    """ """


@app.command(name="init")
def init_command(
    federation_name: str = FEDERATION_NAME_OPTION,
    csv_root_directory: Optional[str] = CSV_ROOT_DIRECTORY_OPTION,
    database_name: Optional[str] = DATABASE_FILENAME_OPTION,
) -> None:
    """
    The init command creates a new sqlite database in your local file system and loads
    data from CSV files located in data-compliance directory.
    """
    datasource_root_directory = Path(csv_root_directory).absolute()
    database_path = Path(database_name).absolute()

    console.print(f"Data source directory: {datasource_root_directory}", style="yellow")
    console.print(f"Database name: {database_path}", style="yellow")
    console.print(f"Application directory: {get_app_dir(__app_name__)}", style="yellow")

    if not datasource_root_directory.exists() or not datasource_root_directory.is_dir():
        error_console.print(f"{datasource_root_directory} does not exist")
        raise Exit(1)

    use_case = ComplianceDatabaseInitUseCase(
        input_directory=datasource_root_directory,
        federation_name=federation_name,
        database_name=database_path,
    )

    use_case.create_database()
    use_case.load_database()
