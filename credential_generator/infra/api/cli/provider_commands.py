# -*- coding: utf-8 -*-
"""
It defines provider command and its options
"""

import asyncio
import logging
from pathlib import Path
from typing import Optional

from rich.console import Group
from rich.padding import Padding
from rich.text import Text
from typer import Option, Typer

from credential_generator.config import (
    DEFAULT_DATABASE_FILENAME,
    GaiaxParticipantRole,
)
from credential_generator.infra.api.cli.common_commands import (
    AUDITOR_USER_AGENT_API_KEY_OPTION,
    CATALOGUE_API_KEY_OPTION,
    CSV_ROOT_DIRECTORY_OPTION,
    DATABASE_FILENAME_OPTION,
    ENABLE_GAIAX_CES_OPTION,
    ENABLE_GAIAX_COMPLIANCE_OPTION,
    FEDERATION_PARENT_DOMAIN_OPTION,
    JSONLD_ROOT_DIRECTORY_OPTION,
    NUMBER_OF_WORKERS_OPTION,
    PORT_NUMBER_OPTION,
    PROVIDER_PARENT_DOMAIN_OPTION,
    USER_AGENT_API_KEY_OPTION,
    __display_error_messages,
    __instantiate_participant_use_case, MEMBERSHIP_VC_ID_OPTION, FED_CATALOGUE_API_KEY_OPTION, FEDERATION_NAME_OPTION,
)
from credential_generator.infra.spi.clients import HostNotReachable
from credential_generator.use_cases.provider import ProviderUseCase

PROVIDER_NAME_OPTION_HELP = "Provider name."
PROVIDER_NAME_OPTION = Option(
    ...,
    "--name",
    "-n",
    help=PROVIDER_NAME_OPTION_HELP,
)
app = Typer()

log = logging.getLogger(__name__)


@app.command(name="bootstrap")
def bootstrap(
    provider_name: str = PROVIDER_NAME_OPTION,
    csv_root_directory: Optional[str] = CSV_ROOT_DIRECTORY_OPTION,
    jsonld_root_directory: Optional[str] = JSONLD_ROOT_DIRECTORY_OPTION,
    parent_domain: Optional[str] = PROVIDER_PARENT_DOMAIN_OPTION,
    federation_parent_domain: Optional[str] = FEDERATION_PARENT_DOMAIN_OPTION,
    database_name: Optional[str] = DATABASE_FILENAME_OPTION,
    enable_compliance_call: bool = ENABLE_GAIAX_COMPLIANCE_OPTION,
):
    """
    The `bootstrap` command reads a CSV File from provider_name directory and
    generates compliance-criterion json-ld files and compliance-labels json-ld files in public directory.
    """

    provider = __instantiate_participant_use_case(
        role=GaiaxParticipantRole.PROVIDER,
        csv_root_directory=Path(csv_root_directory),
        jsonld_root_directory=Path(jsonld_root_directory).absolute(),
        parent_domain=parent_domain,
        federation_parent_domain=federation_parent_domain,
        name=provider_name,
        database_name=database_name,
    )
    provider.bootstrap(enable_compliance_call=enable_compliance_call)


@app.command(name="cleanup")
def cleanup(
    provider_name: str = PROVIDER_NAME_OPTION,
    jsonld_root_directory: Optional[str] = JSONLD_ROOT_DIRECTORY_OPTION,
    parent_domain: Optional[str] = PROVIDER_PARENT_DOMAIN_OPTION,
):
    """
    The `cleanup` command removes all generated json files from output directory.
    """
    provider = __instantiate_participant_use_case(
        role=GaiaxParticipantRole.PROVIDER,
        csv_root_directory=None,
        jsonld_root_directory=Path(jsonld_root_directory).absolute(),
        parent_domain=parent_domain,
        name=provider_name,
        database_name=DEFAULT_DATABASE_FILENAME,
        federation_parent_domain=None,
    )
    provider.cleanup()


@app.command(name="label")
def label_command(
    provider_name: str = PROVIDER_NAME_OPTION,
    jsonld_root_directory: Optional[str] = JSONLD_ROOT_DIRECTORY_OPTION,
    parent_domain: Optional[str] = PROVIDER_PARENT_DOMAIN_OPTION,
    federation_parent_domain: Optional[str] = FEDERATION_PARENT_DOMAIN_OPTION,
    api_key: str = USER_AGENT_API_KEY_OPTION,
    port_number: int = PORT_NUMBER_OPTION,
    database_name: Optional[str] = DATABASE_FILENAME_OPTION,
):
    """
    The `label` command sends provider's service offering to labelling to get aster-x labels.
    """
    provider: ProviderUseCase = __instantiate_participant_use_case(
        role=GaiaxParticipantRole.PROVIDER,
        csv_root_directory=None,
        jsonld_root_directory=Path(jsonld_root_directory).absolute(),
        name=provider_name,
        database_name=database_name,
        parent_domain=parent_domain,
        federation_parent_domain=federation_parent_domain,
    )

    try:
        asyncio.run(provider.call_labelling(api_key=api_key, port_number=port_number))
    except HostNotReachable:
        __display_error_messages(
            text=Group(
                Padding(
                    renderable=f"Cannot upload files to {provider}.{parent_domain}.", style="red", pad=(0, 0, 1, 0)
                ),
                Text(f" Check user agent of {provider}", justify="center"),
            )
        )


@app.command(name="upload")
def upload(
    provider_name: str = PROVIDER_NAME_OPTION,
    jsonld_root_directory: Optional[str] = JSONLD_ROOT_DIRECTORY_OPTION,
    parent_domain: Optional[str] = PROVIDER_PARENT_DOMAIN_OPTION,
    enable_compliance_call: bool = ENABLE_GAIAX_COMPLIANCE_OPTION,
    port_number: int = PORT_NUMBER_OPTION,
    api_key: str = USER_AGENT_API_KEY_OPTION,
    auditor_api_key: str = AUDITOR_USER_AGENT_API_KEY_OPTION,
    number_of_workers: int = NUMBER_OF_WORKERS_OPTION,
):
    """
    The `upload` command is used to upload generated VCs into provider user agent catalogue.participant agent is not
    reachable, an error message is displayed.
    """

    provider = __instantiate_participant_use_case(
        role=GaiaxParticipantRole.PROVIDER,
        csv_root_directory=None,
        jsonld_root_directory=Path(jsonld_root_directory).absolute(),
        name=provider_name,
        database_name=DEFAULT_DATABASE_FILENAME,
        parent_domain=parent_domain,
        federation_parent_domain=None,
    )

    try:
        asyncio.run(
            provider.upload_self_description_objects(
                api_key=api_key,
                auditor_api_key=auditor_api_key,
                port_number=port_number,
                number_of_workers=number_of_workers,
            )
        )

        if enable_compliance_call:
            asyncio.run(provider.call_compliance(api_key=api_key, port_number=port_number))

        asyncio.run(provider.clear_cache(api_key=api_key, port_number=port_number))
    except HostNotReachable:
        __display_error_messages(
            text=Group(
                Padding(
                    renderable=f"Cannot upload files to {provider}.{parent_domain}.", style="red", pad=(0, 0, 1, 0)
                ),
                Text(f" Check user agent of {provider}", justify="center"),
            )
        )


@app.command(name="sync")
def sync(
    provider_name: str = PROVIDER_NAME_OPTION,
    jsonld_root_directory: Optional[str] = JSONLD_ROOT_DIRECTORY_OPTION,
    parent_domain: Optional[str] = PROVIDER_PARENT_DOMAIN_OPTION,
    database_name: Optional[str] = DATABASE_FILENAME_OPTION,
    port_number: int = PORT_NUMBER_OPTION,
    api_key: str = CATALOGUE_API_KEY_OPTION,
    enable_ces_sync: bool = ENABLE_GAIAX_CES_OPTION,
):
    """
    The `sync` command is used to index generated VCs into federated catalogue.
    """
    provider = __instantiate_participant_use_case(
        role=GaiaxParticipantRole.PROVIDER,
        csv_root_directory=None,
        jsonld_root_directory=Path(jsonld_root_directory).absolute(),
        parent_domain=parent_domain,
        name=provider_name,
        database_name=database_name,
        federation_parent_domain=None,
    )

    try:
        asyncio.run(provider.index_vcs(api_key=api_key, port_number=port_number, enable_ces_sync=enable_ces_sync))

    except HostNotReachable:
        __display_error_messages(
            text=Group(
                Padding(
                    renderable=f"Cannot upload files to {provider_name}.{parent_domain}.",
                    style="red",
                    pad=(0, 0, 1, 0),
                ),
                Text(text=" Check Catalogue API setup", justify="center"),
            )
        )


@app.command(name="aster-x-compliance")
def aster_x_compliance(
    provider_name: str = PROVIDER_NAME_OPTION,
    jsonld_root_directory: Optional[str] = JSONLD_ROOT_DIRECTORY_OPTION,
    parent_domain: Optional[str] = PROVIDER_PARENT_DOMAIN_OPTION,
    port_number: int = PORT_NUMBER_OPTION,
    membership_vc_id: str = MEMBERSHIP_VC_ID_OPTION,
    user_agent_api_key: str = USER_AGENT_API_KEY_OPTION,
):
    """
    The `aster-x-compliance` command is used to get Aster-X compliance thanks to the Membership VC.
    """

    provider: ProviderUseCase = __instantiate_participant_use_case(
        role=GaiaxParticipantRole.PROVIDER,
        csv_root_directory=None,
        jsonld_root_directory=Path(jsonld_root_directory).absolute(),
        name=provider_name,
        database_name=DEFAULT_DATABASE_FILENAME,
        parent_domain=parent_domain,
        federation_parent_domain=None,
    )

    try:
        asyncio.run(
            provider.call_aster_x_compliance(
                membership_vc_id=membership_vc_id,
                port_number=port_number,
                user_agent_api_key=user_agent_api_key
            )
        )

        # asyncio.run(provider.clear_cache(api_key=api_key, port_number=port_number))
    except HostNotReachable as e:
        log.error(e)
        __display_error_messages(
            text=Group(
                Padding(
                    renderable="Cannot retrieve Aster-X compliance.", style="red", pad=(0, 0, 1, 0)
                ),
                Text(f" Check user agent of {provider}", justify="center"),
            )
        )


@app.command(name="sync_aster_x_compliance")
def sync_aster_x_compliance(
    provider_name: str = PROVIDER_NAME_OPTION,
    federation_name: str = FEDERATION_NAME_OPTION,
    jsonld_root_directory: Optional[str] = JSONLD_ROOT_DIRECTORY_OPTION,
    parent_domain: Optional[str] = PROVIDER_PARENT_DOMAIN_OPTION,
    fed_parent_domain: Optional[str] = FEDERATION_PARENT_DOMAIN_OPTION,
    database_name: Optional[str] = DATABASE_FILENAME_OPTION,
    port_number: int = PORT_NUMBER_OPTION,
    api_key: str = FED_CATALOGUE_API_KEY_OPTION,
):
    """
    The `sync_aster_x_compliance` command is used to index Aster-X compliance VCs generated into federated catalogue.
    """
    provider = __instantiate_participant_use_case(
        role=GaiaxParticipantRole.PROVIDER,
        csv_root_directory=None,
        jsonld_root_directory=Path(jsonld_root_directory).absolute(),
        parent_domain=parent_domain,
        name=provider_name,
        database_name=database_name,
        federation_parent_domain=None,
    )

    try:
        asyncio.run(provider.index_aster_x_compliance_vcs(federation_name=federation_name,
                                                          fed_parent_domain=fed_parent_domain,
                                                          api_key=api_key, port_number=port_number))

    except HostNotReachable as err:
        log.error(f"error: {err}")
        __display_error_messages(
            text=Group(
                Padding(
                    renderable=f"Cannot upload files to {provider_name}.{parent_domain}.",
                    style="red",
                    pad=(0, 0, 1, 0),
                ),
                Text(text=" Check user agent", justify="center"),
            )
        )
