# -*- coding: utf-8 -*-
"""
It defines common enum.
"""

from __future__ import annotations

# Standard Library
from enum import Enum
from typing import Optional


class OntologyType(Enum):
    """
    It defines type of ontologies supported in our environment.
    """

    SERVICE_OFFERING = "service-offering"
    LEGAL_PARTICIPANT = "legal-participant"
    GAIAX_TERMS_AND_CONDITIONS = "gaiax-terms-and-conditions"
    GAIAX_LEGAL_REGISTRATION_NUMBER = "gaiax-legal-registration-number"
    LOCATED_SERVICE = "located-service-offering"
    LOCATION = "location"
    COMPLIANCE_ASSESSMENT_BODY = "compliance-assessment-body"
    COMPLIANCE_REF = "compliance-reference"
    COMPLIANCE_CERT_SCHEME = "compliance-certification-scheme"
    COMPLIANCE_CERT_CRED = "compliance-certificate-credential-vc"
    SELF_ASSESSED_COMPLIANCE_CRITERIA_CRED = "self-assessed-compliance-criteria-credential"
    SELF_ASSESSED_COMPLIANCE_CRITERIA_CLAIM = "self-assessed-compliance-criteria-claim"
    COMPLIANCE_CERT_CLAIM = "compliance-certificate-claim"
    THIRD_PARTY_COMPLIANCE_CERT_SCHEME = "third-party-compliance-certification-scheme"
    THIRD_PARTY_COMPLIANCE_CERT_CRED = "third-party-compliance-certificate-credential-vc"
    THIRD_PARTY_COMPLIANCE_CERT_CLAIM = "third-party-compliance-certificate-claim"
    COMPLIANCE_CRITERION = "compliance-criterion"
    COMPLIANCE_LABEL = "compliance-label"
    VC = "vc"
    DATA_PRODUCT = "data-product"

    @staticmethod
    def is_valid_ontology_type(object_type: str) -> bool:
        """
        This method checks if a given object type is a valid ontology type by comparing it to the values of the
        OntologyType enum.
        :param object_type (str): The type of the object to be checked.
        :return: bool: True if the object type is valid, False otherwise.
        """
        return object_type in [ontology.value for ontology in OntologyType]

    @staticmethod
    def from_jsonld_type(object_type: str) -> OntologyType:
        """
        The from_jsonld_type method is a static method of the OntologyType class. It takes a string parameter
        object_type and returns an instance of the OntologyType enum based on the value of object_type.

        ### Example Usage
        ```python
        object_type = "LegalParticipant"
        ontology_type = OntologyType.from_jsonld_type(object_type)
        print(ontology_type)
        ```
        :param object_type (str): The type of the object in JSON-LD format.
        :return: An instance of the OntologyType enum based on the value of object_type.
        """
        # pylint: disable=too-many-return-statements
        ontology_type = object_type.split(":")[1] if ":" in object_type else object_type
        if isinstance(ontology_type, list):
            ontology_type = ontology_type[0] if len(ontology_type) > 0 else ""

        match ontology_type:
            case "LegalParticipant":
                return OntologyType.LEGAL_PARTICIPANT
            case "GaiaXTermsAndConditions":
                return OntologyType.GAIAX_TERMS_AND_CONDITIONS
            case "legalRegistrationNumber":
                return OntologyType.GAIAX_LEGAL_REGISTRATION_NUMBER
            case "ServiceOffering":
                return OntologyType.SERVICE_OFFERING
            case "LocatedServiceOffering":
                return OntologyType.LOCATED_SERVICE
            case "Location":
                return OntologyType.LOCATION
            case "ComplianceAssessmentBody":
                return OntologyType.COMPLIANCE_ASSESSMENT_BODY
            case "ComplianceReference":
                return OntologyType.COMPLIANCE_REF
            case "ComplianceCriterion":
                return OntologyType.COMPLIANCE_CRITERION
            case "ComplianceLabel":
                return OntologyType.COMPLIANCE_LABEL
            case "VerifiableCredential":
                return OntologyType.VC
            case "DataProduct":
                return OntologyType.DATA_PRODUCT
            case "ComplianceCertificationScheme":
                return OntologyType.COMPLIANCE_CERT_SCHEME
            case "ComplianceCertificateClaim":
                return OntologyType.COMPLIANCE_CERT_CLAIM
            case "ComplianceCertificateCredential":
                return OntologyType.COMPLIANCE_CERT_CRED
            case "ThirdPartyComplianceCertificationScheme":
                return OntologyType.THIRD_PARTY_COMPLIANCE_CERT_SCHEME
            case "ThirdPartyComplianceCertificateClaim":
                return OntologyType.THIRD_PARTY_COMPLIANCE_CERT_CLAIM
            case "ThirdPartyComplianceCertificateCredential":
                return OntologyType.THIRD_PARTY_COMPLIANCE_CERT_CRED
            case "SelfAssessedComplianceCriteriaClaim":
                return OntologyType.SELF_ASSESSED_COMPLIANCE_CRITERIA_CLAIM
            case "SelfAssessedComplianceCriteriaCredential":
                return OntologyType.SELF_ASSESSED_COMPLIANCE_CRITERIA_CRED
            case _:
                raise ValueError(f"Unknown jsonld type {object_type}")

    @staticmethod
    def get_directory_name(ontology_type: OntologyType, is_vc: bool = False) -> str:
        """Get the directory name for the given ontology type.

        Args:
            ontology_type (OntologyType): The ontology type.
            is_vc (bool, optional): Whether the ontology type is a Verifiable Credential. Defaults to False.

        Returns:
            str: The directory name.

        """
        return str(ontology_type.value) if is_vc else f"{ontology_type.value!s}-json"


class UnknownComplianceLevelError(ValueError):
    """
    Error raised when ComplianceLevel is unknown
    """

    def __init__(self, level: Optional[str]):
        super().__init__(f"Unknown Compliance : {level}")


class UnknownCriterionCategoryError(ValueError):
    """
    Error raised when ComplianceLevel is unknown
    """

    def __init__(self, category: Optional[str]):
        super().__init__(f"Unknown CriterionCategory : {category}")


class ComplianceLevel(Enum):
    """
    ComplianceLevel defines a compliance level
    """

    LEVEL_1 = "Level 1"
    LEVEL_2 = "Level 2"
    LEVEL_3 = "Level 3"

    @staticmethod
    def from_str(level):
        """
        The from_str function is a helper function that takes in a string and returns the corresponding ComplianceLevel.

        :param level: Determine the level of compliance
        :return: An object of type compliancelevel
        """

        if level is None:
            raise UnknownComplianceLevelError(level)

        match level.lower():
            case "level 1":
                return ComplianceLevel.LEVEL_1
            case "level 2":
                return ComplianceLevel.LEVEL_2
            case "level 3":
                return ComplianceLevel.LEVEL_3
            case _:
                raise UnknownComplianceLevelError(level)

    def __str__(self) -> str:
        """
        The __str__ function is called when you print an object.
        It's a special function that returns a string representation of the object.
        If you don't define it, Python will use its own default implementation.

        :param self: Refer to the current instance of the class, and is used to access variables that belongs to the
        class
        :return: The string representation of the object
        """
        return f"{self.value}"


class CriterionCategory(Enum):
    """
    CriterionCategory defines well known category of criteria
    """

    CONTRACTUAL_GOVERNANCE = "Contractual governance"
    TRANSPARENCY = "Transparency"
    DATA_PROTECTION = "Data Protection"
    SECURITY = "Security"
    PORTABILITY = "Portability"
    EUROPEAN_CONTROL = "European Control"

    @staticmethod
    def from_str(category: str):
        """
        The from_str function is a helper function that allows us to convert the string representation of a criterion
        category to its corresponding enum value. This is useful when we want to create an instance of CriterionCategory
        from user input, for example.

        :param category: str: Define the type of data that is expected to be passed into the function
        :return: A criterioncategory enum
        """

        if category is None:
            raise UnknownCriterionCategoryError(category)

        match category.lower():
            case "contractual governance":
                return CriterionCategory.CONTRACTUAL_GOVERNANCE
            case "transparency":
                return CriterionCategory.TRANSPARENCY
            case "data protection":
                return CriterionCategory.DATA_PROTECTION
            case "security":
                return CriterionCategory.SECURITY
            case "portability":
                return CriterionCategory.PORTABILITY
            case "european control":
                return CriterionCategory.EUROPEAN_CONTROL
            case _:
                raise UnknownCriterionCategoryError(category)

    def __str__(self) -> str:
        """
        The __str__ function is called when you call str() on an object.
        It should return a string representation of the object.
        The goal is to be unambiguous and, if possible, readable.

        :param self: Refer to the current instance of the class
        :return: A string representation of the object
        """
        return f"{self.value}"
