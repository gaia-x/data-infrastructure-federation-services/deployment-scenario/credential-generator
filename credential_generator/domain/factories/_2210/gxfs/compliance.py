# -*- coding: utf-8 -*-
"""
It defines methods to create compliance domain object
"""
from typing import Optional

# data-initialiser Library
from credential_generator.domain import ComplianceLevel, CriterionCategory
from credential_generator.domain._2210 import GaiaxObjectWithDidWeb
from credential_generator.domain._2210.gxfs.gx_compliance import (
    ComplianceAssessmentBody,
    ComplianceCertificateClaim,
    ComplianceCertificationScheme,
    ComplianceCriterion,
    ComplianceLabel,
    ComplianceReference,
    ComplianceReferenceManager,
    SelfAssessedComplianceCriteriaClaim,
    ThirdPartyComplianceCertificateClaim,
    ThirdPartyComplianceCertificationScheme,
)
from credential_generator.domain._2210.gxfs.gx_service import LocatedServiceOffering
from credential_generator.domain._2210.trust_framework.participant import Address, LegalRegistrationNumber
from credential_generator.domain.factories._2210 import (
    create_legal_registration_number,
    get_auditor_legal_participant_document_id,
    get_federator_legal_participant_document_id,
)
from credential_generator.domain.factories._2210.gxfs import (
    get_compliance_certificate_claim_id,
    get_compliance_certification_scheme_id,
    get_compliance_criterion_id,
    get_compliance_label_id,
    get_compliance_reference_id,
    get_located_service_did,
    get_self_assessed_certificate_claim_did,
    get_third_party_compliance_certificate_claim_did,
    get_third_party_compliance_certification_scheme_id,
)
from credential_generator.domain.factories._2210.trust_framework import (
    get_gaiax_legal_registration_number_vc_id,
)
from credential_generator.utilities import date_to_xds
from credential_generator.utilities.did_helpers import calculate_auditor_hash_value, calculate_federator_hash_value


def create_compliance_criterion(
    parent_domain_name: str,
    provider_name: str,
    name: str,
    level: ComplianceLevel,
    category: CriterionCategory,
    description: str,
    self_assessed: bool,
) -> ComplianceCriterion:
    """
    The create_compliance_criterion function is used to create a ComplianceCriterion object. It takes in several inputs
    such as the parent domain name, provider name, criterion name, criterion level, criterion category, description,
    and self-assessed flag. It uses these inputs to generate a Decentralized Identifier (DID) for the compliance
    criterion and constructs a ComplianceCriterion object with the provided inputs and the generated DID.

    ### Example Usage
    ```python
    parent_domain_name = "example.com"
    provider_name = "acme"
    criterion_name = "security"
    criterion_level = ComplianceLevel.LEVEL_1
    criterion_category = CriterionCategory.SECURITY
    description = "This criterion ensures the security of the system."
    self_assessed = True

    criterion = create_compliance_criterion(
        parent_domain_name=parent_domain_name,
        provider_name=provider_name,
        name=criterion_name,
        level=criterion_level,
        category=criterion_category,
        description=description,
        self_assessed=self_assessed,
    )

    print(criterion)
    ```
    :param parent_domain_name:
    :param provider_name:
    :param name:
    :param level:
    :param category:
    :param description:
    :param self_assessed:
    :return:
    """
    return ComplianceCriterion(
        did_web=get_compliance_criterion_id(
            parent_domain_name=parent_domain_name,
            federator_name=provider_name,
            criterion_name=name,
            criterion_level=level.value,
        ),
        name=name,
        level=level,
        reference_type=category,
        description=description,
        self_assessed=self_assessed,
    )


def create_compliance_label(
    parent_domain_name: str,
    federator_name: str,
    level: ComplianceLevel,
) -> ComplianceLabel:
    """
    The create_compliance_label function generates a compliance label based on the provided inputs.

    ### Example Usage
    ```python
    parent_domain_name = "example.com"
    provider_name = "acme"
    level = ComplianceLevel.LEVEL_1

    result = create_compliance_label(parent_domain_name, provider_name, level)
    print(result)
    ```
    :param parent_domain_name:
    :param provider_name:
    :param level:
    :return:
    """
    label_description = f"Aster-X label {level.value}"

    return ComplianceLabel(
        did_web=get_compliance_label_id(
            parent_domain_name=parent_domain_name,
            federator_name=federator_name,
            label_name=level.value,
            label_level=level.value,
        ),
        name=level.value,
        description=label_description,
        level=level,
        criteria_list=[],
    )


def create_compliance_reference(
    parent_domain_name: str,
    federator_name: str,
    compliance_reference_schemes: list[str],
    title: str,
    version: Optional[str],
    category: str,
    document_reference_url: str,
    document_hash256: str,
    compliance_reference_manager_did_web: str,
    valid_from: str,
    valid_until: str,
    compliance_reference_id: str,
) -> ComplianceReference:
    """
    The create_compliance_reference function is used to create a ComplianceReference object by providing the necessary
    inputs such as parent domain name, federator name, compliance reference ID, title, and version.

    ### Example Usage
    parent_domain_name = "example.com"
    federator_name = "federator1"
    compliance_reference_id = "123456"
    title = "GDPR"
    version = "1.0"

    result = create_compliance_reference(parent_domain_name, federator_name, compliance_reference_id, title, version)
    print(result)

    :param compliance_reference_id:
    :param parent_domain_name: a string representing the domain name of the parent entity.
    :param federator_name: a string representing the name of the federator.
    :param compliance_reference_schemes: a string representing the ID of the compliance reference.
    :param title: a string representing the title of the compliance reference.
    :param version: a string representing the version of the compliance reference.
    :return: A ComplianceReference object with the generated DID, compliance reference ID, title, and version.
    """

    version = version if version is not None else ""

    return ComplianceReference(
        compliance_reference_id=compliance_reference_id,
        did_web=get_compliance_reference_id(
            parent_domain_name=parent_domain_name,
            federator_name=federator_name,
            compliance_reference_title=title,
            compliance_reference_version=version,
        ),
        compliance_certification_schemes=[
            GaiaxObjectWithDidWeb(did_web=scheme) for scheme in compliance_reference_schemes
        ],
        title=title,
        version=version,
        reference_type=category,
        reference_url=document_reference_url if document_reference_url is not None else "",
        reference_sha256=document_hash256 if document_hash256 is not None else "",
        compliance_reference_manager=compliance_reference_manager_did_web,
        valid_from=date_to_xds(valid_from) if valid_from is not None and valid_from != "" else None,
        valid_until=date_to_xds(valid_until) if valid_until is not None and valid_until != "" else None,
    )


def create_compliance_certification_scheme(
    parent_domain_name: str,
    federator_name: str,
    compliance_reference: ComplianceReference,
    compliance_criterions: list[ComplianceCriterion],
) -> ComplianceCertificationScheme:
    """
    The create_compliance_certification_scheme function creates a ComplianceCertificationScheme object by generating
    a unique identifier (DID) for the scheme and assigning the provided compliance reference and criteria list.
    Example Usage
    parent_domain_name = "example.com"
    federator_name = "federator1"
    compliance_reference = ComplianceReference(...)
    compliance_criterions = [ComplianceCriterion(...), ComplianceCriterion(...)]

    scheme = create_compliance_certification_scheme(
        parent_domain_name, federator_name, compliance_reference, compliance_criterions)
    :param parent_domain_name (str): The domain name of the parent organization.
    :param federator_name (str): The name of the federator.
    :param compliance_reference (ComplianceReference): The compliance reference object.
    :param compliance_criterions (list[ComplianceCriterion]): The list of compliance criterion objects.
    :return:
    """
    return ComplianceCertificationScheme(
        did_web=get_compliance_certification_scheme_id(
            parent_domain_name=parent_domain_name,
            federator_name=federator_name,
            compliance_reference_id=compliance_reference.compliance_reference_id,
        ),
        compliance_reference=compliance_reference,
        criteria_list=compliance_criterions,
    )


def create_third_party_compliance_certification_scheme(
    parent_domain_name: str,
    federator_name: str,
    compliance_reference: ComplianceReference,
    compliance_criterions: list[ComplianceCriterion],
    compliance_assessment_bodies: list[ComplianceAssessmentBody],
) -> ThirdPartyComplianceCertificationScheme:
    """
    The create_third_party_compliance_certification_scheme function creates a third-party compliance certification
    scheme object by combining the provided inputs.

    ### Example Usage
    ```python
    parent_domain_name = "example.com"
    federator_name = "federator1"
    compliance_reference = ComplianceReference(...)
    compliance_criterions = [ComplianceCriterion(...), ComplianceCriterion(...)]
    compliance_assessment_bodies = [ComplianceAssessmentBody(...), ComplianceAssessmentBody(...)]

    third_party_scheme = create_third_party_compliance_certification_scheme(parent_domain_name, federator_name,
    compliance_reference, compliance_criterions, compliance_assessment_bodies)
    print(third_party_scheme)
    ```

    :param parent_domain_name (str): The domain name of the parent organization.
    :param federator_name (str): The name of the federator
    :param compliance_reference (ComplianceReference): The compliance reference object.
    :param compliance_criterions (list[ComplianceCriterion]): A list of compliance criterion objects.
    :param compliance_assessment_bodies (list[ComplianceAssessmentBody]): A list of compliance assessment body objects.
    :return: ThirdPartyComplianceCertificationScheme: The created third-party compliance certification scheme object.
    """
    return ThirdPartyComplianceCertificationScheme(
        did_web=get_third_party_compliance_certification_scheme_id(
            parent_domain_name=parent_domain_name,
            federator_name=federator_name,
            compliance_reference_id=compliance_reference.compliance_reference_id,
        ),
        compliance_reference=compliance_reference,
        criteria_list=compliance_criterions,
        compliance_assessment_bodies=compliance_assessment_bodies,
    )


def create_compliance_assessment_body(
    parent_domain_name: str,
    auditor_name: str,
    legal_name: str,
    country_name: str,
    lei_code: Optional[str],
    registration_number: Optional[str],
) -> ComplianceAssessmentBody:
    """
    Create a compliance assessment body for a given parent domain, auditor, legal entity, and registration details.

    Args:
        parent_domain_name (str): The name of the parent domain.
        auditor_name (str): The name of the auditor.
        legal_name (str): The legal name of the entity.
        country_name (str): The name of the country.
        lei_code (Optional[str]): The LEI code of the entity, if available.
        registration_number (Optional[str]): The registration number of the entity, if available.

    Returns:
        ComplianceAssessmentBody: The created compliance assessment body.

    Raises:
        ValueError: If neither lei_code nor registration_number is provided.
    """
    legal_address = Address(country_subdivision_code=country_name)
    headquarter_address = Address(country_subdivision_code=country_name)

    auditor_did_web = get_auditor_legal_participant_document_id(
        parent_domain_name=parent_domain_name,
        auditor_name=auditor_name,
        hash_value=calculate_auditor_hash_value(auditor_name=auditor_name),
    )

    legal_registration_number_did_web = get_gaiax_legal_registration_number_vc_id(
        parent_domain_name=parent_domain_name, name=auditor_name
    )

    if lei_code or registration_number:
        number: LegalRegistrationNumber = create_legal_registration_number(
            did_web=legal_registration_number_did_web, lei_code=lei_code, registration_number=registration_number
        )
    else:
        raise ValueError("Either lei_code or registration_number must be provided.")

    return ComplianceAssessmentBody(
        did_web=auditor_did_web,
        legalName=legal_name,
        legalAddress=legal_address,
        headquarterAddress=headquarter_address,
        legalRegistrationNumber=number,
    )


def create_compliance_reference_manager(
    parent_domain_name: str,
    federation_name: str,
    legal_name: str,
    country_name: str,
    lei_code: Optional[str],
    registration_number: Optional[str],
    has_compliance_references: Optional[list[str]],
) -> ComplianceReferenceManager:
    """
    Create a compliance reference manager for a given parent domain, auditor, legal entity, and registration details.

    Args:
        parent_domain_name (str): The name of the parent domain.
        federation_name (str): The name of the auditor.
        legal_name (str): The legal name of the entity.
        country_name (str): The name of the country.
        lei_code (Optional[str]): The LEI code of the entity, if available.
        registration_number (Optional[str]): The registration number of the entity, if available.
        has_compliance_references (Optional[list[str]]): List of compliance references, default is an empty list.

    Returns:
        ComplianceReferenceManager: The created compliance reference manager.
    """
    if has_compliance_references is None:
        has_compliance_references = []

    legal_address = Address(country_subdivision_code=country_name)
    headquarter_address = Address(country_subdivision_code=country_name)

    federation_object_id = get_federator_legal_participant_document_id(
        parent_domain_name=parent_domain_name,
        federator_name=federation_name,
        hash_value=calculate_federator_hash_value(federator_name=federation_name),
    )

    legal_registration_number_did_web = get_gaiax_legal_registration_number_vc_id(
        parent_domain_name=parent_domain_name, name=federation_name
    )

    number: LegalRegistrationNumber = create_legal_registration_number(
        did_web=legal_registration_number_did_web, lei_code=lei_code, registration_number=registration_number
    )

    return ComplianceReferenceManager(
        did_web=federation_object_id,
        legalName=legal_name,
        legalAddress=legal_address,
        headquarterAddress=headquarter_address,
        legalRegistrationNumber=number,
        has_compliance_references=has_compliance_references,
    )


def create_compliance_certification_claim(
    parent_domain_name: str,
    provider_name: str,
    located_service_offering: LocatedServiceOffering,
    compliance_certification_scheme: Optional[GaiaxObjectWithDidWeb],
) -> ComplianceCertificateClaim:
    """
    This code defines a function named create_compliance_certification_claim that creates a ComplianceCertificateClaim
    object. The function takes in several inputs including the parent domain name, provider name, a
    LocatedServiceOffering object, and an optional ComplianceCertificationScheme object. It then uses these inputs to
    construct a ComplianceCertificateClaim object by calling the get_compliance_certificate_claim_id function to
    generate the did_web attribute. The function returns the created ComplianceCertificateClaim object.

    ##Example Usage
    ```python
    located_service_offering = LocatedServiceOffering(did_web="did:web:12345", ...)
    compliance_certification_scheme = ComplianceCertificationScheme(did_web="did:web:67890", ...)
    claim = create_compliance_certification_claim(parent_domain_name="example.com", provider_name="Provider A",
    located_service_offering=located_service_offering, compliance_certification_scheme=compliance_certification_scheme)
    print(claim.did_web)
    ``
    :param parent_domain_name (str): The name of the parent domain.
    :param provider_name (str): The name of the provider.
    :param located_service_offering (LocatedServiceOffering): An instance of the LocatedServiceOffering class
    representing the located service offering.
    :param compliance_certification_scheme (Optional[ComplianceCertificationScheme]): An optional instance of the
    ComplianceCertificationScheme class representing the compliance certification scheme.
    :return: ComplianceCertificateClaim: The created ComplianceCertificateClaim object with the specified attributes.
    """
    return ComplianceCertificateClaim(
        did_web=get_compliance_certificate_claim_id(
            parent_domain_name=parent_domain_name,
            provider_name=provider_name,
            located_service_did_web=located_service_offering.did_web,
            scheme_did_web=compliance_certification_scheme.did_web,
        ),
        located_service_offering=located_service_offering,
        compliance_certification_scheme=compliance_certification_scheme,
    )


def create_third_party_compliance_certification_claim(
    parent_domain_name: str,
    provider_name: str,
    location_id: str,
    service_id: str,
    compliance_certification_scheme: Optional[GaiaxObjectWithDidWeb],
    compliance_assessment_body: GaiaxObjectWithDidWeb,
) -> ThirdPartyComplianceCertificateClaim:
    """
    This function creates a third-party compliance certification claim object by combining various inputs and
    generating a unique DID web for the claim.

    ## Example Usage
    ```python
    located_service_offering = LocatedServiceOffering(...)
    compliance_certification_scheme = ThirdPartyComplianceCertificationScheme(...)
    compliance_assessment_body = ComplianceAssessmentBody(...)
    claim = create_third_party_compliance_certification_claim(
        parent_domain_name="example.com",
        provider_name="Provider A",
        located_service_offering=located_service_offering,
        compliance_certification_scheme=compliance_certification_scheme,
        compliance_assessment_body=compliance_assessment_body,
    )
    ``
    :param parent_domain_name (str): The name of the parent domain.
    :param provider_name (str): The name of the provider.
    :param located_service_offering (LocatedServiceOffering): An instance of the LocatedServiceOffering class
    representing the service offering.
    :param compliance_certification_scheme (Optional[ThirdPartyComplianceCertificationScheme]): An optional instance of
    the ThirdPartyComplianceCertificationScheme class representing the certification scheme.
    :param compliance_assessment_body (ComplianceAssessmentBody): An instance of the ComplianceAssessmentBody class
    representing the compliance assessment body.
    :return: claim (ThirdPartyComplianceCertificateClaim): An instance of the ThirdPartyComplianceCertificateClaim class
    representing the third-party compliance certification claim.
    """

    located_service_offering_did = get_located_service_did(
        parent_domain_name=parent_domain_name,
        provider_name=provider_name,
        location_id=location_id,
        service_id=service_id,
    )
    return ThirdPartyComplianceCertificateClaim(
        did_web=get_third_party_compliance_certificate_claim_did(
            parent_domain_name=parent_domain_name,
            provider_name=provider_name,
            located_service_did_web=located_service_offering_did,
            scheme_did_web=compliance_certification_scheme.did_web,
        ),
        located_service_offering=GaiaxObjectWithDidWeb(did_web=located_service_offering_did),
        compliance_certification_scheme=compliance_certification_scheme,
        compliance_assessment_body=compliance_assessment_body,
    )


def create_self_assessed_compliance_criteria_claim(
    parent_domain_name: str,
    federator_name: str,
    federator_domain_name: str,
    provider_name: str,
    located_service_offerings: list[GaiaxObjectWithDidWeb],
    criteria: list[(str, str)],
) -> SelfAssessedComplianceCriteriaClaim:
    """
    The create_self_assessed_compliance_criteria_claim function creates a self-assessed compliance criteria claim
    object. It generates a unique identifier for the claim and constructs the claim object with the provided inputs.

    ## Example Usage
    ```python
    parent_domain_name = "example.com"
    provider_name = "provider"
    located_service_offerings = [GaiaxObjectWithDidWeb(...), GaiaxObjectWithDidWeb(...)]
    criteria = [("criterion1", "1"), ("criterion2", "2")]
    result = create_self_assessed_compliance_criteria_claim(parent_domain_name, provider_name,
    located_service_offerings, criteria)
    print(result)
    ```
    :param parent_domain_name (str): The domain name of the parent organization.
    :param provider_name (str): The name of the provider.
    :param located_service_offerings (list[GaiaxObjectWithDidWeb]): A list of located service offerings.
    :param cricriteria (list[(str, str)]): A list of tuples containing criterion names and levels.
    :return:SelfAssessedComplianceCriteriaClaim: The created self-assessed compliance criteria claim object.
    """
    criterion_names = [criterion[0] for criterion in criteria]

    return SelfAssessedComplianceCriteriaClaim(
        did_web=get_self_assessed_certificate_claim_did(
            parent_domain_name=parent_domain_name, provider_name=provider_name, criteria=criterion_names
        ),
        located_service_offering=located_service_offerings,
        criteria=[
            GaiaxObjectWithDidWeb(
                did_web=get_compliance_criterion_id(
                    parent_domain_name=federator_domain_name,
                    federator_name=federator_name,
                    criterion_name=criterion[0],
                    criterion_level=ComplianceLevel.from_str(level=f"Level {criterion[1]}").value,
                )
            )
            for criterion in criteria
        ],
    )
