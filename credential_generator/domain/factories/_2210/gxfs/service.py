# -*- coding: utf-8 -*-
"""
It defines methods to create gax-services objects
"""
from typing import List, Optional

# data-initialiser Library
from credential_generator.domain._2210 import GaiaxObjectWithDidWeb
from credential_generator.domain._2210.gxfs.gx_service import GxfsServiceOffering, LocatedServiceOffering
from credential_generator.domain._2210.trust_framework.service_offering import (
    AccessTypeMeans,
    DataAccountExport,
    RequestType,
    ServiceOfferingTermsAndConditions,
)
from credential_generator.domain.factories._2210 import get_provider_legal_participant_document_id
from credential_generator.domain.factories._2210.gxfs import (
    get_compliance_certificate_claim_id,
    get_compliance_certification_scheme_id,
    get_located_service_did,
    get_location_did,
    get_third_party_compliance_certificate_claim_did,
    get_third_party_compliance_certification_scheme_id,
)
from credential_generator.domain.factories._2210.trust_framework import build_description, get_service_offering_id
from credential_generator.utilities.did_helpers import calculate_provider_hash_value, compute_hash_value


def create_located_service_offering(
    parent_domain_name: str,
    federator_name: str,
    federator_domain_name: str,
    provider_name: str,
    location_id: str,
    service_id: str,
    compliance_certificate_claims: Optional[list[str]] = None,
    third_party_compliance_certificate_claims: Optional[list[str]] = None,
    self_assessed_compliance_certificate_claim: Optional[str] = None,
) -> LocatedServiceOffering:
    """
    Create a located service offering with specified details within a parent domain.

    Args:
        parent_domain_name (str): The name of the parent domain.
        federator_name (str): The name of the federator.
        federator_domain_name (str): The domain name of the federator.
        provider_name (str): The name of the service provider.
        location_id (str): The ID of the location.
        service_id (str): The ID of the service.
        compliance_certificate_claims (Optional[list[str]]): List of compliance certificate claims.
        third_party_compliance_certificate_claims (Optional[list[str]]): List of third-party compliance certificate
        claims.
        self_assessed_compliance_certificate_claim (Optional[str]): Self-assessed compliance certificate claim.

    Returns:
        LocatedServiceOffering: The created located service offering.
    """
    if compliance_certificate_claims is None:
        compliance_certificate_claims = []

    if third_party_compliance_certificate_claims is None:
        third_party_compliance_certificate_claims = []

    located_service_offering_did = get_located_service_did(
        parent_domain_name=parent_domain_name,
        provider_name=provider_name,
        location_id=location_id,
        service_id=service_id,
    )

    has_claims = [
        GaiaxObjectWithDidWeb(
            did_web=get_compliance_certificate_claim_id(
                parent_domain_name=parent_domain_name,
                provider_name=provider_name,
                located_service_did_web=located_service_offering_did,
                scheme_did_web=get_compliance_certification_scheme_id(
                    parent_domain_name=federator_domain_name,
                    federator_name=federator_name,
                    compliance_reference_id=compliance_reference_id,
                ),
            )
        )
        for compliance_reference_id in compliance_certificate_claims
    ]
    has_third_party_claims = [
        GaiaxObjectWithDidWeb(
            did_web=get_third_party_compliance_certificate_claim_did(
                parent_domain_name=parent_domain_name,
                provider_name=provider_name,
                located_service_did_web=located_service_offering_did,
                scheme_did_web=get_third_party_compliance_certification_scheme_id(
                    parent_domain_name=federator_domain_name,
                    federator_name=federator_name,
                    compliance_reference_id=compliance_reference_id,
                ),
            )
        )
        for compliance_reference_id in third_party_compliance_certificate_claims
    ]

    has_self_assessed_compliance_criteria_claim = (
        None
        if self_assessed_compliance_certificate_claim is None
        else GaiaxObjectWithDidWeb(did_web=self_assessed_compliance_certificate_claim)
    )

    return LocatedServiceOffering(
        isHostedOn=GaiaxObjectWithDidWeb(
            did_web=get_location_did(
                parent_domain_name=parent_domain_name,
                provider_name=provider_name,
                location_id=location_id.strip(),
            )
        ),
        isImplementationOf=GaiaxObjectWithDidWeb(
            did_web=get_service_offering_id(
                parent_domain_name=parent_domain_name,
                provider_name=provider_name,
                service_id=service_id.strip(),
            )
        ),
        did_web=located_service_offering_did,
        hasComplianceCertificateClaim=has_claims + has_third_party_claims,
        hasSelfAssessedComplianceCriteriaClaim=has_self_assessed_compliance_criteria_claim,
        hasComplianceCertificateCredential=[],
        hasSelfAssessedComplianceCriteriaCredential=None,
    )


def create_gxfs_service_offering(
    parent_domain_name: str,
    provider_name: str,
    service_id: str,
    service_name: str,
    description: Optional[str] = None,
    keywords: Optional[List[str]] = None,
    layers: Optional[List[str]] = None,
    terms_and_conditions: Optional[str] = None,
    format_type: str = "application/zip",
    policy: str = "default: allow",
    request_type: RequestType = RequestType.API,
    access_type: AccessTypeMeans = AccessTypeMeans.DIGITAL,
) -> GxfsServiceOffering:
    """
    Create a Gaia-X Federation Services (GXFS) service offering with various parameters.

    Args:
        parent_domain_name (str): The name of the parent domain.
        provider_name (str): The name of the service provider.
        service_id (str): The ID of the service.
        service_name (str): The name of the service.
        description (Optional[str]): The description of the service.
        keywords (Optional[List[str]]): Keywords related to the service.
        layers (Optional[List[str]]): Layers associated with the service.
        terms_and_conditions (Optional[str]): Terms and conditions of the service.
        format_type (str): The format type of the service.
        policy (str): The policy of the service.
        request_type (RequestType): The request type for the service.
        access_type (AccessTypeMeans): The access type for the service.

    Returns:
        GxfsServiceOffering: The created GXFS service offering.
    """
    terms_and_conditions_hash = (
        compute_hash_value(bytes(terms_and_conditions, "utf-8")) if terms_and_conditions is not None else ""
    )

    if description is None:
        description = build_description(description, provider_name, service_name)
    return GxfsServiceOffering(
        did_web=get_service_offering_id(
            parent_domain_name=parent_domain_name, provider_name=provider_name, service_id=service_id.strip()
        ),
        name=service_name,
        providedBy=GaiaxObjectWithDidWeb(
            did_web=get_provider_legal_participant_document_id(
                parent_domain_name=parent_domain_name,
                provider_name=provider_name,
                hash_value=calculate_provider_hash_value(provider_name=provider_name),
            )
        ),
        dataAccountExport=[
            DataAccountExport(requestType=request_type, accessType=access_type, formatType=format_type),
        ],
        policy=policy,
        termsAndConditions=ServiceOfferingTermsAndConditions(
            url=terms_and_conditions if terms_and_conditions is not None else "", hash=terms_and_conditions_hash
        ),
        description=description,
        keywords=keywords,
        layers=[] if layers is None else layers,
    )
