# -*- coding: utf-8 -*-
"""
It defines methods to create gax-participant objects
"""
# data-initialiser Library
from credential_generator.domain._2210 import GaiaxObjectWithDidWeb
from credential_generator.domain._2210.gxfs.gx_participant import Location
from credential_generator.domain.factories._2210 import get_provider_legal_participant_document_id
from credential_generator.domain.factories._2210.gxfs import get_located_service_did, get_location_did
from credential_generator.domain.factories._2210.trust_framework import get_service_offering_id
from credential_generator.utilities import find_administrative_location
from credential_generator.utilities.did_helpers import calculate_provider_hash_value


def create_location(
    parent_domain_name: str,
    provider_name: str,
    location_id: str,
    country: str,
    state: str,
    urban_area: str,
    provider_designation: str,
    service_ids: list[str],
) -> Location:
    """
    Create a location with specified details for a provider within a parent domain.

    Args:
        parent_domain_name (str): The name of the parent domain.
        provider_name (str): The name of the provider.
        location_id (str): The ID of the location.
        country (str): The country of the location.
        state (str): The state of the location.
        urban_area (str): The urban area of the location.
        provider_designation (str): The designation of the provider.
        service_ids (list[str]): List of service IDs associated with the location.

    Returns:
        Location: The created location object.
    """
    location_id = location_id.strip()
    service_ids = [service_id.strip() for service_id in service_ids]

    location_did = get_location_did(
        parent_domain_name=parent_domain_name, provider_name=provider_name, location_id=location_id
    )
    provider_did = get_provider_legal_participant_document_id(
        parent_domain_name=parent_domain_name,
        provider_name=provider_name,
        hash_value=calculate_provider_hash_value(provider_name=provider_name),
    )
    administrative_location = find_administrative_location(country_name=country, department=state)
    service_offering_dids = [
        get_service_offering_id(
            parent_domain_name=parent_domain_name, provider_name=provider_name, service_id=service_id
        )
        for service_id in service_ids
    ]
    located_service_dids = [
        get_located_service_did(
            parent_domain_name=parent_domain_name,
            provider_name=provider_name,
            location_id=location_id,
            service_id=service_id,
        )
        for service_id in service_ids
    ]
    return Location(
        did_web=location_did,
        hasProvider=GaiaxObjectWithDidWeb(did_web=provider_did),
        country=country,
        state=state,
        urbanArea=urban_area,
        providerDesignation=provider_designation,
        hasAdministrativeLocation=administrative_location,
        canHostServiceOffering=[
            GaiaxObjectWithDidWeb(did_web=service_offering_did) for service_offering_did in service_offering_dids
        ],
        hasLocatedServiceOffering=[
            GaiaxObjectWithDidWeb(did_web=located_service_did) for located_service_did in located_service_dids
        ],
    )
