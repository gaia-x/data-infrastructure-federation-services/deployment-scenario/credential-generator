# -*- coding: utf-8 -*-
"""
It defines how to compute did web for gxfs-fr objects
"""
from credential_generator.domain import OntologyType

# data-initialiser Library
from credential_generator.domain.factories._2210 import (
    get_federator_id_root,
    get_provider_id_root,
    simplify_domain_name,
)
from credential_generator.utilities.did_helpers import (
    calculate_compliance_certificate_claim_hash_value,
    calculate_compliance_certification_scheme_hash_value,
    calculate_compliance_criterion_hash_value,
    calculate_compliance_label_hash_value,
    calculate_compliance_reference_hash_value,
    calculate_located_service_hash_value,
    calculate_location_hash_value,
    calculate_self_assessed_certificate_claim_hash_value,
    calculate_third_party_compliance_certificate_claim_hash_value,
)
from credential_generator.utilities.validators import check_required_parameters


def get_compliance_criterion_id(
    parent_domain_name: str, federator_name: str, criterion_name: str, criterion_level: str
) -> str:
    """
    Get the compliance criterion ID based on the parent domain name, federator name, criterion name, and criterion
    level.

    Args:
        parent_domain_name (str): The name of the parent domain.
        federator_name (str): The name of the federator.
        criterion_name (str): The name of the compliance criterion.
        criterion_level (str): The level of the compliance criterion.

    Returns:
        str: The compliance criterion DID.
    """
    check_required_parameters(
        parent_domain_name=parent_domain_name,
        federator_name=federator_name,
        criterion_name=criterion_name,
        criterion_level=criterion_level,
    )

    simplified_domain_name = simplify_domain_name(parent_domain_name)
    federator_id_root = get_federator_id_root(simplified_domain_name, federator_name)
    directory = OntologyType.get_directory_name(OntologyType.COMPLIANCE_CRITERION)
    compliance_hash = calculate_compliance_criterion_hash_value(criterion_name, criterion_level)

    return f"{federator_id_root}/{directory}/{compliance_hash}/data.json"


def get_compliance_label_id(parent_domain_name: str, federator_name: str, label_name: str, label_level: str) -> str:
    """
    Get the compliance label ID based on the parent domain name, federator name, label name, and label level.

    Args:
        parent_domain_name (str): The name of the parent domain.
        federator_name (str): The name of the federator.
        label_name (str): The name of the compliance label.
        label_level (str): The level of the compliance label.

    Returns:
        str: The compliance label ID.
    """
    check_required_parameters(
        parent_domain_name=parent_domain_name,
        federator_name=federator_name,
        label_name=label_name,
        label_level=label_level,
    )

    simplified_domain_name = simplify_domain_name(parent_domain_name)
    federator_id_root = get_federator_id_root(simplified_domain_name, federator_name)
    directory = OntologyType.get_directory_name(OntologyType.COMPLIANCE_LABEL)
    label_hash = calculate_compliance_label_hash_value(label_level=label_level)

    return f"{federator_id_root}/{directory}/{label_hash}/data.json"


def get_location_did(parent_domain_name: str, provider_name: str, location_id: str) -> str:
    """
    Get the location DID based on the parent domain name, provider name, and location ID.

    Args:
        parent_domain_name (str): The name of the parent domain.
        provider_name (str): The name of the service provider.
        location_id (str): The ID of the location.

    Returns:
        str: The location DID.
    """
    check_required_parameters(
        parent_domain_name=parent_domain_name,
        provider_name=provider_name,
        location_id=location_id,
    )

    provider_id_root = get_provider_id_root(parent_domain_name, provider_name)
    directory = OntologyType.get_directory_name(OntologyType.LOCATION)
    location_hash = calculate_location_hash_value(location_id=location_id)

    return f"{provider_id_root}/{directory}/{location_hash}/data.json"


def get_location_vc_did(parent_domain_name: str, provider_name: str, location_id: str) -> str:
    """
    Get the location VC DID based on the parent domain name, provider name, and location ID.

    Args:
        parent_domain_name (str): The name of the parent domain.
        provider_name (str): The name of the service provider.
        location_id (str): The ID of the location.

    Returns:
        str: The location VC DID.
    """
    check_required_parameters(
        parent_domain_name=parent_domain_name,
        provider_name=provider_name,
        location_id=location_id,
    )

    provider_id_root = get_provider_id_root(parent_domain_name, provider_name)
    directory = OntologyType.get_directory_name(OntologyType.LOCATION, is_vc=True)
    location_hash = calculate_location_hash_value(location_id=location_id)

    return f"{provider_id_root}/{directory}/{location_hash}/data.json"


def get_located_service_did(parent_domain_name: str, provider_name: str, location_id: str, service_id: str) -> str:
    """
    Get the located service DID based on the parent domain name, provider name, location ID, and service ID.

    Args:
        parent_domain_name (str): The name of the parent domain.
        provider_name (str): The name of the service provider.
        location_id (str): The ID of the location.
        service_id (str): The ID of the service.

    Returns:
        str: The located service DID.
    """
    check_required_parameters(
        parent_domain_name=parent_domain_name,
        provider_name=provider_name,
        location_id=location_id,
        service_id=service_id,
    )

    provider_id_root = get_provider_id_root(parent_domain_name, provider_name)
    directory = OntologyType.get_directory_name(OntologyType.LOCATED_SERVICE)
    located_service_hash = calculate_located_service_hash_value(location_id=location_id, service_id=service_id)

    return f"{provider_id_root}/{directory}/{located_service_hash}/data.json"


def get_located_service_vc_did(parent_domain_name: str, provider_name: str, location_id: str, service_id: str) -> str:
    """
    Get the located service VC DID based on the parent domain name, provider name, location ID, and service ID.

    Args:
        parent_domain_name (str): The name of the parent domain.
        provider_name (str): The name of the service provider.
        location_id (str): The ID of the location.
        service_id (str): The ID of the service.

    Returns:
        str: The located service VC DID.
    """
    check_required_parameters(
        parent_domain_name=parent_domain_name,
        provider_name=provider_name,
        location_id=location_id,
        service_id=service_id,
    )

    provider_id_root = get_provider_id_root(parent_domain_name, provider_name)
    directory = OntologyType.get_directory_name(OntologyType.LOCATED_SERVICE, is_vc=True)
    located_service_hash = calculate_located_service_hash_value(location_id=location_id, service_id=service_id)

    return f"{provider_id_root}/{directory}/{located_service_hash}/data.json"


def get_compliance_reference_id(
    parent_domain_name: str, federator_name: str, compliance_reference_title: str, compliance_reference_version: str
) -> str:
    """
    Get the compliance reference ID based on the parent domain name, federator name, compliance reference title, and
    version.

    Args:
        parent_domain_name (str): The name of the parent domain.
        federator_name (str): The name of the federator.
        compliance_reference_title (str): The title of the compliance reference.
        compliance_reference_version (str): The version of the compliance reference.

    Returns:
        str: The compliance reference ID.
    """
    check_required_parameters(
        parent_domain_name=parent_domain_name,
        federator_name=federator_name,
        compliance_reference_title=compliance_reference_title,
    )

    federator_id_root = get_federator_id_root(parent_domain_name, federator_name)
    directory = OntologyType.get_directory_name(OntologyType.COMPLIANCE_REF)
    compliance_hash = calculate_compliance_reference_hash_value(
        title=compliance_reference_title, version=compliance_reference_version
    )

    return f"{federator_id_root}/{directory}/{compliance_hash}/data.json"


def get_compliance_certification_scheme_id(
    parent_domain_name: str, federator_name: str, compliance_reference_id: str
) -> str:
    """
    Get the compliance certification scheme ID based on the parent domain name, federator name, and
    compliance reference ID.

    Args:
        parent_domain_name (str): The name of the parent domain.
        federator_name (str): The name of the federator.
        compliance_reference_id (str): The ID of the compliance reference.

    Returns:
        str: The compliance certification scheme ID.
    """
    check_required_parameters(
        parent_domain_name=parent_domain_name,
        federator_name=federator_name,
        compliance_reference_id=compliance_reference_id,
    )

    federator_id_root = get_federator_id_root(parent_domain_name, federator_name)
    directory = OntologyType.get_directory_name(OntologyType.COMPLIANCE_CERT_SCHEME)
    compliance_certification_scheme_hash = calculate_compliance_certification_scheme_hash_value(
        compliance_reference_id=compliance_reference_id
    )

    return f"{federator_id_root}/{directory}/{compliance_certification_scheme_hash}/data.json"


def get_third_party_compliance_certification_scheme_id(
    parent_domain_name: str, federator_name: str, compliance_reference_id: str
) -> str:
    """
    Get the third-party compliance certification scheme ID based on the parent domain name, federator name, and
    compliance reference ID.

    Args:
        parent_domain_name (str): The name of the parent domain.
        federator_name (str): The name of the federator.
        compliance_reference_id (str): The ID of the compliance reference.

    Returns:
        str: The third-party compliance certification scheme ID.
    """
    check_required_parameters(
        parent_domain_name=parent_domain_name,
        federator_name=federator_name,
        compliance_reference_id=compliance_reference_id,
    )

    federator_id_root = get_federator_id_root(parent_domain_name, federator_name)
    directory = OntologyType.get_directory_name(OntologyType.THIRD_PARTY_COMPLIANCE_CERT_SCHEME)
    compliance_certification_scheme_hash = calculate_compliance_certification_scheme_hash_value(
        compliance_reference_id=compliance_reference_id
    )

    return f"{federator_id_root}/{directory}/{compliance_certification_scheme_hash}/data.json"


def get_compliance_certificate_claim_id(
    parent_domain_name: str, provider_name: str, located_service_did_web: str, scheme_did_web: str
) -> str:
    """
    Get the compliance certificate claim ID based on the parent domain name, provider name, located service DID web,
    and scheme DID web.

    Args:
        parent_domain_name (str): The name of the parent domain.
        provider_name (str): The name of the provider.
        located_service_did_web (str): The DID web of the located service.
        scheme_did_web (str): The DID web of the compliance certification scheme.

    Returns:
        str: The compliance certificate claim ID.
    """
    check_required_parameters(
        parent_domain_name=parent_domain_name,
        provider_name=provider_name,
        located_service_did_web=located_service_did_web,
        scheme_did_web=scheme_did_web,
    )

    provider_id_root = get_provider_id_root(parent_domain_name, provider_name)
    directory = OntologyType.get_directory_name(OntologyType.COMPLIANCE_CERT_CLAIM)
    hash_value = calculate_compliance_certificate_claim_hash_value(located_service_did_web, scheme_did_web)

    return f"{provider_id_root}/{directory}/{hash_value}/data.json"


def get_third_party_compliance_certificate_claim_did(
    parent_domain_name: str, provider_name: str, located_service_did_web: str, scheme_did_web: str
) -> str:
    """
    Get the third-party compliance certificate claim DID based on the parent domain name, provider name,
    located service DID web, and scheme DID web.

    Args:
        parent_domain_name (str): The name of the parent domain.
        provider_name (str): The name of the provider.
        located_service_did_web (str): The DID web of the located service.
        scheme_did_web (str): The DID web of the compliance certification scheme.

    Returns:
        str: The third-party compliance certificate claim DID.
    """
    check_required_parameters(
        parent_domain_name=parent_domain_name,
        provider_name=provider_name,
        located_service_did_web=located_service_did_web,
        scheme_did_web=scheme_did_web,
    )

    provider_id_root = get_provider_id_root(parent_domain_name, provider_name)
    directory = OntologyType.get_directory_name(OntologyType.THIRD_PARTY_COMPLIANCE_CERT_CLAIM)
    hash_value = calculate_third_party_compliance_certificate_claim_hash_value(located_service_did_web, scheme_did_web)

    return f"{provider_id_root}/{directory}/{hash_value}/data.json"


def get_self_assessed_certificate_claim_did(parent_domain_name: str, provider_name: str, criteria: list[str]) -> str:
    """
    Get the self-assessed certificate claim DID based on the parent domain name, provider name, and criteria.

    Args:
        parent_domain_name (str): The name of the parent domain.
        provider_name (str): The name of the provider.
        criteria (list[str]): The list of criteria for the self-assessed certificate claim.

    Returns:
        str: The self-assessed certificate claim DID.
    """
    check_required_parameters(parent_domain_name=parent_domain_name, criteria=criteria)

    provider_id_root = get_provider_id_root(parent_domain_name, provider_name)
    directory = OntologyType.get_directory_name(OntologyType.SELF_ASSESSED_COMPLIANCE_CRITERIA_CLAIM)
    hash_value = calculate_self_assessed_certificate_claim_hash_value(criteria=criteria)

    return f"{provider_id_root}/{directory}/{hash_value}/data.json"
