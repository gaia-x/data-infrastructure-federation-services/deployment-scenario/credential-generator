# -*- coding: utf-8 -*-
"""
It defines factories to build domain objects.
"""

# Standard Library
from typing import Optional

from credential_generator.domain import OntologyType

# data-initialiser Library
from credential_generator.domain._2210 import GaiaxObjectWithDidWeb
from credential_generator.domain._2210.trust_framework.participant import Address, LegalPerson, LegalRegistrationNumber
from credential_generator.domain._2210.trust_framework.service_offering import (
    AccessTypeMeans,
    DataAccountExport,
    RequestType,
    ServiceOffering,
    ServiceOfferingTermsAndConditions,
)
from credential_generator.domain._2210.trust_framework.terms_and_conditions import GaiaXTermsAndConditions
from credential_generator.domain.factories._2210 import (
    create_legal_registration_number,
    get_id_root,
    get_provider_id_root,
    get_provider_legal_participant_document_id,
)
from credential_generator.utilities.did_helpers import (
    calculate_provider_hash_value,
    calculate_service_offering_hash_value,
)


class InvalidParticipantNameException(Exception):
    """Raised when the input value is not a valid provider name."""


class UnknownParticipantNameException(Exception):
    """Raised when the input value is an unknown provider name."""


class InvalidServiceIDException(Exception):
    """Raised when the input value is not a valid service id."""


def get_service_offering_id(parent_domain_name: str, provider_name: str, service_id: str) -> str:
    """
    Get the service offering ID based on the parent domain name, provider name, and service ID.

    Args:
        parent_domain_name (str): The name of the parent domain.
        provider_name (str): The name of the service provider.
        service_id (str): The ID of the service.

    Returns:
        str: The service offering ID.
    """
    check_service_offering_parameters(parent_domain_name, provider_name, service_id)

    return (
        f"{get_provider_id_root(parent_domain_name, provider_name)}/"
        f"{OntologyType.get_directory_name(OntologyType.SERVICE_OFFERING, is_vc=False)}/"
        f"{calculate_service_offering_hash_value(service_id=service_id)}/"
        f"data.json"
    )


def check_service_offering_parameters(parent_domain_name: str, provider_name: str, service_id: str) -> None:
    """
    Check the validity of the service offering parameters.

    Args:
        parent_domain_name (str): The name of the parent domain.
        provider_name (str): The name of the service provider.
        service_id (str): The ID of the service.

    Raises:
        TypeError: If any of the parameters are not non-empty strings.
        ValueError: If the service ID is an empty string after stripping.
    """
    if not isinstance(parent_domain_name, str):
        raise TypeError("parent_domain_name should be a non empty string")
    if not isinstance(provider_name, str):
        raise TypeError("provider_name should be a non empty string")
    if not isinstance(service_id, str):
        raise TypeError("service_id should be a non empty string")
    if not service_id.strip():
        raise ValueError("service_id should be a non empty string")


def get_service_offering_vc_id(parent_domain_name: str, provider_name: str, service_id: str) -> str:
    """
    Get the service offering VC ID based on the parent domain name, provider name, and service ID.

    Args:
        parent_domain_name (str): The name of the parent domain.
        provider_name (str): The name of the service provider.
        service_id (str): The ID of the service.

    Returns:
        str: The service offering VC ID.
    """
    check_service_offering_parameters(parent_domain_name, provider_name, service_id)
    return (
        f"{get_provider_id_root(parent_domain_name, provider_name)}/"
        f"{OntologyType.get_directory_name(OntologyType.SERVICE_OFFERING, is_vc=True)}/"
        f"{calculate_service_offering_hash_value(service_id=service_id)}/"
        f"data.json"
    )


def get_gaiax_terms_and_conditions_id(parent_domain_name: str, provider_name: str) -> str:
    """
    Get the Gaia-X terms and conditions ID based on the parent domain name and provider name.

    Args:
        parent_domain_name (str): The name of the parent domain.
        provider_name (str): The name used to generate the Gaia-X terms and conditions ID.

    Returns:
        str: The Gaia-X terms and conditions ID.
    """
    return (
        f"{get_id_root(parent_domain_name, provider_name)}/"
        f"{OntologyType.get_directory_name(OntologyType.GAIAX_TERMS_AND_CONDITIONS, is_vc=False)}/"
        f"{calculate_provider_hash_value(provider_name)}/"
        f"data.json"
    )


def get_gaiax_terms_and_conditions_vc_id(parent_domain_name: str, provider_name: str) -> str:
    """
    Get the Gaia-X terms and conditions VC ID based on the parent domain name and provider name.

    Args:
        parent_domain_name (str): The name of the parent domain.
        provider_name (str): The name used to generate the Gaia-X terms and conditions VC ID.

    Returns:
        str: The Gaia-X terms and conditions VC ID.
    """
    return (
        f"{get_id_root(parent_domain_name, provider_name)}/"
        f"{OntologyType.get_directory_name(OntologyType.GAIAX_TERMS_AND_CONDITIONS, is_vc=True)}/"
        f"{calculate_provider_hash_value(provider_name)}/"
        f"data.json"
    )


def get_gaiax_legal_registration_number_id(parent_domain_name: str, name: str) -> str:
    """
    Get the Gaia-X legal registration number ID based on the parent domain name and name.

    Args:
        parent_domain_name (str): The name of the parent domain.
        name (str): The name used to generate the Gaia-X legal registration number ID.

    Returns:
        str: The Gaia-X legal registration number ID.
    """
    return (
        f"{get_id_root(parent_domain_name, name)}/"
        f"{OntologyType.get_directory_name(OntologyType.GAIAX_LEGAL_REGISTRATION_NUMBER, is_vc=True)}/"
        f"{calculate_provider_hash_value(name)}/"
        f"data.json"
    )


def get_gaiax_legal_registration_number_vc_id(parent_domain_name: str, name: str) -> str:
    """
    Get the Gaia-X legal registration number VC ID based on the parent domain name and name.

    Args:
        parent_domain_name (str): The name of the parent domain.
        name (str): The name used to generate the Gaia-X legal registration number VC ID.

    Returns:
        str: The Gaia-X legal registration number VC ID.
    """
    return (
        f"{get_id_root(parent_domain_name, name)}/"
        f"{OntologyType.get_directory_name(OntologyType.GAIAX_LEGAL_REGISTRATION_NUMBER, is_vc=True)}/"
        f"{calculate_provider_hash_value(name)}/"
        f"data.json"
    )


def create_provider(
    parent_domain_name: str,
    provider_name: str,
    legal_name: str,
    country_name: str,
    lei_code: str,
    registration_number: str,
) -> LegalPerson:
    """
    Generates a LegalParticipant object from a provider name.
    :param parent_domain_name: name of parent domain
    :param provider_name: name of provider
    :return: a LegalPerson object
    :raise: InvalidProviderNameException if provider name is empty.
    :raise: UnknownProviderNameException if provider name is not present in provider's information db.
    """

    if provider_name is None or not provider_name.strip():
        raise InvalidParticipantNameException("Participant name is empty, cannot create a LegalPerson object")

    legal_participant_did_web = get_provider_legal_participant_document_id(
        parent_domain_name=parent_domain_name,
        provider_name=provider_name,
        hash_value=calculate_provider_hash_value(provider_name=provider_name),
    )

    legal_registration_number_vc_did_web = get_gaiax_legal_registration_number_vc_id(
        parent_domain_name=parent_domain_name, name=provider_name
    )

    address = Address(country_subdivision_code=country_name)

    registration_number: LegalRegistrationNumber = create_legal_registration_number(
        did_web=legal_registration_number_vc_did_web,
        lei_code=lei_code,
        registration_number=registration_number,
    )

    return LegalPerson(
        did_web=legal_participant_did_web,
        legalName=legal_name,
        headquarterAddress=address,
        legalAddress=address,
        legalRegistrationNumber=registration_number,
    )


def create_service_offering(
    parent_domain_name: str,
    provider_name: str,
    service_id: str,
    service_name: str,
    service_version: str,
    complies: Optional[str] = None,
    availables: Optional[str] = None,
    description: Optional[str] = None,
    keywords: Optional[list[str]] = None,
    terms_and_conditions: Optional[str] = None,
) -> ServiceOffering:
    """
    create a ServiceOffering instance from given parameters.
    :param description:
    :param availables:
    :param complies:
    :param service_version:
    :param service_name:
    :param parent_domain_name: name of parent domain
    :param provider_name: name of provider
    :param service_id: id of service offering
    :return: a ServiceOffering instance
    :raise: InvalidProviderNameException if provider name is empty.
    :raise: InvalidServiceIDException if service id is empty.
    """
    # pylint: disable=unused-argument
    return ServiceOffering(
        did_web=get_service_offering_id(
            parent_domain_name=parent_domain_name, provider_name=provider_name, service_id=service_id.strip()
        ),
        name=service_name,
        providedBy=GaiaxObjectWithDidWeb(
            did_web=get_provider_legal_participant_document_id(
                parent_domain_name=parent_domain_name,
                provider_name=provider_name,
                hash_value=calculate_provider_hash_value(provider_name=provider_name),
            )
        ),
        dataAccountExport=[
            DataAccountExport(
                requestType=RequestType.API, accessType=AccessTypeMeans.DIGITAL, formatType="application/zip"
            ),
        ],
        policy=[""],
        termsAndConditions=ServiceOfferingTermsAndConditions(
            url=terms_and_conditions if terms_and_conditions is not None else "", hash="123"
        ),
        description=build_description(description, provider_name, service_name),
        keywords=keywords,
    )


def build_description(description, provider_name, service_name):
    """
    Build description
    :param description:
    :param provider_name:
    :param service_name:
    :return:
    """
    return (
        description
        if description is not None and description.strip() != ""
        else f"{service_name} is a service offered by {provider_name.upper()}"
    )


def create_gaiax_terms_and_conditions(
    parent_domain_name: str, provider_name: str, text: str
) -> GaiaXTermsAndConditions:
    """
    Creates a GaiaXTermsAndConditions instance from given parameters.
    :param parent_domain_name: name of parent domain
    :param provider_name: name of provider
    :param text: text of applicable GaiaX Terms and Conditions
    :return: GaiaXTermsAndConditions instance
    :raise: InvalidProviderNameException if provider name is empty.
    :raise: InvalidServiceIDException if service id is empty.
    """

    if not isinstance(text, str):
        raise TypeError("terms and conditions text must be a non empty string")

    if text is None or not text.strip():
        raise ValueError("terms and conditions text must be a non empty string")

    return GaiaXTermsAndConditions(
        did_web=get_gaiax_terms_and_conditions_id(parent_domain_name=parent_domain_name, provider_name=provider_name),
        termAndConditions=text,
    )
