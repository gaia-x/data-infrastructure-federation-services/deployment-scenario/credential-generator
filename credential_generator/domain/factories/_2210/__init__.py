# -*- coding: utf-8 -*-
"""
It defines utilities functions
"""
from typing import Optional
import uuid

from credential_generator.domain import OntologyType
from credential_generator.domain._2210.trust_framework.participant import LegalRegistrationNumber
from credential_generator.utilities.validators import check_required_parameters

# data-initialiser Library


def simplify_domain_name(domain_name: str) -> str:
    """
    Simplify the domain name by removing leading and trailing spaces, converting to lowercase,
    and removing 'provider.' if present.

    Args:
        domain_name (str): The domain name to be simplified.

    Returns:
        str: The simplified domain name.
    """
    check_required_parameters(domain_name=domain_name)

    domain_name = domain_name.strip().lower()
    return domain_name.replace("provider.", "")


def check_hash_value(hash_value: Optional[str]) -> str:
    """Check and validate the UUID parameter.

    Args:
        hash_value (str): The UUID parameter.

    Raises:
        TypeError: If the UUID parameter is not a string.
        ValueError: If the UUID parameter is invalid.

    Returns:
        str: The validated UUID value.

    """
    if hash_value is None:
        return str(uuid.uuid4())

    if not isinstance(hash_value, str):
        raise TypeError(f"The UUID '{hash_value}' is not a string.")

    return hash_value


def get_federator_id_root(parent_domain_name: str, federator_name: str) -> str:
    """Get the root of the federator's DID web.

    Args:
        parent_domain_name (str): The parent domain name.
        federator_name (str): The name of the federator.

    Returns:
        str: The root of the federator's DID web.

    """
    check_required_parameters(role=federator_name, parent_domain_name=parent_domain_name)

    if "provider" in parent_domain_name:
        new_domain_name = parent_domain_name.replace("provider.", "")
    elif "auditor" in parent_domain_name:
        new_domain_name = parent_domain_name.replace("auditor.", "")
    else:
        new_domain_name = parent_domain_name

    return f"https://{federator_name}.{new_domain_name}"


def get_auditor_id_root(parent_domain_name: str, auditor_name: str) -> str:
    """Get the root of the auditor's DID web.

    Args:
        parent_domain_name (str): The parent domain name.
        auditor_name (str): The name of the auditor.

    Returns:
        str: The root of the auditor's DID web.

    """
    check_required_parameters(role=auditor_name, parent_domain_name=parent_domain_name)

    if "provider" in parent_domain_name:
        new_domain_name = parent_domain_name.replace("provider", "auditor")
    elif "auditor" not in parent_domain_name:
        new_domain_name = f"auditor.{parent_domain_name}"
    else:
        new_domain_name = parent_domain_name

    return f"https://{auditor_name}.{new_domain_name}"


def get_id_root(parent_domain_name: str, name: str) -> str:
    """
    Get the root ID for a given parent domain and name.

    Args:
        parent_domain_name (str): The name of the parent domain.
        name (str): The name for which the root ID is generated.

    Returns:
        str: The root ID generated based on the parent domain and name.
    """
    check_required_parameters(role=name, parent_domain_name=parent_domain_name)

    return f"https://{name}.{parent_domain_name}"


def get_provider_id_root(parent_domain_name: str, provider_name: str) -> str:
    """Get the root of the provider's DID web.

    Args:
        parent_domain_name (str): The parent domain name.
        provider_name (str): The name of the provider.

    Returns:
        str: The root of the provider's id.

    """
    check_required_parameters(role=provider_name, parent_domain_name=parent_domain_name)

    if "auditor" in parent_domain_name:
        new_domain_name = parent_domain_name.replace("auditor", "provider")
    elif "provider" not in parent_domain_name:
        new_domain_name = f"provider.{parent_domain_name}"
    else:
        new_domain_name = parent_domain_name

    return f"https://{provider_name}.{new_domain_name}"


def get_provider_legal_participant_document_id(
    parent_domain_name: str, provider_name: str, hash_value: Optional[str] = None
) -> str:
    """Get the document ID for the Gaia-X Legal Participant object associated with a provider.

    Args:
        parent_domain_name (str): The parent domain name.
        provider_name (str): The name of the provider.
        hash_value (Optional[str], optional): An hash value or a uuid value to uniquely identify object.
        Defaults to None.

    Returns:
        str: The document ID.

    """
    uuid_value = check_hash_value(hash_value)

    return (
        f"{get_provider_id_root(parent_domain_name, provider_name)}"
        f"/{OntologyType.get_directory_name(ontology_type=OntologyType.LEGAL_PARTICIPANT, is_vc=False)}"
        f"/{uuid_value}"
        f"/data.json"
    )


def get_federator_legal_participant_document_id(
    parent_domain_name: str, federator_name: str, hash_value: Optional[str] = None
) -> str:
    """Get the document ID for the Gaia-X Legal Participant object associated with a federator.

    Args:
        parent_domain_name (str): The domain name.
        federator_name (str): The name of the federator.
        hash_value (Optional[str], optional): The UUID parameter. Defaults to None.

    Returns:
        str: The document ID.

    """
    uuid_value = check_hash_value(hash_value)

    return (
        f"{get_federator_id_root(parent_domain_name=parent_domain_name, federator_name=federator_name)}"
        f"/{OntologyType.get_directory_name(ontology_type=OntologyType.LEGAL_PARTICIPANT, is_vc=False)}"
        f"/{uuid_value}"
        f"/data.json"
    )


def get_auditor_legal_participant_document_id(
    parent_domain_name: str, auditor_name: str, hash_value: Optional[str] = None
) -> str:
    """Get the document ID for the Gaia-X Legal Participant object associated with an auditor.

    Args:
        parent_domain_name (str): The parent domain name.
        auditor_name (str): The name of the auditor.
        hash_value (Optional[str], optional): An hash value or a uuid value to uniquely identify object.
        Defaults to None.

    Returns:
        str: The document ID.

    """
    uuid_value = check_hash_value(hash_value)

    return (
        f"{get_auditor_id_root(parent_domain_name=parent_domain_name, auditor_name=auditor_name)}"
        f"/{OntologyType.get_directory_name(ontology_type=OntologyType.LEGAL_PARTICIPANT, is_vc=False)}"
        f"/{uuid_value}"
        f"/data.json"
    )


def get_compliance_reference_manager_legal_participant_document_id(parent_domain_name: str, manager_name: str) -> str:
    """Get the document ID for the Gaia-X Legal Participant object associated with a compliance reference manager.

    Args:
        parent_domain_name (str): The parent domain name.
        manager_name (str): The name of the compliance reference manager.

    Returns:
        str: The document ID.

    """
    return get_federator_legal_participant_document_id(
        parent_domain_name=parent_domain_name, federator_name=manager_name
    )


def compute_french_vat_id(french_legal_registration_number: str) -> str:
    """Compute the French VAT ID based on the French legal registration number.

    Args:
        french_legal_registration_number (str): The French legal registration number.

    Returns:
        str: The computed French VAT ID.

    """
    if not french_legal_registration_number.startswith("FR"):
        return french_legal_registration_number

    siret_number = french_legal_registration_number.split(".")[1]
    control_code = 12 + (3 * ((int(siret_number) % 97) % 97)) % 97

    return f"FR{control_code:2}{siret_number}"


def create_legal_registration_number(did_web: str, lei_code: str, registration_number: str) -> LegalRegistrationNumber:
    """Create a legal registration number object based on the provided information.

    Args:
        did_web (str): The DID web.
        lei_code (str): The LEI code.
        registration_number (str): The registration number.

    Returns:
        LegalRegistrationNumber: The created legal registration number object.

    """
    if lei_code is not None and lei_code != "":
        return LegalRegistrationNumber(did_web=did_web, leiCode=lei_code)

    return LegalRegistrationNumber(did_web=did_web, vatID=registration_number)
