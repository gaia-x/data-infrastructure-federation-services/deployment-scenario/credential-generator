# -*- coding: utf-8 -*-
"""
It defines Resources related classes.
"""
# Standard Library
from typing import Optional

from pydantic import Field

# data-initialiser Library
from credential_generator.domain._2210 import GaiaxObjectWithDidWeb
from credential_generator.domain._2210.trust_framework.participant import Address, LegalPerson


class Resource(GaiaxObjectWithDidWeb):
    """
    A resource that may be aggregated in a Service Offering or exist independently of it.
    """

    name: Optional[str] = Field(
        default=None, description="A human readable name of the data resource", serialization_alias="gx:name"
    )
    description: Optional[str] = Field(
        default=None, description="A free text description of the data resource", serialization_alias="gx:description"
    )
    aggregationOf: Optional[list] = Field(
        default=None,
        description="Resources related to the resource and that can exist independently of it",
        serialization_alias="gx:aggregationOf",
    )


class PhysicalResource(Resource):
    """
    A Physical resource is, but not limited to, a datacenter, a bare-metal service, a warehouse, a plant.
    """

    maintainedBy: LegalPerson = Field(
        description="Participant maintaining the resource in operational condition",
        serialization_alias="gx:maintainedBy",
    )
    ownedBy: Optional[LegalPerson] = Field(
        default=None, description="Participant owning the resource", serialization_alias="gx:ownedBy"
    )
    manufacturedBy: Optional[LegalPerson] = Field(
        default=None, description="Participant manufacturing the resource", serialization_alias="gx:manufacturedBy"
    )
    locationGPS: Optional[str] = Field(
        default=None,
        description="A list of physical GPS in ISO 6709:2008/Cor 1:2009 format",
        serialization_alias="gx:locationGPS",
    )
    locationAddress: Address = Field(
        description="A vcard:Address object that contains the physical location in ISO 3166-2 format.",
        serialization_alias="gx:locationGPS",
    )
