# -*- coding: utf-8 -*-
"""
It defines gaia-x trust framework participant relatives classes
"""
from __future__ import annotations
# Standard Library
from typing import Optional

from pydantic import Field, constr, model_validator

# data-initialiser Library
from credential_generator.domain._2210 import GaiaxObject, GaiaxObjectWithDidWeb


class Participant(GaiaxObjectWithDidWeb):
    """
    A Participant is a Legal Person or Natural Person, which is identified, onboarded and has a Gaia-X Self-Description
    """


class LegalRegistrationNumber(GaiaxObjectWithDidWeb):
    """
    Country`s registration number, which identifies one specific entity.
    """

    local: Optional[constr(min_length=3)] = Field(default=None, serialization_alias="gx:local")
    taxID: Optional[constr(min_length=3)] = Field(default=None, serialization_alias="gx:taxID")
    vatID: Optional[constr(min_length=0)] = Field(default=None, serialization_alias="gx:vatID")
    euid: Optional[constr(min_length=3)] = Field(default=None, serialization_alias="gx:EUID")
    eori: Optional[constr(min_length=3)] = Field(default=None, serialization_alias="gx:EORI")
    leiCode: Optional[constr(min_length=3)] = Field(default=None, serialization_alias="gx:leiCode")

    @model_validator(mode="after")
    def check_at_least_one_registration_number_set(self) -> "LegalRegistrationNumber":
        """
        Validator that checked that at least taxID or leiCode is set
        :return:
        """
        if all([not self.vatID, not self.leiCode, not self.local, not self.taxID, not self.euid, not self.eori]):
            raise ValueError("Either vatID or leiCode is required")
        return self


class Address(GaiaxObject):
    """
    location in ISO 3166-2 alpha2, alpha-3 or numeric format.

    """

    country_code: Optional[str] = Field(
        default=None,
        description="Optional country code in ISO 3166-1 alpha2 format.",
        serialization_alias="gx:addressCountryCode",
        pattern="^[A-Z]{2}$",
    )
    country_subdivision_code: str = Field(
        description="Country principal subdivision code in ISO 3166-2 format.",
        pattern="^[A-Z]{2}-[A-Z0-9]{1,3}$",
        serialization_alias="gx:countrySubdivisionCode",
    )


class Relationship(GaiaxObject):
    """
    A relationship between two organisations.
    """

    parentOrganizationOf: Optional[LegalPerson] = Field(
        default=None,
        serialization_alias="gx:parentOrganizationOf",
        description="A list of direct participant that this entity is a subOrganization of, if any",
    )
    subOrganizationOf: Optional[LegalPerson] = Field(
        default=None,
        serialization_alias="gx:subOrganizationOf",
        description="A direct participant with a legal mandate on this entity.",
    )


class LegalPerson(Participant):
    """
    A LegalParticipant is a Legal Person which is identified, onboarded and has a Gaia-X Self-Description.
    """

    legalName: str = Field(serialization_alias="gx:legalName")
    legalRegistrationNumber: LegalRegistrationNumber = Field(..., serialization_alias="gx:legalRegistrationNumber")
    relatedOrganization: Optional[list] = Field(default=None, serialization_alias="gx:relatedOrganizations")
    headquarterAddress: Address = Field(..., serialization_alias="gx:headquarterAddress")
    legalAddress: Address = Field(..., serialization_alias="gx:legalAddress")
    # gaiaxTermsAndConditions: str = Field(serialization_alias="gx-terms-and-conditions:gaiaxTermsAndConditions")
