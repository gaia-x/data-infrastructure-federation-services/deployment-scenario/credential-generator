# -*- coding: utf-8 -*-
"""
It defines GaiaXTermsAndConditions object
"""
# Standard Library
from typing import Final

from pydantic import Field

# data-initialiser Library
from credential_generator.domain._2210 import GaiaxObjectWithDidWeb

GAIAX_REGISTRY_TERMS_AND_CONDITION_URL: Final = "https://registry.lab.gaia-x.eu/v1/api/termsAndConditions?version=22.10"


class GaiaXTermsAndConditions(GaiaxObjectWithDidWeb):
    """
    Gaia-X terms and conditions, each issuer has to agree.
    """

    termAndConditions: str = Field(serialization_alias="gx:termsAndConditions")
