# -*- coding: utf-8 -*-
"""
It defines gaia-x trust framework service offering relatives classes
"""
# Standard Library
from enum import Enum
from typing import List, Optional

from pydantic import AnyUrl, Field

# data-initialiser Library
from credential_generator.domain._2210 import GaiaxObject, GaiaxObjectWithDidWeb


class PersonalDataProtectionRegime(str, Enum):
    """
    DataProtectionRegime defines known data protection regime.
    Refer to https://gaia-x.gitlab.io/policy-rules-committee/trust-framework/service_and_subclasses/#service-offering
    Personal Data Protection Regimes
    """

    GDPR2016 = "GDPR2016"  # General Data Protection Regulation / EEA®
    LGPD2019 = "LGPD2019"  # General Personal Data Protection Law (Lei Geral de Proteção de Dados Pessoais...
    PDPA2012 = "PDPA2012"  # Personal Data Protection Act 2012 / SGP
    CCPA2018 = "CCPA2018"  # California Consumer Privacy Act / US-CA
    VCDPA2021 = "VCDPA2021"  # Virginia Consumer Data Protection Act / US-VA


class RequestType(str, Enum):
    """
    the mean to request data retrieval: API, email, webform,
    unregisteredLetter, registeredLetter, supportCenter
    """

    API = "API"
    EMAIL = "email"
    WEBFORM = "webform"
    UNREGISTERED_LETTER = "unregisteredLetter"
    REGISTERED_LETTER = "registeredLetter"
    SUPPORT_CENTER = "supportCenter"


class AccessTypeMeans(str, Enum):
    """
    type of data support: digital, physical
    """

    DIGITAL = "digital"
    PHYSICAL = "physical"


class DataAccountExport(GaiaxObject):
    """
    The purpose is to enable the participant ordering the service to assess the feasability
    to export its personal and non-personal data out of the service.
    """

    requestType: RequestType = Field(
        serialization_alias="gx:requestType",
        description="the mean to request data retrieval: "
        "API, email, webform, unregisteredLetter, registeredLetter, supportCenter",
    )
    accessType: AccessTypeMeans = Field(
        serialization_alias="gx:accessType", description="type of data support: digital, physical."
    )
    formatType: str = Field(
        serialization_alias="gx:formatType",
        pattern="^\\w+/[-+.\\w]+$",
        description="type of Media Types (formerly known as MIME types) as defined by the IANA.",
    )


class ServiceOfferingTermsAndConditions(GaiaxObject):
    """
    Terms and Conditions applying to that service.
    """

    url: str = Field(serialization_alias="gx:URL", description="a resolvable link to document")
    hash: str = Field(serialization_alias="gx:hash", description="sha256 hash of the document.")


class ServiceOffering(GaiaxObjectWithDidWeb):
    """
    This is the generic format for all service offerings
    """

    providedBy: GaiaxObjectWithDidWeb = Field(
        serialization_alias="gx:providedBy",
        description="DID of the participant (self-description) providing this service offering.",
    )

    name: str = Field(
        serialization_alias="gx:name",
        # description="human readable name of the service offering"
    )

    termsAndConditions: ServiceOfferingTermsAndConditions = Field(
        serialization_alias="gx:termsAndConditions",
        description="list of methods to export data from your user`s account out of the service",
    )

    policy: list[str] | str = Field(
        default="default: allow",
        min_items=1,
        serialization_alias="gx:policy",
        description="a list of policy expressed using a DSL (e.g., Rego or ODRL) "
        "(access control, throttling, usage, retention, …)",
    )

    dataProtectionRegime: Optional[list[PersonalDataProtectionRegime]] = Field(
        default=None,
        min_items=0,
        serialization_alias="gx:dataProtectionRegime",
        description="a list of data protection regime from the list available",
    )

    dataAccountExport: list[DataAccountExport] = Field(
        min_items=1,
        serialization_alias="gx:dataAccountExport",
        description="list of methods to export data from your user`s account out of the service",
    )

    description: Optional[str] = Field(
        default=None,
        serialization_alias="gx:description",
        description="A description in natural language",
    )

    keywords: Optional[List[str]] = Field(
        default=None,
        serialization_alias="gx:keyword",
        description="Keywords that describe / tag the service",
    )

    provisionType: Optional[str] = Field(
        default=None,
        serialization_alias="gx:provisionType",
        description="Provision type of the service",
    )

    endpoint: Optional[AnyUrl] = Field(
        default=None,
        serialization_alias="gx:endpoint",
        description="Endpoint through which the Service Offering can be accessed.",
    )

    hostedOn: Optional[list[str]] = Field(
        default=None,
        serialization_alias="gx:hostedOn",
        description="List of Resource references where service is hosted and can be instantiated.",
    )

    dependsOn: Optional[list[str]] = Field(
        default=None,
        serialization_alias="gx:dependsOn",
        description="a resolvable link to the service offering self-description related to "
        "the service and that can exist independently of it.",
    )

    aggregationOf: Optional[list[str]] = Field(
        default=None,
        serialization_alias="gx:aggregationOf",
        description="DID of the resource (self-description) related to the service and "
        "that can exist independently of it.",
    )
