# -*- coding: utf-8 -*-
"""
It defines GaiaX root classes.
"""
# Standard Library
from abc import ABC

from pydantic import BaseModel, Field


class GaiaxObject(BaseModel, ABC):
    """
    Root class for Gaia-X objects.
    NOTE: this is an abstract class and should not be instantiated directly.
    """

    class Config:
        """
        Pydantic configuration
        """

        populate_by_name = True
        str_strip_whitespace = True
        use_enum_values = True


class GaiaxObjectWithDidWeb(GaiaxObject):
    """
    Root class for Gaia-X objects with did-web.
    """

    did_web: str = Field(
        serialization_alias="id",
        description="DID web",
    )
