# -*- coding: utf-8 -*-
"""
It defines compliance_references relatives classes
"""

# Standard Library
from typing import Optional

from pydantic import Field

from credential_generator.config import ASTER_CONFORMITY_PREFIX

# data-initialiser Library
from credential_generator.domain import ComplianceLevel, CriterionCategory
from credential_generator.domain._2210 import GaiaxObjectWithDidWeb
from credential_generator.domain._2210.trust_framework.participant import LegalPerson


class ComplianceAssessmentBody(LegalPerson):
    """
    ComplianceReference defines a compliance assessment body as defined in aster-conformity schema
    """

    cab_id: Optional[str] = None
    name: Optional[str] = None


class ComplianceReferenceManager(LegalPerson):
    """
    ComplianceReference defines a compliance reference manager as defined in aster-conformity schema
    """

    has_compliance_references: list[str] = Field(
        default=[],
        serialization_alias=f"{ASTER_CONFORMITY_PREFIX}:hasComplianceReferences",
        description="List of Ids of ComplianceReferences (self-description) managed by this ComplianceReferenceManager",
    )


class ComplianceReference(GaiaxObjectWithDidWeb):
    """
    ComplianceReference defines compliance references
    """

    compliance_reference_id: str

    reference_url: str = Field(
        serialization_alias=f"{ASTER_CONFORMITY_PREFIX}:hasReferenceUrl",
        description="URI to reference the content of the compliance reference in a single PDF file.",
    )
    reference_sha256: str = Field(
        serialization_alias=f"{ASTER_CONFORMITY_PREFIX}:hasSha256",
        description="SHA256 of pdf document referenced by hasReferenceUrl",
    )
    reference_type: Optional[str] = Field(
        default=None,
        serialization_alias=f"{ASTER_CONFORMITY_PREFIX}:referenceType",
        description="Type of Compliance Reference.",
    )
    title: str = Field(
        serialization_alias=f"{ASTER_CONFORMITY_PREFIX}:hasComplianceReferenceTitle",
        description="Name of the Compliance Reference.",
    )
    description: Optional[str] = Field(
        default=None,
        serialization_alias=f"{ASTER_CONFORMITY_PREFIX}:hasDescription",
        description="A description in natural language.",
    )
    compliance_reference_manager: str = Field(
        serialization_alias=f"{ASTER_CONFORMITY_PREFIX}:hasComplianceReferenceManager",
        description="Id of Participant (self-description) in charge of managing this Compliance Reference",
    )
    version: Optional[str] = Field(
        default=None,
        serialization_alias=f"{ASTER_CONFORMITY_PREFIX}:hasVersion",
        description="String designated the version of the reference document.",
    )
    valid_from: Optional[str] = Field(
        default=None,
        serialization_alias=f"{ASTER_CONFORMITY_PREFIX}:cRValidFrom",
        description="Indicates the first date when the compliance reference goes into effect.",
    )
    valid_until: Optional[str] = Field(
        default=None,
        serialization_alias=f"{ASTER_CONFORMITY_PREFIX}:cRValidUntil",
        description="Indicates the last date when the compliance reference is no more into effect.",
    )
    compliance_certification_schemes: Optional[list[GaiaxObjectWithDidWeb]] = Field(
        default=[],
        serialization_alias=f"{ASTER_CONFORMITY_PREFIX}:hasComplianceCertificationScheme",
        description="List of schemes that grants certification.",
    )


class ComplianceCriterion(GaiaxObjectWithDidWeb):
    """
    ComplianceCriterion defines list of criterion
    """

    name: str = Field(
        serialization_alias=f"{ASTER_CONFORMITY_PREFIX}:hasName",
    )
    level: ComplianceLevel = Field(
        serialization_alias=f"{ASTER_CONFORMITY_PREFIX}:hasLevel",
    )
    reference_type: CriterionCategory = Field(
        serialization_alias=f"{ASTER_CONFORMITY_PREFIX}:hasCategory",
    )
    description: str = Field(
        serialization_alias=f"{ASTER_CONFORMITY_PREFIX}:hasDescription",
        description="DID of the compliance (self-description) providing this service offering.",
    )
    self_assessed: bool = Field(
        serialization_alias=f"{ASTER_CONFORMITY_PREFIX}:canBeSelfAssessed",
    )


class ComplianceLabel(GaiaxObjectWithDidWeb):
    """
    ComplianceLabel defines gaia-x labels
    """

    level: ComplianceLevel = Field(
        serialization_alias=f"{ASTER_CONFORMITY_PREFIX}:hasLevel",
    )
    name: str = Field(
        serialization_alias=f"{ASTER_CONFORMITY_PREFIX}:hasName",
    )
    description: str = Field(
        serialization_alias=f"{ASTER_CONFORMITY_PREFIX}:hasDescription",
        description="DID of the compliance (self-description) providing this service offering.",
    )
    criteria_list: Optional[list[GaiaxObjectWithDidWeb]] = Field(
        serialization_alias=f"{ASTER_CONFORMITY_PREFIX}:hasRequiredCriteria",
    )


class ComplianceCertificationScheme(GaiaxObjectWithDidWeb):
    """
    ComplianceCertificationScheme defines certification schemes (self-declared)
    """

    compliance_reference: ComplianceReference = Field(
        serialization_alias=f"{ASTER_CONFORMITY_PREFIX}:hasComplianceReference",
    )

    criteria_list: Optional[list[GaiaxObjectWithDidWeb]] = Field(
        serialization_alias=f"{ASTER_CONFORMITY_PREFIX}:grantsComplianceCriteria",
    )


class ThirdPartyComplianceCertificationScheme(ComplianceCertificationScheme):
    """
    ThirdPartyComplianceCertificationScheme defines certification schemes certified by a ComplianceAssessmentBody
    """

    compliance_assessment_bodies: list[ComplianceAssessmentBody] = Field(
        serialization_alias=f"{ASTER_CONFORMITY_PREFIX}:hasComplianceAssessmentBodies", default=[]
    )


class ComplianceCertificateClaim(GaiaxObjectWithDidWeb):
    """
    ComplianceCertificateClaim defines a certificate claim
    """

    located_service_offering: GaiaxObjectWithDidWeb = Field(
        serialization_alias=f"{ASTER_CONFORMITY_PREFIX}:hasLocatedServiceOffering"
    )
    compliance_certification_scheme: Optional[GaiaxObjectWithDidWeb] = Field(
        serialization_alias=f"{ASTER_CONFORMITY_PREFIX}:hasComplianceCertificationScheme"
    )


class ThirdPartyComplianceCertificateClaim(ComplianceCertificateClaim):
    """
    ThirdPartyComplianceCertificateClaim defines a third party certificate claim
    """

    compliance_assessment_body: GaiaxObjectWithDidWeb = Field(
        serialization_alias=f"{ASTER_CONFORMITY_PREFIX}:hasComplianceAssessmentBody"
    )


class SelfAssessedComplianceCriteriaClaim(GaiaxObjectWithDidWeb):
    """
    ComplianceCertificateCredential is a compliance certificate credential
    """

    located_service_offering: list[GaiaxObjectWithDidWeb] = Field(
        serialization_alias=f"{ASTER_CONFORMITY_PREFIX}:hasLocatedServiceOffering"
    )
    criteria: Optional[list[GaiaxObjectWithDidWeb]] = Field(
        serialization_alias=f"{ASTER_CONFORMITY_PREFIX}:hasAssessedComplianceCriteria"
    )


class SelfAssessedComplianceCriteriaCredential(GaiaxObjectWithDidWeb):
    """
    SelfAssessedComplianceCriteriaCredential is a self assessed compliance criteria credential
    """

    credential_subject: SelfAssessedComplianceCriteriaClaim = Field(serialization_alias="credentialSubject")
    is_valid: bool = Field(serialization_alias="isValid")


class ComplianceCertificateCredential(SelfAssessedComplianceCriteriaCredential):
    """
    ComplianceCriteriaCredential is a compliance criteria credential
    """


class ThirdPartyComplianceCertificateCredential(SelfAssessedComplianceCriteriaCredential):
    """
    ThirdPartyComplianceCriteriaCredential is a third party compliance criteria credential
    """
