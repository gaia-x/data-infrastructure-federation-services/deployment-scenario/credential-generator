# -*- coding: utf-8 -*-
"""
It defines gxfs-fr gx-participant relatives classes
"""
# Standard Library
from typing import Optional

from pydantic import Field

from credential_generator.config import ASTER_CONFORMITY_PREFIX

# data-initialiser Library
from credential_generator.domain._2210 import GaiaxObjectWithDidWeb


class Location(GaiaxObjectWithDidWeb):
    """
    The Location class is a Pydantic model that represents a location. It has four optional attributes: country, state,
    urbanArea, and providerDesignation. Each attribute has a default value of None and is validated using regular
    expressions.

    Example Usage
    ```python
    # Creating a location object
    location = Location(country="US", state="CA", urbanArea="LA", providerDesignation="ABC")

    # Accessing the attributes of the location object
    print(location.country)  # Output: US
    print(location.state)  # Output: CA
    print(location.urbanArea)  # Output: LA
    print(location.providerDesignation)  # Output: ABC
    ```
    """

    hasProvider: GaiaxObjectWithDidWeb = Field(
        serialization_alias=f"{ASTER_CONFORMITY_PREFIX}:hasProvider",
        description="DID of the participant (self-description) providing this service offering.",
    )

    canHostServiceOffering: Optional[list[GaiaxObjectWithDidWeb]] = Field(
        serialization_alias=f"{ASTER_CONFORMITY_PREFIX}:canHostServiceOffering",
        description="DID of the service offering that can be hosted in this location.",
    )

    country: Optional[str] = Field(
        default=None,
        description="country code for location according to iso 3166-2 list",
        serialization_alias=f"{ASTER_CONFORMITY_PREFIX}:country",
    )

    state: Optional[str] = Field(
        default=None,
        description="State or Department string for location according to iso 3166-2 list",
        serialization_alias=f"{ASTER_CONFORMITY_PREFIX}:state",
    )

    urbanArea: Optional[str] = Field(
        default=None,
        description="Urban Area string for location",
        serialization_alias=f"{ASTER_CONFORMITY_PREFIX}:urbanArea",
    )

    hasAdministrativeLocation: Optional[str] = Field(
        default=None,
        description="iso 3166-2 5 digit string for location",
        serialization_alias=f"{ASTER_CONFORMITY_PREFIX}:hasAdministrativeLocation",
    )

    providerDesignation: Optional[str] = Field(
        default=None,
        description="Provider designation string for location",
        serialization_alias=f"{ASTER_CONFORMITY_PREFIX}:providerDesignation",
    )

    hasLocatedServiceOffering: Optional[list[GaiaxObjectWithDidWeb]] = Field(
        serialization_alias=f"{ASTER_CONFORMITY_PREFIX}:hasLocatedServiceOffering",
        description="DID of the located service offering that can be hosted in this location.",
    )
