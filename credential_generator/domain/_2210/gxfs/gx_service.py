# -*- coding: utf-8 -*-
"""
It defines gx-services objects
"""
from typing import Optional

# data-initialiser Library
from pydantic import Field

from credential_generator.config import ASTER_CONFORMITY_PREFIX
from credential_generator.domain._2210 import GaiaxObjectWithDidWeb
from credential_generator.domain._2210.trust_framework.service_offering import ServiceOffering


class LocatedServiceOffering(GaiaxObjectWithDidWeb):
    """
    This class represents a LocatedServiceOffering as defined in gxfs-fr service ontology

    """

    isImplementationOf: GaiaxObjectWithDidWeb = Field(
        serialization_alias=f"{ASTER_CONFORMITY_PREFIX}:isImplementationOf",
        description="did web of associated service offering",
    )

    isHostedOn: GaiaxObjectWithDidWeb = Field(
        serialization_alias=f"{ASTER_CONFORMITY_PREFIX}:isHostedOn", description="did web of associated location"
    )

    hasComplianceCertificateClaim: list[GaiaxObjectWithDidWeb] = Field(
        default=[],
        serialization_alias=f"{ASTER_CONFORMITY_PREFIX}:hasComplianceCertificateClaim",
        description="list of compliance certificate claim associated with this service",
    )

    hasSelfAssessedComplianceCriteriaClaim: Optional[GaiaxObjectWithDidWeb] = Field(
        default=None,
        serialization_alias=f"{ASTER_CONFORMITY_PREFIX}:hasSelfAssessedComplianceCriteriaClaim",
        description="self assessed compliance certificate claim associated with this service",
    )

    hasComplianceCertificateCredential: list[GaiaxObjectWithDidWeb] = Field(
        default=[],
        serialization_alias=f"{ASTER_CONFORMITY_PREFIX}:hasComplianceCertificateCredential",
        description="list of compliance certificate credential associated with this service",
    )

    hasSelfAssessedComplianceCriteriaCredential: Optional[GaiaxObjectWithDidWeb] = Field(
        default=None,
        serialization_alias=f"{ASTER_CONFORMITY_PREFIX}:hasSelfAssessedComplianceCriteriaCredential",
        description="self assessed compliance certificate credential associated with this service",
    )


class GxfsServiceOffering(ServiceOffering):
    """
    This class represents a ServiceOffering that extends trust framework ones with additional fields
    """

    layers: list[str] = Field(
        serialization_alias=f"{ASTER_CONFORMITY_PREFIX}:layer", description="list of layers associated to a service"
    )
