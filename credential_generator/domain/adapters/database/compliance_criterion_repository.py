# -*- coding: utf-8 -*-
"""
It defines Compliance Criterion Reposity behavior
"""
# Standard Library
from abc import ABC, abstractmethod
from dataclasses import dataclass
from typing import Optional

from credential_generator.infra.spi.database.sqlite.entities import ComplianceCriterion

# data-initialiser Library


@dataclass
class ComplianceCriterionRepository(ABC):
    """
    ComplianceCriterionRepository is the root object that defines repository behavior
    """

    @abstractmethod
    def add(self, criterion: Optional[ComplianceCriterion]) -> None:
        """
        The add function adds a new criterion to the list of criteria.

        :param self: Represent the instance of the class
        :param criterion: ComplianceCriterion: Pass in the ComplianceCriterion object
        :return: A ComplianceCriterion
        """
        raise NotImplementedError

    @abstractmethod
    def get(self, name: str, level: str) -> Optional[ComplianceCriterion]:
        """
        The retrieve function takes a name and level as parameters.
        It then filters the ComplianceCriterion references list for any criterion that matches both the name and level.
        If no such criterion exists, it returns None. Otherwise, it returns the first match.

        :param name: str: Specify the name of the ComplianceCriterion
        :param level: str: Filter the list of ComplianceCriterion objects by level
        :return: A ComplianceCriterion object
        """
        raise NotImplementedError

    @abstractmethod
    def total(self) -> int:
        """
        The total function returns the total number of items in the inventory.

        :param self: Represent the instance of the class
        :return: The total number of items in the bag
        """
        raise NotImplementedError
