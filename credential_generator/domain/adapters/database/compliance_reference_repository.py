# -*- coding: utf-8 -*-
"""
It defines Compliance Criterion Reposity behavior
"""
# Standard Library
from abc import ABC, abstractmethod
from typing import Optional

# data-initialiser Library
from credential_generator.domain._2210.gxfs.gx_compliance import ComplianceReference


class ComplianceReferenceRepository(ABC):
    """
    ComplianceReferenceRepository is the root object that defines repository behavior
    """

    @abstractmethod
    def add(self, reference: Optional[ComplianceReference]) -> None:
        """
        The add function adds a new criterion reference to the list of references.

        :param self: Represent the instance of the class
        :param reference: ComplianceReference: Pass in the ComplianceReference object
        :return: None
        """
        raise NotImplementedError

    @abstractmethod
    def get(self, compliance_reference_id: str) -> Optional[ComplianceReference]:
        """
        The get function takes a name and level as parameters.
        It then filters the ComplianceReference list for any references that matches id.
        If no such reference exists, it returns None. Otherwise, it returns the first match.

        :param compliance_reference_id: str: Specify the ID of the ComplianceReference
        :return: A ComplianceReference object
        """
        raise NotImplementedError

    @abstractmethod
    def list(self, page: Optional[int] = 1) -> list[ComplianceReference]:
        """
        The retrieve function takes a name and level as parameters.
        It then filters the ComplianceReference list for any references that matches id.
        If no such reference exists, it returns None. Otherwise, it returns the first match.

        :param page: int: Page to return
        :return: A list of ComplianceReference objects
        """
        raise NotImplementedError

    @abstractmethod
    def total(self) -> int:
        """
        The total function returns the total number of items in the inventory.

        :param self: Represent the instance of the class
        :return: The total number of items in the bag
        """
        raise NotImplementedError
