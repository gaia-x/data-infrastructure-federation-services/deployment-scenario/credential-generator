# -*- coding: utf-8 -*-
"""
It defines constants used for project
"""
from __future__ import annotations
from enum import Enum
from pathlib import Path

# Standard Library
from typing import Final

DEFAULT_CREDENTIAL_CONTEXT: Final = "https://www.w3.org/2018/credentials/v1"
DEFAULT_JWS_CONTEXT: Final = "https://w3id.org/security/suites/jws-2020/v1"

ASTER_CONFORMITY_CONTEXT: Final = "https://schema-registry.aster-x.demo23.gxfs.fr/contexts/aster-conformity"
ASTER_DATA_CONTEXT: Final = "https://schema-registry.aster-x.demo23.gxfs.fr/contexts/aster-data"
ASTER_CREDENTIAL_CONTEXT: Final = "https://schema-registry.aster-x.demo23.gxfs.fr/contexts/credentials"
ASTER_JWS_CONTEXT: Final = "https://schema-registry.aster-x.demo23.gxfs.fr/contexts/jws-2020"

DCT_CONTEXT: Final = "https://purl.org/dc/terms/"
GX_TRUST_FRAMEWORK_CONTEXT: Final = (
    "https://registry.lab.gaia-x.eu/v1/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#"
)

DEFAULT_ASTER_CONTEXT: Final = [GX_TRUST_FRAMEWORK_CONTEXT, ASTER_CONFORMITY_CONTEXT, ASTER_DATA_CONTEXT]

DEFAULT_CONTEXT_PREFIX: Final = "gx"
GX_TRUST_FRAMEWORK_PREFIX: Final = "gx"
ASTER_CONFORMITY_PREFIX: Final = "aster-conformity"
ASTER_DATA_PREFIX: Final = "aster-data"
DEFAULT_GAIAX_FEDERATION_NAME: Final = "aster-x"

ROOT_DIR: Final = Path(__file__).parent.parent.absolute()
DEFAULT_PUBLIC_DIRECTORY: Final = Path(ROOT_DIR, "public").absolute()
DEFAULT_CSV_FILE_DIRECTORY: Final = Path(ROOT_DIR, "data-compliance").absolute()
DEFAULT_DATABASE_FILENAME: Final = Path(ROOT_DIR, "compliance.sqlite3").absolute()
DEFAULT_PARENT_DOMAIN_NAME: Final = "dev.gaiax.ovh"
DEFAULT_AUDITOR_PARENT_DOMAIN_NAME: Final = "auditor.dev.gaiax.ovh"
DEFAULT_PROVIDER_PARENT_DOMAIN_NAME: Final = "provider.dev.gaiax.ovh"

GAIA_X_CLEARING_HOUSE_REGISTRY_BASE_URL: Final = "https://registry.lab.gaia-x.eu"
GAIA_X_CLEARING_HOUSE_NOTARY_BASE_URL: Final = "https://registrationnumber.notary.lab.gaia-x.eu"
GAIA_X_CLEARING_HOUSE_COMPLIANCE_BASE_URL: Final = "https://compliance.lab.gaia-x.eu/"

ASTER_X_GATEWAY_BASE_URL: Final = "api.aster-x.demo23.gxfs.fr"


class GaiaxParticipantRole(Enum):
    """
    Defines default hash algorithm types
    """

    FEDERATION = "federation"
    PROVIDER = "provider"
    AUDITOR = "auditor"

    @staticmethod
    def is_valid_gaiax_participant_role(role: str) -> bool:
        """
        Checks if the role is in the GaiaxParticipantRole enum

        :param role: a role to be checked.
        :return: True if the role is in the list of valid gaiax participant roles
        """

        if not isinstance(role, str):
            raise TypeError

        return role in [item.value for item in GaiaxParticipantRole]
