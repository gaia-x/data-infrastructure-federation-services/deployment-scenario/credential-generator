# -*- coding: utf-8 -*-
"""
credential-generator entry point script.
"""
from credential_generator import __app_name__
from credential_generator.infra.api.cli import app

app(prog_name=__app_name__)
