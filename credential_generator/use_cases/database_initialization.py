# -*- coding: utf-8 -*-
"""
It defines init use cases
"""
import logging
from pathlib import Path

from credential_generator.config import (
    DEFAULT_DATABASE_FILENAME,
    DEFAULT_GAIAX_FEDERATION_NAME,
)
from credential_generator.infra.spi.database.sqlite.repository import SQLiteRepository
from credential_generator.infra.spi.filesystem.reader.compliance_criterion_csv_reader import (
    ComplianceCriterionCSVReader,
)
from credential_generator.infra.spi.filesystem.reader.compliance_reference_csv_reader import (
    ComplianceReferenceCSVReader,
)
from credential_generator.infra.spi.filesystem.reader.legal_participant_csv_reader import read_legal_participant
from credential_generator.infra.spi.filesystem.reader.provider_csv_reader import (
    read_keywords,
    read_layers,
    read_location,
    read_provider_compliance_schemes,
    read_self_assessments,
    read_services,
)
from credential_generator.utilities import compute_sha256_document_from_url
from credential_generator.utilities.file_helpers import (
    get_all_files_from_name,
)

log = logging.getLogger(__name__)


class ComplianceDatabaseInitUseCase:
    """
    The `ComplianceDatabaseInitUseCase` class is responsible for loading CSV files for each provider and inserting
    the data into a compliance database. It uses helper classes `ComplianceReferenceCSVReader` and `ProviderCSVReader`
    to read the CSV files and retrieve the necessary data. The class also interacts with the `SQLiteRepository` class
    to create the database and add the data.
    """

    def __init__(
        self,
        input_directory: Path,
        federation_name: str = DEFAULT_GAIAX_FEDERATION_NAME,
        database_name: Path = DEFAULT_DATABASE_FILENAME,
    ) -> None:
        self.__input_directory = input_directory
        self.__compliance_repository = SQLiteRepository(database_url=f"sqlite:///{database_name}")
        self.__federation_name = federation_name

    def create_database(self) -> None:
        """
        The `create_database` method is a part of the `ComplianceDatabaseInitUseCase` class. It calls the
        `create_database` method of the `compliance_repository` object, which is responsible for creating tables
        related to entities defined in the `.entities` package.

        :return: None
        """
        log.info("creating database")
        self.__compliance_repository.create_database()

    def load_database(self) -> None:
        """
        The `load_database` method is responsible for loading data from CSV files into a database. It first cleans the
        existing data in the database, then proceeds to load the compliance references, locations, layers, keywords,
        services, and provider compliances from the CSV files.

        :return: None
        """
        log.info(f"loading data from {self.__input_directory} into database")

        self.__load_legal_participants()
        self.__load_compliance_reference()
        self.__load_compliance_criterion()
        self.__load_locations()
        layers = self.__load_layers()
        keywords = self.__load_keywords()
        self_assessments = self.__load_self_assessments()
        self.__load_services(layers=layers, keywords=keywords, self_assessments=self_assessments)
        self.__load_provider_compliance_schemes()

    def __load_compliance_reference(self):
        input_directory = Path(self.__input_directory, self.__federation_name)
        if not input_directory.exists() or not input_directory.is_dir():
            log.error(f"{input_directory} does not exists")
            raise ValueError(f"{input_directory} does not exists")

        compliance_reference_reader = ComplianceReferenceCSVReader(
            datasource_root_directory=input_directory,
            filename="ComplianceResource.csv",
        )

        compliance_reference_list = [
            item
            | {
                "compliance_reference_manager_name": self.__federation_name,
                "document_sha256": compute_sha256_document_from_url(url=item["reference"]),
            }
            for item in compliance_reference_reader.read()
        ]
        self.__compliance_repository.add_compliance_references(compliance_references=compliance_reference_list)

    def __load_compliance_criterion(self):
        input_directory = Path(self.__input_directory, self.__federation_name)
        if not input_directory.exists() or not input_directory.is_dir():
            log.error(f"{input_directory} does not exists")
            raise ValueError(f"{input_directory} does not exists")

        compliance_criterion_reader = ComplianceCriterionCSVReader(
            datasource_root_directory=input_directory,
            filename="LabelingCriteria-Matrix.csv",
        )

        compliance_criterion_list, levels = compliance_criterion_reader.read_criterions_and_labels()
        self.__compliance_repository.add_levels(levels=levels)
        self.__compliance_repository.add_compliance_criterions(compliance_criterions=compliance_criterion_list)

        (
            compliance_certification_schemes,
            third_party_compliance_certification_schemes,
        ) = compliance_criterion_reader.read_compliance_certification_schemes()

        self.__compliance_repository.add_compliance_certification_scheme(
            compliance_certification_schemes=compliance_certification_schemes
        )

        self.__compliance_repository.add_third_party_compliance_certification_scheme(
            compliance_certification_schemes=third_party_compliance_certification_schemes
        )

    def __load_keywords(self) -> list:
        files = get_all_files_from_name(source_directory=self.__input_directory, filename="6-Keywords.csv")

        keyword_services = []
        keywords = set()
        for file in files:
            parent_file = file.parent
            participant_name = parent_file.name.lower()

            current_keywords = read_keywords(input_file=file, participant_name=participant_name)
            for current_keyword in current_keywords:
                keywords.add(current_keyword["keyword"])

            keyword_services.extend(current_keywords)

        self.__compliance_repository.add_keywords(keywords=list(keywords))

        return keyword_services

    def __load_layers(self) -> list:
        files = get_all_files_from_name(source_directory=self.__input_directory, filename="5-Layers.csv")

        layer_services = []
        layers = set()
        for file in files:
            parent_file = file.parent
            participant_name = parent_file.name.lower()

            current_layers = read_layers(input_file=file, participant_name=participant_name)
            for current_layer in current_layers:
                layers.add(current_layer["layer"])

            layer_services.extend(current_layers)

        self.__compliance_repository.add_layers(layers=list(layers))

        return layer_services

    def __load_legal_participants(self):
        participant_files = get_all_files_from_name(
            source_directory=self.__input_directory, filename="1-Participant.csv"
        )

        for participant_file in participant_files:
            parent_file = participant_file.parent
            participant_name = parent_file.name.lower()

            legal_participant_dict = read_legal_participant(
                input_file=participant_file, participant_name=participant_name
            )
            if "role" in legal_participant_dict:
                match legal_participant_dict["role"]:
                    case "federation":
                        self.__compliance_repository.add_compliance_reference_manager(
                            participant=legal_participant_dict
                        )
                    case "provider":
                        self.__compliance_repository.add_provider(participant=legal_participant_dict)
                    case "cab":
                        self.__compliance_repository.add_cab(participant=legal_participant_dict)

    def __load_locations(self):
        files = get_all_files_from_name(source_directory=self.__input_directory, filename="3-Locations.csv")

        locations = []
        for location_file in files:
            parent_file = location_file.parent
            participant_name = parent_file.name.lower()

            locations.extend(read_location(input_file=location_file, participant_name=participant_name))

        self.__compliance_repository.add_locations(locations=list(locations))

    def __load_provider_compliance_schemes(self):
        files = get_all_files_from_name(source_directory=self.__input_directory, filename="10-CAB2CR.csv")

        schemes = []
        for file in files:
            parent_file = file.parent
            participant_name = parent_file.name.lower()

            schemes.extend(read_provider_compliance_schemes(input_file=file, participant_name=participant_name))

        self.__compliance_repository.add_provider_compliances_schemes(provider_compliances_schemes=list(schemes))

    def __load_self_assessments(self) -> list[dict]:
        files = get_all_files_from_name(source_directory=self.__input_directory, filename="self-assessment.csv")

        self_assessments_services = []
        for file in files:
            parent_file = file.parent
            participant_name = parent_file.name.lower()

            self_assessments = read_self_assessments(input_file=file, participant_name=participant_name)
            self_assessments_services.extend(self_assessments)

        return self_assessments_services

    def __load_services(self, layers: list[dict], keywords: list[dict], self_assessments: list[dict]):
        files = get_all_files_from_name(source_directory=self.__input_directory, filename="4-Services.csv")

        services = []
        for file in files:
            parent_file = file.parent
            participant_name = parent_file.name.lower()

            services.extend(read_services(input_file=file, participant_name=participant_name))

        for current_service in services:
            provider_name = current_service["provider_name"]
            current_service["layers"] = []
            current_service["keywords"] = []
            current_service["self_assessments"] = []

            for current_layer in layers:
                if (
                    current_layer["provider_name"] == provider_name
                    and current_layer["service_ids"] is not None
                    and current_service["service_id"] in current_layer["service_ids"]
                ):
                    current_service["layers"].append(current_layer["layer"])

            for current_keyword in keywords:
                if (
                    current_keyword["provider_name"] == provider_name
                    and current_keyword["service_ids"] is not None
                    and current_service["service_id"] in current_keyword["service_ids"]
                ):
                    current_service["keywords"].append(current_keyword["keyword"])

            for current_self_assessments in self_assessments:
                if (
                    current_self_assessments["provider_name"] == provider_name
                    and current_self_assessments["service_ids"] is not None
                    and current_service["service_id"] in current_self_assessments["service_ids"]
                ):
                    current_service["self_assessments"].append(current_self_assessments["criterion"])

        self.__compliance_repository.add_services(services=list(services))
