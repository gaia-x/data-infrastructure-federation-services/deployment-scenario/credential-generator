# -*- coding: utf-8 -*-
"""
It defines gaiax participant use cases
"""

from abc import ABCMeta, abstractmethod
import asyncio
import json
import logging
from pathlib import Path
from typing import Any, Optional

import more_itertools

from credential_generator.config import ASTER_CONFORMITY_PREFIX, DEFAULT_DATABASE_FILENAME, DEFAULT_PUBLIC_DIRECTORY
from credential_generator.domain import OntologyType
from credential_generator.domain._2210.gxfs.gx_compliance import (
    ComplianceAssessmentBody,
    ComplianceReference,
    ComplianceReferenceManager,
)
from credential_generator.domain._2210.trust_framework.participant import LegalPerson
from credential_generator.domain._2210.trust_framework.terms_and_conditions import GaiaXTermsAndConditions
from credential_generator.domain.factories._2210 import (
    get_compliance_reference_manager_legal_participant_document_id,
)
from credential_generator.domain.factories._2210.gxfs.compliance import create_compliance_reference
from credential_generator.domain.factories._2210.trust_framework import (
    create_gaiax_terms_and_conditions,
    get_gaiax_legal_registration_number_id,
    get_gaiax_legal_registration_number_vc_id,
)
from credential_generator.infra.spi.clients.asterx import ProviderCatalogueClient, UserAgentClient, AsterXGatewayClient
from credential_generator.infra.spi.clients.gaiax_clearing_house import GXCHNotaryClient, GXCHRegistryClient
from credential_generator.infra.spi.database.sqlite.repository import SQLiteRepository
from credential_generator.infra.spi.filesystem.writer.gx_trust_framework_generator import (
    dump_gaiax_terms_and_conditions_to_json_ld,
    dump_legal_registration_number_credential_subject,
    dump_legal_registration_number_vc,
)
from credential_generator.utilities.did_helpers import (
    HashType,
    compute_hash_value,
    extract_file_path_from_object_id,
    get_participant_folder,
    get_root_url_from_object_id,
)
from credential_generator.utilities.file_helpers import (
    clean_output_directory,
    get_all_files_from,
    get_all_files_of_type,
    save_json_on_disk_by_filepath,
)

log = logging.getLogger(__name__)


class LegalParticipantUseCase(metaclass=ABCMeta):
    """
    LegalParticipantUseCase is an abstract class that defines common behavior to federation,
    provider and auditor use cases.
    """

    _name: Optional[str]
    _parent_domain_name: Optional[str]
    _hash_value: Optional[str]
    _compliance_repository: Optional[SQLiteRepository]
    _base_url: Optional[str]
    _jsonld_root_directory: Optional[Path]

    def __init__(
        self,
        name: str,
        csv_root_directory: Optional[Path],
        jsonld_root_directory: Optional[Path] = DEFAULT_PUBLIC_DIRECTORY,
        database_name: Optional[Path] = DEFAULT_DATABASE_FILENAME,
    ):
        self._name = name
        self._csv_root_directory = csv_root_directory
        self._jsonld_root_directory = jsonld_root_directory
        self._compliance_repository = SQLiteRepository(database_url=f"sqlite:///{database_name}")

    def bootstrap(self, enable_compliance_call: bool = True) -> None:
        """
        The bootstrap function is used to generate json-ld.

        :param self: Refer to the object itself
        :param enable_compliance_call: True if we must generate a GaiaX Object for LegalParticipant
        :return: None
        """
        log.debug(f"Bootstraping files for service provider : {self._name}")
        log.debug(f"Participant output directory {self._jsonld_root_directory}")
        log.debug(f"Creating a LegalParticipant object for {self._name}{self._parent_domain_name}.")

        gaiax_legal_person = self._create_gaiax_legal_participant()
        if gaiax_legal_person is None:
            return

        output_file = Path(
            self._jsonld_root_directory,
            extract_file_path_from_object_id(gaiax_legal_person.did_web),
        )
        self._dump_participant_to_json_ld(legal_participant=gaiax_legal_person, output_file=output_file)

        if enable_compliance_call:
            self.__generate_gaiax_participant_person(gaiax_legal_person=gaiax_legal_person)

    async def call_compliance(
        self,
        api_key: Optional[str] = None,
        port_number: int = 443,
    ) -> None:
        """
        The call_compliance_with_self_description_objects method is used to call the compliance process for a legal
        participant and its service offerings. It takes an optional API key and port number as inputs.

        :param api_key: The API key used for authentication with the participant agent.
        :param port_number: The port number used to connect to the participant agent.
        """
        user_agent_client = self._get_user_agent_client(api_key, port_number)

        log.info("Call compliance for legal-participant")
        await user_agent_client.call_compliance(object_type=OntologyType.LEGAL_PARTICIPANT.value)

    async def clear_cache(
        self,
        api_key: Optional[str] = None,
        port_number: int = 443,
    ) -> None:
        """
        The clear_cache method is used to clear cache on participant agent

        :param api_key: The API key used for authentication with the participant agent.
        :param port_number: The port number used to connect to the participant agent.
        """
        user_agent_client = self._get_user_agent_client(api_key, port_number)

        log.info("Clearing cache on participant agent")
        await user_agent_client.clear_cache()

    def cleanup(self) -> None:
        """
        The cleanup function is used to clean up legal participant's root directory

        :param self: Refer to the object itself
        :return: None. The method does not return any value.
        """
        log.debug(f"Cleaning files : {self._name}.{self._parent_domain_name}")
        output_file = Path(self._jsonld_root_directory, f"{self._name}.{self._parent_domain_name}")
        clean_output_directory(directory=str(output_file))

        log.debug("Cleaning up done.")

    async def index_vcs(
        self,
        api_key: Optional[str] = None,
        port_number: int = 443,
        enable_ces_sync: bool = False,
        enable_catalogue_sync: bool = True,
        number_of_workers: int = 5,
    ):
        """
        The index_vcs method is used to call the provider catalog to index the Verifiable Credentials (VCs) generated by
        the legal participant.

        ## Example Usage
        ```python
        participant = LegalParticipant(database_name="my_database", output_directory="/path/to/output")
        await participant.index_vcs(api_key="my_api_key", port_number=443, enable_ces_sync=True)
        ```

        :param api_key (optional): The API key used for authentication with the provider catalog.
        :param port_number (optional): The port number used to connect to the provider catalog.
        :param enable_ces_sync (optional): A boolean flag indicating whether to enable CES synchronization for service
        offering VCs.
        """
        provider_catalogue_client = ProviderCatalogueClient(
            # BTA --> to skip according to the place you deployed your catalogue
            # base_url=self._base_url.replace("provider.", ""),
            base_url=self._base_url,
            api_key=api_key,
            port_number=port_number,
        )

        log.info("Call provider catalog to index vcs")
        json_index = self._get_json_index()
        for object_type in json_index.keys():
            log.info(f"Call provider catalog to index vcs - {object_type}")
            vcs = json_index[object_type]
            vc_ids = vcs if isinstance(vcs, list) else list(vcs.values())

            for current_list in more_itertools.batched(vc_ids, number_of_workers):
                await self.__index_vc(
                    client=provider_catalogue_client,
                    vc_ids=list(current_list),
                    enable_ces_sync=enable_ces_sync and object_type == "gx:ServiceOffering",
                    enable_catalogue_sync=enable_catalogue_sync,
                )
                await asyncio.sleep(5)

    async def index_aster_x_compliance_vcs(
        self,
        federation_name: str,
        fed_parent_domain: Optional[str] = None,
        api_key: Optional[str] = None,
        port_number: int = 443,
        number_of_workers: int = 100,
    ):
        """
        The index_vcs method is used to call the provider catalog to index the Aster-X compliance Verifiable Credentials
        (VCs) generated by the legal participant.

        :param fed_parent_domain:
        :param federation_name: name of the Federation to know which federation catalogue aliment
        :param api_key: (optional) The API key used for authentication with the provider catalog.
        :param port_number: (optional) The port number used to connect to the provider catalog.
        :param number_of_workers: (optional) Configure number of worker to batch call to provider catalogue
        offering VCs.
        """
        if fed_parent_domain:
            base_url = f"{federation_name}.{fed_parent_domain}"
        else:
            base_url = self._base_url.replace("provider.", "").replace(self._name, federation_name)

        log.info(f"Base_url: {base_url}")

        provider_catalogue_client = ProviderCatalogueClient(
            base_url=base_url,
            api_key=api_key,
            port_number=port_number,
        )

        log.info(f"Call Federated catalog {base_url} to index Aster-X Compliance vcs")

        json_index = self._get_json_index()
        if "gx:aster-compliance" in json_index:
            aster_compliance_vcs = iter(json_index["gx:aster-compliance"])

            for current_list in more_itertools.batched(aster_compliance_vcs, number_of_workers):
                await self.__index_vc(
                    client=provider_catalogue_client,
                    vc_ids=list(current_list),
                    enable_ces_sync=False,
                    enable_catalogue_sync=False
                )
                await asyncio.sleep(5)

    async def upload_self_description_objects(
        self,
        api_key: Optional[str] = None,
        auditor_api_key: Optional[str] = None,
        port_number: int = 443,
        number_of_workers: int = 5,
    ) -> None:
        """
        The upload_self_description_objects method is used to upload self-description objects to a participant agent.
        It takes an optional API key as input and retrieves the files from the output directory. It then checks if the
        participant agent is reachable and stores each file using the store_object method of the UserAgentClient class.

        :param api_key: The API key used for authentication with the participant agent.
        :param auditor_api_key: The API key used for authentication with the auditor participant agent.
        :param number_of_workers: Number of workers to simultaneously upload files. Default is 5
        :param port_number: port to access to user agent url. Default is 443
        :return: None. The method does not return any value.
        """
        if api_key is None or api_key == "":
            raise ValueError("API Key is missing")

        if auditor_api_key is None or auditor_api_key == "":
            auditor_api_key = api_key

        source_directory = Path(self._jsonld_root_directory, f"{self._name}.{self._parent_domain_name}")
        user_agent_client = self._get_user_agent_client(api_key, port_number)
        if await user_agent_client.ping():
            log.debug(f"Bootstrapping provider {self._name}.{self._parent_domain_name}")
            await user_agent_client.bootstrap()

            self_signed_files = get_all_files_from(
                source_directory=source_directory,
                exclude_patterns=[
                    OntologyType.get_directory_name(OntologyType.THIRD_PARTY_COMPLIANCE_CERT_CLAIM),
                    OntologyType.get_directory_name(OntologyType.LEGAL_PARTICIPANT, is_vc=True),
                    OntologyType.get_directory_name(OntologyType.GAIAX_TERMS_AND_CONDITIONS, is_vc=True),
                    OntologyType.get_directory_name(OntologyType.GAIAX_LEGAL_REGISTRATION_NUMBER, is_vc=False),
                    OntologyType.get_directory_name(OntologyType.GAIAX_LEGAL_REGISTRATION_NUMBER, is_vc=True),
                    OntologyType.get_directory_name(OntologyType.SERVICE_OFFERING, is_vc=True),
                    OntologyType.get_directory_name(OntologyType.LOCATED_SERVICE, is_vc=True),
                    OntologyType.get_directory_name(OntologyType.LOCATION, is_vc=True),
                    OntologyType.get_directory_name(OntologyType.COMPLIANCE_ASSESSMENT_BODY, is_vc=True),
                    OntologyType.get_directory_name(OntologyType.COMPLIANCE_REF, is_vc=True),
                    OntologyType.get_directory_name(OntologyType.COMPLIANCE_CRITERION, is_vc=True),
                    OntologyType.get_directory_name(OntologyType.COMPLIANCE_LABEL, is_vc=True),
                    OntologyType.get_directory_name(OntologyType.DATA_PRODUCT, is_vc=True),
                    OntologyType.get_directory_name(OntologyType.COMPLIANCE_CERT_SCHEME, is_vc=True),
                    OntologyType.get_directory_name(OntologyType.COMPLIANCE_CERT_CLAIM, is_vc=True),
                    OntologyType.get_directory_name(OntologyType.COMPLIANCE_CERT_CRED, is_vc=True),
                    OntologyType.get_directory_name(OntologyType.THIRD_PARTY_COMPLIANCE_CERT_SCHEME, is_vc=True),
                    OntologyType.get_directory_name(OntologyType.THIRD_PARTY_COMPLIANCE_CERT_CLAIM, is_vc=True),
                    OntologyType.get_directory_name(OntologyType.THIRD_PARTY_COMPLIANCE_CERT_CRED, is_vc=True),
                    OntologyType.get_directory_name(OntologyType.SELF_ASSESSED_COMPLIANCE_CRITERIA_CLAIM, is_vc=True),
                    OntologyType.get_directory_name(OntologyType.SELF_ASSESSED_COMPLIANCE_CRITERIA_CRED, is_vc=True),
                ],
            )
            cab_files = get_all_files_of_type(
                source_directory=source_directory,
                types=[OntologyType.get_directory_name(OntologyType.THIRD_PARTY_COMPLIANCE_CERT_CLAIM)],
            )

            vc_files = get_all_files_of_type(
                source_directory=source_directory,
                types=[
                    OntologyType.get_directory_name(OntologyType.LEGAL_PARTICIPANT, is_vc=True),
                    OntologyType.get_directory_name(OntologyType.GAIAX_TERMS_AND_CONDITIONS, is_vc=True),
                    OntologyType.get_directory_name(OntologyType.GAIAX_LEGAL_REGISTRATION_NUMBER, is_vc=False),
                    OntologyType.get_directory_name(OntologyType.GAIAX_LEGAL_REGISTRATION_NUMBER, is_vc=True),
                    OntologyType.get_directory_name(OntologyType.SERVICE_OFFERING, is_vc=True),
                    OntologyType.get_directory_name(OntologyType.LOCATED_SERVICE, is_vc=True),
                    OntologyType.get_directory_name(OntologyType.LOCATION, is_vc=True),
                    OntologyType.get_directory_name(OntologyType.COMPLIANCE_ASSESSMENT_BODY, is_vc=True),
                    OntologyType.get_directory_name(OntologyType.COMPLIANCE_REF, is_vc=True),
                    OntologyType.get_directory_name(OntologyType.COMPLIANCE_CRITERION, is_vc=True),
                    OntologyType.get_directory_name(OntologyType.COMPLIANCE_LABEL, is_vc=True),
                    OntologyType.get_directory_name(OntologyType.DATA_PRODUCT, is_vc=True),
                    OntologyType.get_directory_name(OntologyType.COMPLIANCE_CERT_SCHEME, is_vc=True),
                    OntologyType.get_directory_name(OntologyType.COMPLIANCE_CERT_CLAIM, is_vc=True),
                    OntologyType.get_directory_name(OntologyType.COMPLIANCE_CERT_CRED, is_vc=True),
                    OntologyType.get_directory_name(OntologyType.THIRD_PARTY_COMPLIANCE_CERT_SCHEME, is_vc=True),
                    OntologyType.get_directory_name(OntologyType.THIRD_PARTY_COMPLIANCE_CERT_CLAIM, is_vc=True),
                    OntologyType.get_directory_name(OntologyType.THIRD_PARTY_COMPLIANCE_CERT_CRED, is_vc=True),
                    OntologyType.get_directory_name(OntologyType.SELF_ASSESSED_COMPLIANCE_CRITERIA_CLAIM, is_vc=True),
                    OntologyType.get_directory_name(OntologyType.SELF_ASSESSED_COMPLIANCE_CRITERIA_CRED, is_vc=True),
                ],
            )
            files = self_signed_files + cab_files

            log.debug(f"Uploading {len(files)}files to {self._name}.{self._parent_domain_name}")
            await self.__upload_objects_with_limit(
                client=user_agent_client, files=files + vc_files, limit=number_of_workers
            )

            log.debug("Generate VerifiableCredentials")
            results = await self.__sign_objects_with_limit(client=user_agent_client, files=self_signed_files, limit=1)

            log.debug("Send json files to CAB")
            results.extend(
                await self.__sign_third_party_vc(
                    api_key=auditor_api_key, cab_files=cab_files, port_number=port_number, number_of_workers=1
                )
            )

            # log.debug("5- Generate index of VCs")
            json_index = self.__generate_vc_index(results)

            index_file = Path(
                source_directory, OntologyType.get_directory_name(OntologyType.VC, is_vc=True), "index.json"
            )
            self._store_json_index(index_file=index_file, json_index=json_index)

    def _calculate_provider_hash_value(self):
        """
        Compute hash value of the Gaia-X Provider Identifier.

        :param self: Refer to the current object
        :return: A string with computed hash value.
        """
        self._hash_value = compute_hash_value(bytes(self._name, "utf-8"), HashType.SHA256)

    @abstractmethod
    def _create_gaiax_legal_participant(self) -> Optional[LegalPerson]:
        raise NotImplementedError

    @abstractmethod
    def _dump_participant_to_json_ld(
        self,
        legal_participant: LegalPerson | ComplianceAssessmentBody | ComplianceReferenceManager,
        output_file: Path,
    ):
        raise NotImplementedError

    def __generate_gaiax_participant_person(self, gaiax_legal_person: Optional[LegalPerson]) -> None:
        log.debug("Creating a GaiaXTermsAndConditions object for {self._name}{self._parent_domain_name}.")
        gaiax_terms_and_conditions = self.__create_gaiax_terms_and_condition_object()

        log.debug("Request a LegalRegistrationNumber vc from GXCH notary for {self._name}{self._parent_domain_name}.")

        legal_registration_number = gaiax_legal_person.legalRegistrationNumber
        legal_registration_number_vc = self.__request_registration_number_vc(legal_person=gaiax_legal_person)

        log.debug("Generating Participant JSON-LD files.")
        output_file = Path(
            self._jsonld_root_directory,
            extract_file_path_from_object_id(gaiax_terms_and_conditions.did_web),
        )
        dump_gaiax_terms_and_conditions_to_json_ld(
            terms_and_conditions=gaiax_terms_and_conditions, output_file=output_file
        )

        output_file = Path(
            self._jsonld_root_directory,
            extract_file_path_from_object_id(legal_registration_number.did_web),
        )
        dump_legal_registration_number_credential_subject(legal_registration_number, output_file)

        output_file = Path(
            self._jsonld_root_directory,
            extract_file_path_from_object_id(legal_registration_number_vc["id"]),
        )
        dump_legal_registration_number_vc(legal_registration_number_vc, output_file)

    @staticmethod
    def __generate_vc_index(results: list[dict[str, Any]]) -> dict[str, Any]:
        json_index = {}
        for result in [result for result in results if result is not None]:
            subject = result["credentialSubject"]
            object_types = subject["type"]
            object_type = object_types[1] if isinstance(object_types, list) and len(object_types) > 1 else object_types
            object_id = result["@id"] if "@id" in result else result["id"]
            credential_subject = result["credentialSubject"]
            credential_subject_id = ""
            if credential_subject is not None:
                credential_subject_id = (
                    credential_subject["@id"] if "@id" in credential_subject else credential_subject["id"]
                )

            if not isinstance(object_type, str):
                raise TypeError("Invalid object type")
            if object_type not in json_index:
                json_index[object_type] = {}

            json_index[object_type][credential_subject_id] = object_id

        return json_index

    def _get_json_index(self) -> dict[str, Any]:
        index_filepath = Path(
            self._jsonld_root_directory,
            get_participant_folder(self._name, self._parent_domain_name),
            "vc",
            "index.json",
        )

        try:
            with open(index_filepath, "r", encoding="utf-8") as index_file:
                log.debug(f"Loading index json file: {index_file}")
                json_index = json.load(index_file)
        except FileNotFoundError:
            log.warning(f"Index json file not found: {index_filepath}")
            json_index = {}
        except (json.JSONDecodeError, UnicodeDecodeError) as e:
            log.error(f"Error loading index json file: {index_filepath}")
            raise e

        return json_index

    async def __index_vc(
        self,
        client: ProviderCatalogueClient,
        vc_ids: list[str],
        enable_ces_sync: bool = False,
        enable_catalogue_sync: bool = True,
    ) -> str:
        return await client.index_vcs(
            did_webs=vc_ids, enable_catalogue_sync=enable_catalogue_sync, enable_ces_sync=enable_ces_sync
        )

    def _store_json_index(self, index_file: Path, json_index: dict[str, Any]) -> None:
        save_json_on_disk_by_filepath(filepath=index_file, data=json_index)

    def _get_user_agent_client(
        self, api_key: Optional[str], port_number: int, did_web: Optional[str] = None
    ) -> UserAgentClient:
        return (
            UserAgentClient(
                url=f"{self._name}.{self._parent_domain_name}",
                api_key=api_key,
                port_number=port_number,
            )
            if did_web is None
            else UserAgentClient(
                url=get_root_url_from_object_id(object_id=did_web), api_key=api_key, port_number=port_number
            )
        )

    @staticmethod
    def _get_aster_x_gateway_client(
        port_number: int
    ) -> AsterXGatewayClient:
        return (
            AsterXGatewayClient(
                port_number=port_number
            )
        )

    def _create_compliance_reference_dict(self, compliance_reference_entities: list[ComplianceReference]):
        compliance_reference_manager_object_id = get_compliance_reference_manager_legal_participant_document_id(
            parent_domain_name=self._parent_domain_name, manager_name=self._name
        )

        return {
            f"{entity.compliance_reference_id}": create_compliance_reference(
                compliance_reference_id=entity.compliance_reference_id,
                parent_domain_name=self._parent_domain_name,
                federator_name=self._name,
                compliance_reference_schemes=[],
                title=entity.designation,
                version=entity.version,
                category=entity.category,
                document_reference_url=entity.reference,
                document_hash256=entity.reference_hash,
                compliance_reference_manager_did_web=compliance_reference_manager_object_id,
                valid_from=entity.valid_from,
                valid_until=entity.valid_until,
            )
            for entity in compliance_reference_entities
        }

    def __create_gaiax_terms_and_condition_object(self) -> GaiaXTermsAndConditions:
        terms_and_conditions = GXCHRegistryClient().get_gaix_terms_and_conditions()
        return create_gaiax_terms_and_conditions(
            parent_domain_name=self._parent_domain_name,
            provider_name=self._name,
            text=terms_and_conditions.text,
        )

    def __request_registration_number_vc(self, legal_person: LegalPerson) -> dict:
        legal_registration_number = legal_person.legalRegistrationNumber
        legal_registration_number.did_web = get_gaiax_legal_registration_number_id(
            parent_domain_name=self._parent_domain_name, name=self._name
        )

        vc_id = get_gaiax_legal_registration_number_vc_id(parent_domain_name=self._parent_domain_name, name=self._name)

        return GXCHNotaryClient().get_registration_number_vc(
            did_web=vc_id,
            legal_registration_number=legal_registration_number,
        )

    @staticmethod
    async def __sign_objects_with_limit(client: UserAgentClient, files: list[Path], limit: int = 10) -> list[str]:
        results = []
        for current_list in more_itertools.batched(files, limit):
            batch = [asyncio.create_task(client.sign_vc(filename=filename)) for filename in current_list]
            results.extend(await asyncio.gather(*batch))

        return results

    async def __sign_third_party_vc(self, api_key, cab_files, port_number, number_of_workers: int = 5) -> list[dict]:
        results = []
        for current_list in more_itertools.batched(cab_files, number_of_workers):
            batch = []
            for filename in current_list:
                with open(file=filename, mode="r", encoding="utf-8") as file:
                    contents = file.read()

                jsonld = json.loads(contents)
                cab = jsonld[f"{ASTER_CONFORMITY_PREFIX}:hasComplianceAssessmentBody"]
                did_web = cab["id"]

                log.debug(f"Current file: {jsonld['id']}")
                log.debug(f"Sign VC with this did_web vc issuer: {did_web}")
                client = self._get_user_agent_client(api_key=api_key, port_number=port_number, did_web=did_web)
                batch.append(asyncio.create_task(client.sign_vc_with_content(jsonld=jsonld)))
            results.extend(await asyncio.gather(*batch))

        return results

    @staticmethod
    async def __upload_objects_with_limit(client: UserAgentClient, files: list[Path], limit: int = 10) -> None:
        for current_list in more_itertools.batched(files, limit):
            batch = [asyncio.create_task(client.store_object(filename=filename)) for filename in current_list]
            await asyncio.gather(*batch)
