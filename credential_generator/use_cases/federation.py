# -*- coding: utf-8 -*-
"""
It defines gaiax federation use cases
"""
import itertools
import logging
from pathlib import Path
from typing import Optional

from credential_generator.domain import ComplianceLevel, CriterionCategory
from credential_generator.domain._2210 import GaiaxObjectWithDidWeb
from credential_generator.domain._2210.gxfs.gx_compliance import (
    ComplianceAssessmentBody,
    ComplianceCertificationScheme,
    ComplianceReference,
    ComplianceReferenceManager,
    ThirdPartyComplianceCertificationScheme,
)
from credential_generator.domain._2210.trust_framework.participant import LegalPerson
from credential_generator.domain.factories._2210 import (
    get_compliance_reference_manager_legal_participant_document_id,
)
from credential_generator.domain.factories._2210.gxfs import (
    get_compliance_certification_scheme_id,
    get_third_party_compliance_certification_scheme_id,
)
from credential_generator.domain.factories._2210.gxfs.compliance import (
    create_compliance_assessment_body,
    create_compliance_certification_scheme,
    create_compliance_criterion,
    create_compliance_label,
    create_compliance_reference,
    create_compliance_reference_manager,
    create_third_party_compliance_certification_scheme,
)
from credential_generator.infra.spi.filesystem.writer.gxfs_fr_generator import (
    dump_gxfs_compliance_certification_scheme_to_json_ld,
    dump_gxfs_compliance_criterion_to_json_ld,
    dump_gxfs_compliance_label_to_json_ld,
    dump_gxfs_compliance_reference_manager_to_json_ld,
    dump_gxfs_compliance_reference_to_json_ld,
    dump_gxfs_third_party_compliance_certification_scheme_to_json_ld,
)
from credential_generator.use_cases.gaiax_participant import LegalParticipantUseCase
from credential_generator.utilities.did_helpers import extract_file_path_from_object_id

log = logging.getLogger(__name__)


class FederationUseCase(LegalParticipantUseCase):
    """
    GaiaxProvider is responsible to generate GaiaX criterion and label json-ld files
    """

    def __init__(
        self,
        csv_root_directory: Optional[Path],
        jsonld_root_directory: Optional[Path],
        name: str,
        parent_domain_name: str,
        database_name: Optional[str] = None,
    ):
        super().__init__(
            name=name,
            csv_root_directory=csv_root_directory,
            jsonld_root_directory=jsonld_root_directory,
            database_name=database_name,
        )
        self._parent_domain_name = parent_domain_name
        self._base_url = f"{name}.{parent_domain_name}"
        self._calculate_provider_hash_value()

    def bootstrap(self, enable_compliance_call: bool = True) -> None:
        """
        The bootstrap function is used to generate json-ld.

        :param self: Refer to the object itself
        :param enable_compliance_call: True if we must generate a GaiaX Object for LegalParticipant
        :return: None
        """
        super().bootstrap(enable_compliance_call=enable_compliance_call)

        log.debug("Generating ComplianceCriterion and ComplianceLabels JSON-LD files.")
        self._dump_criterion_and_labels()

        log.debug("Generating ComplianceReference JSON-LD files.")
        compliance_reference_list = self.__get_compliance_reference_list()
        for compliance_reference in compliance_reference_list:
            output_file = Path(
                self._jsonld_root_directory,
                extract_file_path_from_object_id(object_id=compliance_reference.did_web),
            )
            dump_gxfs_compliance_reference_to_json_ld(
                compliance_reference=compliance_reference, output_file=output_file
            )

        compliance_certification_schemes = self.__get_compliance_certification_scheme()
        for scheme in compliance_certification_schemes:
            output_file = Path(
                self._jsonld_root_directory,
                extract_file_path_from_object_id(object_id=scheme.did_web),
            )
            dump_gxfs_compliance_certification_scheme_to_json_ld(
                compliance_certification_scheme=scheme, output_file=output_file
            )

        compliance_certification_schemes = self.__get_third_party_compliance_certification_scheme()
        for scheme in compliance_certification_schemes:
            output_file = Path(
                self._jsonld_root_directory,
                extract_file_path_from_object_id(object_id=scheme.did_web),
            )
            dump_gxfs_third_party_compliance_certification_scheme_to_json_ld(
                compliance_certification_scheme=scheme, output_file=output_file
            )

    def _create_gaiax_legal_participant(self) -> Optional[LegalPerson]:
        log.debug("Creating Federation LegalParticipant object.")
        cab_entity = self._compliance_repository.get_compliance_reference_manager_from_name(name=self._name)

        compliance_reference_list = self.__get_compliance_reference_list()

        return create_compliance_reference_manager(
            parent_domain_name=self._parent_domain_name,
            federation_name=self._name,
            legal_name=cab_entity.designation,
            country_name=cab_entity.country,
            lei_code=cab_entity.lei_code,
            registration_number=cab_entity.registration_number,
            has_compliance_references=[
                compliance_reference.did_web for compliance_reference in compliance_reference_list
            ],
        )

    def _dump_participant_to_json_ld(
        self, legal_participant: LegalPerson | ComplianceAssessmentBody | ComplianceReferenceManager, output_file: Path
    ):
        log.debug("Generating Federation LegalParticipant json-ld.")
        dump_gxfs_compliance_reference_manager_to_json_ld(manager=legal_participant, output_file=output_file)

    def _dump_criterion_and_labels(self):
        # pylint: disable=no-member
        entities = self._compliance_repository.get_compliance_criterions()
        criterions = [
            [
                create_compliance_criterion(
                    parent_domain_name=self._parent_domain_name,
                    provider_name=self._name,
                    name=entity.name,
                    description=entity.description,
                    level=ComplianceLevel.from_str(level=f"Level {level.level_id}"),
                    self_assessed=entity.self_assessed,
                    category=CriterionCategory.from_str(entity.category),
                )
                for level in entity.levels
            ]
            for entity in entities
        ]
        criterion_list = list(itertools.chain.from_iterable(criterions))
        label1 = create_compliance_label(
            parent_domain_name=self._parent_domain_name, federator_name=self._name, level=ComplianceLevel.LEVEL_1
        )
        label1.criteria_list.extend(
            [
                GaiaxObjectWithDidWeb(did_web=criterion.did_web)
                for criterion in criterion_list
                if criterion.level == label1.level
            ]
        )
        log.debug(f"Label 1 : {len(label1.criteria_list)}")
        label2 = create_compliance_label(
            parent_domain_name=self._parent_domain_name, federator_name=self._name, level=ComplianceLevel.LEVEL_2
        )
        label2.criteria_list.extend(
            [
                GaiaxObjectWithDidWeb(did_web=criterion.did_web)
                for criterion in criterion_list
                if criterion.level == label2.level
            ]
        )
        log.debug(f"Label 2 : {len(label2.criteria_list)}")
        label3 = create_compliance_label(
            parent_domain_name=self._parent_domain_name, federator_name=self._name, level=ComplianceLevel.LEVEL_3
        )
        label3.criteria_list.extend(
            [
                GaiaxObjectWithDidWeb(did_web=criterion.did_web)
                for criterion in criterion_list
                if criterion.level == label3.level
            ]
        )
        log.debug(f"Label 3 : {len(label3.criteria_list)}")
        for criterion in criterion_list:
            output_file = Path(
                self._jsonld_root_directory,
                extract_file_path_from_object_id(object_id=criterion.did_web),
            )
            dump_gxfs_compliance_criterion_to_json_ld(compliance_criterion=criterion, output_file=output_file)
        for label in [label1, label2, label3]:
            output_file = Path(
                self._jsonld_root_directory,
                extract_file_path_from_object_id(object_id=label.did_web),
            )
            dump_gxfs_compliance_label_to_json_ld(compliance_label=label, output_file=output_file)

    def __get_compliance_reference_list(self) -> list[ComplianceReference]:
        compliance_reference_entities = self._compliance_repository.get_compliance_references_from_manager_name(
            manager_name=self._name
        )

        compliance_reference_manager_did_web = get_compliance_reference_manager_legal_participant_document_id(
            parent_domain_name=self._parent_domain_name, manager_name=self._name
        )

        compliance_references = []
        for entity in compliance_reference_entities:
            compliance_certification_scheme_entity = (
                self._compliance_repository.get_compliance_certification_scheme_from_id(
                    compliance_reference_id=entity.compliance_reference_id
                )
            )
            third_party_compliance_certification_scheme_entity = (
                self._compliance_repository.get_third_party_compliance_certification_scheme_from_id(
                    compliance_reference_id=entity.compliance_reference_id
                )
            )

            compliance_reference_schemes = []
            if compliance_certification_scheme_entity is not None:
                compliance_reference_schemes.append(
                    get_compliance_certification_scheme_id(
                        parent_domain_name=self._parent_domain_name,
                        federator_name=self._name,
                        compliance_reference_id=entity.compliance_reference_id,
                    )
                )

            if third_party_compliance_certification_scheme_entity is not None:
                compliance_reference_schemes.append(
                    get_third_party_compliance_certification_scheme_id(
                        parent_domain_name=self._parent_domain_name,
                        federator_name=self._name,
                        compliance_reference_id=entity.compliance_reference_id,
                    )
                )

            compliance_references.append(
                create_compliance_reference(
                    parent_domain_name=self._parent_domain_name,
                    federator_name=self._name,
                    compliance_reference_schemes=compliance_reference_schemes,
                    title=entity.designation,
                    version=entity.version,
                    category=entity.category,
                    document_reference_url=entity.reference,
                    document_hash256=entity.reference_hash if entity.reference_hash is not None else "",
                    compliance_reference_manager_did_web=compliance_reference_manager_did_web,
                    valid_from=entity.valid_from,
                    valid_until=entity.valid_until,
                    compliance_reference_id=entity.compliance_reference_id,
                )
            )

        return compliance_references

    def __get_compliance_certification_scheme(self) -> list[ComplianceCertificationScheme]:
        (
            scheme_entities,
            reference_entities,
        ) = self._compliance_repository.get_compliance_certification_scheme_with_compliance_reference()

        compliance_references_dict = self._create_compliance_reference_dict(reference_entities)

        compliance_certification_schemes = []
        for scheme_entity in scheme_entities:
            criterion_list = []
            criterions_entities = self._compliance_repository.get_compliance_criterion_from_compliance_scheme_id(
                scheme_entity
            )
            for criterion_entity in criterions_entities:
                criterion = self._compliance_repository.get_compliance_criterion(
                    criterion_id=criterion_entity[0], fetch_levels=False
                )
                criterion_list.append(
                    create_compliance_criterion(
                        parent_domain_name=self._parent_domain_name,
                        provider_name=self._name,
                        name=criterion.name,
                        description=criterion.description,
                        level=ComplianceLevel.from_str(level=f"Level {criterion_entity[1]}"),
                        self_assessed=criterion.self_assessed,
                        category=CriterionCategory.from_str(criterion.category),
                    )
                )

            compliance_certification_schemes.append(
                create_compliance_certification_scheme(
                    parent_domain_name=self._parent_domain_name,
                    federator_name=self._name,
                    compliance_criterions=criterion_list,
                    compliance_reference=compliance_references_dict[scheme_entity],
                )
            )

        return compliance_certification_schemes

    def __get_third_party_compliance_certification_scheme(self) -> list[ThirdPartyComplianceCertificationScheme]:
        (
            scheme_entities,
            reference_entities,
        ) = self._compliance_repository.get_third_party_compliance_certification_scheme_with_compliance_reference()

        compliance_references_dict = self._create_compliance_reference_dict(reference_entities)
        cabs = {}
        for compliance_reference_id in scheme_entities:
            cab_entities = self._compliance_repository.get_cabs_from_third_party_compliance_certification_scheme_id(
                compliance_reference_id=compliance_reference_id
            )
            for entity in cab_entities:
                cab = create_compliance_assessment_body(
                    parent_domain_name=self._parent_domain_name,
                    auditor_name=entity.name,
                    legal_name=entity.designation,
                    country_name=entity.country,
                    lei_code=entity.lei_code,
                    registration_number=entity.registration_number,
                )
                if compliance_reference_id not in cabs:
                    cabs[compliance_reference_id] = []

                cabs[compliance_reference_id].append(cab)

        compliance_certification_schemes = []
        for compliance_reference_id in scheme_entities:
            criterion_list = []
            criterions_entities = (
                self._compliance_repository.get_third_party_compliance_criterion_from_compliance_scheme_id(
                    compliance_reference_id
                )
            )

            for criterion_entity in criterions_entities:
                criterion = self._compliance_repository.get_compliance_criterion(
                    criterion_id=criterion_entity[0], fetch_levels=False
                )
                criterion_list.append(
                    create_compliance_criterion(
                        parent_domain_name=self._parent_domain_name,
                        provider_name=self._name,
                        name=criterion.name,
                        description=criterion.description,
                        level=ComplianceLevel.from_str(level=f"Level {criterion_entity[1]}"),
                        self_assessed=criterion.self_assessed,
                        category=CriterionCategory.from_str(criterion.category),
                    )
                )

            compliance_certification_schemes.append(
                create_third_party_compliance_certification_scheme(
                    parent_domain_name=self._parent_domain_name,
                    federator_name=self._name,
                    compliance_criterions=criterion_list,
                    compliance_reference=compliance_references_dict[compliance_reference_id],
                    compliance_assessment_bodies=cabs.get(compliance_reference_id, []),
                )
            )
        return compliance_certification_schemes
