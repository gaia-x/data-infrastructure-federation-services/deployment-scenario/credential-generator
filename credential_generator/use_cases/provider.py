# -*- coding: utf-8 -*-
"""
It defines gaiax provider use cases
"""
import asyncio
import logging
from pathlib import Path
from typing import Any, Optional

import more_itertools

from credential_generator.config import (
    ASTER_CONFORMITY_PREFIX,
    DEFAULT_GAIAX_FEDERATION_NAME,
    GX_TRUST_FRAMEWORK_PREFIX,
)
from credential_generator.domain import OntologyType
from credential_generator.domain._2210 import GaiaxObjectWithDidWeb
from credential_generator.domain._2210.gxfs.gx_compliance import (
    ComplianceAssessmentBody,
    ComplianceCertificateClaim,
    ComplianceReferenceManager,
    SelfAssessedComplianceCriteriaClaim,
    ThirdPartyComplianceCertificateClaim,
)
from credential_generator.domain._2210.gxfs.gx_participant import Location
from credential_generator.domain._2210.gxfs.gx_service import LocatedServiceOffering
from credential_generator.domain._2210.trust_framework.participant import LegalPerson
from credential_generator.domain._2210.trust_framework.service_offering import ServiceOffering
from credential_generator.domain.factories._2210 import get_auditor_legal_participant_document_id
from credential_generator.domain.factories._2210.gxfs import (
    get_compliance_certification_scheme_id,
    get_located_service_did,
    get_third_party_compliance_certification_scheme_id,
)
from credential_generator.domain.factories._2210.gxfs.compliance import (
    create_compliance_certification_claim,
    create_self_assessed_compliance_criteria_claim,
    create_third_party_compliance_certification_claim,
)
from credential_generator.domain.factories._2210.gxfs.participant import create_location
from credential_generator.domain.factories._2210.gxfs.service import (
    create_gxfs_service_offering,
    create_located_service_offering,
)
from credential_generator.domain.factories._2210.trust_framework import create_provider
from credential_generator.infra.spi.clients.asterx import LabellingAgentClient, UserAgentClient
from credential_generator.infra.spi.database.sqlite.entities.factory import get_entity_external_id, get_entity_id
from credential_generator.infra.spi.filesystem.writer.gx_trust_framework_generator import (
    dump_participant_to_json_ld,
    dump_service_offering_to_json_ld,
)
from credential_generator.infra.spi.filesystem.writer.gxfs_fr_generator import (
    dump_gxfs_compliance_certificate_claim_to_json_ld,
    dump_gxfs_located_service_offering_to_json_ld,
    dump_gxfs_location_to_json_ld,
    dump_gxfs_self_assessed_compliance_certificate_claim_to_json_ld,
    dump_gxfs_third_party_compliance_certificate_claim_to_json_ld,
)
from credential_generator.use_cases.gaiax_participant import LegalParticipantUseCase
from credential_generator.utilities import Map2
from credential_generator.utilities.did_helpers import calculate_auditor_hash_value, extract_file_path_from_object_id
from credential_generator.utilities.file_helpers import save_json_on_disk_by_filepath

log = logging.getLogger(__name__)


class ProviderUseCase(LegalParticipantUseCase):
    """
    ProviderUseCase is responsible to generate GaiaX json-ld files, upload, and index them to provider's user agent
    and catalogue

    """

    def __init__(
        self,
        csv_root_directory: Optional[Path],
        jsonld_root_directory: Optional[Path],
        name: str,
        parent_domain_name: str,
        federation_domain_name: Optional[str] = None,
        database_name: Optional[str] = None,
    ):
        super().__init__(
            name=name,
            csv_root_directory=csv_root_directory,
            jsonld_root_directory=jsonld_root_directory,
            database_name=database_name,
        )
        self._parent_domain_name = parent_domain_name
        self._federation_domain_name = federation_domain_name
        self._base_url = f"{name}.{parent_domain_name}"
        self._calculate_provider_hash_value()

    def bootstrap(self, enable_compliance_call: bool = True) -> None:
        """
        The bootstrap function is used to generate json-ld.

        :param self: Refer to the object itself
        :param enable_compliance_call: True if we must generate a GaiaX Object for LegalParticipant
        :return: None
        """
        super().bootstrap(enable_compliance_call=enable_compliance_call)

        federation_names = self._compliance_repository.get_compliance_reference_manager_names()
        federation_name = federation_names[0] if len(federation_names) > 0 else DEFAULT_GAIAX_FEDERATION_NAME

        log.info("Creating ServiceOffering objects.")
        service_offering_list = self.__create_gaiax_service_offering_list()
        for service_offering in service_offering_list:
            output_file = Path(self._jsonld_root_directory, extract_file_path_from_object_id(service_offering.did_web))
            dump_service_offering_to_json_ld(service_offering=service_offering, output_file=output_file)

        log.info("Creating Location objects.")
        location_list = self.__create_gxfs_location()
        for location in location_list:
            output_file = Path(self._jsonld_root_directory, extract_file_path_from_object_id(location.did_web))
            dump_gxfs_location_to_json_ld(location=location, output_file=output_file)

        log.info("Creating compliance certificate claims.")
        compliance_certificate_claims = self.__create_compliance_certificate_claim(federation_name)
        for compliance_certificate_claim in compliance_certificate_claims:
            output_file = Path(
                self._jsonld_root_directory, extract_file_path_from_object_id(compliance_certificate_claim.did_web)
            )
            dump_gxfs_compliance_certificate_claim_to_json_ld(
                output_file=output_file, compliance_certificate_claim=compliance_certificate_claim
            )

        log.info("Creating third party compliance certificate claims.")
        third_party_compliance_certificate_claims = self.__create_third_party_compliance_certificate_claim(
            federation_name
        )
        for compliance_certificate_claim in third_party_compliance_certificate_claims:
            output_file = Path(
                self._jsonld_root_directory, extract_file_path_from_object_id(compliance_certificate_claim.did_web)
            )
            dump_gxfs_third_party_compliance_certificate_claim_to_json_ld(
                output_file=output_file, compliance_certificate_claim=compliance_certificate_claim
            )

        log.info("Creating self assessed certificate claims.")
        (
            self_assessed_certificate_claims,
            located_service_offerings_2_self_assessed_claims,
        ) = self.__create_self_assessed_certificate_claim(federation_name)
        for self_assessed_certificate_claim in self_assessed_certificate_claims:
            output_file = Path(
                self._jsonld_root_directory, extract_file_path_from_object_id(self_assessed_certificate_claim.did_web)
            )

            dump_gxfs_self_assessed_compliance_certificate_claim_to_json_ld(
                output_file=output_file, compliance_certificate_claim=self_assessed_certificate_claim
            )
        log.info("Creating located service offerings.")
        located_service_offerings = self.__create_located_service_offering(
            federation_name=federation_name,
            located_service_offerings_2_self_assessed_claims=located_service_offerings_2_self_assessed_claims,
        )

        for located_service_offering in located_service_offerings:
            output_file = Path(
                self._jsonld_root_directory, extract_file_path_from_object_id(located_service_offering.did_web)
            )

            dump_gxfs_located_service_offering_to_json_ld(
                located_service_offering=located_service_offering, output_file=output_file
            )

    async def call_compliance(
        self,
        api_key: Optional[str] = None,
        port_number: int = 443,
    ) -> None:
        """
        The call_compliance_with_self_description_objects method is used to call the compliance process for a legal
        participant and its service offerings. It takes an optional API key and port number as inputs.

        :param api_key: The API key used for authenti cation with the participant agent.
        :param port_number: The port number used to connect to the participant agent.
        """
        await super().call_compliance(api_key, port_number)

        log.info("Call compliance for service-offering")

        user_agent_client = self._get_user_agent_client(api_key, port_number)

        json_index = self._get_json_index()
        if "gx:ServiceOffering" in json_index:
            service_offerings_vcs = iter(json_index["gx:ServiceOffering"].values())
            results = []

            for current_list in more_itertools.batched(service_offerings_vcs, 2):
                batch = [
                    asyncio.create_task(
                        user_agent_client.call_compliance(
                            object_type=OntologyType.SERVICE_OFFERING.value, vc_id=service_offering
                        )
                    )
                    for service_offering in current_list
                ]
                results.extend(await asyncio.gather(*batch))

            log.info("Add Compliance VC for service-offering in index.json")

            for result in results:
                if result is not None:
                    object_type = "gx:compliance"

                    object_id = result.get("id")
                    json_index.setdefault(object_type, []).append(object_id)

            source_directory = Path(self._jsonld_root_directory, f"{self._name}.{self._parent_domain_name}")
            index_file = Path(
                source_directory, OntologyType.get_directory_name(OntologyType.VC, is_vc=True), "index.json"
            )
            self._store_json_index(index_file=index_file, json_index=json_index)

    async def call_aster_x_compliance(
        self,
        membership_vc_id: str,
        user_agent_api_key: str,
        port_number: int = 443,
    ) -> None:
        """
        The call_compliance_with_self_description_objects method is used to call the compliance process for a legal
        participant and its service offerings. It takes an optional API key and port number as inputs.

        :param user_agent_api_key:
        :param membership_vc_id: id (url) of the membership VC
        :param port_number: The port number used to connect to the participant agent.
        """
        log.info("Call Aster-X compliance for service-offering")

        user_agent_client = self._get_user_agent_client(user_agent_api_key, port_number)
        gateway_client = self._get_aster_x_gateway_client(port_number)

        access_token = await self._retrieve_access_token(gateway_client, membership_vc_id, user_agent_client)

        gaia_x_service_offering_ids = await self._retrieve_gaia_x_compliance_ids()

        aster_x_compliances = await self._call_aster_x_compliance(access_token, gaia_x_service_offering_ids,
                                                                  gateway_client, user_agent_client)

        await self._log_aster_x_compliance_inside_index(aster_x_compliances)

    async def _log_aster_x_compliance_inside_index(self, aster_x_compliances):
        log.info("Add Aster-X Compliance VC for service-offering in index.json")
        for result in aster_x_compliances:
            if result is not None:
                object_type = "gx:aster-compliance"
                object_id = result.get("@id")

                json_index = self._get_json_index()
                json_index.setdefault(object_type, []).append(object_id)

                source_directory = Path(self._jsonld_root_directory, f"{self._name}.{self._parent_domain_name}")
                index_file = Path(
                    source_directory, OntologyType.get_directory_name(OntologyType.VC, is_vc=True), "index.json"
                )
                self._store_json_index(index_file=index_file, json_index=json_index)

    @staticmethod
    async def _call_aster_x_compliance(access_token, gaia_x_service_offering_ids, gateway_client,
                                       user_agent_client) -> list:
        aster_x_compliances = []

        for gaia_x_service_offering_id in gaia_x_service_offering_ids:
            log.info(f"Ask Aster-X compliance for ServiceOffering {gaia_x_service_offering_id}")

            gaia_x_service_offering = await user_agent_client.get_vc(vc_url=gaia_x_service_offering_id)
            compliance_session_token = await gateway_client.initiate_compliance()
            vp = await user_agent_client.sign_vp_with_content(
                [gaia_x_service_offering],
                {'domain': compliance_session_token["domain"], 'challenge': compliance_session_token["challenge"]}
            )

            aster_x_compliance = await gateway_client.call_aster_x_service_compliance(content=vp, vp_token=access_token)
            aster_x_compliances.append(aster_x_compliance)

        return aster_x_compliances

    async def _retrieve_gaia_x_compliance_ids(self) -> set:
        gaia_x_service_offering_ids = set()
        json_index = self._get_json_index()

        if "gx:compliance" in json_index:
            gx_compliance_vcs = iter(json_index["gx:compliance"])
            for gx_compliance_vc in gx_compliance_vcs:
                gaia_x_service_offering_ids.add(gx_compliance_vc)

        return gaia_x_service_offering_ids

    @staticmethod
    async def _retrieve_access_token(gateway_client, membership_vc_id, user_agent_client):
        response = await gateway_client.authenticate(await user_agent_client.get_vc(vc_url=membership_vc_id))
        jwt_token = response.get("access_token")

        return jwt_token

    async def call_labelling(self, api_key: Optional[str] = None, port_number: int = 443) -> None:
        """
        call labelling to check label of service offering
        :param api_key: User agent api key
        :param port_number: The port number used to connect to the participant agent.
        :return:
        """
        user_agent_client = self._get_user_agent_client(api_key=api_key, port_number=port_number)
        labelling_client = self.__get_labelling_client()

        json_index = self._get_json_index()
        object_type = f"{ASTER_CONFORMITY_PREFIX}:grantedLabel"
        if object_type not in json_index:
            json_index[object_type] = []

        for located_service_offering_vc_did in json_index[f"{ASTER_CONFORMITY_PREFIX}:LocatedServiceOffering"].values():
            log.info(f"Generating vp_request for current located service offering {located_service_offering_vc_did}")
            located_service_offering_vc = await user_agent_client.get_vc(vc_url=located_service_offering_vc_did)
            jsonld = [located_service_offering_vc]

            jsonld.append(
                await self.__get_service_offering_vc(
                    user_agent_client=user_agent_client,
                    located_service_offering_vc=located_service_offering_vc,
                    json_index=json_index,
                )
            )

            jsonld.extend(
                await self.__get_located_service_offering_claims_vc(
                    user_agent_client=user_agent_client,
                    located_service_offering_vc=located_service_offering_vc,
                    json_index=json_index,
                )
            )

            self_assessed_claim = await self.__get_located_service_offering_self_assessed_claims_vc(
                user_agent_client=user_agent_client,
                located_service_offering_vc=located_service_offering_vc,
                json_index=json_index,
            )
            if self_assessed_claim is not None:
                jsonld.append(self_assessed_claim)

            vp_file = Path(self._jsonld_root_directory, "vp_request.json")
            save_json_on_disk_by_filepath(filepath=vp_file, data=jsonld)
            vp_request = await user_agent_client.sign_vp_with_content(jsonld=jsonld)

            # save_json_on_disk_by_filepath(filepath=vp_file, data=vp_request)

            labeling_response = await labelling_client.check_labels(vp_request=vp_request)
            print(labeling_response)
            if "Error" in labeling_response:
                log.warning("located service offering do not have any label level")
            else:
                log.info("storing vc id into index")
                vc_id = labeling_response["@id"]
                json_index[object_type].append(vc_id)

        source_directory = Path(self._jsonld_root_directory, f"{self._name}.{self._parent_domain_name}")
        index_file = Path(source_directory, OntologyType.get_directory_name(OntologyType.VC, is_vc=True), "index.json")
        self._store_json_index(index_file=index_file, json_index=json_index)

    def _create_gaiax_legal_participant(self) -> Optional[LegalPerson]:
        log.debug("Creating Provider LegalParticipant object.")
        provider = self._compliance_repository.get_provider_from_name(name=self._name)

        return create_provider(
            parent_domain_name=self._parent_domain_name,
            provider_name=self._name,
            legal_name=provider.designation,
            lei_code=provider.lei_code,
            registration_number=provider.registration_number,
            country_name=provider.country,
        )

    def __create_gaiax_service_offering_list(self) -> list[ServiceOffering]:
        service_entities = self._compliance_repository.get_services_of_provider(self._name, fetch_keywords=True)

        return [
            create_gxfs_service_offering(
                parent_domain_name=self._parent_domain_name,
                provider_name=self._name,
                service_id=entity.service_external_id,
                service_name=entity.designation,
                description=entity.description,
                keywords=[value.id for value in entity.keywords],
                layers=self._compliance_repository.get_layer_of_service(service_id=entity.id),
            )
            for entity in service_entities
        ]

    def __create_gxfs_location(self) -> list[Location]:
        # pylint: disable=line-too-long
        location_entities = self._compliance_repository.get_location_from_provider(self._name, fetch_services=True)

        return [
            create_location(
                parent_domain_name=self._parent_domain_name,
                provider_name=self._name,
                location_id=entity.location_external_id,
                country=entity.country,
                state=entity.state,
                urban_area=entity.city,
                provider_designation=entity.designation,
                service_ids=[service.service_external_id for service in entity.services],
            )
            for entity in location_entities
        ]

    def _dump_participant_to_json_ld(
        self, legal_participant: LegalPerson | ComplianceAssessmentBody | ComplianceReferenceManager, output_file: Path
    ):
        log.debug("Generating Provider LegalParticipant json-ld.")
        dump_participant_to_json_ld(legal_participant=legal_participant, output_file=output_file)

    def __create_compliance_certificate_claim(self, federation_name: str) -> list[ComplianceCertificateClaim]:
        entities = self._compliance_repository.get_compliance_certificate_claim_for_provider(provider_name=self._name)

        claims = []

        for service_id, compliance_reference_id, location_id in entities:
            current_claim = create_compliance_certification_claim(
                parent_domain_name=self._parent_domain_name,
                provider_name=self._name,
                located_service_offering=create_located_service_offering(
                    parent_domain_name=self._parent_domain_name,
                    federator_name=federation_name,
                    federator_domain_name=self._federation_domain_name,
                    provider_name=self._name,
                    service_id=service_id,
                    location_id=location_id,
                ),
                compliance_certification_scheme=GaiaxObjectWithDidWeb(
                    did_web=get_compliance_certification_scheme_id(
                        parent_domain_name=self._federation_domain_name,
                        federator_name=federation_name,
                        compliance_reference_id=compliance_reference_id,
                    )
                ),
            )

            claims.append(current_claim)

        return claims

    def __create_third_party_compliance_certificate_claim(
        self, federation_name: str
    ) -> list[ThirdPartyComplianceCertificateClaim]:
        entities = self._compliance_repository.get_third_party_compliance_certificate_claim_for_provider(
            provider_name=self._name
        )

        claims = []
        for service_id, location_id, compliance_reference_id, auditor_name in entities:
            current_claim = create_third_party_compliance_certification_claim(
                parent_domain_name=self._parent_domain_name,
                provider_name=self._name,
                service_id=service_id,
                location_id=location_id,
                compliance_certification_scheme=GaiaxObjectWithDidWeb(
                    did_web=get_third_party_compliance_certification_scheme_id(
                        parent_domain_name=self._federation_domain_name,
                        federator_name=federation_name,
                        compliance_reference_id=compliance_reference_id,
                    )
                ),
                compliance_assessment_body=GaiaxObjectWithDidWeb(
                    did_web=get_auditor_legal_participant_document_id(
                        parent_domain_name=self._federation_domain_name,
                        auditor_name=auditor_name,
                        hash_value=calculate_auditor_hash_value(auditor_name=auditor_name),
                    )
                ),
            )
            claims.append(current_claim)

        return claims

    def __create_self_assessed_certificate_claim(
        self, federation_name: str
    ) -> (list[SelfAssessedComplianceCriteriaClaim], dict):
        service_ids = [
            service.id
            for service in self._compliance_repository.get_services_of_provider(
                provider_name=self._name, fetch_keywords=False
            )
        ]

        service_2_crit = {}
        for service_id in service_ids:
            criterion_ids = self._compliance_repository.get_self_assessed_compliance_criteria_for_service(
                service_id=service_id
            )

            if len(criterion_ids) > 0:
                service_2_crit[service_id] = set(sorted(criterion_ids))

        for service, criteria in service_2_crit.items():
            services = Map2.retrieve(criteria) or Map2(criteria)
            services.service_ids.add(service)

        self_assessed_references = [(reference.service_ids, reference.criteria) for reference in Map2.references]

        self_assessed_certificate_claims = []
        claims_2_services = {}
        for current_service_ids, current_criterions in self_assessed_references:
            located_services = []
            for current_service_id in current_service_ids:
                current_located_services = [
                    GaiaxObjectWithDidWeb(
                        did_web=get_located_service_did(
                            parent_domain_name=self._parent_domain_name,
                            provider_name=self._name,
                            location_id=location_id,
                            service_id=service_id,
                        )
                    )
                    for service_id, location_id in self._compliance_repository.get_service_location_from_service_id(
                        service_id=current_service_id
                    )
                ]
                located_services.extend(current_located_services)

            current_claim = create_self_assessed_compliance_criteria_claim(
                parent_domain_name=self._parent_domain_name,
                federator_name=federation_name,
                federator_domain_name=self._federation_domain_name,
                provider_name=self._name,
                located_service_offerings=located_services,
                criteria=current_criterions,
            )

            self_assessed_certificate_claims.append(current_claim)
            claims_2_services[current_claim.did_web] = current_service_ids

        # pylint: disable=consider-using-dict-items
        services_2_claims = {
            get_entity_external_id(provider_name=self._name, entity_id=service_id): claim_id
            for claim_id in claims_2_services
            for service_id in claims_2_services[claim_id]
        }

        return self_assessed_certificate_claims, services_2_claims

    def __create_located_service_offering(
        self, federation_name: str, located_service_offerings_2_self_assessed_claims: dict
    ) -> list[LocatedServiceOffering]:
        service_location_list = self._compliance_repository.get_service_location_of_provider(self._name)
        located_service_offerings = []
        for service_external_id, location_external_id in service_location_list:
            compliance_certificate_claims = self._compliance_repository.get_compliance_certificate_claim_for_service(
                get_entity_id(self._name, service_external_id)
            )
            third_party_compliance_certificate_claims = (
                self._compliance_repository.get_third_party_compliance_certificate_claim_for_service(
                    get_entity_id(self._name, service_external_id)
                )
            )
            self_assessed_compliance_certificate_claim = None
            if service_external_id in located_service_offerings_2_self_assessed_claims:
                self_assessed_compliance_certificate_claim = located_service_offerings_2_self_assessed_claims[
                    service_external_id
                ]

            located_service_offering = create_located_service_offering(
                parent_domain_name=self._parent_domain_name,
                federator_name=federation_name,
                federator_domain_name=self._federation_domain_name,
                provider_name=self._name,
                location_id=location_external_id,
                service_id=service_external_id,
                compliance_certificate_claims=compliance_certificate_claims,
                third_party_compliance_certificate_claims=third_party_compliance_certificate_claims,
                self_assessed_compliance_certificate_claim=self_assessed_compliance_certificate_claim,
            )
            located_service_offerings.append(located_service_offering)

        return located_service_offerings

    def __get_labelling_client(self) -> LabellingAgentClient:
        federation_names = self._compliance_repository.get_compliance_reference_manager_names()
        federation_name = federation_names[0] if len(federation_names) > 0 else "aster-x"

        return LabellingAgentClient(base_url=f"{federation_name}.{self._federation_domain_name}")

    async def __get_located_service_offering_claims_vc(
        self, user_agent_client: UserAgentClient, located_service_offering_vc: dict, json_index: dict[str, Any]
    ):
        compliance_claim_ids = located_service_offering_vc["credentialSubject"][
            f"{ASTER_CONFORMITY_PREFIX}:hasComplianceCertificateClaim"
        ]

        jsonld = []
        for compliance_claim in compliance_claim_ids:
            if "id" in compliance_claim:
                compliance_claim_id = compliance_claim["id"]
                claim_did_web = None
                if OntologyType.THIRD_PARTY_COMPLIANCE_CERT_CLAIM.value in compliance_claim_id:
                    if (
                        compliance_claim_id
                        in json_index[f"{ASTER_CONFORMITY_PREFIX}:ThirdPartyComplianceCertificateClaim"]
                    ):
                        claim_did_web = json_index[f"{ASTER_CONFORMITY_PREFIX}:ThirdPartyComplianceCertificateClaim"][
                            compliance_claim_id
                        ]
                elif OntologyType.COMPLIANCE_CERT_CLAIM.value in compliance_claim_id:
                    if compliance_claim_id in json_index[f"{ASTER_CONFORMITY_PREFIX}:ComplianceCertificateClaim"]:
                        claim_did_web = json_index[f"{ASTER_CONFORMITY_PREFIX}:ComplianceCertificateClaim"][
                            compliance_claim_id
                        ]
                else:
                    claim_did_web = compliance_claim_id

                if claim_did_web is not None:
                    compliance_claim_vc = await user_agent_client.get_vc(vc_url=claim_did_web)
                    jsonld.append(compliance_claim_vc)

        return jsonld

    async def __get_located_service_offering_self_assessed_claims_vc(
        self, user_agent_client: UserAgentClient, located_service_offering_vc: dict, json_index: dict[str, Any]
    ):
        if (
            f"{ASTER_CONFORMITY_PREFIX}:hasSelfAssessedComplianceCriteriaClaim"
            not in located_service_offering_vc["credentialSubject"]
        ):
            return None

        self_assessed_claim = located_service_offering_vc["credentialSubject"][
            f"{ASTER_CONFORMITY_PREFIX}:hasSelfAssessedComplianceCriteriaClaim"
        ]

        if "id" not in self_assessed_claim:
            return None

        self_assessed_claim_id = self_assessed_claim["id"]
        self_assessed_claim_vc_did_web = json_index[f"{ASTER_CONFORMITY_PREFIX}:SelfAssessedComplianceCriteriaClaim"][
            self_assessed_claim_id
        ]

        return await user_agent_client.get_vc(vc_url=self_assessed_claim_vc_did_web)

    async def __get_service_offering_vc(
        self, user_agent_client: UserAgentClient, located_service_offering_vc: dict, json_index: dict[str, Any]
    ):
        if f"{ASTER_CONFORMITY_PREFIX}:isImplementationOf" not in located_service_offering_vc["credentialSubject"]:
            return None

        service_offering = located_service_offering_vc["credentialSubject"][
            f"{ASTER_CONFORMITY_PREFIX}:isImplementationOf"
        ]

        if "id" not in service_offering:
            return None

        service_offering_id = service_offering["id"]
        service_offering_vc_did_web = json_index[f"{GX_TRUST_FRAMEWORK_PREFIX}:ServiceOffering"][service_offering_id]

        response = await user_agent_client.get_vc(vc_url=service_offering_vc_did_web)
        return response
