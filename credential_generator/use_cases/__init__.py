# -*- coding: utf-8 -*-
"""
It defines common exceptions
"""


class HostNotReachable(Exception):
    """
    The HostNotReachable class is a custom exception class that is raised when a host is not reachable. It inherits
    from the base Exception class and takes a message as input.
    """

    def __init__(self, message: str) -> None:
        super().__init__(message)

class ResourceConflictException(Exception):
    """
    The HostNotReachable class is a custom exception class that is raised when a host is not reachable. It inherits
    from the base Exception class and takes a message as input.
    """

    def __init__(self, message: str) -> None:
        super().__init__(message)

class RemoteProtocolErrorException(Exception):
    """
    The HostNotReachable class is a custom exception class that is raised when a host is not reachable. It inherits
    from the base Exception class and takes a message as input.
    """

    def __init__(self, message: str) -> None:
        super().__init__(message)
