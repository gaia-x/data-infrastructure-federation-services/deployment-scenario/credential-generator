# -*- coding: utf-8 -*-
"""
It defines gaiax auditor use cases
"""
import logging
from pathlib import Path
from typing import Optional

from credential_generator.domain._2210.gxfs.gx_compliance import ComplianceAssessmentBody, ComplianceReferenceManager
from credential_generator.domain._2210.trust_framework.participant import LegalPerson
from credential_generator.domain.factories._2210.gxfs.compliance import create_compliance_assessment_body
from credential_generator.infra.spi.filesystem.writer.gxfs_fr_generator import dump_gxfs_cab_to_json_ld
from credential_generator.use_cases.gaiax_participant import LegalParticipantUseCase

log = logging.getLogger(__name__)


class AuditorUseCase(LegalParticipantUseCase):
    """
    AuditorUseCase is responsible to generate GaiaX json-ld files, upload  them to auditor's user agent

    """

    def __init__(
        self,
        csv_root_directory: Path,
        jsonld_root_directory: Optional[Path],
        name: str,
        parent_domain_name: str,
        federation_domain_name: Optional[str] = None,
        database_name: Optional[str] = "",
    ):
        super().__init__(
            name=name,
            csv_root_directory=csv_root_directory,
            jsonld_root_directory=jsonld_root_directory,
            database_name=database_name,
        )
        self._parent_domain_name = parent_domain_name
        self._federation_domain_name = federation_domain_name
        self._base_url = f"{name}.{parent_domain_name}"
        self._calculate_provider_hash_value()

    def _create_gaiax_legal_participant(self) -> Optional[LegalPerson]:
        log.debug("Creating Auditor LegalParticipant object.")
        cab_entity = self._compliance_repository.get_compliance_assessments_body_from_name(name=self._name)
        return create_compliance_assessment_body(
            parent_domain_name=self._parent_domain_name,
            auditor_name=self._name,
            legal_name=cab_entity.designation,
            country_name=cab_entity.country,
            lei_code=cab_entity.lei_code,
            registration_number=cab_entity.registration_number,
        )

    def _dump_participant_to_json_ld(
        self, legal_participant: LegalPerson | ComplianceAssessmentBody | ComplianceReferenceManager, output_file: Path
    ):
        log.debug("Generating Auditor LegalParticipant json-ld.")
        dump_gxfs_cab_to_json_ld(cab=legal_participant, output_file=output_file)
