# -*- coding: utf-8 -*-
"""
Module: Parameter Validator

This module provides functionality to validate parameters passed to functions or methods within an application.
It ensures that parameters are either non-empty strings or lists of non-empty strings. The module aims to enhance input
validation, thereby improving data integrity and error handling throughout the application.

Functions:
    check_required_parameters(**kwargs): Validates that each keyword argument is either a non-empty string or a list
    containing only non-empty strings. Raises exceptions for invalid inputs.

Utility Functions:
    _check_none_value(parameter, value): Checks if the provided value is None and raises a TypeError if so.
    _check_string(parameter, value): Checks if the provided value is a non-empty string and raises a TypeError or
    ValueError if it does not meet the criteria.
    _check_list(parameter, value): Checks if the provided value is a list containing only non-empty strings and raises
    a ValueError if it does not meet the criteria.

Exceptions raised:
    TypeError: Raised if a parameter is neither a string nor a list, or if it is None.
    ValueError: Raised if a parameter is an empty string, or if it is a list containing non-strings or empty strings.

Usage:
The `check_required_parameters` function can be used to validate the parameters of any function or method to ensure
they adhere to the expected format, thus preventing common input-related errors.

Example:
    def some_function(param1, param2, param3):
        check_required_parameters(param1=param1, param2=param2, param3=param3)
        # Function logic follows...

This module promotes the implementation of robust input validation across different components of an application,
contributing to overall software quality and reliability.
"""


def check_required_parameters(**kwargs):
    """
    Check that all required parameters are provided and have valid values, including non-empty strings and
    lists of non-empty strings.

    Args:
        **kwargs: Arbitrary keyword arguments representing parameter names and values.

    Raises:
        TypeError: If any required parameter is None.
        ValueError: If any required parameter is an empty string or a list containing empty strings.
    """
    for parameter, value in kwargs.items():
        _check_none_value(parameter, value)

        if isinstance(value, list):
            _check_list_of_non_empty_strings(parameter, value)
        else:
            _check_non_empty_string(parameter, value)


def _check_none_value(parameter, value):
    """
    Check that a parameter value is not None.

    Args:
        parameter (str): The name of the parameter being checked.
        value: The value of the parameter being checked.

    Raises:
        TypeError: If the parameter value is None.
    """
    if value is None:
        raise TypeError(f"{parameter} value must not be null")


def _check_non_empty_string(parameter, value):
    """
    Check that a parameter value is a non-empty string.

    Args:
        parameter (str): The name of the parameter being checked.
        value: The value of the parameter being checked.

    Raises:
        TypeError: If the parameter value is not a string.
        ValueError: If the parameter value is an empty string.
    """
    if not isinstance(value, str):
        raise TypeError(f"{parameter} value must be a string")
    if not value.strip():
        raise ValueError(f"{parameter} value must not be empty")


def _check_list_of_non_empty_strings(parameter, value):
    """
    Check that a list of values consists of non-empty strings.

    Args:
        parameter (str): The name of the parameter being checked.
        value (list): The list of values to be checked.

    Raises:
        TypeError: If the parameter value is not a list.
        ValueError: If the list is empty or contains items that are not non-empty strings.
    """
    if not isinstance(value, list):
        raise TypeError(f"{parameter} value must be a list")

    if not value:
        raise ValueError(f"{parameter} list must not be empty")
    for item in value:
        if not isinstance(item, str) or not item.strip():
            raise ValueError(f"All items in {parameter} list must be non-empty strings")
