# -*- coding: utf-8 -*-
"""
It defines logging utilities
"""
# Standard Library
import logging
import logging.config
from typing import Any, Callable, MutableMapping, Tuple, TypeAlias

import structlog

EventDict: TypeAlias = MutableMapping[str, Any]

ProcessorType = Callable[
    [
        structlog.types.WrappedLogger,
        str,
        structlog.types.EventDict,
    ],
    str | bytes,
]


def get_render_processor(
    render_json_logs: bool = False,
    colors: bool = True,
) -> ProcessorType:
    """
    The get_render_processor function returns a processor that will render the log entries in either JSON or
    human-readable format. The default is human-readable, but if you pass True to the render_json_logs parameter,
    it will return a JSONRenderer instead of ConsoleRenderer.

    Args:
        render_json_logs: bool: Determine if the logs should be rendered in json format
        colors: bool: Specify whether the logs should be rendered in color or not
        : Determine whether the logs should be rendered in json format or not

    Returns:
        A processor that renders logs in a

    """
    if render_json_logs:
        return structlog.processors.JSONRenderer()
    return structlog.dev.ConsoleRenderer(colors=colors)


def remove_color_message(_, __, event_dict: EventDict) -> EventDict:
    """
    The remove_color_message function removes the color_message key from an event dictionary.

    Args:
        _: Pass the event_dict to the next function in the chain
        __: Pass the event_dict to the function
        event_dict: EventDict: Pass the event dictionary to the function

    Returns:
        The event_dict with the color_message key removed
    """
    event_dict.pop("color_message", None)
    return event_dict


def configure_logging(log_level: str = "INFO") -> None:
    """
    The configure_logging function is responsible for configuring the logging system.
    It does this by setting up a StreamHandler, which will log to stdout, and then
    configures structlog with some processors that are common to all environments.
    The configure_logging function also sets the LOG_LEVEL environment variable as well as ENVIRONMENT.

    Args:

    Returns:
        None, so it's not a good candidate for
    """

    common_processors: Tuple[structlog.typing.Processor, ...] = (
        structlog.stdlib.add_log_level,
        structlog.stdlib.add_logger_name,
        structlog.stdlib.ExtraAdder(),
        structlog.dev.set_exc_info,
        structlog.processors.TimeStamper(fmt="iso", utc=True),
        structlog.processors.dict_tracebacks,
        remove_color_message,
    )

    structlog_processors = (
        structlog.stdlib.filter_by_level,
        structlog.stdlib.PositionalArgumentsFormatter(),
        structlog.processors.StackInfoRenderer(),
        structlog.processors.format_exc_info,
        structlog.processors.UnicodeDecoder(),
        structlog.stdlib.ProcessorFormatter.wrap_for_formatter,
    )

    logging_processors = (structlog.stdlib.ProcessorFormatter.remove_processors_meta,)
    logging_console_processors = (
        *logging_processors,
        get_render_processor(render_json_logs=False, colors=True),
    )

    handler = logging.StreamHandler()
    handler.set_name("default")
    handler.setLevel(log_level)
    console_formatter = structlog.stdlib.ProcessorFormatter(
        foreign_pre_chain=common_processors,  # type: ignore
        processors=logging_console_processors,
    )
    handler.setFormatter(console_formatter)

    handlers: list[logging.Handler] = [handler]

    logging.basicConfig(handlers=handlers, level=log_level)
    structlog.configure(
        processors=common_processors + structlog_processors,
        logger_factory=structlog.stdlib.LoggerFactory(),
        wrapper_class=structlog.stdlib.BoundLogger,  # type: ignore
        cache_logger_on_first_use=True,
    )
