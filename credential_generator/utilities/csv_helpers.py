# -*- coding: utf-8 -*-
"""
This module lists csv helper functions..
"""
# Standard Library
from typing import Dict, Iterable, List, Optional


def get_value_of(row: Dict[str, str], field: str) -> Optional[str]:
    """
    The get_value_of function takes a row and a field as input,
    and returns the value of that field in the row. If the field is not present in
    the row, it returns an empty string.

    :param row: Dict[str: Specify the type of row, which is a dictionary
    :param str]: Specify the type of row, which is a dictionary with string keys and string values
    :param field: str: Specify the field name that we want to get the value of
    :return: The value of a field in a row
    """
    field = row.get(field)
    return None if field is None else field.strip()


def get_splitted_values_of(row: Optional[str]) -> List[str]:
    """
    The get_splitted_values_of function takes a string and returns a list of strings.
    The input string is split by the semicolon character, and each resulting substring is stripped of whitespace.

    :param list: Specify the type of the parameter
    :return: A list of strings
    :doc-author: Trelent
    """
    return [val.strip() for val in row.split(";")] if row is not None else None


def list_to_csv(value_list: Optional[Iterable], delimiter: str = ";") -> str:
    """
    The list_to_csv function takes a list of values and returns them as a string,
        separated by the delimiter.

    :param value_list: Iterable: Specify the type of the value_list parameter
    :param delimiter: str: Specify the delimiter used to separate values in the list
    :return: A string
    """

    if value_list is None:
        raise TypeError("value_list cannot be None")

    if delimiter is None:
        raise TypeError("delimiter cannot be None")

    if not isinstance(delimiter, str):
        raise TypeError("delimiter must be a string")
    return delimiter.join(value_list)
