# -*- coding: utf-8 -*-
"""
This module lists did web helper functions.
"""
# Standard Library
from enum import Enum
import hashlib
from typing import Any, Dict, Final, Optional

DID_WEB_PREFIX: Final = "https://"


class HashType(Enum):
    """
    Defines default hash algorithm types
    """

    SHA256 = "SHA256"
    SHA512 = "SHA512"


def compute_hash_value(field: bytes, hash_type: HashType = HashType.SHA256) -> str:
    """Compute the SHA-256 hash value of a byte string.

    :param field: The byte string to hash.
    :param hash_type: The type of hash to apply.
    :return: The hash value as a hexadecimal string.
    """
    if field is None or not isinstance(field, bytes) or field == b"":
        raise TypeError("Input must be of type bytes")

    match hash_type:
        case HashType.SHA256:
            return hashlib.sha256(field).hexdigest()
        case HashType.SHA512:
            return hashlib.sha512(field).hexdigest()
        case _:
            raise TypeError("Type is not a valid HashType.")


def calculate_provider_hash_value(provider_name: str):
    """
    Compute hash value of the Gaia-X Provider Identifier.

    :param provider_name: Name of provider
    :return: A string with computed hash value.
    """
    return compute_hash_value(bytes(provider_name, "utf-8"))


def calculate_federator_hash_value(federator_name: str):
    """
    Compute hash value of the Gaia-X Federator Identifier.

    :param federator_name: Name of federator
    :return: A string with computed hash value.
    """
    return compute_hash_value(bytes(federator_name, "utf-8"))


def calculate_auditor_hash_value(auditor_name: str):
    """
    Compute hash value of the Gaia-X Auditor Identifier.

    :param auditor_name: Name of auditor
    :return: A string with computed hash value.
    """
    return compute_hash_value(bytes(auditor_name, "utf-8"))


def calculate_service_offering_hash_value(service_id: str):
    """
    Compute hash value of the Gaia-X Service Offering Identifier.

    :param service_id: ID of service offering
    :return: A string with computed hash value.
    """
    return compute_hash_value(bytes(service_id, "utf-8"))


def calculate_location_hash_value(location_id: str):
    """
    Compute hash value of the GXFS-FR Location Identifier.

    :param location_id: ID of location
    :return: A string with computed hash value.
    """
    return compute_hash_value(bytes(location_id, "utf-8"))


def calculate_compliance_criterion_hash_value(criterion_name: str, criterion_level: str):
    """
    Compute hash value of the GXFS-FR Compliance Criterion Identifier.

    :param criterion_id: ID of compliance criterion
    :return: A string with computed hash value.
    """
    hash_value = compute_hash_value(bytes(criterion_name + criterion_level, "utf-8"))
    return compute_hash_value(bytes(hash_value, "utf-8"))


def calculate_compliance_label_hash_value(label_level: str):
    """
    Compute hash value of the GXFS-FR Compliance Label Identifier.

    :return: A string with computed hash value.
    """
    return compute_hash_value(bytes(label_level, "utf-8"))


def calculate_located_service_hash_value(location_id: str, service_id: str) -> str:
    """
    Compute hash value of the GXFS-FR Located Service Offering Identifier.

    :param location_id: ID of location
    :param service_id: ID of service
    :return: A string with computed hash value.
    """

    if not isinstance(service_id, str) or not isinstance(location_id, str):
        raise TypeError

    return compute_hash_value(bytes(f"{service_id}_{location_id}", "utf-8"))


def calculate_compliance_reference_hash_value(title: str, version: str):
    """
    Compute hash value of the GXFS-FR Compliance Reference.

    :param title: Title of compliance reference
    :param version: Version of compliance reference
    :return: A string with computed hash value.
    """
    return compute_hash_value(bytes(title + version, "utf-8"))


def calculate_compliance_certification_scheme_hash_value(compliance_reference_id: str):
    """
    Compute hash value of the GXFS-FR Compliance Certification Scheme.

    :param compliance_reference_id: ID of compliance reference
    :return: A string with computed hash value.
    """
    return compute_hash_value(bytes(compliance_reference_id, "utf-8"))


def calculate_compliance_certificate_claim_hash_value(located_service_object_id: str, scheme_object_id: str):
    """
    Compute hash value of the GXFS-FR Compliance Certification Claim.

    :param located_service_object_id : did web of associated located service offering
    :param scheme_object_id : did web of associated compliance certification scheme
    :return: A string with computed hash value.
    """
    return compute_hash_value(bytes(located_service_object_id + scheme_object_id, "utf-8"))


def calculate_third_party_compliance_certificate_claim_hash_value(
    located_service_object_id: str, scheme_object_id: str
):
    """
    Compute hash value of the GXFS-FR Third Party Compliance Certification Claim.

    :param located_service_object_id : did web of associated located service offering
    :param scheme_object_id : did web of associated third party compliance certification scheme
    :return: A string with computed hash value.
    """
    return compute_hash_value(bytes(located_service_object_id + scheme_object_id, "utf-8"))


def calculate_self_assessed_certificate_claim_hash_value(criteria: list[str]):
    """
    This function calculates the hash value of a list of criteria by converting it to a byte string and applying the
    SHA-256 hash algorithm.

    ## Example Usage
    ```
    criteria = ["criterion1", "criterion2"]
    result = calculate_self_assessed_certificate_claim_hash_value(criteria)
    print(result)
    ```

    :param criteria: A list of strings representing the criteria.
    :return:The hash value of the criteria as a hexadecimal string.
    """

    if criteria is None:
        raise TypeError("criteria list must be not null")

    if not isinstance(criteria, list):
        raise TypeError("criteria list must be a list")

    if any(not isinstance(crit, str) or not crit for crit in criteria):
        raise TypeError("criteria must be a list of string")

    if not criteria:
        raise TypeError("criteria must not be an empty string")

    return compute_hash_value(bytes(str(criteria), "utf-8"))


def extract_directory_from_object_id(object_id: str) -> str:
    """
    Extracts the directory from an object ID.

    Args:
        object_id (str): The object ID to extract the directory from.

    Returns:
        str: The directory extracted from the object ID.

    Raises:
        ValueError: If object_id is None, not a string, or not starting with DID_WEB_PREFIX.

    Examples:
        extract_directory_from_object_id("did:web:somedirectory/data.json")
    """
    if object_id is None:
        raise ValueError("object_id must be not None")

    if not isinstance(object_id, str):
        raise ValueError(f"object_id must be a string starting with {DID_WEB_PREFIX}")

    if not object_id.startswith(DID_WEB_PREFIX):
        raise ValueError(f"{object_id} is not a valid did web. It must start with {DID_WEB_PREFIX}")

    return object_id.removeprefix(DID_WEB_PREFIX).removesuffix("data.json")


def extract_file_path_from_object_id(object_id: str) -> str:
    """
    Extracts the file path from an object ID.

    Args:
        object_id (str): The object ID to extract the file path from.

    Returns:
        str: The file path extracted from the object ID.

    Raises:
        ValueError: If object_id is None, not a string, or not starting with DID_WEB_PREFIX.
    """
    if object_id is None:
        raise ValueError("object_id must be not None")

    if not isinstance(object_id, str):
        raise ValueError(f"object_id must be a string starting with {DID_WEB_PREFIX}")

    if not object_id.startswith(DID_WEB_PREFIX):
        raise ValueError(f"{object_id} is not a valid did web. It must start with {DID_WEB_PREFIX}")

    return object_id.removeprefix(DID_WEB_PREFIX).replace(":", "/")


def extract_file_path_from_object_id_without_participant_part(object_id: str, type_object: str) -> str:
    """
    Extracts the file path from an object ID without the participant part.

    Args:
        object_id (str): The object ID to extract the file path from.
        type_object (str): The type of object to search for in the object ID.

    Returns:
        str: The file path extracted from the object ID without the participant part.

    Raises:
        ValueError: If object_id is None, not a string, or not starting with DID_WEB_PREFIX.
    """
    if object_id is None:
        raise ValueError("object_id must be not None")

    if not isinstance(object_id, str):
        raise ValueError(f"object_id must be a string starting with {DID_WEB_PREFIX}")

    if not object_id.startswith(DID_WEB_PREFIX):
        raise ValueError(f"{object_id } is not a valid did web. It must start with {DID_WEB_PREFIX}")

    index = object_id.find(type_object)
    value = object_id[index:] if index != -1 else extract_file_path_from_object_id(object_id)

    return value.replace(":", "/")


def get_well_known_root_object_id(object_id: str) -> str:
    """
    Gets the well-known root from an object ID.

    Args:
        object_id (str): The object ID to extract the well-known root from.

    Returns:
        str: The well-known root extracted from the object ID.

    Raises:
        ValueError: If object_id is None, not a string, or not starting with DID_WEB_PREFIX.
    """
    if object_id is None:
        raise ValueError("object_id must be not None")

    if not isinstance(object_id, str):
        raise ValueError(f"object_id must be a string starting with {DID_WEB_PREFIX}")

    if not object_id.startswith(DID_WEB_PREFIX):
        raise ValueError(f"{object_id } is not a valid did web. It must start with {DID_WEB_PREFIX}")

    index = object_id.find(":participant:")
    return f"{object_id [:index]}/.well-known" if index != -1 else f"{object_id }/.well-known"


def get_participant_folder(provider_name: str, parent_domain_name: str) -> str:
    """
    Gets the participant folder path based on the provider name and parent domain name.

    Args:
        provider_name (str): The name of the provider.
        parent_domain_name (str): The name of the parent domain.

    Returns:
        str: The participant folder path generated using the provider name and parent domain name.

    Raises:
        TypeError: If provider_name or parent_domain_name is None or not a string.
    """
    if provider_name is None or parent_domain_name is None:
        raise TypeError

    if not isinstance(provider_name, str) or not isinstance(parent_domain_name, str):
        raise TypeError

    return f"{provider_name}.{parent_domain_name}"


def get_root_url_from_object_id(object_id: str) -> str:
    """
    Gets the root URL from an object ID.

    Args:
        object_id (str): The object ID to extract the root URL from.

    Returns:
        str: The root URL extracted from the object ID.

    Raises:
        ValueError: If object_id is None, not a string, or not starting with DID_WEB_PREFIX.
    """
    if object_id is None:
        raise ValueError("object_id must be not None")

    if not isinstance(object_id, str):
        raise ValueError(f"object_id must be a string starting with {DID_WEB_PREFIX}")

    if not object_id.startswith(DID_WEB_PREFIX):
        raise ValueError(f"{object_id } is not a valid did web. It must start with {DID_WEB_PREFIX}")

    object_id = object_id.removeprefix(DID_WEB_PREFIX)
    return object_id.split("/")[0]


def get_id_from_jsonld(jsonld: Dict[str, Any]) -> Optional[str]:
    """
    Get the ID from a JSON-LD dictionary.

    Args:
        jsonld: The JSON-LD dictionary.

    Returns:
        The ID value, or None if not found or not a string.
    """
    if isinstance(jsonld, dict):
        if not jsonld:
            return None
        for current in ["@id", "id"]:
            if current in jsonld and isinstance(jsonld[current], str):
                return jsonld[current]
    return None


def get_type_from_jsonld(jsonld: Dict[str, Any]) -> Optional[str | list[str]]:
    """
    Get the Type from a JSON-LD dictionary.

    Args:
        jsonld: The JSON-LD dictionary.

    Returns:
        The Type value, or None if not found or not a string.
    """
    if isinstance(jsonld, dict):
        if not jsonld:
            return None
        for current in ["@type", "type"]:
            if current in jsonld and isinstance(jsonld[current], (str, list)):
                return jsonld[current]
    return None
