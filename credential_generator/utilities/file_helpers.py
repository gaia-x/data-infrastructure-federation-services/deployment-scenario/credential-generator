# -*- coding: utf-8 -*-
"""
This module lists file helper functions.
"""

import contextlib
import json
import logging
import os
from pathlib import Path
import shutil
from typing import Any, Optional

log = logging.getLogger(__name__)


def save_on_disk(directory: str, filename: str, data: Any):
    """
    The save_on_disk function saves the data to a file on disk.

    :param directory: str: Specify the directory where the file will be saved
    :param filename: str: Specify the name of the file to save
    :param data: Any: Specify the type of data that will be saved
    :return: None
    """

    if directory is None or not directory:
        raise FileNotFoundError("directory is empty")

    if filename is None or not filename:
        raise IsADirectoryError("filename is empty")

    save_json_on_disk_by_filepath(Path(directory, filename), data)


def save_json_on_disk_by_filepath(filepath: Path, data: Any):
    """
    The save_on_disk_by_filepath function saves the data to a file on disk.

    :param filepath: Path: Specify the path of the file to be saved
    :param data: Any: Pass the data to be saved
    :return: Nothing
    """
    dirname = filepath.parent
    dirname.mkdir(parents=True, exist_ok=True)
    with open(filepath, "w", encoding="utf-8") as result_file:
        json.dump(obj=data, indent=2, fp=result_file)


def clean_output_directory(directory: str):
    """
    The clean_output_directory function deletes the contents of a directory, if it exists.
    If the directory does not exist, it is created.


    :param directory: str: Specify the directory that is to be cleaned
    :return: None
    """
    with contextlib.suppress(FileNotFoundError):
        shutil.rmtree(path=directory, ignore_errors=False)

    Path(directory).mkdir(parents=True, exist_ok=True)


def get_all_files_from(source_directory: Path, exclude_patterns: Optional[list[str]] = None) -> list[Path]:
    """
    The get_all_files_from function recursively retrieves all files from a given directory and its subdirectories.
    :param exclude_patterns:  list of string to exclude when searching files
    :param source_directory: source_directory (type: Path): The directory from which to retrieve all files.
    :return: files (type: list[Path]): A list containing the paths of all files in the source_directory and its
    subdirectories.
    """
    if not isinstance(source_directory, Path):
        raise TypeError("source_directory must be a Path")

    if not source_directory.exists():
        return []

    if exclude_patterns is None:
        exclude_patterns = []

    exclude_patterns.append(".DS_Store")
    files: list[Path] = []
    for file in source_directory.iterdir():
        if file.is_file():
            if not file.name.endswith("index.json") and all(
                f"{os.sep}{pattern}{os.sep}" not in str(file) for pattern in exclude_patterns
            ):
                files.append(file)
        else:
            files.extend(get_all_files_from(file, exclude_patterns))

    return files


def get_all_files_of_type(source_directory: Path, types: list[str]) -> list[Path]:
    """
    This code defines a function named get_all_files_of_type that takes a source directory and a list of file types as
    inputs. It recursively searches the source directory and its subdirectories for files that match the specified
    types, excluding files with the name "index.json". The function returns a list of paths to the matching files.

    ## Example Usage
    ```source_directory = Path("/path/to/directory")
    types = [".txt", ".csv"]
    files = get_all_files_of_type(source_directory, types)
    print(files)
    ```
    :param source_directory: A Path object representing the directory to search for files.
    :param types: A list of file types to match, specified as strings.
    :return: A list of Path objects representing the paths of files that match the specified types in the source
    directory and its subdirectories.
    """
    if not isinstance(source_directory, Path):
        raise TypeError("source_directory must be a Path")

    if not source_directory.exists():
        return []

    if not isinstance(types, list):
        raise TypeError("types must be a list")

    if not all(isinstance(current_type, str) for current_type in types):
        raise TypeError("types must be a list of strings")

    files: list[Path] = []

    for file in source_directory.iterdir():
        if file.is_file():
            if not file.name.endswith("index.json") and any(f"{os.sep}{pattern}{os.sep}" in str(file) for pattern in types):
                (files.append(file))
        else:
            files.extend(get_all_files_of_type(source_directory=file, types=types))

    return files


def get_all_files_from_name(source_directory: Path, filename: str) -> list[Path]:
    """
    From a source directory, returns all files corresponding to filename.

    :param source_directory: A Path object representing the directory to search for files.
    :param filename: A filename.
    :return: A list of Path objects representing the paths of files that match the specified types in the source
    directory and its subdirectories.
    """
    if not isinstance(source_directory, Path):
        raise TypeError("source_directory must be a Path")

    if not source_directory.exists():
        return []

    if not isinstance(filename, str):
        raise TypeError("filename must be a str")

    files: list[Path] = []

    for file in source_directory.iterdir():
        if file.is_file():
            if file.name == filename:
                (files.append(file))
        else:
            files.extend(get_all_files_from_name(source_directory=file, filename=filename))

    return files
