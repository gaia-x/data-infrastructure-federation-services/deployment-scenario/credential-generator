# -*- coding: utf-8 -*-
"""
It defines utilities functions
"""
from datetime import datetime
import hashlib
import logging
from typing import ClassVar, Optional

import httpx
import pycountry

log = logging.getLogger(__name__)

ADMINISTRATIVE_LOCATIONS = {
    "France-Seine": "FR-75C",
    "France-Alsace": "FR-6AE",
    "United Kingdom-London": "GB-LND",
    "Italy-Milan": "IT-MI",
}


def compute_sha256_document_from_url(url: str) -> str:
    """
    The compute_sha256 function takes a URL as input and returns the SHA256 hash of the file at that URL.

    :param url: str: Pass in the url of the file to be downloaded
    :return: The sha256 hash of the file at the given url
    """
    if url is None or not url:
        return ""

    try:
        client = httpx.Client(follow_redirects=True)
        response = client.get(url=url, timeout=30)

        sha256_hash = hashlib.sha256(response.content)
        match response.status_code:
            case 200:
                return sha256_hash.hexdigest()
            case _:
                return ""
    except (httpx.ConnectError, httpx.TimeoutException) as err:
        log.warning(f"Cannot connect to {url} due to {err}")
        return ""


def date_to_xds(date: str) -> str:
    """
    The date_to_xds function takes a date in the format of MM/DD/YY and converts it to an ISO 8601 formatted string.

    :param date: str: Tell the function that it will be receiving a string as an argument
    :return: A string in the format of yyyy-mm-dd
    """
    if not isinstance(date, str):
        raise TypeError
    return datetime.strptime(date, "%m/%d/%y").isoformat()


def find_administrative_location(country_name: str, department: str) -> Optional[str]:
    """
    The find_administrative_location function takes a country name and department as input,
    and returns the corresponding administrative location code.


    :param country_name: str: Pass the country name to the function
    :param department: str: Find the administrative location of a country
    :return: The code of the administrative location
    """
    if country_name is None:
        raise ValueError("country_name must be not None")

    if department is None:
        raise ValueError("department must be not None")

    if ADMINISTRATIVE_LOCATIONS.get(f"{country_name}-{department}") is not None:
        return ADMINISTRATIVE_LOCATIONS.get(f"{country_name}-{department}")

    admin_country = pycountry.countries.get(name=country_name)

    if admin_country is None:
        log.warning(f"Unable to find administrative country with country-name {country_name}")
        return None

    subdivisions = iter(pycountry.subdivisions.get(country_code=admin_country.alpha_2))

    admin_subdivision = next(
        (subdivision for subdivision in subdivisions if subdivision.name == department),
        None,
    )
    if admin_subdivision is None:
        log.warning(
            f"Unable to find administrative subdivision with country-name {country_name} and department "
            f"{department}"
        )
        return admin_country.alpha_2
    return admin_subdivision.code


class Map2:
    """
    List of services associated with criterias.
    """

    references: ClassVar[list["Map2"]] = []

    def __init__(self, criteria):
        self.criteria = set(criteria)
        self.service_ids = set()
        Map2.references.append(self)

    @staticmethod
    def retrieve(criteria: set):
        """
        The retrieve function takes a set of criteria and returns the first reference
        that matches that criteria. If no references match, it returns None.

        :param criteria: set: Find the reference in the map2
        :return: The first object in the references list that matches the criteria
        """
        return next((r for r in Map2.references if r.criteria == criteria), None)
